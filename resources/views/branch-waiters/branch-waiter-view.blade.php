@extends('layouts.master')

@section('main-content')


    @if(\Session::has('success'))
        <div class="alert alert-success">
            <p>{{\Session::get('success')}}</p>
        </div>
    @endif


    <div class="breadcrumb">
        <h1>{{$branch_waiter->name}}</h1>
        <ul>
            <li><a href="">Branch Waiter</a></li>
            <li>View</li>
        </ul>
        <a type="button" class="btn  btn-primary m-1" href="{{url('create-branch-waiter')}}" style="position: absolute; right: 45px;"><i class="nav-icon mr-2 i-Add"></i>Create</a>

        <a type="button" class="btn  btn-primary m-1" href="{{url('list-branch-waiters')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-File-Horizontal-Text"></i>Back to List</a>

    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row">

        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="card-title mb-3">Waiter Information</div>
                    <form >
                        <div class="row">
                            <div class="col-md-6 form-group mb-3">
                                <label for="name">Name</label>
                                <input type="text" class="form-control form-control-rounded" disabled value="{{$branch_waiter->name}}" >
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="branch">Restaurant Branch</label>
                                <input type="text" name="branch" class="form-control form-control-rounded" disabled value="{{$branch_waiter->getBranch->name}}" >
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-md-6 form-group mb-3">
                                <label class="switch switch-success mr-3">
                                    <span>Enable</span>
                                    <input type="checkbox" name="is_active" disabled id="is_active" {{(isset($branch_waiter->is_active) && $branch_waiter->is_active==1)?'checked':''}}>
                                    <span class="slider"></span>
                                </label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <a type="button" class="btn btn-success" href="{{url('edit-branch-waiter/'.$branch_waiter->id)}}">
                                    <i class="nav-icon mr-2 i-Pen-2"></i>
                                    Edit
                                </a>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>


@endsection
