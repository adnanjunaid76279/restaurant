@extends('layouts.master')
@section('page-css')
    <link rel="stylesheet" href="{{asset('public/assets/styles/vendor/dropzone.min.css')}}">
@endsection
@section('main-content')


    <div class="breadcrumb">
        @if(isset($branch_table))
            <h1>Update Branch Table</h1>
        @else
            <h1>Create Branch Table</h1>
        @endif

        <a type="button" class="btn  btn-primary m-1" href="{{url('list-branch-tables')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-File-Horizontal-Text"></i>Back to List</a>

    </div>

    @if(count($errors) > 0)

        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <div class="separator-breadcrumb border-top"></div>
    <div class="2-columns-form-layout">
        <div class="">
            <div class="row">
                <div class="col-lg-12">

                    <!-- start card 3 Columns  Form Layout-->
                    <div class="card">

                        <!--begin::form-->
                        <form  action = "{{isset($branch_table)?url('/update-branch-table'):url('/store-branch-table') }}" method = "POST" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{isset($branch_table)?$branch_table->id:''}}">
                            <div class="card-body">
                                <div class="form-row">
                                    @if(Auth::guard('admin_user')->User()->role_id == 1 )
                                        <div class="form-group col-md-4">
                                            <label for="select_restaurant" class="ul-form__label">Search Restaurant:</label>
                                            <input type="text" class="typeahead form-control" autocomplete="off" placeholder="Search Restaurant">
                                        </div>
                                    @endif

                                    <div class="form-group col-md-4">
                                        <label for="select_branch" class="ul-form__label">Select Branch:</label>
                                        <select class="form-control" name="branch" id="branch" required >

                                            @if(isset($branch))
                                                @foreach ($branch as $item)
                                                    <option {{isset($branch_table)&& $branch_table->branch_id == $item->id? "Selected":""}} value="{{$item->id}}">{{$item->name}} </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="name" class="ul-form__label">Table:</label>
                                        <input type="text" class="form-control" name="table_no" value="{{isset($branch_table)?$branch_table->table_no:''}}"  id="table_no" placeholder="Enter Table Name" required>
                                        <div class="invalid-tooltip">
                                            Please enter table name
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="name" class="ul-form__label">Capacity:</label>
                                        <input type="text" class="form-control" name="capacity" value="{{isset($branch_table)?$branch_table->capacity:''}}"  id="name" placeholder="Enter Table Capacity" required>
                                        <div class="invalid-tooltip">
                                            Please enter table capacity
                                        </div>
                                    </div>

                                    <div class="form-group col-md-8">
                                        <label for="name" class="ul-form__label">Description:</label>
                                        <input type="text" class="form-control" name="description" value="{{isset($branch_table)?$branch_table->description:''}}"  id="description" placeholder="Enter Table Description" >
                                        <div class="invalid-tooltip">
                                            Please enter table description
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">

                                    <label class="switch switch-success mr-3">
                                        <span>Table Availability</span>
                                        <input type="checkbox" name="availability" id="availability" {{(isset($branch_table->availability) && $branch_table->availability==1)?'checked':''}}>
                                        <span class="slider"></span>
                                    </label>

                                </div>


                            </div>

                            <div class="card-footer">
                                <div class="mc-footer">
                                    <div class="row">
                                        <div class="col-lg-12 text-right">
                                            <button class="btn btn-primary" type="submit" >{{isset($branch_table)?"Update":"Save"}}</button>
                                            <a class="btn btn-outline-secondary m-1" href={{url('list-branch-tables')}}>Cancel</a>


                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>


                        <!-- end::form -->

                    </div>
                    <!-- end card 3-->





                </div>

            </div>
            <!-- end of main row -->
        </div>
    </div>
@endsection

@section('page-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
@endsection

@section('bottom-js')
    <script src="{{asset('public/assets/js/form.validation.script.js')}}"></script>

    <script type="text/javascript">

        $('input.typeahead').typeahead({
            minLength: 3,
            source:function (query, process) {
                return $.ajax({
                    url: '{{url("searchRestaurantsByName")}}',
                    type: 'get',
                    data: { name: query },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    success: function (result)
                    {
                        var results = result.Data;
                        if(!results)
                            return false;
                        var resultList = results.map(function (item) {
                            var aItem = { id: item.id, name: item.name };
                            return aItem;
                        });

                        return process(resultList);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                    }
                });
            }

        });

        $(document).on('click','ul.typeahead li',function() {

            var data = $('ul.typeahead li.active').data().value;

            if(data != null && data != '' && data != undefined )
            {
                var tempItemId = data.id;
                var tempItemName = data.name;

                $.ajax({
                    url: '{{url("getBranchesByRestaurantId")}}',
                    type: 'get',
                    data: { id: tempItemId },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    success: function (result)
                    {
                        var results = result.Data;
                        if(results.length == 0)
                            return false;

                        var branches = '';

                        $.each(results,function(index,obj){

                            branches += "<option value='"+obj.id+"'>"+obj.name+"</option>";


                        });

                        $('#branch').html(branches);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                    }
                });

            }

        });

        $(".typeahead").on('keyup', function(e){

            if(e.which == 13)
            {

                $('ul.typeahead li.active').trigger('click');

            }

        });


    </script>
@endsection
