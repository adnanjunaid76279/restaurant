@extends('layouts.master')
@section('page-css')
    <link rel="stylesheet" href="{{asset('public/assets/styles/vendor/apexcharts.css')}}">
@endsection

@section('main-content')
           <div class="breadcrumb">
                <h1> {{!isset(Auth::guard('admin_user')->User()->userRestaurant->restaurantBranch->name)?"Admin": Auth::guard('admin_user')->User()->userRestaurant->restaurant->name}}</h1>
                <ul>
                    <li>Restaurant</li>
                    <li>Dashboard</li>

                </ul>
                @if(Auth::guard('admin_user')->User()->role_id < 3)
                   <div class="form-group col-md-4" style=" position: absolute; top: 0; right: 0;">
                    <input class="form-control" list="branch" name="branche" id="branches" placeholder="Select Branch">
                    <datalist id="branch">
                        @foreach ($branches as $branch)
                        <option data-value='{{$branch->id}}' value="{{$branch->name}}">{{$branch->name}}</option>
                       @endforeach
                    </datalist>
                   </div>
               @endif
           </div>

           <div class="separator-breadcrumb border-top"></div>
           <div id="index-dashboard">
                <div class="row">
                    <!-- ICON BG -->

                </div>
           </div>

@endsection

@section('page-js')


@endsection

@section('bottom-js')

    <script type="text/javascript">

        $(document).ready(function(){
            get_branch_dashboard(null);
        });


        $(document).on('change','#branches',function(){

            var branch_id = null;

            var $input = $(this),
            val = $input.val();
            list = $input.attr('list'),

            match = $('#'+list + ' option').filter(function() {
                return ($(this).val() === val);
            });

            if(match.length > 0)
            {
                branch_id = match[0].getAttribute('data-value');// value is in list
            }
            get_branch_dashboard(branch_id);
        });

        function get_branch_dashboard(branch_id){

            var url ="{{ url('getDashboardStatsByBranchId') }}";
            $.ajax({
                url: url,
                type: "post",
                data: { id: branch_id },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function() {
                    $("#loading").show();
                },
                success: function (value)
                {
                    $('#index-dashboard').html(value);
                    if(branch_id)
                        $('.breadcrumb h1').html($("#branches").val());
                    else if('{{Auth::guard('admin_user')->User()->role_id}}' > 1)
                        $('.breadcrumb h1').html('{{isset(Auth::guard('admin_user')->User()->userRestaurant->restaurant)?Auth::guard('admin_user')->User()->userRestaurant->restaurant->name:''}}');
                    else
                        $('.breadcrumb h1').html('Admin');
                },
                complete: function(data) {
                    $("#loading").hide();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    console.log(thrownError);
                }
            });
        }

    </script>
@endsection
