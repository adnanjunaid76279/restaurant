<style>
    .card-box{
        background: #00bcd4ab!important;
    }

    .card-icon-bg-primary [class^=i-]{
        color: #fff;
    }

    .text-primary,.text-muted,.app-footer{
        color: #fff !important;;
    }

    .app-footer
    {
        background: #fff;
    }

    .text-top{
        color: #70657b!important;
    }

</style>
    <div class="row">
        <!-- ICON BG -->
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4 card-box" >
                <div class="card-body text-center">
                    <i class="i-Add-User"></i>
                    <div class="content" style="max-width:none">
                        <p class="text-muted mt-2 mb-0">New Users</p>
                        <p id="today-user" class="text-primary text-24 line-height-1 mb-2 ">{{isset($dashboard)?$dashboard['Stats']['todayusers']:0}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6" >
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4 card-box">
                <div class="card-body text-center">
                    <i class="i-Add-User"></i>
                    <div class="content" style="max-width:none">
                        <p class="text-muted mt-2 mb-0">Old Users</p>
                        <p id="yesterday-user" class="text-primary text-24 line-height-1 mb-2 ">{{isset($dashboard)?$dashboard['Stats']['yesterdayusers']:0}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4 card-box">
                <div class="card-body text-center">
                    <i class="i-Financial"></i>
                    <div class="content" style="max-width:none">
                        <p class="text-muted mt-2 mb-0">Today's Sales</p>
                        <p id="today-sale" class="text-primary text-24 line-height-1 mb-2 ">{{isset($dashboard)?number_format($dashboard['Stats']['todaysale']):0}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4 card-box">
                <div class="card-body text-center">
                    <i class="i-Financial"></i>
                    <div class="content" style="max-width:none">
                        <p class="text-muted mt-2 mb-0">Yesterday's Sales</p>
                        <p id="yesterday-sale" class="text-primary text-24 line-height-1 mb-2 ">{{isset($dashboard)?number_format($dashboard['Stats']['yesterdaysale']):0}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4 card-box">
                <div class="card-body text-center">
                    <i class="i-Financial"></i>
                    <div class="content" style="max-width:none">
                        <p class="text-muted mt-2 mb-0">Current Week Sales(Mon-Sun)</p>
                        <p id="week-sale" class="text-primary text-24 line-height-1 mb-2 ">{{isset($dashboard)?number_format($dashboard['Stats']['weeksale']):0}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4 card-box">
                <div class="card-body text-center">
                    <i class="i-Financial"></i>
                    <div class="content" style="max-width:none">
                        <p class="text-muted mt-2 mb-0">Last Week Sales(Mon-Sun)</p>
                        <p id="preweek-sale" class="text-primary text-24 line-height-1 mb-2 ">{{isset($dashboard)?number_format($dashboard['Stats']['preweeksale']):0}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4 card-box">
                <div class="card-body text-center">
                    <i class="i-Financial"></i>
                    <div class="content" style="max-width:none">
                        <p class="text-muted mt-2 mb-0">Monthly Sales</p>
                        <p id="monthly-sale" class="text-primary text-24 line-height-1 mb-2 ">{{isset($dashboard)?number_format($dashboard['Stats']['monthlysale']):0}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4 card-box">
                <div class="card-body text-center">
                    <i class="i-Financial"></i>
                    <div class="content" style="max-width:none">
                        <p class="text-muted mt-2 mb-0">Total Sales</p>
                        <p id="yearly-sale" class="text-primary text-24 line-height-1 mb-2 ">{{isset($dashboard)?number_format($dashboard['Stats']['yearlysale']):0}}</p>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4 card-box">
                <div class="card-body text-center">
                    <i class="i-Checkout-Basket"></i>
                    <div class="content" style="max-width:none">
                        <p class="text-muted mt-2 mb-0">New Orders</p>
                        <p id="today-orders" class="text-primary text-24 line-height-1 mb-2 ">{{isset($dashboard)?$dashboard['Stats']['todayorders']:0}}</p>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4 card-box">
                <div class="card-body text-center">
                    <i class="i-Checkout-Basket"></i>
                    <div class="content" style="max-width:none">
                        <p class="text-muted mt-2 mb-0">Yesterday's Orders</p>
                        <p id="yesterday-orders" class="text-primary text-24 line-height-1 mb-2 ">{{isset($dashboard)?$dashboard['Stats']['yesterdayorders']:0}}</p>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4 card-box">
                <div class="card-body text-center">
                    <i class="i-Checkout-Basket"></i>
                    <div class="content" style="max-width:none">
                        <p class="text-muted mt-2 mb-0">Current Week Orders(Mon-Sun)</p>
                        <p id="week-orders" class="text-primary text-24 line-height-1 mb-2 ">{{isset($dashboard)?$dashboard['Stats']['weekorders']:0}}</p>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4 card-box">
                <div class="card-body text-center">
                    <i class="i-Checkout-Basket"></i>
                    <div class="content" style="max-width:none">
                        <p class="text-muted mt-2 mb-0">Last Week Orders(Mon-Sun)</p>
                        <p id="preweek-orders" class="text-primary text-24 line-height-1 mb-2 ">{{isset($dashboard)?$dashboard['Stats']['preweekorders']:0}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4 card-box">
                <div class="card-body text-center">
                    <i class="i-Checkout-Basket"></i>
                    <div class="content" style="max-width:none">
                        <p class="text-muted mt-2 mb-0">Current Month Orders</p>
                        <p id="monthly-orders" class="text-primary text-24 line-height-1 mb-2 ">{{isset($dashboard)?$dashboard['Stats']['monthlyorders']:0}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4 card-box">
                <div class="card-body text-center">
                    <i class="i-Checkout-Basket"></i>
                    <div class="content" style="max-width:none">
                        <p class="text-muted mt-2 mb-0">Web App Orders</p>
                        <p id="website-orders" class="text-primary text-24 line-height-1 mb-2 ">{{isset($dashboard)?$dashboard['Stats']['website_orders']:0}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4 card-box">
                <div class="card-body text-center">
                    <i class="i-Checkout-Basket"></i>
                    <div class="content" style="max-width:none">
                        <p class="text-muted mt-2 mb-0">Mobile App Orders</p>
                        <p id="mobile-orders" class="text-primary text-24 line-height-1 mb-2 ">{{isset($dashboard)?$dashboard['Stats']['mobile_orders']:0}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4 card-box">
                <div class="card-body text-center">
                    <i class="i-Checkout-Basket"></i>
                    <div class="content" style="max-width:none">
                        <p class="text-muted mt-2 mb-0">Pos Orders</p>
                        <p id="pos-orders" class="text-primary text-24 line-height-1 mb-2 ">{{isset($dashboard)?$dashboard['Stats']['pos_orders']:0}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4 card-box">
                <div class="card-body text-center">
                    <i class="i-Checkout-Basket"></i>
                    <div class="content" style="max-width:none">
                        <p class="text-muted mt-2 mb-0">Average Preparation Time (Min.)</p>
                        <p id="avg-prepare-time" class="text-primary text-24 line-height-1 mb-2 ">{{isset($dashboard)?$dashboard['Stats']['avg_prepare_time']:0}}</p>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div>
        <div class="row">

            <div class=" col-md-6">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="card-title">
                            Monthly Sale vs Week Days
                        </div>

                        <div id="stackedColumn"></div>
                    </div>
                </div>
            </div>

            <div class=" col-md-6">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="card-title">
                            Weekly Sale vs Week Days
                        </div>

                        <div id="stackedColumn2"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="card-title">Town wise Sale Monthly</div>
                        <div id="simplePie"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="card-title"> Category wise Sale Monthly </div>
                        <div id="simpleDonut"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">

                <div class="card mb-4">
                    <div class="card-body">
                        <div class="card-title">Top Selling Days Monthly </div>
                        @foreach ($dashboard['Stats']['list_topsaledays'] as $item)
                            <h6 class="mb-2 text-top">{{$item->weekday_name}}</h6>
                            <p class="mb-1 text-22 font-weight-light">{{$item->amount}} , {{$item->percent}}%</p>
                            <div class="progress mb-1" style="height: 4px">
                                <div class="progress-bar bg-success" style="width: {{$item->percent}}% ; {{$item->day_color}}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class=" col-md-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="card-title">Orders vs Branch</div>
                        <div id="stackedAreaChart"></div>
                    </div>
                </div>
            </div>

            <div class=" col-md-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="card-title"> Orders vs Week</div>
                        <div id="stackedAreaChart1"></div>
                    </div>
                </div>
            </div>

            <div class=" col-md-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="card-title">Top Users By Order </div>
                        @foreach (isset($dashboard)?$dashboard['Stats']['list_toporderusers']:[] as $item)
                            <h6 class="mb-2 text-top">{{$item->user_name}}</h6>
                            <p class="mb-1 text-22 font-weight-light">{{$item->order_num}}%</p>
                            <div class="progress mb-1" style="height: 4px">
                                <div class="progress-bar bg-success" style="width: {{$item->percent}}% ; {{$item->day_color}}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>

            </div>

        </div>
    </div>

    <script type="text/javascript">
        var stackArray =@php echo json_encode(isset($dashboard)?$dashboard['Stats']['graph_saleweekday']:0); @endphp;
        var stackArray2 =@php echo json_encode(isset($dashboard)?$dashboard['Stats']['graph_saleweeklyday']:0); @endphp ;
        var townwisesale =@php echo json_encode(isset($dashboard)?$dashboard['Stats']['graph_saletownwise']:0); @endphp ;
        var categorywisesale =@php echo json_encode(isset($dashboard)?$dashboard['Stats']['graph_salecategorywise']:0); @endphp ;
        var branch_orders_month = @php echo json_encode(isset($dashboard)?$dashboard['Stats']['graph_branch_orders_month']:0); @endphp ;
        var branch_orders_week = @php echo json_encode(isset($dashboard)?$dashboard['Stats']['graph_branch_orders_week']:0); @endphp ;

        // var stackArray2 =@php echo json_encode(isset($dashboard)?$dashboard['Stats']['graph_saletownwise']:0); @endphp ;
    </script>

    <script src="{{asset('public/assets/js/vendor/apexcharts.dataseries.js')}}"></script>
    <script src="{{asset('public/assets/js/vendor/apexcharts.min.js')}}"></script>
    <script src="{{asset('public/assets/js/es5/apexColumnChart.script.js')}}"></script>
    <script  src="{{asset('public/assets/js/es5/apexPieDonutChart.script.js')}}"></script>
    <script src="{{asset('public/assets/js/es5/apexAreaChart.script.js')}}"></script>

    <script src="{{asset('public/assets/js/vendor/echarts.min.js')}}"></script>
    <script src="{{asset('public/assets/js/es5/echart.options.min.js')}}"></script>
    <script src="{{asset('public/assets/js/es5/dashboard.v1.script.js')}}"></script>

