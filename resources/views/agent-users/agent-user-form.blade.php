@extends('layouts.master')
@section('page-css')
    <link rel="stylesheet" href="{{asset('/public/assets/styles/vendor/dropzone.min.css')}}">
@endsection
@section('main-content')

    <div class="breadcrumb">
        <h1>{{isset($agent_users)?'Update User':'Create User'}} </h1>
        <a type="button" class="btn  btn-primary m-1" href="{{url('list-agent-user')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-File-Horizontal-Text"></i>Back to List</a>
    </div>

    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="separator-breadcrumb border-top"></div>
    <div class="2-columns-form-layout">
        <div class="">
            <div class="row">
                <div class="col-lg-12">
                    <!-- start card 3 Columns  Form Layout-->
                    <div class="card">
                        <!--begin::form-->
                        <form  action = "{{isset($agent_users)?url('update-agent-user'):url('store-agent-user')}}" method = "POST" class="needs-validation" >
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{isset($agent_users)?$agent_users->id:''}}">
                            <div class="card-body">
                                <div id="form_row" class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="name" class="ul-form__label">Name</label>
                                        <input value="{{isset($agent_users)?$agent_users->name:old('name')}}" type="text" class="form-control" autocomplete="off" placeholder="Agent Name" name="name" id="name" >
                                    </div>
                                    <div class="form-group col-md-4" id="role-div">
                                        <label for="role" class="ul-form__label">Role</label>
                                        <select class="form-control" id="role" name="role" required>
                                            <option>Select Role</option>
                                            @foreach($agent_roles as $role)

                                                <option value="{{ $role->id }}" {{isset($agent_users)&& $agent_users->agent_role_id == $role->id ? 'selected' :old('role') }}>{{ $role->role_name}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="username" class="ul-form__label">Login</label>
                                        <input value="{{isset($agent_users)?$agent_users->username:old('username')}}" type="text" class="form-control" autocomplete="off" placeholder="Email" name="username" id="username" required>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="password" class="ul-form__label">Password</label>
                                        <input  value="{{isset($agent_users)?$agent_users->password:old('password')}}" type="text" class="form-control" autocomplete="off" placeholder="Password" id="password"  name="password" required >
                                    </div>

                                    @if(Auth::guard('admin_user')->User()->role_id == 1 )
                                        <div class="form-group col-md-4" id="rest-div">
                                            <label for="restaurant" class="ul-form__label">Restaurant</label>
                                            <input type="text" class="typeahead form-control" autocomplete="off" placeholder="Search Restaurant" required id="restaurtant" name="restaurtant"  aria-describedby="basic-addon3" value="{{isset($agent_users->branch->restaurant)?$agent_users->branch->restaurant->name:old('restaurant')}}" >
                                            <input type="hidden" id="restaurantid" name="restaurantid" >
                                        </div>
                                    @endif

                                    @if(Auth::guard('admin_user')->User()->role_id == 2 || Auth::guard('admin_user')->User()->role_id == 1 )
                                        <div class="form-group col-md-4" id="branch-div">
                                            <label for="branch" class="ul-form__label">Branch</label>
                                            <select class="form-control" id="branch" name="branch">
                                                <option>Select Branch</option>
                                                @if(isset($branch))
                                                    @foreach($branch as $branch)
                                                        <option value="{{ $branch->id }}" {{isset($agent_users)?($branch->id == $agent_users->branch_id ? 'selected'  :''):'' }} >{{ $branch->name}}</option>
                                                    @endforeach
                                                @endif

                                            </select>
                                        </div>
                                    @endif

                                    <div class="form-group col-md-4">
                                        <br>
                                        <label class="switch switch-success mr-3">

                                            <span>Allow POS</span>
                                            <input type="checkbox" name="allowPos" id="allowPos" {{(isset($agent_users->allowPos) && $agent_users->allowPos==1)?'checked':''}}>
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="mc-footer">
                                    <div class="row">
                                        <div class="col-lg-12 text-right">
                                            <button class="btn btn-primary" type="submit" >{{isset($agent_users)?"Update":"Save"}}</button>
                                            <a class="btn btn-outline-secondary m-1" href="{{url('list-agent-user')}}">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                            <!-- end::form 3-->
                    </div>
                    <!-- end card 3-->
                </div>

            </div>
            <!-- end of main row -->
        </div>
    </div>

@endsection

@section('page-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

@endsection

@section('bottom-js')
    <script type="text/javascript">

        $('input.typeahead').typeahead({
            minLength: 3,
            source:function (query, process) {
                return $.ajax({
                    url: "{{url('searchRestaurantsByName')}}",
                    type: 'get',
                    data: { name: query },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    success: function (result)
                    {
                        var results = result.Data;
                        if(!results)
                            return false;
                        var resultList = results.map(function (item) {
                            var aItem = { id: item.id, name: item.name };
                            return aItem;
                        });

                        return process(resultList);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                    }
                });
            }

        });

        $(document).on('click','ul.typeahead li',function() {

            var data = $('ul.typeahead li.active').data().value;

            if(data != null && data != '' && data != undefined )
            {
                var tempItemId = data.id;
                var tempItemName = data.name;
                $('#restaurantid').val(tempItemId);    // hidden field for storing restaurant id
                $.ajax({
                    url: "{{url('getBranchesByRestaurantId')}}",
                    type: 'get',
                    data: { id: tempItemId },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    success: function (result)
                    {

                        $('#branch-div').show();
                        var results = result.Data;
                        if(results.length == 0)
                            return false;

                        var branches = '';

                        $.each(results,function(index,obj){

                            branches += "<option value='"+obj.id+"'>"+obj.name+"</option>";


                        });

                        $('#branch').html(branches);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $('#branch-div').hide();
                        alert(xhr.status);
                        alert(thrownError);
                    }
                });

            }

        });

        $(".typeahead").on('keyup', function(e){

            if(e.which == 13)
            {

                $('ul.typeahead li.active').trigger('click'); // for treiggering of click evetn of typhead for selec an item

            }

        });

    </script>
@endsection
