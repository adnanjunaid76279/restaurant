@extends('layouts.master')
@section('page-css')

    <link rel="stylesheet" href="{{asset('/public/assets/styles/vendor/datatables.min.css')}}">
@endsection


@section('main-content')

    <div class="breadcrumb">
        <h1>Agent Users</h1>
        <ul>
            <li>List</li>
            <li>All</li>
        </ul>
        <a type="button" class="btn  btn-primary m-1" href="{{url('create-agent-user')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-Add"></i>Create</a>
    </div>
    <div class="separator-breadcrumb border-top"></div>
    <div class="row mb-4">
        <div class="col-md-12 mb-4">
            <div class="table-responsive">
                <table id="agent_users_table" class="table table-striped table-dark" style="width:100%">
                    <thead>
                    <tr>

                        <th>User Email</th>
                        @if( Auth::guard('admin_user')->User()->role_id < 4 )
                            <th>Branch</th>
                        @endif
                        <th>Agent Role</th>
                        <th>Allow POS</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('page-js')

    <script src="{{asset('/public/assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('/public/assets/js/datatables.script.js')}}"></script>
    <script>

        $(document).ready(function(){
            getData();
        });

        function getData()
        {
            $('#agent_users_table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{url('get-agent-users')}}",

            });
        }

    </script>
@endsection
