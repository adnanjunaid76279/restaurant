@extends('layouts.master')
@section('page-css')
    <link rel="stylesheet" href="{{asset('public/assets/styles/vendor/dropzone.min.css')}}">
@endsection
@section('main-content')


    <div class="breadcrumb">
        @if(isset($permission))
            <h1>Update Permission</h1>
        @else
            <h1>Create Permission</h1>
        @endif

        <a type="button" class="btn  btn-primary m-1" href="{{url('list-permissions')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-File-Horizontal-Text"></i>Back to List</a>

    </div>

    @if(count($errors) > 0)

        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <div class="separator-breadcrumb border-top"></div>
    <div class="2-columns-form-layout">
        <div class="">
            <div class="row">
                <div class="col-lg-12">

                    <!-- start card 3 Columns  Form Layout-->
                    <div class="card">

                        <!--begin::form-->
                        <form  action = "{{isset($permission)?url('update-permission/'.$permission->id):url('/store-permission') }}" method = "POST" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{isset($permission)?$permission->id:''}}">
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="name" class="ul-form__label">Title</label>
                                        <input type="text" class="form-control" name="title" value="{{isset($permission)?$permission->title:''}}"  id="title" placeholder="Enter Permission Title" required>
                                        <div class="invalid-tooltip">
                                            Please enter permission title
                                        </div>
                                    </div>
                                </div>
                                

                            </div>

                            <div class="card-footer">
                                <div class="mc-footer">
                                    <div class="row">
                                        <div class="col-lg-12 text-right">
                                            <button class="btn btn-primary" type="submit" >{{isset($permission)?"Update":"Save"}}</button>
                                            <a class="btn btn-outline-secondary m-1" href={{url('list-permissions')}}>Cancel</a>


                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>


                        <!-- end::form -->

                    </div>
                    <!-- end card 3-->





                </div>

            </div>
            <!-- end of main row -->
        </div>
    </div>
@endsection

@section('page-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
@endsection

