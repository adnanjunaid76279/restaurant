
// drag and drop sortable
$( init );
function init() {
    $( ".droppable-area1" ).sortable({
        connectWith: ".connected-sortable",
        stack: '.connected-sortable tbody',
        stop: function() {
            $.map($(this).find('tr'), function(el) {
            //var id = el.id;
            var sorting = $(el).index();
            $(el).find('td:eq(0)').html(sorting+1);

            });  
          }
      }).disableSelection();
}

// modal pop by create button  
$('.add-choice').click(function(event){
    $('#choice-modal').modal('toggle');
    $('#choice-modal .modal-title').html('New choice');
    $('#choice-modal .btn-primary').html('Save');
    $('#choice-name').val('');
    $('#choice-price').val('')
});

// modal save button
$('#choice-modal .btn-primary').click(function(event){
    event.preventDefault();
    var choice_name;
    var choice_price;
    var sorting;

    choice_name = $('#choice-name').val();
    choice_price =  Number($('#choice-price').val()).toFixed(2);
    if(choice_name == "" || choice_price == "")
    {
        alert("choice name or price fields are empty");
    }

    sorting = $('#choice-div tbody tr:last-child').index();
    sorting = sorting ==-1 ?0:sorting+1;
    var row = '<tr id="0">';
    row += '<td>'+Number(sorting+1)+'</td>';
    row += '<td>' + choice_name + '</td><td>'+ choice_price + '</td>';
    row += '<td><button class="btn btn-danger" ><i class="nav-icon i-Close-Window"></i></button></td>';
    row += '</tr>';
    $('#choice-div tbody').append(row);
    $('#choice-modal').modal('toggle');
});

//form submit button
$("form").submit(function (event) {
    event.preventDefault();
    var formaction = $('#add-form').attr('action');
    var formdata = $('#add-form').serializeArray();
    formdata = setForm(formdata);

    var tableobj = [];
    
    $('#choice-div table > tbody  > tr').each(function(index, tr) { 
        tableobj.push({"chogrpid":  $(this).attr('id'),"sorting":  $(this).find('td:eq(0)').text(),"choicename":  $(this).find('td:eq(1)').text(),"price": $(this).find('td:eq(2)').text()});
    });

    formdata.choice = tableobj;
    console.log(formdata);
    $.ajax({
      type: "POST",
      url: formaction,
      data: formdata,
      dataType: "json",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    }).done(function (data) {
      
      var success_url = "";
      var location_path = window.location.pathname;
        
      success_url = window.location.origin;
        
    
      success_url += location_path.substr(0,location_path.lastIndexOf("/"));

      success_url +=  "/view-choice-group/" + data;
        
      window.location.href = success_url;

    });
    
  });

    $(document).on("click", ".btn-danger", function (event) {

        event.preventDefault();
        $(this).parent().parent().remove();
    });


function setForm(dd) {
    let data = {};
    for (let i = 0; i < dd.length; i++) {
      let k = dd[i].name
      if (dd[i].name) {
        data[k] = dd[i].value
      }
    }
    return data
}
    
