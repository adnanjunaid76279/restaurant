@extends('layouts.master')
@section('page-css')
<link rel="stylesheet" href="{{asset('public/assets/styles/vendor/dropzone.min.css')}}">
<style>
.draggable-item
{
    cursor: pointer;
}
/* styles during drag */
li.draggable-item.ui-sortable-helper {
  background-color: #e5e5e5;
  -webkit-box-shadow: 0 0 8px rgba(53,41,41, .8);
  -moz-box-shadow: 0 0 8px rgba(53,41,41, .8);
  box-shadow: 0 0 8px rgba(53,41,41, .8);
  transform: scale(1.015);
  z-index: 100;
}
li.draggable-item.ui-sortable-placeholder {
  background-color: #ddd;
  -moz-box-shadow:    inset 0 0 10px #000000;
   -webkit-box-shadow: inset 0 0 10px #000000;
   box-shadow:         inset 0 0 10px #000000;
}
</style>
@endsection
@section('main-content')

<div class="breadcrumb">
    @if(isset($choice_group))
    <h1>Update Choice Group</h1>
    @else
    <h1>Create New Choice Group</h1>
    @endif

    <a type="button" class="btn  btn-primary m-1" href="{{url('list-choicegroups')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-File-Horizontal-Text"></i>Back to List</a>
                
</div>

@if(count($errors) > 0)

<div class="alert alert-danger">
    <ul>
    @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
    @endforeach
    </ul>
</div>
@endif


<div class="separator-breadcrumb border-top"></div>
<div class="2-columns-form-layout">
    <div class="">
        <div class="row">
            <div class="col-lg-12">
                
                <!-- start card 3 Columns  Form Layout-->
                <div class="card">
                    
                    <!--begin::form-->
                    <form id="add-form" action = "{{isset($choice_group)?url('update-choice-group'):url('store-choice-group') }}" method = "POST" class="needs-validation" novalidate>
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{isset($choice_group)?$choice_group->id:''}}">
                        <div class="card-body">
                        <div class="card-title mb-3">Choice Group Details</div>    
                            
                            <div class="form-row">

                                @if(Auth::guard('admin_user')->User()->role_id == 1 )
                                    <div class="form-group col-md-4">
                                        <label for="select_restaurant" class="ul-form__label">Search Restaurant:</label>
                                        <input type="text" class="typeahead form-control" autocomplete="off" placeholder="Search Restaurant" id="basic-url" aria-describedby="basic-addon3">
                                    </div>
                                @endif

                                <div class="form-group col-md-4">
                                    <label for="select_branch" class="ul-form__label">Select Branch:</label>
                                    <select class="form-control" name="branch" id="branch" required > 
                                        
                                        @if(isset($branch))
                                            @foreach ($branch as $item)
                                                <option value="{{$item->id}}">{{$item->name}} </option>
                                            @endforeach   
                                        @endif
                                    </select>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="name" class="ul-form__label">Name:</label>
                                    <input type="text" class="form-control" name="name" value="{{isset($choice_group)?$choice_group->name:''}}"  id="name" placeholder="Enter name" required>
                                    <div class="invalid-tooltip">
                                        Please enter name
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="min_choice" class="ul-form__label">Minimum Number of Choices:</label>
                                    <select class="form-control" name="min_choice" id="min_choice" required > 
                                        <option value="0" {{isset($choice_group->min_choices)?($choice_group->min_choices== 0?"selected":""):""}}>0-Optional</option>
                                        <option value="1" {{isset($choice_group->min_choices)?($choice_group->min_choices== 1?"selected":""):""}}>1</option>
                                    </select>
                                    <label for="select_branch" class="ul-form__label">
                                        Select '0' if this group is optional for your customer
                                    </label>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="select_max_choices" class="ul-form__label">Maximum Number of Choices:</label>
                                    <select class="form-control" name="max_choice" id="max_choice" required > 
                                        @php
                                        
                                        for ($i=1; $i <= 50; $i++) 
                                        { 
                                            if(isset($choice_group))
                                                echo "<option value='$i' isset($choice_group->max_choices)?($choice_group->max_choices== $i?'selected':''):''>$i</option>";
                                            else 
                                                echo "<option value='$i'>$i</option>";
                                        }
                                            
                                        @endphp
                                    </select>
                                    <label for="select_max_choices" class="ul-form__label">
                                        Select '1' if customer can choose only 1
                                    </label>
                                </div>

                            </div>
                            
                            <div class="custom-separator"></div>
                            <div class="card-title mb-3">Choices
                                <button class="btn btn-primary m-1 add-choice" type="button" >Add a Choice</button>
                            </div>
                            <p>Add choices for your customers</p>
                            <p>You can sort a variation by drag and drop.</p>
                            <div class="table-responsive" id="choice-div">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">Sr#</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Price</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="connected-sortable droppable-area1">
                                        @if(isset($choice_group->choices))
                                        
                                            @foreach ($choice_group->choices as $item)
                                                <tr id="{{$item->id}}" class="draggable-item">
                                                    <td>{{$item->sorting}}</td>
                                                    <td>{{$item->name}}</td>
                                                    <td>{{$item->price}}</td>
                                                    <td>
                                                        <button class="btn btn-danger" ><i class="nav-icon i-Close-Window"></i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="custom-separator"></div>  

                        </div>
                        
                        <div class="card-footer">
                            <div class="mc-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-right">
                                        <button class="btn btn-primary" type="submit" >{{isset($choice_group)?"Update":"Save"}}</button>
                                        <a class="btn btn-outline-secondary m-1" href={{url('list-choicegroups')}}>Cancel</a>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </form>
                    <!-- end::form 3-->
                    
                </div>
                <!-- end card 3-->
            </div>
            
        </div>
        <!-- end of main row -->
    </div>
</div>
                {{-- modal --}}
<div class="modal fade" id="choice-modal" tabindex="-1" role="dialog" aria-labelledby="choice-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="choice-title">New Choice</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label class="col-form-label" for="choice-name">Choice Name*:</label>
                        <input class="form-control" id="choice-name" type="text" required/>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label" for="choice-price">Price*:</label>
                        <input class="form-control" id="choice-price" type="number" required/>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" type="button">Save</button>
            </div>
        </div>
    </div>
</div>
        {{-- end modal --}}
@endsection

@section('page-js')
    {{-- drag and drop ui --}}
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
@endsection

@section('bottom-js')
<script src="{{asset('public/assets/js/form.validation.script.js')}}"></script>
<script src="{{asset('resources/views/choice-groups/js/choicegroupform.js')}}"></script>

@endsection
