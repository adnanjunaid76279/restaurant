@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('/public/assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('/public/assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection

@section('main-content')


@if(\Session::has('success'))
<div class="alert alert-success">
    <p>{{\Session::get('success')}}</p>
</div>
@endif


   <div class="breadcrumb">
                <h1>{{$choice_group->name}}</h1>
                <ul>
                    <li><a href="">Choice Group</a></li>
                    <li>View</li>
                </ul>
                <a type="button" class="btn  btn-primary m-1" href="{{url('list-choicegroups')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-File-Horizontal-Text"></i>Back to List</a>
                
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row">
                
                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="card-title mb-3">Choice Group Details</div>  
                            <form ">
                                <div class="row">

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="firstName2">Branch:</label>
                                        <input type="text" class="form-control form-control-rounded" disabled value="{{$choice_group->restaurantBranch->name}}" >
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="firstName2">Name:</label>
                                        <input type="text" class="form-control form-control-rounded" disabled value="{{$choice_group->name}}" >
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="firstName2">Minimum Number of Choices:</label>
                                        <input type="text" class="form-control form-control-rounded" disabled value="{{$choice_group->min_choices}}" >
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="firstName2">Maximum Number of Choices:</label>
                                        <input type="text" class="form-control form-control-rounded" disabled value="{{$choice_group->max_choices}}" >
                                    </div>

                                </div>
                            <div class="custom-separator"></div>
                            <div class="card-title mb-3">Prices & Sizes</div>
                            <div class="table-responsive" id="vari-div">
                                <table class="table table-hover">
                                    <thead>
                                        <tr><th scope="col">Sr#</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($choice_group->choices))
                                        
                                            @foreach ($choice_group->choices as $item)
                                                <tr id="{{$item->id}}">
                                                    <td>{{$item->sorting}}</td>
                                                    <td>{{$item->name}}</td>
                                                    <td>{{$item->price}}</td>
                                                </tr>
                                            @endforeach
                                        
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                                
                                <div class="row">
                                    <div class="col-lg-12 text-right">                
                                        <a type="button" class="btn btn-success" href="{{url('edit-choice-group/'.$choice_group->id)}}">
                                            <i class="nav-icon mr-2 i-Pen-2"></i>
                                            Edit
                                        </a>
                                        
                                    </div>
                                </div>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>


@endsection

@section('page-js')
<script src="{{asset('/public/assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('/public/assets/js/vendor/pickadate/picker.date.js')}}"></script>



@endsection

@section('bottom-js')
<script src="{{asset('/public/assets/js/form.basic.script.js')}}"></script>


@endsection
