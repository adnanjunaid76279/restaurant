
<style>
    .dropdown-submenu {
        position: relative;
    }

    .dropdown-submenu .dropdown-menu {
        top: 0;
        left: 100%;
        margin-top: -1px;
    }
</style>
<div class="horizontal-bar-wrap">
    <div class="header-topnav">
        <div class="container-fluid">
            <div class=" topnav rtl-ps-none" id="" data-perfect-scrollbar data-suppress-scroll-x="true">
                <ul class="menu float-left">
                    <li class="{{ request()->is('dashboard/*') ? 'active' : '' }}">

                        <div>
                            <a href="{{route('home')}}">
                                <i class="nav-icon mr-2 i-Bar-Chart"></i>
                                Dashboard
                            </a>
                        </div>
                    </li>

                    <li>

                        <div>
                            @if( Auth::guard('admin_user')->User()->role_id < 4 )
                                <a href="#">
                                    <i class="nav-icon i-Library mr-2"></i> Restaurant Management
                                </a><input type="checkbox" id="drop-2">
                            @endif
                            <ul>
                                @if( Auth::guard('admin_user')->User()->role_id < 2 )
                                <li class="nav-item">
                                    <a class="{{ Route::currentRouteName()=='list-restaurants' ? 'open' : '' }}" href="{{route('list-restaurants')}}">
                                        <i class="nav-icon mr-2 i-Arrow-Down-in-Circle"></i>
                                        <span class="item-name">Restaurants</span>
                                    </a>

                                </li>
                                @endif

                                @if( Auth::guard('admin_user')->User()->role_id < 3 )
                                <li class="nav-item">
                                    <a class="{{ Route::currentRouteName()=='list-restaurant-branches' ? 'open' : '' }}" href="{{route('list-restaurant-branches')}}">
                                        <i class="nav-icon mr-2 i-Arrow-Down-in-Circle"></i>
                                        <span class="item-name">Branches</span>
                                    </a>
                                </li>

                                
                                @endif

                                @if( Auth::guard('admin_user')->User()->role_id < 4 )
                                    <li class="nav-item">
                                        <a class="{{ Route::currentRouteName()=='list-choicegroups' ? 'open' : '' }}" href="{{route('list-choicegroups')}}">
                                            <i class="nav-icon mr-2 i-Arrow-Down-in-Circle"></i>
                                            <span class="item-name">Choice Groups</span>
                                        </a>
                                    </li>
                                @endif

                                @if( Auth::guard('admin_user')->User()->role_id < 4 )
                                <li class="nav-item">
                                    <a class="{{ Route::currentRouteName()=='list-agent-user' ? 'open' : '' }}" href="{{route('list-agent-user')}}">
                                        <i class="nav-icon mr-2 i-Administrator"></i>
                                        Pos Users
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="{{ Route::currentRouteName()=='list-categories' ? 'open' : '' }}" href="{{route('list-categories')}}">
                                        <i class="nav-icon mr-2 i-Arrow-Down-in-Circle"></i>
                                        <span class="item-name">Menu Category</span>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="{{ Route::currentRouteName()=='list-menus' ? 'open' : '' }}" href="{{route('list-menus')}}">
                                        <i class="nav-icon mr-2 i-Arrow-Down-in-Circle"></i>
                                        <span class="item-name">Menu</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="{{ Route::currentRouteName()=='list-orders' ? 'open' : '' }}" href="{{route('list-orders')}}">
                                        <i class="nav-icon mr-2 i-Arrow-Down-in-Circle"></i>
                                        <span class="item-name">Orders</span>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="{{ Route::currentRouteName()=='list-riders' ? 'open' : '' }}" href="{{route('list-riders')}}">
                                        <i class="nav-icon mr-2 i-Arrow-Down-in-Circle"></i>
                                        <span class="item-name">Riders</span>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="" href="{{route('reconcile-drawer')}}">
                                        <i class="nav-icon mr-2 i-Arrow-Down-in-Circle"></i>
                                        <span class="item-name">POS Reconcile Drawer</span>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="" href="{{route('list-branch-tables')}}">
                                        <i class="nav-icon mr-2 i-Arrow-Down-in-Circle"></i>
                                        <span class="item-name">Branch Table</span>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="" href="{{route('list-branch-waiters')}}">
                                        <i class="nav-icon mr-2 i-Administrator"></i>
                                        Branch Waiters
                                    </a>
                                </li>

                                @endif    
                            </ul>
                        </div>

                        </li>
                        @if( Auth::guard('admin_user')->User()->role_id < 3 )
                        <li>
                        <div>
                            <a href="#">
                                <i class="nav-icon i-Library mr-2"></i> Users
                            </a><input type="checkbox" id="drop-2">
                            
                            <ul>
                                <li class="nav-item">
                                    <a class="{{ Route::currentRouteName()=='list-permissions' ? 'open' : '' }}" href="{{route('list-admin-users')}}">
                                        <i class="nav-icon mr-2 i-Arrow-Down-in-Circle"></i>
                                        <span class="item-name">Users</span>
                                    </a>

                                </li>

                                @can('permission_access')
                                <li class="nav-item">
                                    <a class="{{ Route::currentRouteName()=='list-permissions' ? 'open' : '' }}" href="{{route('list-permissions')}}">
                                        <i class="nav-icon mr-2 i-Arrow-Down-in-Circle"></i>
                                        <span class="item-name">Permissions</span>
                                    </a>

                                </li>
                                @endcan
                                @can('role_access')
                                <li class="nav-item">
                                    <a class="{{ Route::currentRouteName()=='roles.index' ? 'open' : '' }}" href="{{route('roles.index')}}">
                                        <i class="nav-icon mr-2 i-Arrow-Down-in-Circle"></i>
                                        <span class="item-name">Roles</span>
                                    </a>

                                </li>
                                @endcan
                                <ul>
                        </div>
                        </li>
                        @endif
<!-- Reports -->
                        <li>
                            <div>
                                <div>
                                    <label class="toggle" for="drop-2">

                                        Reports
                                    </label>
                                <a href="#">
                                    <i class="i-File-Clipboard-File--Text"></i> Reports
                                </a>
                                <input type="checkbox" id="drop-2">

                                <ul>
                                    <li class="nav-item">
                                        <a  href="{{route('rpt-consumed-menu')}}">
                                            <i class="i-File-Clipboard-File--Text"></i>
                                            <span class="item-name">Consumed Menu Report</span>
                                        </a>

                                    </li>
                                    <li class="nav-item">
                                        <a  href="{{route('rpt-sale-detail')}}">
                                            <i class="i-File-Clipboard-File--Text"></i>
                                            <span class="item-name">Sale Detail Report</span>
                                        </a>

                                    </li>
                                    <li class="nav-item">
                                        <a  href="{{route('rpt-rider-delivery')}}">
                                            <i class="i-File-Clipboard-File--Text"></i>
                                            <span class="item-name">Rider Delivery Report</span>
                                        </a>

                                    </li>


                                </ul>
                            </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <!--=============== Horizontal bar End ================-->
