@extends('layouts.master')
@section('page-css')
    <link rel="stylesheet" href="{{ asset('public/assets/styles/vendor/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/assets/styles/vendor/toastr.css') }}">
@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>Roles</h1>
        <?php

        // var_export($permissions->data);
        // exit();
        ?>
        <ul>
            <li>List</li>
            <li>All</li>
        </ul>

        <a type="button" class="btn  btn-primary m-1" href="{{ route('roles.create') }}"
            style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-Add"></i>Create</a>

    </div>

    <div class="separator-breadcrumb border-top"></div>
    <!-- end of row -->
    <div class="row mb-4">


        <div class="col-md-12 mb-4">


            <div class="table-responsive">
                <table id="branch_table" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Permissions</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($roles as $key => $role)
                        <tr data-entry-id="{{ $role->id }}">
                            <td>
                                {{ $role->role_name ?? '' }}
                            </td>
                            <td>
                                @foreach($role->permissions as $key => $item)
                                    <span class="badge badge-info">{{ $item->title }}</span>
                                @endforeach
                            </td>
                            <td>
                                

                                @can('role_edit')
                                    <a class="text-primary mr-2" href="{{ route('roles.edit', $role->id) }}">
                                        <i title='Add' class='nav-icon mr-2 i-Pen-2'></i>Edit
                                    </a>
                                @endcan
                                @can('role_show')
                                    <a class="text-warning mr-2" href="{{ route('roles.show', $role->id) }}">
                                        <i title='View' class='nav-icon mr-2 i-Full-View-Window'></i>View
                                    </a>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>



        </div>
        <!-- end of col -->


    </div>
    <!-- end of row -->
@endsection

@section('page-js')
    <script src="{{ asset('public/assets/js/vendor/datatables.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/datatables.script.js') }}"></script>

    {{-- <script>
        $(document).ready(function() {
            console.log("He");
            getData();
        });

        function getData() {
            $('#branch_table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ url('roles') }}",
            });
        }
    </script> --}}
@endsection
