@extends('layouts.master')
@section('page-css')
    <link rel="stylesheet" href="{{asset('public/assets/styles/vendor/dropzone.min.css')}}">
@endsection
@section('main-content')


    <div class="breadcrumb">
        @if(isset($permission))
            <h1>Update Role</h1>
        @else
            <h1>Create Role</h1>
        @endif

        <a type="button" class="btn  btn-primary m-1" href="{{url('roles')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-File-Horizontal-Text"></i>Back to List</a>

    </div>

    @if(count($errors) > 0)

        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <div class="separator-breadcrumb border-top"></div>
    <div class="2-columns-form-layout">
        <div class="">
            <div class="row">
                <div class="col-lg-12">

                    <!-- start card 3 Columns  Form Layout-->
                    <div class="card">

                        <!--begin::form-->
                        <form  action = "{{isset($role)?route('roles.update',$role->id):route('roles.store') }}" method = "POST" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{isset($role)?$role->id:''}}">
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="name" class="ul-form__label">Title</label>
                                        <input type="text" class="form-control" name="role_name" value="{{isset($role)?$role->role_name:''}}"  id="title" placeholder="Enter Role Title" required>
                                        <div class="invalid-tooltip">
                                            Please enter role title
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="name" class="ul-form__label">Permissions</label>
                                        <select style="min-height: 130px;" class="form-control select2" name="permissions[]" id="permissions" multiple required>
                                            @foreach($permissions as $id => $permissions)
                                                <option value="{{ $id }}" @if(isset($role)) {{ (in_array($id, old('permissions', [])) || $role->permissions->contains($id)) ? 'selected' : '' }} @else {{ in_array($id, old('permissions', [])) ? 'selected' : '' }} @endif>{{ $permissions }}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-tooltip">
                                            Please select role Permission
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="card-footer">
                                <div class="mc-footer">
                                    <div class="row">
                                        <div class="col-lg-12 text-right">
                                            <button class="btn btn-primary" type="submit" >{{isset($role)?"Update":"Save"}}</button>
                                            <a class="btn btn-outline-secondary m-1" href={{url('list-permissions')}}>Cancel</a>


                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>


                        <!-- end::form -->

                    </div>
                    <!-- end card 3-->





                </div>

            </div>
            <!-- end of main row -->
        </div>
    </div>
@endsection

@section('page-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
@endsection

