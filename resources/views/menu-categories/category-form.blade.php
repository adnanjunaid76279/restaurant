@extends('layouts.master')
@section('page-css')
<link rel="stylesheet" href="{{asset('public/assets/styles/vendor/dropzone.min.css')}}">
@endsection
@section('main-content')
@php
if(isset($category->id) )
{
    $show_edit_image = 1;
    $edit_image = $category->image;
    $branchId = $category->restaurantBranch->id;
}
else
{
    $image = "";
    $branchId="";
    $edit_image="";
}

     $img_url = asset('public'). '/Temp/'.$branchId.'/images/';
@endphp

<div class="breadcrumb">
    @if(isset($category))
    <h1>Update Menu Category</h1>
    @else
    <h1>Create New Menu Category</h1>
    @endif

    <a type="button" class="btn  btn-primary m-1" href="{{url('list-categories')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-File-Horizontal-Text"></i>Back to List</a>

</div>

@if(count($errors) > 0)

<div class="alert alert-danger">
    <ul>
    @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
    @endforeach
    </ul>
</div>
@endif

@include('image-modal',['branchId'=>$branchId, 'img_url'=>$img_url])

<div class="separator-breadcrumb border-top"></div>
<div class="2-columns-form-layout">
    <div class="">
        <div class="row">
            <div class="col-lg-12">

                <!-- start card 3 Columns  Form Layout-->
                <div class="card">

                    <!--begin::form-->
                    <form  action = "{{isset($category)?url('/update-category'):url('/store-category') }}" method = "POST" class="needs-validation" novalidate>
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{isset($category)?$category->id:''}}">
                        <div class="card-body">


                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <button type="button" class="btn btn-success"  data-toggle="modal" data-target="#imageModal" style="{{isset($category)?'' :'display:none' }};"><i class="nav-icon mr-2 i-File-Text--Image"></i>Upload Image </button>
                                </div>
                                @if(Auth::guard('admin_user')->User()->role_id == 1 )
                                <div class="form-group col-md-4">
                                    <label for="select_restaurant" class="ul-form__label">Search Restaurant:</label>
                                    <input type="text" class="typeahead form-control" autocomplete="off" placeholder="Search Restaurant" id="basic-url" aria-describedby="basic-addon3">
                                </div>
                                @endif

                                <div class="form-group col-md-4">
                                    <label for="select_branch" class="ul-form__label">Select Branch:</label>
                                    <select class="form-control" name="branch" id="branch" required >

                                        @if(isset($branch))
                                            @foreach ($branch as $item)
                                                <option {{isset($category)&& $category->restaurant_branch_id == $item->id? "Selected":""}} value="{{$item->id}}">{{$item->name}} </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="name" class="ul-form__label">Name:</label>
                                    <input type="text" class="form-control" name="name" value="{{isset($category)?$category->name:''}}"  id="name" placeholder="Enter category name" required>
                                    <div class="invalid-tooltip">
                                        Please enter category name
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div id="image-div" class="col-md-4 form-group" style="{{isset($category->image)?'' :'display:none' }}">
                                        <label for="Logo" class="ul-form__label mr-3">Image</label>
                                        <input class="form-control" type="hidden" name="image" value="{{ $edit_image}}"  id="image" readonly>
                                        <img id="category-image" name="category-image"  src="{{asset($img_url.$edit_image)}}" style="width: 150px;">
                                    </div>

                                </div>

                            </div>


                        </div>

                        <div class="card-footer">
                            <div class="mc-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-right">
                                        <button class="btn btn-primary" type="submit" >{{isset($category)?"Update":"Save"}}</button>
                                        <a class="btn btn-outline-secondary m-1" href={{url('list-categories')}}>Cancel</a>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>


                    <!-- end::form -->

                </div>
                <!-- end card 3-->





            </div>

        </div>
        <!-- end of main row -->
    </div>
</div>
@endsection

@section('page-js')
<script src="{{asset('public/assets/js/vendor/dropzone.min.js')}}"></script>
<script type="text/javascript">
Dropzone.autoDiscover = false;
$(document).ready(function(){
    var branchId = $('#branch').val();
    var myDropzone = new Dropzone("#upload_image", {

        createImageThumbnails: true,
        addRemoveLinks: true,
        url: "{{url('saveImage')}}",
        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
        init: function() {
            this.on("sending", function(file, xhr, formData){
                formData.append( 'branchId', branchId);
            })
        },
    });

    myDropzone.on("addedfile", function(file)
    {   if (this.files[1]!=null)
        {
            this.removeFile(this.files[0]);
        }
    });

    myDropzone.on("removedfile", function(file)
    {
        $('#image').val('');
    });


    myDropzone.on("success", function(file)
    {
        var uploadedfile_name = "";

        if(file.xhr.response)
            uploadedfile_name = file.xhr.response;
        $('#image').val(uploadedfile_name);
        $('#imageModal').modal('toggle');
        var branchID =$('#branch').val();
        document.getElementById('category-image').src= '{{ asset('public') }}'+'/Temp/'+branchID+'/images/'+uploadedfile_name;

        $('#category-image').focus();
    });

    });
</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

@endsection

@section('bottom-js')
<script src="{{asset('public/assets/js/form.validation.script.js')}}"></script>

<script type="text/javascript">

$('input.typeahead').typeahead({
    minLength: 3,
    source:function (query, process) {
        return $.ajax({
                    url: '{{url("searchRestaurantsByName")}}',
                    type: 'get',
                    data: { name: query },
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    success: function (result)
                    {
                        var results = result.Data;
                        if(!results)
                            return false;
                        var resultList = results.map(function (item) {
                            var aItem = { id: item.id, name: item.name };
                            return aItem;
                        });

                        return process(resultList);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                    }
                });
    }

 });

 $(document).on('click','ul.typeahead li',function() {

    var data = $('ul.typeahead li.active').data().value;

    if(data != null && data != '' && data != undefined )
    {
        var tempItemId = data.id;
        var tempItemName = data.name;

        $.ajax({
            url: '{{url("getBranchesByRestaurantId")}}',
            type: 'get',
            data: { id: tempItemId },
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            success: function (result)
            {
                var results = result.Data;
                if(results.length == 0)
                    return false;

                var branches = '';

                $.each(results,function(index,obj){

                    branches += "<option value='"+obj.id+"'>"+obj.name+"</option>";


                });

                $('#branch').html(branches);

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });

    }

});

$(".typeahead").on('keyup', function(e){

    if(e.which == 13)
    {

      $('ul.typeahead li.active').trigger('click'); // for treiggering of click evetn of typhead for selec an item

    }

});

function getImageVal(val) {

    var branchID =$('#branch').val();
    const imageVals = val.src;
    const pieces = imageVals.split(/[\s/]+/);
    const imgVal = pieces[pieces.length - 1];

    var img_url = '{{ asset('public') }}';

    $('#image-div').show();
    $('#image').val(imgVal);
    document.getElementById('category-image').src= img_url+'/Temp/'+branchID+'/images/'+imgVal;
    $('#imageModal').modal('toggle');
    $('#category-image').focus();


}


 </script>
@endsection
