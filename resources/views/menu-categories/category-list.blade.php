@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('public/assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('public/assets/styles/vendor/toastr.css')}}">
@endsection

@section('main-content')
  <div class="breadcrumb">
                <h1>Menu Category</h1>
                <ul>
                    <li>List</li>
                    <li>All</li>
                </ul>
              
            <a type="button" class="btn  btn-primary m-1" href="{{url('create-category')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-Add"></i>Create</a>
                
            </div>
            
            <div class="separator-breadcrumb border-top"></div>
<!-- 
            <div class="row mb-4">
                <div class="col-md-12">
                    <h4>Datatables</h4>
                    <p>DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, build upon the foundations of progressive enhancement, that adds all of these advanced features to any HTML table.</p>
                </div>
            </div> -->
            <!-- end of row -->
            <div class="row mb-4">
               
                <div class="col-md-12 mb-4">
                    

                            <div class="table-responsive">
                                <table id="category_pagination_table" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Enable</th>
                                            <th>Branch</th>
                                            {{-- <th>Contact Person</th>
                                            <th>Email</th>
                                            <th>Contact</th>  --}}
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                      
                                    </tbody>
                                </table>
                            </div>

                        
                   
                </div>
                <!-- end of col -->


            </div>
            <!-- end of row -->



@endsection

@section('page-js')

    <script src="{{asset('public/assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('public/assets/js/datatables.script.js')}}"></script>
    <script src="{{asset('public/assets/js/vendor/toastr.min.js')}}"></script>
    <script src="{{asset('public/assets/js/toastr.script.js')}}"></script>
    <script>
 
 $(document).ready(function(){
        var groupColumn = 2;
        $('#category_pagination_table').DataTable( {
         "processing": true,
         "serverSide": true,
         "ajax": "{{url('getcategories')}}",
         "pageLength": 1000,
         "lengthMenu": [[50, 100, 500, 1000,-1], [50, 100, 500, 1000, "All"]],
         "columnDefs": [
            { "visible": false, "targets": groupColumn },{width: "50%", targets: [0]},{width: "20%", targets: [1]},{ className: 'dt-center',width: "30%", targets: [2]}
        ],
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="4">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
     } );

       $(document).on("click",".cat_active",function() {
        var is_active = $(this).is(':checked') ? 1: 0;
        var cat_id = $(this).attr('data-cat-id');

        
            $.ajax({
                    url: "{{url('enableCategory')}}",
                    type: 'POST',
                    data: { is_active ,cat_id},
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    success: function (response) 
                    {
                        toastr.success(response.msg,"Status", {
                            showMethod: "slideDown",
                            hideMethod: "slideUp",
                            timeOut: 2e3
    
                        });
                    }
            });
        });

    });

</script>
@endsection
