@extends('layouts.master')
@section('page-css')
<link rel="stylesheet" href="{{asset('/public/assets/styles/vendor/dropzone.min.css')}}">
@endsection
@section('main-content')

<div class="breadcrumb">

    <h1>{{isset($admin_users)?'Update User':'Create User'}} </h1>
 <a type="button" class="btn  btn-primary m-1" href="{{url('list-admin-users')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-File-Horizontal-Text"></i>Back to List</a>

</div>

@if(count($errors) > 0)

<div class="alert alert-danger">
    <ul>
    @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
    @endforeach
    </ul>
</div>
@endif


<div class="separator-breadcrumb border-top"></div>
<div class="2-columns-form-layout">
    <div class="">
        <div class="row">
            <div class="col-lg-12">

                <!-- start card 3 Columns  Form Layout-->
                <div class="card">

                    <!--begin::form-->
                    <form  action = "{{isset($admin_users)?url('update-admin-user'):url('store-admin-user')}}" method = "POST" class="needs-validation" novalidate>
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{isset($admin_users)?$admin_users->id:''}}">
                        <div class="card-body">
                             <div id="form_row" class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="first-name" class="ul-form__label">First Name</label>
                                    <input value="{{isset($admin_users)?$admin_users->first_name:''}}" type="text" class="form-control" autocomplete="off" placeholder="First Name" name="firstname" id="firstname" >
                                </div>
                                    <div class="form-group col-md-4">
                                        <label for="last-name" class="ul-form__label">Last Name</label>
                                        <input value="{{isset($admin_users)?$admin_users->last_name:''}}"  type="text" class="typeahead form-control" autocomplete="off" placeholder="Last Name" name="lastname" id="lastname" >
                                    </div>

                                <div class="form-group col-md-4">
                                    <label for="login" class="ul-form__label">Login</label>
                                    <input value="{{isset($admin_users)?$admin_users->login:''}}" type="text" class="form-control" autocomplete="off" placeholder="Login" id="login" name="login" >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="password" class="ul-form__label">Password</label>
                                    <input  value="{{isset($admin_users)?$admin_users->password:''}}" type="text" class="form-control" autocomplete="off" placeholder="Password" id="password"  name="password" >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="contact-number" class="ul-form__label">Contact*(Minimum 11 Characters)</label>
                                    <input  value="{{isset($admin_users)?$admin_users->cell_num:''}}"  type="text" class="form-control" autocomplete="off" placeholder="Contact" id="cell_num" name="cell_num">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="role" class="ul-form__label">Role</label>
                                    <select class="form-control" id="role" name="role">
                                @foreach($roles as $role)

                                    <option value="{{ $role->id }}" {{isset($admin_users)?($role->id == $admin_users->role_id ? 'selected' :''  ):'' }} >{{ $role->role_name}}</option>
                                @endforeach

                                    </select>
                                </div>

                                <div id="rest-div" class="form-group col-md-4" style="{{!isset($admin_users)?(Auth::guard('admin_user')->User()->role_id <=2 ? 'display:none'  :''):(Auth::guard('admin_user')->User()->role_id = 1 && $admin_users->role_id == 2 ? '':'display:none') }}">
                                    <label for="restaurant" class="ul-form__label">Restaurant</label>
                                    <input type="text" class="typeahead form-control" autocomplete="off" placeholder="Search Restaurant" id="restaurtant" name="restaurtant"  aria-describedby="basic-addon3" value="{{isset($admin_users->userRestaurant->restaurant)?$admin_users->userRestaurant->restaurant->name:''}}">
                                    <input type="hidden" id="restaurantid" name="restaurantid">
                                </div>


                                <div id="branch-div" class="form-group col-md-4" style="{{!isset($admin_users)?(Auth::guard('admin_user')->User()->role_id <=2 ? 'display:none'  :''):($admin_users->role_id == 2 ? 'display:none'  :'') }}" >
                                    <label for="branch" class="ul-form__label">Branch</label>
                                    <select class="form-control" id="branch" name="branch">
                                @if(isset($branches))
                                @foreach($branches as $branch)
                                    <option value="{{ $branch->id }}" {{isset($admin_users)?($branch->id == $admin_users->userRestaurant->branch_id ? 'selected'  :''):'' }} >{{ $branch->name}}</option>
                                @endforeach
                                @endif

                                    </select>
                                </div>

                            </div>


                        </div>

                        <div class="card-footer">
                            <div class="mc-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-right">
                                        <button class="btn btn-primary" type="submit" >{{isset($admin_users)?"Update":"Save"}}</button>
                                        <a class="btn btn-outline-secondary m-1" href="{{url('list-admin-users')}}">Cancel</a>


                                    </div>
                                </div>
                            </div>
                        </div>

                    <!-- end::form 3-->
                    </form>
                </div>
                <!-- end card 3-->





            </div>

        </div>
        <!-- end of main row -->
    </div>
</div>

@endsection

@section('page-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

@endsection

@section('bottom-js')
<script type="text/javascript">

$('input.typeahead').typeahead({
    minLength: 3,
    source:function (query, process) {
        return $.ajax({
                    url: "{{url('searchRestaurantsByName')}}",
                    type: 'get',
                    data: { name: query },
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    success: function (result)
                    {
                        var results = result.Data;
                        if(!results)
                            return false;
                        var resultList = results.map(function (item) {
                            var aItem = { id: item.id, name: item.name };
                            return aItem;
                        });

                        return process(resultList);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                    }
                });
    }

 });

$(document).on('click','ul.typeahead li',function() {

     var data = $('ul.typeahead li.active').data().value;

     if(data != null && data != '' && data != undefined )
     {
        var tempItemId = data.id;
        var tempItemName = data.name;
        $('#restaurantid').val(tempItemId);    // hidden field for storing restaurant id
        $.ajax({
             url: "{{url('getBranchesByRestaurantId')}}",
             type: 'get',
             data: { id: tempItemId },
             headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
             dataType: 'json',
             success: function (result)
             {
                 var results = result.Data;
                 if(results.length == 0)
                     return false;

                 var branches = '';

                 $.each(results,function(index,obj){

                     branches += "<option value='"+obj.id+"'>"+obj.name+"</option>";


                 });

                 $('#branch').html(branches);

             },
             error: function (xhr, ajaxOptions, thrownError) {

                 alert(xhr.status);
                 alert(thrownError);
             }
         });

     }

 });

 $(".typeahead").on('keyup', function(e){

     if(e.which == 13)
     {

       $('ul.typeahead li.active').trigger('click'); // for treiggering of click evetn of typhead for selec an item

     }

 });

 $('#role').change(function(){

    var roleid = $(this).val();
    var loginuser_role = '{{Auth::guard('admin_user')->User()->role_id }}';

    if(loginuser_role == 1 && roleid == 2)
    {
        $('#rest-div').show();
        $('#branch-div').hide();
        $( "#branch" ).prop( "disabled", true );
    }
    else if(loginuser_role == 1 && roleid == 3)
    {
        $('#rest-div').show();
        $('#branch-div').show();
        $( "#branch" ).prop( "disabled", false );
    }else if(loginuser_role == 2 && roleid == 3)
    {
        $('#rest-div').hide();$( "#restaurantid" ).prop( "disabled", true );
        $('#branch-div').show();
    }else if(loginuser_role == 2 && roleid == 2)
    {
        $('#rest-div').hide();
        $('#branch-div').hide();
        $( "#branch" ).prop( "disabled", true );
        $( "#restaurantid" ).prop( "disabled", true );
    }else
    {
        $('#rest-div').hide();
        $('#branch-div').hide();
        $( "#branch" ).prop( "disabled", true );
        $( "#restaurantid" ).prop( "disabled", true );
    }


 });
 </script>
@endsection
