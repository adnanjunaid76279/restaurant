@extends('layouts.master')

@section('main-content')


@if(\Session::has('success'))
<div class="alert alert-success">
    <p>{{Session::get('success')}}</p>
</div>
@endif


   <div class="breadcrumb">
        <h1>{{$admin_users->first_name}}</h1>
        <ul>
            <li><a href="">User</a></li>
            <li>View</li>
        </ul>
        <a type="button" class="btn  btn-primary m-1" href="{{url('list-admin-users')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-File-Horizontal-Text"></i>Back to List</a>
    </div>

    <div class="separator-breadcrumb border-top"></div>

            <div class="row">

                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <form>
                                <div id="form_row" class="row">

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="FirstName">First Name</label>
                                        <input type="text" class="form-control form-control-rounded" disabled value="{{$admin_users->first_name}} " >
                                    </div>
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="LastName">Last Name</label>
                                        <input type="text" class="form-control form-control-rounded" disabled value=" {{$admin_users->last_name}}" >
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="Login">Login</label>
                                        <input type="text" class="form-control form-control-rounded" disabled value="{{$admin_users->login}}" >
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="Password">Password</label>
                                        <input type="text" class="form-control form-control-rounded" disabled value="{{$admin_users->password}}" >
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="Contact">Contact</label>
                                        <input type="text" class="form-control form-control-rounded" disabled value="{{$admin_users->cell_num}}" >
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="Role">Role</label>
                                        <input type="text" class="form-control form-control-rounded" disabled value="{{$admin_users->userRole->role_name}}" >
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="Role">Restaurant</label>
                                        <input type="text" class="form-control form-control-rounded" disabled value="{{isset($admin_users->userRestaurant->restaurant)?$admin_users->userRestaurant->restaurant->name:''}}"
                                         >
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="Role">Branch</label>
                                        <input type="text" class="form-control form-control-rounded" disabled 
                                        value="{{isset($admin_users->userRestaurant->restaurantBranch)?$admin_users->userRestaurant->restaurantBranch->name:''}}"
                                        >
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-lg-12 text-right">
                                        <a type="button" class="btn btn-success" href="{{url('edit-admin-user/'.$admin_users->id)}}">
                                            <i class="nav-icon mr-2 i-Pen-2"></i>
                                            Edit
                                        </a>

                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>


@endsection

@section('page-js')


@if(Auth::guard('admin_user')->User()->role_id=="1" && isset($admin_users) );
<script
  src="https://code.jquery.com/jquery-3.6.0.js"
  integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
  crossorigin="anonymous"></script>
<script>
$(document).ready(function(){

    ConditionalSelectBox();
});

function ConditionalSelectBox()
{

  $restaurent_name=$('#restaurent_name').val();
   $restaruentObj=JSON.parse($restaurent_name);
  $branch_data=$('#branch_data').val();
   $branchObj=JSON.parse($branch_data);
   $admin_users=$('#admin_users').val();
   $adminUsersObj=JSON.parse($admin_users);
  $roleid=$adminUsersObj.role_id;
 console.log('role id ',$roleid);
   switch ($roleid) {

       case 2:
        $string='<div class="col-md-6 form-group mb-3"><label for="Role">Restaurent</label><input type="text" class="form-control form-control-rounded" disabled value="'+ $restaruentObj.name+'" > </div>'
        $('#form_row').append($string); 
        break;
        case 3:
        
            $string='<div class="col-md-6 form-group mb-3"><label for="Role">Restaurent</label><input type="text" class="form-control form-control-rounded" disabled value="'+ $restaruentObj.name+'" > </div>'
            $string2='<div class="col-md-6 form-group mb-3"><label for="Role">Branch</label><input type="text" class="form-control form-control-rounded" disabled value="'+ $branchObj.name+'" > </div>'
            $('#form_row').append($string); 
            $('#form_row').append($string2);
            break;
       default:
           break;
   }


  
//  
//   if($role_id=="2") //Restaurent owner
//   {
        
//       $('#form_row').append('<label for="role" class="ul-form__label">Select Restaurent</label>'); //label
//       $('#form_row').append('<select class="form-control"  "><option value="" >'+obj.name+'</option> </select>'); //value
//   }
//   if($role_id=="3") //branch owner Owner
//   {
//       $('#form_row').append('<label for="role" class="ul-form__label">Select Restaurent</label>'); //label
//       $('#form_row').append('<select class="form-control"  ><option selected >'+obj.name+'</option>'); //value
   

//       $('#form_row').append('<label for="role" class="ul-form__label"> Restaurent Branch</label>'); //label
//       $('#form_row').append('<select class="form-control"><option> Select Branch</option> </select>'); //value
    
   
//   }

    
   
}
</script>
@endif

@endsection

@section('bottom-js')



@endsection
