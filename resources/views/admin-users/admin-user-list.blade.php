@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('/public/assets/styles/vendor/datatables.min.css')}}">
@endsection


@section('main-content')

    <div class="breadcrumb">
        <h1>Users</h1>
        <ul>
            <li>List</li>
            <li>All</li>
        </ul>

        <a type="button" class="btn  btn-primary m-1" href="{{url('create-user')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-Add"></i>Create</a>

    </div>

            <div class="separator-breadcrumb border-top"></div>
            <!-- end of row -->
            <div class="row mb-4">

                <div class="col-md-12 mb-4">


                            <div class="table-responsive">
                                <table id="users_table" class="table table-striped table-dark" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Login</th>
                                            <th>Role</th>
                                            <th>Contact</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>



                </div>
                <!-- end of col -->


            </div>
            <!-- end of row -->
@endsection

@section('page-js')

    <script src="{{asset('/public/assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('/public/assets/js/datatables.script.js')}}"></script>
    <script>

 $(document).ready(function(){
    getData();
 });

 function getData()
 {
    $('#users_table').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{{url('get-admin-users')}}",
      
    });
 }

</script>
@endsection
