@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('public/assets/styles/vendor/datatables.min.css')}}">

@endsection
@section('main-content')

<div class="breadcrumb">
    <h1>Sale Detail</h1>
    <ul>
        <li>Report</li>
    </ul>
</div>

<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-lg-12 col-md-12">
        
    <div class="card mb-5">
            <div class="card-body">
                <form method="POST" target="_blank" action="{{route('export-sale-detail')}}" class="needs-validation" novalidate>
                    @csrf
                <div class="row row-xs">
                    <div class="col-md-2">
                        <input name="start_date" id="start_date" value="<?php echo date('Y-m-d'); ?>" class="form-control" type="date" placeholder="Start Date" />
                    </div>
                    <div class="col-md-2 mt-3 mt-md-0">
                        <input name="end_date" id="end_date" value="<?php echo date('Y-m-d'); ?>" class="form-control" type="date" placeholder="End Date" />
                    </div>
                    <div class="col-md-2 mt-3 mt-md-0">
                        <select class="form-control" name="branch" id="branch">
                            <option value="0">All</option>
                            @foreach ($branches as $branch)
                            <option value="{{$branch->id}}">{{$branch->name}}</option>
                            @endforeach
                            

                        </select>
                    </div>

                    <div class="col-md-2 mt-3 mt-md-0">
                        <button id="search_button" class="btn btn-primary btn-block">Search</button>
                    </div>

                    <div class="col-md-2 mt-3 mt-md-0">
                    <button  type='submit' class="btn btn-danger btn-block" name="export-pdf">Export Pdf</button>
                    </div>

                    <div class="col-md-2 mt-3 mt-md-0">
                        <button  type='submit' class="btn btn-success btn-block" name="export-excel">Export Excel</button>
                    </div>
                   
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<div class="row mb-4">

    <div class="col-md-12 mb-4">
        
        <div class="table-responsive ">
            
            <table id="report_table" class="table table-striped table-dark" style="width:100%">
                <thead>
                    <tr>
                        <th>Time</th>
                        <th>OrderNo</th>
                        <th>Customer</th>
                        <th>Type</th>
                        <th>Resource</th>
                        <th>Status</th>
                        <th>Prepare Time(Min.)</th>
                        <th>Tax</th>
                        <th>Dis</th>
                        <th>Subtotal</th>
                        <th>Total</th>
                    </tr>
                </thead>
                
                <tbody>
                </tbody>
                <tfoot>
                    <tr>    
                        <th colspan="6"style="text-align:left">Total:</th>
                        <th id ='total_time'></th>
                        <th id ='total_tax'></th>
                        <th id ='total_dis'></th>
                        <th id ='subtotal_amt'></th>
                        <th id ='total_amt'></th>
                    </tr>
                    <tr>    
                        <th colspan="7"style="text-align:left">Average Preaparation Time(Min):</th>
                        <th id ='avg_time'></th>
                        <th colspan="3"></th>
                    </tr>
                </tfoot>
            </table>
        </div>
        
        
        
    </div>
    
    
</div>



@endsection

@section('page-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="{{asset('public/assets/js/vendor/datatables.min.js')}}"></script>
<script src="{{asset('public/assets/js/datatables.script.js')}}"></script>

<script>
    
    
    function getData() 
    {   
        let start_dt = $('#start_date').val();;
        let end_dt = $('#end_date').val();
        let branch = $('#branch').val();
        
        table=  $('#report_table').DataTable({
            // "pageLength": "-1",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "processing": true,
            "serverSide": true,
            "destroy":true,
            "searching": false,
            "columns": [
                    { data: '0' ,render: function (data, type, row) {//data
                        return moment(data).format('DD-MM-YY @ hh:mm:ss');
                    }},
                    { data: '1' },
                    { data: '2'},
                    { data: '3' },
                    { data: '4' },
                    { data: '5' },
                    { data: '6' },
                    { data: '7' },
                    { data: '8' },
                    { data: '9' },
                    { data: '10' },
            ],
            "ajax": {
                "url": "sale-detail",
                "data": {
                    "start_date": start_dt,
                    "end_date":end_dt,
                    "branch":branch,
                },
            },
            
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                    i : 0;
                };
                

                // Total over all pages

                totaltime = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


                total_tax = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                totaldis = api
                .column( 8 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                subtotal = api
                .column( 9 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                total = api
                .column( 10 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                totaltime = parseFloat(totaltime).toFixed(2);
                total_tax = parseFloat(total_tax).toFixed(2);
                totaldis = parseFloat(totaldis).toFixed(2);
                subtotal = parseFloat(subtotal).toFixed(2);
                total = parseFloat(total).toFixed(2);
                
                // Update footer
                $('#total_time').html(totaltime);
                $('#total_tax').html(total_tax);
                $('#total_dis').html(totaldis);
                $('#subtotal_amt').html(subtotal);
                $('#total_amt').html(total);

                avgtime = (parseFloat(totaltime)/ data.length).toFixed(2) ;
                $('#avg_time').html(avgtime);
            }
            
        });
        
    }
    $('#search_button').click(function(e){
        e.preventDefault();
        getData();        
    });    

    
</script>
@endsection
