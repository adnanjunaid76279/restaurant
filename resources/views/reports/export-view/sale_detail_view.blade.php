
          <h2 style="text-align: center;">Sale Detail Report</h2>
          <p style="text-align: center;line-height: 0px;">From Date {{$parms->start_date}} </p>
          <p style="text-align: center;">To Date {{$parms->end_date}} </p>

            <table style="border: 1px solid black;" border="1" id="report_table" width="100%">
                <thead>
                    <tr>
                        <th style="text-align:center;">DateTime(UTC)</th>
                        <th style="text-align:center;">OrderNo</th>
                        <th style="text-align:center;">Customer</th>
                        <th style="text-align:center;">Type</th>
                        <th style="text-align:center;">Resource</th>
                        <th style="text-align:center;">Status</th>
                        <th style="text-align:center;">Prepare Time(Min.)</th>
                        <th style="text-align:center;">Tax</th>
                        <th style="text-align:center;">Discount</th>
                        <th style="text-align:center;">Subtotal</th>
                        <th style="text-align:center;">Total</th>
                    </tr>
                </thead>

                <tbody>
                    @php
                        $sum_dis = 0;
                        $sum_total = 0;
                        $sum_subtotal = 0;
                        $sum_pre_time = 0;
                        $sum_tax = 0;

                    @endphp
                @foreach($parms->data as $item)

                    <tr>
                        <td style="width: 200px;">{{date('Y-m-d g:i:s ',strtotime('+5 hour',strtotime($item[0])))}}</td>
                        <td style="text-align:center;">{{$item[1]}}</td>
                        <td>{{$item[2]}}</td>
                        <td style="text-align:center;">{{$item[3]}}</td>
                        <td style="text-align:center;">{{$item[4]}}</td>
                        <td style="text-align:center;">{{$item[5]}}</td>
                        <td style="text-align:center;">{{$item[6]}}</td>
                        <td style="text-align:center;">{{$item[7]}}</td>
                        <td style="text-align:right;">{{$item[8]}}</td>
                        <td style="text-align:right;">{{$item[9]}}</td>
                        <td style="width: 100px;text-align:right;">{{$item[10]}}</td>

                    </tr>
                    @php
                       
                        
                        $sum_pre_time += str_replace(',', '', $item[6]);
                        $sum_tax += floatval($item[7]);
                        $sum_dis += $item[8];
                        $sum_subtotal += floatval($item[9]);
                        $item[10] = str_replace(',', '', $item[10]);
                        
                        $sum_total += floatval($item[10]);   
                    @endphp
                @endforeach
                </tbody>
                <br/>
                <tfoot>
                <tr>
                    <th colspan="6" style="text-align:center;">Total:</th>
                    <th style="text-align:center;">{{number_format($sum_pre_time,2)}}</th>
                    <th style="text-align:center;">{{number_format($sum_tax,2)}}</th>
                    <th style="text-align:right;">{{number_format($sum_dis,2)}}</th>
                    <th style="text-align:right;">{{number_format($sum_subtotal,2)}}</th>
                    <th style="text-align:right;">{{number_format($sum_total,2)}}</th>
                    
                </tr>
                <tr>
                    <th style="text-align:center;">Total Records:</th>
                    <th style="text-align:center;">{{count($parms->data)}}</th>
                    <th colspan="5" style="text-align:center;">Average Preparation Time (Min.):</th>
                    <th style="text-align:center;">{{number_format($sum_pre_time/count($parms->data),2)}}</th>
                    <th colspan="3" style="text-align:center;"></th>
                </tr>
                </tfoot>
            </table>
