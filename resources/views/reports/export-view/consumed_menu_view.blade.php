
          <h2 style="text-align: center;">Consumed Menu Report</h2>
          <p style="text-align: center;line-height: 0px;">From Date {{$parms->start_date}} </p>
          <p style="text-align: center;">To Date {{$parms->end_date}} </p>

            <table style="border: 1px solid black;" border="1" id="report_table">
                <thead>
                    <tr>
                        <th style="text-align:left;" >Menu item</th>
                        <th style="text-align:left;">Quantity</th>
                        <th style="text-align:left;">Branch</th>
                        <th style="text-align:left;">Amount</th>
                    </tr>
                </thead>

                <tbody>
                    @php
                        $sum_total = 0;
                        $sum_qty = 0;
                    @endphp
                @foreach($parms->data as $item)
                    <tr>
                        <td style="width: 350px">{{$item[0]}}</td>
                        <td >{{$item[1]}}</td>
                        <td style="width: 150px">{{$item[2]}}</td>
                        <td style="width: 100px;text-align:right;">{{$item[3]}}</td>

                    </tr>
                    @php
                     $sum_qty += $item[1];
                     
                    $item[3] = str_replace(',', '', $item[3]);
                     $sum_total += floatval($item[3]);   
                    @endphp
                @endforeach
                </tbody>
                <br/>
                <tfoot>
                <tr>
                    <th style="text-align:left;">Total:</th><th style="text-align:right;">{{$sum_qty}}</th><th></th><th style="text-align:right;">{{number_format($sum_total,2)}}</th>
                </tr>



                </tfoot>
            </table>








