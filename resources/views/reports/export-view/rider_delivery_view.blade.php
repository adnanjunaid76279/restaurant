
<h2 style="text-align: center;">Monthly Delivery Report</h2>
<p style="text-align: right;line-height: 0px;">Month:{{$parms->month_name}} </p>
<p style="text-align: left;line-height: 0px;">Month:{{$parms->branch_name}} </p>
<br>
<table style="width:100%;font-size: 7px;border-collapse: collapse;" border="1" id="report_table">
    <thead>
        <tr>
            
            <th rowspan="2" style="text-align:center;">Name</th>
            @foreach ($parms->month_days as $day)
            <th colspan="2" style="text-align:center;">{{$day}}</th>
            @endforeach
        </tr>
        
        <tr>
            @foreach ($parms->month_days as $day)
            <th  style="text-align:center;">Sale</th>
            <th style="text-align:center;">Km</th>
            @endforeach
        </tr>
    </thead>
    
    <tbody style="font-size: 6px;word-wrap: break-word;">
        @php
        $sum_dis = 0;
        $sum_total = 0;
        $sum_subtotal = 0;
        
        @endphp
        <tr>
            
            @foreach($parms->{2} as $key => $val)
            
            @if($key == "rider_name")
            <td>{{$val}}</td>
            @continue;
            @endif
            
            <td>{{$val['sale_amt']}}</td>
            <td>{{$val['km']}}</td>
            
            @endforeach
        </tr> 
        
        
    </tbody>
    
</table>




