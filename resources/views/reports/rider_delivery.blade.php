@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('public/assets/styles/vendor/datatables.min.css')}}">

@endsection
@section('main-content')

<div class="breadcrumb">
    <h1>Rider Monthly Delivery</h1>
    <ul>
        <li>Report</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-lg-12 col-md-12">
        
    <div class="card mb-5">
            <div class="card-body">
                <form method="POST" target="_blank" action="{{route('export-rider-delivery')}}" class="needs-validation" novalidate>
                    @csrf
                <div class="row row-xs">
                    <div class="col-md-2 mt-3 mt-md-0">
                        <select class="form-control" name="month" id="month">
                            @foreach ($months as $key => $month)
                            <option value="{{$key+1}}">{{$month}}</option>
                            @endforeach
                        </select>
                      
                    </div>
                    <div class="col-md-2 mt-3 mt-md-0">
                        <select class="form-control" name="branch" id="branch">
                            <option value="0">All</option>
                            @foreach ($branches as $branch)
                            <option value="{{$branch->id}}">{{$branch->name}}</option>
                            @endforeach
                            

                        </select>
                    </div>

                    <div class="col-md-2 mt-3 mt-md-0">
                        <button id="search_button" class="btn btn-primary btn-block">Search</button>
                    </div>

                    <div class="col-md-2 mt-3 mt-md-0">
                    <button  type='submit' class="btn btn-danger btn-block" name="export-pdf">Export Pdf</button>
                    </div>

                    <div class="col-md-2 mt-3 mt-md-0">
                        <button  type='submit' class="btn btn-success btn-block" name="export-excel">Export Excel</button>
                    </div>
                   
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<div class="row mb-4">

    <div class="col-md-12 mb-4">
        
        <div class="table-responsive ">
            
            <table id="report_table" class="table table-striped" >
              
            </table>
        </div>
        
        
        
    </div>
    
    
</div>



@endsection

@section('page-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="{{asset('public/assets/js/vendor/datatables.min.js')}}"></script>
<script src="{{asset('public/assets/js/datatables.script.js')}}"></script>

<script>
    
    
    function getData() 
    {   
        let branch = $('#branch').val();
        let month = $('#month').val();

        $.ajax({
            url: "{{url('rider-delivery')}}",
            type: 'get',
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data:{branch,month},
            dataType: 'json',
            success: function (result) 
            {
                if(result)
                    populateDomTable(result);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }

    function populateDomTable(data) 
    {   
        
        var table = document.getElementById("report_table");
        table.innerHTML = "";
        var header = table.createTHead();
        // create first row 
        var row = header.insertRow(0);
        row.insertCell(0).outerHTML = "<th>Name</th>"; // create first cell

        for(var i in data.month_days)
        {
            var item = data.month_days[i];
            row.insertCell(-1).outerHTML = "<th>"+item+"</th>"
        }
        
        // create second row
        row = header.insertRow(-1);
        row.insertCell(0);  // create first empty cell
        
        for(var i in data.month_days)
        {
            row.insertCell(-1).textContent = "Sale-KM";
        }
        
        var tbody = document.createElement("TBODY");
        for(var i in data)
        {   
            if(i == "month_days" || i == "branch_name" )
                continue;
            
            var row = tbody.insertRow(0);
           
            for(var j in data[i])
            {
               if(j == "rider_name")
                continue;
                row.insertCell(0).innerHTML = data[i][j].sale_amt+"-"+data[i][j].km;

            }
            row.insertCell(0).innerHTML = data[i].rider_name; console.log( data[i]);
        }
       
       table.appendChild(tbody);

        
    }

    $('#search_button').click(function(e){
        e.preventDefault();
        getData();        
    });    

    
</script>
@endsection
