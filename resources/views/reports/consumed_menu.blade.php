@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('public/assets/styles/vendor/datatables.min.css')}}">
@endsection
@section('main-content')

<div class="breadcrumb">
    <h1>Consumed Menu</h1>
    <ul>
        <li>Report</li>
    </ul>
</div>

<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-lg-12 col-md-12">
        
    <div class="card mb-5">
            <div class="card-body">
                <form method="POST" target="_blank" action="{{route('export-consumed-menu')}}" class="needs-validation" novalidate>
                    @csrf
                <div class="row row-xs">
                    <div class="col-md-2">
                        <input name="start_date" id="start_date" value="<?php echo date('Y-m-d'); ?>" class="form-control" type="date" placeholder="Start Date" />
                    </div>
                    <div class="col-md-2 mt-3 mt-md-0">
                        <input name="end_date" id="end_date" value="<?php echo date('Y-m-d'); ?>" class="form-control" type="date" placeholder="End Date" />
                    </div>
                    <div class="col-md-2 mt-3 mt-md-0">
                        <select class="form-control" name="branch" id="branch">
                            <option value="0">All</option>
                            @foreach ($branches as $branch)
                            <option value="{{$branch->id}}">{{$branch->name}}</option>
                            @endforeach
                            

                        </select>
                    </div>

                    <div class="col-md-2 mt-3 mt-md-0">
                        <button id="search_button" class="btn btn-primary btn-block">Search</button>
                    </div>

                    <div class="col-md-2 mt-3 mt-md-0">
                    <button  type='submit' class="btn btn-danger btn-block" name="export-pdf">Export Pdf</button>
                    </div>

                    <div class="col-md-2 mt-3 mt-md-0">
                        <button  type='submit' class="btn btn-success btn-block" name="export-excel">Export Excel</button>
                    </div>
                   
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<div class="row mb-4">

    <div class="col-md-12 mb-4">
        
        <div class="table-responsive ">
            
            <table id="report_table" class="table table-striped table-dark" style="width:100%">
                <thead>
                    <tr>
                        <th>Menu item</th>
                        <th>Quantity</th>
                        <th>Branch</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                
                <tbody>
                </tbody>
                <tfoot>
                    <tr>    
                        <th style="text-align:left">Total:</th>
                        <th id ='total_qty'></th>
                        <th></th>
                        <th id ='total_amt'></th>
                    </tr>
                </tfoot>
            </table>
        </div>
        
        
        
    </div>
    
    
</div>



@endsection

@section('page-js')

<script src="{{asset('public/assets/js/vendor/datatables.min.js')}}"></script>
<script src="{{asset('public/assets/js/datatables.script.js')}}"></script>
<script>
    
    
    function getData() 
    {   
        let start_dt = $('#start_date').val();;
        let end_dt = $('#end_date').val();
        let branch = $('#branch').val();
        
        table=  $('#report_table').DataTable({
            // "pageLength": "-1",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "processing": true,
            "serverSide": true,
            "destroy":true,
            "searching": false,
            "ajax": {
                "url": "consumed-menu",
                "data": {
                    "start_date": start_dt,
                    "end_date":end_dt,
                    "branch":branch,
                },
            },
            
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                    i : 0;
                };
                
                // Total over all pages
                total = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
                total = parseFloat(total).toFixed(2);
                // Total over this page
                total_qty = api.column( 1 ).data().reduce( function (a, b) {
                      return intVal(a) + intVal(b);
                  },0 );
                
                // Update footer
                $('#total_amt').html(total);
                $('#total_qty').html(total_qty);
            }
            
        });
    }
    $('#search_button').click(function(e){
        e.preventDefault();
        getData();        
    });    

    
</script>
@endsection
