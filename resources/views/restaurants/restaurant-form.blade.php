@extends('layouts.master')
@section('page-css')
  <link rel="stylesheet" href="{{asset('public/assets/styles/vendor/sweetalert2.min.css')}}">
@endsection
@section('main-content')
<div class="breadcrumb">
    @if(isset($restaurant))
    <h1>Update Restaurant</h1>
    @else
    <h1>Create New Restaurant</h1>
    @endif

    <a type="button" class="btn  btn-primary m-1" href="{{url('list-restaurants')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-File-Horizontal-Text"></i>Back to List</a>
                
</div>

@if(count($errors) > 0)

<div class="alert alert-danger">
    <ul>
    @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
    @endforeach
    </ul>
</div>
@endif


<div class="separator-breadcrumb border-top"></div>
<div class="2-columns-form-layout">
    <div class="">
        <div class="row">
            <div class="col-lg-12">
                
                <!-- start card 3 Columns  Form Layout-->
                <div class="card">
                    
                    <!--begin::form-->
                    <form  action = "{{isset($restaurant)?url('/update-restaurant'):url('/store-restaurant') }}" method = "POST" class="needs-validation" novalidate>
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{isset($restaurant)?$restaurant->id:''}}">
                        <div class="card-body">
                            
                            
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="restaurant_name" class="ul-form__label">Name:</label>
                                <input type="text" class="form-control" name="name" value="{{isset($restaurant)?$restaurant->name:old('name') }}" id="name" placeholder="Enter restaurant name" >
                                    <div class="invalid-tooltip">
                                        Please enter restaurant name
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="owner_name" class="ul-form__label">Owner:</label>
                                    <input type="text" class="form-control" name="owner_name" value="{{isset($restaurant)?$restaurant->owner:old('owner_name')}}"  id="owner_name" placeholder="Enter owner name" required>
                                    <div class="invalid-tooltip">
                                        Please enter owner name
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="contact_person" class="ul-form__label">Contact Person:</label>
                                    <input type="text" class="form-control" name="contact_person" value="{{isset($restaurant)?$restaurant->contact_person:old('contact_person')}}"  id="contact_person" placeholder="Enter Contact Person Name" required>
                                    <div class="invalid-tooltip">
                                        Please enter contact person name
                                    </div>
                                </div>
                                
                            </div>
                            
                            
                            <div class="custom-separator"></div>
                            
                            
                            <div class="form-row">

                                <div class="form-group col-md-4">
                                    <label for="contact_number" class="ul-form__label">Contact Number:</label>
                                    <input type="text" class="form-control" name="contact_number" value="{{isset($restaurant)?$restaurant->contact_number:old('contact_number')}}"  id="contact_number" placeholder="Enter Contact Number" required>
                                    <div class="invalid-tooltip">
                                        Please enter your contact number
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label for="contact_email" class="ul-form__label">Contact Email:</label>
                                    <input type="email" class="form-control" name="contact_email" value="{{isset($restaurant)?$restaurant->contact_email:old('contact_email')}}" id="contact_email" placeholder="Enter Contact Email" required>
                                    <div class="invalid-tooltip">
                                        Please enter contact email
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                        
                        <div class="card-footer">
                            <div class="mc-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-right">
                                        <button class="btn btn-primary" type="submit" >{{isset($restaurant)?"Update":"Save"}}</button>
                                        <a class="btn btn-outline-secondary m-1" href={{url('list-restaurants')}}>Cancel</a>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </form>
                    
                    <!-- end::form 3-->
                    
                </div>
                <!-- end card 3-->
                
                
                
                
                
            </div>
            
        </div>
        <!-- end of main row -->
    </div>
</div>

@endsection

@section('page-js')
 <script src="{{asset('public/assets/js/vendor/sweetalert2.min.js')}}"></script>
<script src="{{asset('public/assets/js/sweetalert.script.js')}}"></script>
@endsection

@section('bottom-js')
<script src="{{asset('public/assets/js/form.validation.script.js')}}"></script>
@endsection
