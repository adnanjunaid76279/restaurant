@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('public/assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('public/assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection

@section('main-content')
@if(\Session::has('success'))
<div class="alert alert-success">
    <p>{{\Session::get('success')}}</p>
</div>
@endif


   <div class="breadcrumb">
                <h1>{{$branch->name}}</h1>
                <ul>
                    <li><a href="">Restaurant Branch</a></li>
                    <li>View</li>
                </ul>
                <a type="button" class="btn  btn-warning m-1" href="{{url('list-restaurant-branches')}}" style="position: absolute;right: 150px;"><i class="nav-icon mr-2 i-File-Horizontal-Text"></i>Back to List</a>
                <a type="button" class="btn  btn-primary m-1" href="{{url('create-branch')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-Add"></i>Create</a>

            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row">

                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <form >
                                <div class="row">


                                    <div class="col-md-6 form-group mb-3">
                                        <label for="firstName2">Name</label>
                                        <input type="text" class="form-control form-control-rounded" disabled value="{{$branch->name}}" >
                                    </div>

                                    <div class="col-md-6 form-group mb-3" style="{{isset($branch->logo)?'' :'display:none' }}" >
                                        <label for="lastName2">Logo</label>
                                        <input type="text" class="form-control form-control-rounded" disabled value="{{$branch->logo}}">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="phone1">Contact Person</label>
                                        <input class="form-control form-control-rounded" disabled value="{{$branch->contact_person}}">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="phone1">Contact Number</label>
                                        <input class="form-control form-control-rounded" disabled value="{{$branch->contact_number}}">
                                    </div>


                                    <div class="col-md-6 form-group mb-3">
                                        <label for="exampleInputEmail2">Contact Email</label>
                                        <input type="email" class="form-control form-control-rounded" disabled value="{{$branch->contact_email}}">
                                    </div>

                                    {{-- <div class="col-md-6 form-group mb-3">
                                        <label for="credit">Facebook Page Name</label>
                                        <input class="form-control form-control-rounded" disabled value="{{$branch->facebook_page_name}}">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="credit">Facebook Page Id</label>
                                        <input class="form-control form-control-rounded" disabled value="{{$branch->facebook_page_id}}">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="credit">Facebook App Id</label>
                                        <input class="form-control form-control-rounded" disabled value="{{$branch->facebook_app_id}}">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="credit">Facebook App Secret</label>
                                        <input class="form-control form-control-rounded" disabled value="{{$branch->facebook_app_secret}}">
                                    </div> --}}



                                </div>

                                        <div class="row">
                                            <div class="col-lg-12 text-right">
                                                <a type="button" class="btn btn-success" href="{{url('edit-branch/'.$branch->id)}}" >
                                                    <i class="nav-icon mr-2 i-Pen-2"></i>
                                                    Edit
                                                </a>

                                            </div>
                                        </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>


@endsection

@section('page-js')
<script src="{{asset('public/assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('public/assets/js/vendor/pickadate/picker.date.js')}}"></script>



@endsection

@section('bottom-js')
<script src="{{asset('public/assets/js/form.basic.script.js')}}"></script>


@endsection
