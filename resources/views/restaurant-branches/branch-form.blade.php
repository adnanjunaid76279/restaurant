@extends('layouts.master')
@section('page-css')
  <link rel="stylesheet" href="{{asset('public/assets/styles/vendor/sweetalert2.min.css')}}">
  <link rel="stylesheet" href="{{asset('public/assets/styles/vendor/dropzone.min.css')}}">
@endsection
@section('main-content')
    @php
        if(isset($branch->id) )
        {
            $edit_image = $branch->logo;
            $branchId = $branch->id;
        }
        else
        {
            $branchId= "";
            $edit_image = "";
        }
        $img_url = asset('public'). '/Temp/'.$branchId.'/images/';

    @endphp
<div class="breadcrumb">
    @if(isset($branch))
    <h1>Update Restaurant Branch</h1>
    @else
    <h1>Create New Restaurant Branch</h1>
    @endif

    <a type="button" class="btn  btn-primary m-1" href="{{url('list-restaurant-branches')}}" style="position: absolute;right: 45px;"><i  class="nav-icon mr-2 i-File-Horizontal-Text"></i>Back to List</a>

</div>


@if(count($errors) > 0)

<div class="alert alert-danger">
    <ul>
    @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
    @endforeach
    </ul>
</div>
@endif

@include('image-modal',['branchId'=>$branchId, 'img_url'=>$img_url])


    <div class="separator-breadcrumb border-top"></div>
<div class="2-columns-form-layout">
    <div class="">
        <div class="row">
            <div class="col-lg-12">

                <!-- start card 3 Columns  Form Layout-->
                <div class="card">

                    <!--begin::form-->
                    <form  action = "{{isset($branch)?url('/update-branch'):url('/store-branch') }}" method = "POST" class="needs-validation" novalidate>
                        {{ csrf_field() }}
                        <input type="hidden" id="branchId" name="id" value="{{isset($branch)?$branch->id:''}}">

                        <div class="card-body">
                            <div class="form-row">

                                <div class="form-group col-md-12">

                                    <button type="button" class="btn btn-success"  data-toggle="modal" data-target="#imageModal" style="{{isset($branch)?'' :'display:none' }};"><i class="nav-icon mr-2 i-File-Text--Image"></i>Upload Image </button>
                                </div>

                                @if(Auth::guard('admin_user')->User()->role_id == 1 )

                                    <div class="form-group col-md-3">
                                        <label for="select_restaurant" class="ul-form__label">Search Restaurant:</label>
                                        <input type="text" class="typeahead form-control" autocomplete="off" placeholder="Search Restaurant" value="{{isset($branch)?$branch->restaurant->name:''}}" id="basic-url" aria-describedby="basic-addon3">
                                        <input type="hidden" id="restaurant_id" name="restaurant_id" value="{{isset($branch)?$branch->restaurant_id:old('restaurant_id')}}">
                                    </div>
                                @endif

                                <div class="form-group col-md-3">
                                    <label for="branch_name" class="ul-form__label">Branch Name:</label>
                                <input type="text" class="form-control" name="branch_name" value="{{isset($branch)?$branch->name:old('branch_name')}}" id="branch_name" placeholder="Enter Branch name" required>
                                    <div class="invalid-tooltip">
                                        Please enter branch name
                                    </div>
                                </div>

                                <div class="form-group col-md-2">
                                    <label for="contact_person" class="ul-form__label">Contact Person:</label>
                                    <input type="text" class="form-control" name="contact_person" value="{{isset($branch)?$branch->contact_person:old('contact_person')}}"  id="contact_person" placeholder="Enter Contact Person" required>
                                    <div class="invalid-tooltip">
                                        Please enter contact person
                                    </div>
                                </div>

                                <div class="form-group col-md-2">
                                    <label for="contact_number" class="ul-form__label">Contact Person Number:</label>
                                    <input type="text" class="form-control" name="contact_number" value="{{isset($branch)?$branch->contact_number:old('contact_number')}}"  id="contact_number" placeholder="Enter Contact Number" required>
                                    <div class="invalid-tooltip">
                                        Please enter your contact number
                                    </div>
                                </div>

                                <div class="form-group col-md-2">
                                    <label for="contact_email" class="ul-form__label">Contact Email:</label>
                                    <input type="email" class="form-control" name="contact_email" value="{{isset($branch)?$branch->contact_email:old('contact_email')}}" id="contact_email" placeholder="Enter Contact Email" required>
                                    <div class="invalid-tooltip">
                                        Please enter contact email
                                    </div>
                                </div>

                            </div>


                            <div class="custom-separator"></div>


                            <div class="form-row">

                                <div class="form-group col-md-3">
                                    <label for="branch_contact1" class="ul-form__label">Branch Contact1:</label>
                                    <input type="text" class="form-control" name="branch_contact1" value="{{isset($branch)?$branch->phone_number:old('branch_contact1')}}"  id="branch_contact1" placeholder="Enter Contact Number" required>
                                    <div class="invalid-tooltip">
                                        Please enter branch contact number
                                    </div>
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="branch_contact2" class="ul-form__label">Branch Contact2:</label>
                                    <input type="text" class="form-control" name="branch_contact2" value="{{isset($branch)?$branch->phone_number1:old('branch_contact2')}}"  id="branch_contact2" placeholder="Enter Contact Number" >
                                    <div class="invalid-tooltip">
                                        Please enter branch contact number
                                    </div>
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="branch_contact3" class="ul-form__label">Branch Contact3:</label>
                                    <input type="text" class="form-control" name="branch_contact3" value="{{isset($branch)?$branch->phone_number2:old('branch_contact3')}}"  id="branch_contact3" placeholder="Enter Contact Number" >
                                    <div class="invalid-tooltip">
                                        Please enter branch contact number
                                    </div>
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="branch_contact4" class="ul-form__label">Branch Contact4:</label>
                                    <input type="text" class="form-control" name="branch_contact4" value="{{isset($branch)?$branch->phone_number3:old('branch_contact4')}}"  id="branch_contact4" placeholder="Enter Contact Number" >
                                    <div class="invalid-tooltip">
                                        Please enter branch contact number
                                    </div>
                                </div>

                            </div>
                            <div class="custom-separator"></div>
                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label for="location" class="ul-form__label">Location:</label>
                                    <input type="text" class="form-control" name="location" value="{{isset($branch)?$branch->location:''}}"  id="location" placeholder="Enter Location" required>
                                    <input type="hidden" name="latitude_input" id ="latitude_input" value="{{isset($branch)?$branch->latitude:''}}">
                                    <input type="hidden" name="longitude_input" id ="longitude_input" value="{{isset($branch)?$branch->longitude:''}}">
                                    <div class="invalid-tooltip">
                                        Please enter location
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="town" class="ul-form__label">Town:</label>
                                    <select name="town" class="form-control"   id="town" placeholder="Select Town" required>
                                    @foreach ($towns as $item)
                                        <option value="{{$item->id}}" {{isset($branch->town_id) && $branch->town_id == $item->id ?'selected':old('town')}}>{{$item->town_name}}</option>
                                    @endforeach
                                    </select>

                                    <div class="invalid-tooltip">
                                        Please enter Town
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="address" class="ul-form__label">Address:</label>
                                    <input type="text" class="form-control" name="address" value="{{isset($branch)?$branch->address:old('address')}}"  id="address" placeholder="Enter Address" required>
                                    <div class="invalid-tooltip">
                                        Please enter address
                                    </div>
                                </div>


                            </div>
                            <div class="custom-separator"></div>

                            <div class="form-row">

                                <div class="form-group col-md-3">
                                    <label for="currency" class="ul-form__label">Currency:</label>
                                    <input type="text" class="form-control" name="currency" value="{{isset($branch)?$branch->currency:old('currency')}}"  id="currency" placeholder="Enter Currency" required>
                                    <div class="invalid-tooltip">
                                        Please enter currency
                                    </div>
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="discount" class="ul-form__label">Discount % (on overall orders):</label>
                                    <input type="text" class="form-control" name="discount" value="{{isset($branch)?$branch->discount:old('discount')}}"  id="discount" placeholder="Enter Discount %" >
                                    <div class="invalid-tooltip">
                                        Please enter discount
                                    </div>
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="tax_percent" class="ul-form__label">Sale Tax % (on overall orders):</label>
                                    <input type="text" class="form-control" name="tax_percent" value="{{isset($branch)?$branch->tax_percent:old('tax_percent')}}"  id="tax_percent" placeholder="Enter Sale Tax %" required>
                                    <div class="invalid-tooltip">
                                        Please enter sale tax
                                    </div>
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="ntn_number" class="ul-form__label">NTN:</label>
                                    <input type="text" class="form-control" name="ntn_number" value="{{isset($branch)?$branch->ntn_number:old('ntn_number')}}"  id="ntn_number" placeholder="Enter NTN Number">
                                    <div class="invalid-tooltip">
                                        Please enter ntn number
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="strn_number" class="ul-form__label">STRN:</label>
                                    <input type="text" class="form-control" name="strn_number" value="{{isset($branch)?$branch->strn_number:old('strn_number')}}"  id="ntn_number" placeholder="Enter NTN Number">
                                    <div class="invalid-tooltip">
                                        Please enter strn_number number
                                    </div>
                                </div>

                            </div>
                            <div class="custom-separator"></div>

                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="minimum_order_value" class="ul-form__label">Minimum Order Amount:</label>
                                    <input type="number" class="form-control" name="minimum_order_value" value="{{isset($branch)?$branch->minimum_order_value:old('minimum_order_value')}}"  id="minimum_order_value" placeholder="Enter Minimum Order Amount" required>
                                    <div class="invalid-tooltip">
                                        Please enter minimum order amount
                                    </div>
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="open_time" class="ul-form__label">Open Time:</label>
                                    <input type="time" class="form-control" name="open_time" value="{{isset($branch)?$branch->open_time:old('open_time')}}"  id="open_time" placeholder="Enter Open Time" required>
                                    <div class="invalid-tooltip">
                                        Please enter open time
                                    </div>
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="close_time" class="ul-form__label">Close Time:</label>
                                    <input type="time" class="form-control" name="close_time" value="{{isset($branch)?$branch->close_time:old('close_time')}}"  id="close_time" placeholder="Enter Close Time" required>
                                    <div class="invalid-tooltip">
                                        Please enter close time
                                    </div>
                                </div>

                            </div>

                            <div class="custom-separator"></div>

                            <div class="form-row">


                                    <label class="switch switch-success mr-3">
                                        <span>Take Away</span>
                                        <input type="checkbox" name="take_away" id="take_away" {{(isset($branch->is_takeaway) && $branch->is_takeaway==1)?'checked':''}}>
                                        <span class="slider"></span>
                                    </label>

                                    <label class="switch switch-success mr-3">
                                        <span>Dine In</span>
                                        <input type="checkbox" name="dinein" id="dinein" {{(isset($branch->is_dinein) && $branch->is_dinein==1)?'checked':''}}>
                                        <span class="slider"></span>
                                    </label>

                                    <label class="switch switch-success mr-3">
                                        <span>Delivery</span>
                                        <input type="checkbox" name="delivery" id="delivery" {{(isset($branch->is_delivery) && $branch->is_delivery==1)?'checked':''}}>
                                        <span class="slider"></span>
                                    </label>

                                    <label class="switch switch-success mr-3">
                                        <span>Free Delivery</span>
                                        <input type="checkbox" name="free_delivery" id="free_delivery" {{(isset($branch->is_freedelivery) && $branch->is_freedelivery==1)?'checked':''}}>
                                        <span class="slider"></span>
                                    </label>

                                    <label class="switch switch-success mr-3">
                                        <span>Sale Tax (Inclusive)</span>
                                        <input type="checkbox" name="tax_inclusive" id="tax_inclusive" {{(isset($branch->tax_include) && $branch->tax_include==1)?'checked':''}}>
                                        <span class="slider"></span>
                                    </label>

                                    <label class="switch switch-success mr-3">
                                        <span>Order Allow</span>
                                        <input type="checkbox" name="order_allow" id="order_allow" {{(isset($branch->order_taking) && $branch->order_taking==1)?'checked':''}}>
                                        <span class="slider"></span>
                                    </label>

                                    {{-- <label class="switch switch-success mr-3">
                                        <span>Upload Facebook</span>
                                        <input type="checkbox" name="upload_fb" id="upload_fb" {{(isset($branch->is_takeaway) && $branch->is_takeaway==1)?'checked':''}}>
                                        <span class="slider"></span>
                                    </label> --}}



                            </div>

                            <div class="custom-separator"></div>

                            <div class="form-row">

{{--                                <div class="form-group col-md-12">--}}
{{--                                    <label for="description" class="ul-form__label">Description:</label>--}}
{{--                                    <textarea class="form-control" id="description" name="description" value="{{isset($branch)?$branch->description:''}}" rows="4" cols="50" placeholder="Enter Description"> </textarea>--}}

{{--                                    <div class="invalid-tooltip">--}}
{{--                                        Please enter description--}}
{{--                                    </div>--}}
{{--                                </div>--}}

                                <div id="logo-div" class="col-md-4 form-group" style="{{isset($branch->logo)?'' :'display:none' }}">
                                    <label for="Logo" class="ul-form__label">Image</label>
                                    <input class="form-control" type="hidden" name="logo" value="{{ $edit_image }}"  id="logo" readonly>
                                    <div id="set-logo">
                                        <img id="logo-image" name="logo"  src="{{asset($img_url.$edit_image)}}" style="width: 150px;">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="card-footer">
                            <div class="mc-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-right">
                                        <button class="btn btn-primary" type="submit" >{{isset($branch)?"Update":"Save"}}</button>
                                        <a class="btn btn-outline-secondary m-1" href={{url('list-restaurant-branches')}}>Cancel</a>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>

                    <!-- end::form 3-->

                </div>
                <!-- end card 3-->





            </div>

        </div>
        <!-- end of main row -->
    </div>
</div>



    @endsection

    @section('page-js')
     <script src="{{asset('public/assets/js/vendor/sweetalert2.min.js')}}"></script>
    <script src="{{asset('public/assets/js/sweetalert.script.js')}}"></script>
     <script src="{{asset('public/assets/js/vendor/dropzone.min.js')}}"></script>
    <script>

        // script for autocomplete geolocation
        Dropzone.autoDiscover = false;
        var searchInput = 'location';

        $(document).ready(function(){

            var branchId =$('#branchId').val();

            var myDropzone = new Dropzone("#upload_image", {

                    createImageThumbnails: true,
                    addRemoveLinks: true,
                    url: "{{url('saveImage')}}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    init: function () {
                        this.on("sending", function (file, xhr, formData) {
                            formData.append('branchId',branchId);
                        })
                    }
                }
            );

            // if({{$show_edit_logo ?? 0}})
           
            // var mockFile = { name: "{{$logo_image??''}}", size: 12345 };
            //    myDropzone.options.addedfile.call(myDropzone, mockFile);
            //  myDropzone.options. thumbnail.call(myDropzone, mockFile);
            

            // myDropzone.on("addedfile", function(file)
            // {
            //     if (this.files[1]!=null)
            //     {
            //         this.removeFile(this.files[0]);
            //     }
            // });

            // myDropzone.on("removedfile", function(file)
            // {
            //     $('#logo').val('');
            // });


            myDropzone.on("success", function(file)
            {
                var uploadedfile_name = "";
                $('#logo').val('');

                if(file.xhr.response)
                    uploadedfile_name = file.xhr.response;
                $('#logo-div').show();

                $('#logo').val(uploadedfile_name);
                var branchID =$('#branchId').val();
                document.getElementById('logo-image').src= '{{ asset('public') }}'+'/Temp/'+branchID+'/images/'+uploadedfile_name;
                $('#imageModal').modal('toggle');
            });


            var autocomplete;

            var options = {
                        types: $('#' + searchInput).val() !== "" ? ($('#' + searchInput).val()).split(',') : []
                    };


            autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)),options);

            google.maps.event.addListener(autocomplete,'place_changed',function(){
                var near_place = autocomplete.getPlace();

                document.getElementById('latitude_input').value = near_place.geometry.location.lat();
                document.getElementById('longitude_input').value = near_place.geometry.location.lng();
            });

            if('{{isset($branch->name)?$branch->name:""}}' != '')
            {
                const geocoder = new google.maps.Geocoder();
                var lat =  document.getElementById('latitude_input').value;
                var lng =  document.getElementById('longitude_input').value;
                const latlng = {
                    lat: parseFloat(lat),
                    lng: parseFloat(lng),
                };
                geocoder.geocode({ location: latlng }, (results, status) => {
                    console.log(results);
                    if (status === "OK") {
                    if (results[0]) {

                        document.getElementById('location').value = results[0].formatted_address;
                    } else {
                        window.alert("No results found");
                    }
                    } else {
                    window.alert("Geocoder failed due to: " + status);
                    }
                });
            }
        });



        function getImageVal(val) {

            var branchID =$('#branchId').val();
            const imageVals = val.src;
            const pieces = imageVals.split(/[\s/]+/);
            const imgVal = pieces[pieces.length - 1];

            var img_url = '{{ asset('public') }}';

            $('#logo-div').show();
            $('#logo').val(imgVal);
            document.getElementById('logo-image').src= img_url+'/Temp/'+branchID+'/images/'+imgVal;
            $('#logo-div').focus();
            $('#imageModal').modal('toggle');


        }
            $(document).on('change','#' + searchInput,function(){
            document.getElementById('latitude_input').value = '';
            document.getElementById('longitude_input').value = '';
        });

        //End script for autocomplete geolocation
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

    @endsection

    @section('bottom-js')
    <script src="{{asset('public/assets/js/form.validation.script.js')}}"></script>
    <script type="text/javascript">

            $('input.typeahead').typeahead({
                minLength: 3,
                source:function (query, process) {
                    return $.ajax({
                                url: "{{url('searchRestaurantsByName')}}",
                                type: 'get',
                                data: { name: query },
                                headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                dataType: 'json',
                                success: function (result)
                                {
                                    var results = result.Data;
                                    if(!results)
                                        return false;
                                    var resultList = results.map(function (item) {
                                        var aItem = { id: item.id, name: item.name };
                                        return aItem;
                                    });

                                    return process(resultList);

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    alert(xhr.status);
                                    alert(thrownError);
                                }
                            });
                }

            });

            $(document).on('click','ul.typeahead li',function() {

                var data = $('ul.typeahead li.active').data().value;

                if(data != null && data != '' && data != undefined )
                {
                    var tempItemId = data.id;
                    var tempItemName = data.name;
                    document.getElementById('restaurant_id').value = tempItemId;
                }
            });

        </script>
    @endsection
