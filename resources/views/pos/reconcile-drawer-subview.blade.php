<h1>{{$branch->name}}</h1>
<input type="hidden" value="{{$branch->id}}" name="branch_id" id="branch_id">

@if($pos_cash_data &&  $pos_cash_data->is_closed)

    @php   

        $cash_out_items = unserialize($pos_cash_data->cash_out_items);
        $cash_in_items = unserialize($pos_cash_data->cash_in_items);
        $cash_detail = unserialize($pos_cash_data->cash_detail);
    
        //========= cash in sum ======================
        $sum_cash_in = 0;
        $sum_cash_in += $pos_cash_data->dinein_sale??0;
        $sum_cash_in += $pos_cash_data->takeaway_sale??0;
        $sum_cash_in += $pos_cash_data->delivery_sale??0;

        
    @endphp



<div class="col-md-12 mb-4">
    <div class="row">
        <div  class="col-md-6">
            <h4 class="card-title mb-3">Closing Cash</h4>
            <table border="1" width="100%" >
                <thead>
                    <tr><th>Cash Value</th><th>Qty</th><th>Total</th></tr>
                </thead>
                <tbody>
                    @php
                        $sum_counter_cash = 0;   
                    @endphp

                    @foreach ($cash_detail["Counter1"] as $key => $val)
                        @php
                            $sum_counter_cash += $val*$key;   
                        @endphp
                        <tr>
                            <td>{{$key}}</td><td>{{$val}}</td><td>{{$val*$key}}</td>
                        </tr>
                    @endforeach
                </tbody>
                
                <tfoot>
                    <tr><th  colspan="2">Total</th><th>{{number_format($sum_counter_cash,2)}}</th></tr>
                </tfoot>
                
            </table>
        </div>
        
        <div  class="col-md-6">
            <h4 class="card-title mb-3">Cash In/Out</h4>
            <table border="1" width="100%">
                <thead>
                    <tr><th colspan="2">Cash In</th></tr>
                    @php
                        $sum_opencash = 0;

                    @endphp
                    
                    @foreach ($cash_in_items as $key => $val)
                        <tr style="color:green"><th>{{$key}}</th><td>{{number_format($val,2)}}</td></tr>
                        @php
                            $sum_opencash += $val;
                        @endphp
                    @endforeach
                    <tr><th>Sales</th><th>Amount</th></tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Dine In</td><td>{{$pos_cash_data->dinein_sale}}</td>
                    </tr>

                    <tr>
                        <td>Takeaway</td><td>{{$pos_cash_data->takeaway_sale}}</td>
                    </tr>

                    <tr>
                        <td>Delivery</td><td>{{$pos_cash_data->delivery_sale}}</td>
                    </tr>
                    <tr><th>Total Sale:</th><th>{{number_format($sum_cash_in,2)}}</th></tr>

                    <tr><th colspan="2">Cash Out</th></tr>
                    @php
                        $sum_cashout = 0;

                    @endphp
                    
                    @foreach ($cash_out_items as $key => $val)
                    @php
                    $sum_cashout += $val;
                    @endphp
                        <tr style='color:red'>
                            <td>{{$key}}</td><td >{{number_format($val,2)}}</td>
                        </tr>
                    @endforeach
                    
                    
                </tbody>
                <tfoot>
                    <tr style='color:red'><th>Total Cash Out:</th><th>{{number_format($sum_cashout,2)}}</th></tr>
                    <tr><th>Cash Balance(In+Sale-Out):</th><th>{{number_format($sum_cash_in + $sum_opencash - $sum_cashout,2)}}</th></tr>
                </tfoot>
                
            </table>
            
        </div>

      
        
    </div>
    
    <br>
    <h3>Difference</h3><span>{{number_format($sum_cash_in + $sum_opencash - $sum_cashout - $sum_counter_cash ,2)}}</span>
    
</div>

@else

{{-- //=============== Else if reconcile is not closed ================== --}}

@php
$cashval = ["1","2","5","10","20","50","100","500","1000","5000"];
@endphp

<div class="col-md-12 mb-4">
    <div class="row">
        <div  class="col-md-6">
            <h4 class="card-title mb-3">Closing Cash</h4>
           
            <table border="1" width="100%" id="cash_detail">
                <thead>
                    <tr><th>Cash Value</th><th>Qty</th><th>Total</th></tr>
                </thead>
                <tbody>
                   
                    @foreach ($cashval as $val)
                        <tr>
                            <td style="text-align: center;width:40%">{{$val}}</td>
                            <td style="text-align: center;">    <input type="number" style="text-align: center;" min="0" onblur="calculateCashValue(this)"> </td>
                            <td style="text-align: center;width:50%"></td>
                        </tr> 
                    @endforeach 

                </tbody>
                
            </table>
        </div>
        
        <div  class="col-md-6">
            <h4 class="card-title mb-3">Cash In/Out</h4>
            <table border="1" width="100%" id="cash_inout">
                <thead>
                    <tr><th style="width:50%">Opening Cash</th><td><input type="number" id="opening_cash" style="text-align: center;width:100%" min="0" ></td></tr>
                    <tr><th>Sales</th><th>Amount</th></tr>
                </thead>
                <tbody>
                    @php
                        $sum_sale = 0;
                    @endphp

                    @foreach ($sales as $val)
                        @php $sum_sale += $val->sum_total; @endphp
                        <tr>
                            <td>{{$val->type}}</td><td style="text-align: center;width:50%">{{$val->sum_total}}</td>
                        </tr>
                    @endforeach 
                   
                    <tr id="sum_sale"><th>Total Sale:</th><th>{{$sum_sale}}</th></tr>

                    <tr id="cash_out"><th>Cash Out</th><th><a href="#" onclick="insertRow(this,event)" class="btn btn-success btn-sm m-1">Click to insert cash out item</a></th></tr>
                    
                    
                    
                </tbody>
             
                
            </table>
            
        </div>
        <div class="separator-breadcrumb border-top"></div>
        <br>
       
        
    </div>
    <br>
    
    <h3>Difference</h3><span id="diff"></span>
    
  
    <br>
    <div class="separator-breadcrumb border-top"></div>
   
        <button value="Submit" type="button" class="btn btn-success" onclick="saveClosing()">Submit</button>
   @endif


