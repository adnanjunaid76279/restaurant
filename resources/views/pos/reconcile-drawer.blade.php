@extends('layouts.master')
@section('page-css')
<style>
    th,td {
        text-align: center;
    }
</style>
@endsection

@section('main-content')
  <div class="breadcrumb">
                <h1>Reconcile</h1>
                <ul>
                    <li>Drawer</li>
                </ul>
              
            <a type="button" class="btn btn-primary m-1" href="{{url('create-menu')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-Add"></i>Create</a>
                
            </div>
            
            <div class="separator-breadcrumb border-top"></div>
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    
                <div class="card mb-5">
                        <div class="card-body">
                            {{-- <form method="POST" target="_blank" action="{{route('export-consumed-menu')}}" class="needs-validation" novalidate>
                                @csrf --}}
                            <div class="row row-xs">
                                
                                <div class="col-md-2">
                                    <input name="start_date" id="start_date" value="<?php echo date('Y-m-d'); ?>" class="form-control" type="date" placeholder="Start Date" />
                                </div>
                            
                                <div class="col-md-2 mt-3 mt-md-0">
                                    <select class="form-control" name="branch" id="branch">
                                        @foreach ($branches as $branch)
                                        <option value="{{$branch->id}}">{{$branch->name}}</option>
                                        @endforeach
                                        
            
                                    </select>
                                </div>

                                <div class="col-md-2 mt-3 mt-md-0">
                                    <button  class="btn btn-primary" onclick="loadComponent()" >Get Totals</button>
                                </div>
                               
                            </div>
                        {{-- </form> --}}
                        </div>
                    </div>
                </div>
            </div>            
            <!-- end of row -->
            <div class="row mb-4" id="subview">


            </div>
            <!-- end of row -->



@endsection

@section('page-js')


    <script>
 
    /**
    * 
    * @param {string} pagename Component Name
    * @returns {Promise} Promoise
    */
    function loadComponent(pagename) {
        var branchid = document.getElementById('branch').value;
        var datee = document.getElementById('start_date').value;

        var xhr = new XMLHttpRequest();
        var params = '?branch_id=' + branchid + '&datee=' + datee;
        
        xhr.open('GET', "reconcile-drawer-subview"+params);
        
        const parser = new DOMParser();
        
        return new Promise((resolve, reject) => {
            xhr.onload = function () {
                try {
                    let domElement = parser.parseFromString(this.response, 'text/html');
                    let domDiv = document.getElementById("subview");
                    domDiv.innerHTML = "";
                    domDiv.innerHTML = this.response;
                    resolve();
                } catch (error) {
                    reject(error);
                }
            };
            xhr.send();
        });
    }


    function calculateCashValue(ele)
    {
        var tr = ele.parentElement.parentElement;
        var tbody = ele.parentElement.parentElement.parentElement;
        var table = ele.parentElement.parentElement.parentElement.parentElement;
        var inputVal = Number(ele.value);
        var cashVal = Number(tr.cells[0].textContent);
        tr.cells[2].textContent = (inputVal*cashVal) ;

        var sumCashValue = 0;
        for(var i=0; i < tbody.rows.length; i++)
        {
            sumCashValue += Number(tbody.rows[i].cells[2].textContent);
        }
        
        table.deleteTFoot();

        // Create an empty <tfoot> element and add it to the table:
        var footer = table.createTFoot();

        // Create an empty <tr> element and add it to the first position of <tfoot>:
        var row = footer.insertRow(0);     

        // Insert a new cell (<td>) at the first position of the "new" <tr> element:
        row.insertCell(0).outerHTML = "<th colspan='2'>Total</th>";

        // Insert a new cell (<td>) at the first position of the "new" <tr> element:
        row.insertCell(1).outerHTML = "<th style='text-align:center'>"+sumCashValue+"</th>";
        
        calculateDiff();
    }

    // insert row for cash out items 
    function insertRow(ele,event)
    {
        event.preventDefault();
        var tbody = ele.parentElement.parentElement.parentElement;
        tbody.insertRow(-1).innerHTML = "<td> <input style='width:100%' type='text'> </td><td> <input style='width:100%;text-align:center' type='number' min='0' onblur='calculateCashInOut(this)'> </td>";
    }

    //calculate sum of cash out items
    function calculateCashInOut(ele)
    {

        var tr = ele.parentElement.parentElement;
        var tbody = ele.parentElement.parentElement.parentElement;
        var table = ele.parentElement.parentElement.parentElement.parentElement;
        var inputVal = Number(ele.value);
        var sumSale = Number(document.getElementById("sum_sale").cells[1].textContent);
        var openCash = Number(table.tHead.rows[0].cells[1].querySelector('input').value);

        var sumValue = 0 + sumSale + openCash;
        var allow_sum = 0;
        for(var i = 0; i < tbody.rows.length; i++)
        {
            if(tbody.rows[i].id == "cash_out")
            {1
                allow_sum = 1;continue;
            }
            if(allow_sum)    
                sumValue -= Number(tbody.rows[i].cells[1].querySelector('input').value);
        }
        
        table.deleteTFoot();

        // Create an empty <tfoot> element and add it to the table:
        var footer = table.createTFoot();

        // Create an empty <tr> element and add it to the first position of <tfoot>:
        var row = footer.insertRow(0);     

        // Insert a new cell (<td>) at the first position of the "new" <tr> element:
        row.insertCell(0).outerHTML = "<th>Cash Balance</th>";

        // Insert a new cell (<td>) at the first position of the "new" <tr> element:
        row.insertCell(1).outerHTML = "<th>"+sumValue+"</th>";

        calculateDiff();

    }

    function calculateDiff()
    {
        var closeCash = 0;
        var sumCashInOut = 0;

        if(document.getElementById("cash_detail").tFoot.rows.length > 0 )
            closeCash = document.getElementById("cash_detail").tFoot.rows[0].cells[1].textContent;

        if(document.getElementById("cash_inout").tFoot.rows.length > 0 )
            sumCashInOut = document.getElementById("cash_inout").tFoot.rows[0].cells[1].textContent;

        closeCash = Number(closeCash);
        sumCashInOut = Number(sumCashInOut);

        document.getElementById("diff").innerText = sumCashInOut - closeCash;
    }
    // save closing 
    function saveClosing()
    {
        
        var table = document.getElementById("cash_detail");
        var rows = table.tBodies[0].rows;
        var data = {};
        data.cash_detail = [];
        data.sales = [];
        data.cashout = [];
        data.cashin = [];
        data.branchid = document.getElementById("branch_id").value;

       

        for(var i = 0; i < rows.length; i++)
        {
            var temp = {};
            temp.cash_value = rows[i].cells[0].textContent;
            temp.qty = rows[i].cells[1].querySelector('input').value == "" ? 0 : rows[i].cells[1].querySelector('input').value;
            temp.total = rows[i].cells[2].textContent == "" ? 0 : rows[i].cells[2].textContent;
            data.cash_detail.push(temp); 
        }

        table = document.getElementById("cash_inout");
        rows = table.tBodies[0].rows;


        data.cashin.push(
            {
                "cashin_item"   :   table.tHead.rows[0].cells[0].textContent,
                "amt"   :    Number(table.tHead.rows[0].cells[1].querySelector('input').value)
            });

        for(var i = 0; i < rows.length; i++)
        {
            if(rows[i].id == "sum_sale")
                break;
            
            var temp = {};
            
            temp.sale_item = rows[i].cells[0].textContent;
            temp.amt = rows[i].cells[1].textContent;
           
            data.sales.push(temp); 
        }

        var allow_sum = 0;
        for(var i = 0; i < rows.length; i++)
        {
            if(rows[i].id == "cash_out")
            {
                allow_sum = 1;continue;
            }

            if(allow_sum)
            {
                var temp = {};
                temp.cashout_item = rows[i].cells[0].querySelector('input').value;
                temp.amt = rows[i].cells[1].querySelector('input').value;
                data.cashout.push(temp); 
            } 
                
        }

        //==========================================================================

        $.ajax({
            url: '{{url("reconcile-drawer-store")}}',
            type: 'post',
            data: data,
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            success: function (result) 
            {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });

    }

</script>
@endsection
