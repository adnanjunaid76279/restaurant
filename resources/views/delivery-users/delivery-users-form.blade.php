@extends('layouts.master')
@section('page-css')
<link rel="stylesheet" href="{{asset('/public/assets/styles/vendor/dropzone.min.css')}}">
@endsection
@section('main-content')


<div class="breadcrumb">

    <h1>{{isset($daily_user_service_data)?'Update Rider':'Create Rider'}} </h1>
 <a type="button" class="btn  btn-primary m-1" href="{{url('list-riders')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-File-Horizontal-Text"></i>Back to List</a>

</div>

@if(count($errors) > 0)

<div class="alert alert-danger">
    <ul>
    @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
    @endforeach
    </ul>
</div>
@endif


<div class="separator-breadcrumb border-top"></div>
<div class="2-columns-form-layout">
    <div class="">
        <div class="row">
            <div class="col-lg-12">

                <!-- start card 3 Columns  Form Layout-->
                <div class="card">

                    <!--begin::form-->
                    <form  action = "{{isset($daily_user_service_data)?url('update-rider'):url('store-rider')}}" method = "POST" class="needs-validation" novalidate>
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{isset($daily_user_service_data)?$daily_user_service_data->id:''}}">
                        <div class="card-body">
                             <div id="form_row" class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="first-name" class="ul-form__label">First Name</label>
                                    <input value="{{isset($daily_user_service_data)?$daily_user_service_data->first_name:''}}" type="text" class="form-control" autocomplete="off" placeholder="First Name" name="firstname" id="firstname" >
                                </div>
                                    <div class="form-group col-md-4">
                                        <label for="last-name" class="ul-form__label">Last Name</label>
                                        <input value="{{isset($daily_user_service_data)?$daily_user_service_data->last_name:''}}"  type="text" class="typeahead form-control" autocomplete="off" placeholder="Last Name" name="lastname" id="lastname" >
                                    </div>

                                <div class="form-group col-md-4">
                                    <label for="login" class="ul-form__label">Login</label>
                                    <input value="{{isset($daily_user_service_data)?$daily_user_service_data->login:''}}" type="text" class="form-control" autocomplete="off" placeholder="Login" id="login" name="login" >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="password" class="ul-form__label">Password</label>
                                    <input  value="{{isset($daily_user_service_data)?$daily_user_service_data->password:''}}" type="text" class="form-control" autocomplete="off" placeholder="Password" id="password"  name="password" >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="contact-number" class="ul-form__label">Contact</label>
                                    <input  value="{{isset($daily_user_service_data)?$daily_user_service_data->cell_num:''}}"  type="text" class="form-control" autocomplete="off" placeholder="Contact" id="cell_num" name="cell_num">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="role" class="ul-form__label">Branch</label>
                                    <select  style="width:100%!important;"class="form-control " id="role" name="branch">
                                @foreach($branches as $branch_name)
                                 
                                    <option value="{{ $branch_name->id }}" {{isset($daily_user_service_data)?($branch_name->id == $daily_user_service_data->branchId ? 'selected' :''  ):'' }} >{{ $branch_name->name}}</option>
                                @endforeach
                                
                                    </select>
                                </div>

                                
                              
                                </div>

                            </div>


                        </div>

                        <div class="card-footer">
                            <div class="mc-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-right">
                                        <button class="btn btn-primary" type="submit" >{{isset($daily_user_service_data)?"Update":"Save"}}</button>
                                        <a class="btn btn-outline-secondary m-1" href="{{url('list-riders')}}">Cancel</a>
    
                                    </div>
                                </div>
                            </div>
                        </div>

                    <!-- end::form 3-->

                </div>
                <!-- end card 3-->





            </div>

        </div>
        <!-- end of main row -->
    </div>

@endsection

@section('page-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@endsection

@section('bottom-js')
<script type="text/javascript">
$(document).ready(function() {
  var select2=  $('#role').select2();
    select2.data('select2').$selection.css('height', '34px');
    select2.data('select2').$selection.css('backgroundColor', '#F8F9FA');

});
</script>

@endsection
