@extends('layouts.master')

@section('main-content')
 <div class="breadcrumb">
        <h1>{{$daily_user_service_data->first_name}}</h1>
        <ul>
            <li><a href="">User</a></li>
            <li>View</li>
        </ul>
        <a type="button" class="btn  btn-primary m-1" href="{{url('list-riders')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-File-Horizontal-Text"></i>Back to List</a>
   </div>

    <div class="separator-breadcrumb border-top"></div>

            <div class="row">

                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <form>
                                <div id="form_row" class="row">

                                    <div class="col-md-6 form-group mb-3">
                                    <label for="first-name" class="ul-form__label">First Name</label>
                                    <input value="{{isset($daily_user_service_data)?$daily_user_service_data->first_name:''}}"  disabled type="text" class="form-control form-control-rounded" autocomplete="off" placeholder="First Name" name="firstname" id="firstname" >
                                   </div>
                                    <div class="col-md-6 form-group mb-3">
                                    <label for="last-name" class="ul-form__label">Last Name</label>
                                        <input value="{{isset($daily_user_service_data)?$daily_user_service_data->last_name:''}}" disabled type="text" class="typeahead form-control form-control-rounded" autocomplete="off" placeholder="Last Name" name="lastname" id="lastname" >
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                    <label for="login" class="ul-form__label">Login</label>
                                    <input value="{{isset($daily_user_service_data)?$daily_user_service_data->login:''}}" disabled type="text" class="form-control form-control-rounded" autocomplete="off" placeholder="Login" id="login" name="login" >
                                   </div>

                                    <div class="col-md-6 form-group mb-3">
                                    <label for="password" class="ul-form__label">Password</label>
                                    <input  value="{{isset($daily_user_service_data)?$daily_user_service_data->password:''}}" disabled type="text" class="form-control form-control-rounded" autocomplete="off" placeholder="Password" id="password"  name="password" >
                                   </div>

                                    <div class="col-md-6 form-group mb-3">
                                    <label for="contact-number" class="ul-form__label">Contact</label>
                                    <input  value="{{isset($daily_user_service_data)?$daily_user_service_data->cell_num:''}}" disabled  type="text" class="form-control form-control-rounded" autocomplete="off" placeholder="Contact" id="cell_num" name="cell_num">
                                    </div>
                                    <div class="col-md-6 form-group mb-3">
                                    <label for="contact-number" class="ul-form__label">Branch</label>
                                    <input type="text" class="form-control form-control-rounded" disabled value="{{isset($daily_user_service_data->branch->name)?$daily_user_service_data->branch->name:''}}">
                                     </div>
                               </div>

                                <div class="row">
                                    <div class="col-lg-12 text-right">
                                        <a type="button" class="btn btn-success" href="{{url('edit-rider/'.$daily_user_service_data->id)}}">
                                            <i class="nav-icon mr-2 i-Pen-2"></i>
                                            Edit
                                        </a>

                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-js')


    
   


@endsection

@section('bottom-js')



@endsection
