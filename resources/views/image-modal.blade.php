

<div class="modal fade" id="imageModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h2>Update Image</h2>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

                <ul class="nav nav-tabs" style="justify-content: space-around;">
                    <li><a data-toggle="tab" href="#new-menu">Upload</a></li>
                    <li><a  data-toggle="tab" href="#saved-menu" >Saved</a></li>
                </ul>
                <div class="tab-content">
                    <div id="new-menu" class="tab-pane fade in form-group col-md-6">

                        <div id="upload_image" class="dropzone"></div>
                    </div>
                    <div id="saved-menu" class="tab-pane fade" >
                        <div class="row">
                            @php
                                $images = array_map(function($filename)use($img_url)
                                {
                                    return $img_url . basename($filename);
                                },
                                glob('public/Temp/'.$branchId.'/images/*.*'));
                            @endphp
                            @if(count($images)>0)
                                @foreach($images as $key => $image)
                                    <div class="form-group col-md-4" >
                                        @if(isset($image)?$image:'')
                                            @php
                                                $pieces = explode("/", $image);
                                            @endphp
                                            @if($pieces[7]== $edit_image)
                                                <div style="border: 4px solid red;">
                                                    <img onclick="getImageVal(this)" id="save-logo" name="logo"  src="{{$image}}" style="width: 200px; height:180px;">
                                                </div>
                                                @else
                                                <div >
                                                    <img onclick="getImageVal(this)" id="save-logo" name="logo"  src="{{$image}}" style="width: 200px; height:180px;">
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                @endforeach
                            @else
                                <div>
                                    <h4>Sorry No Image Found against this Branch</h4>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

