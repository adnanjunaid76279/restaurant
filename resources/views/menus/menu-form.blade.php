@extends('layouts.master')
@section('page-css')
<link rel="stylesheet" href="{{asset('public/assets/styles/vendor/dropzone.min.css')}}">
@endsection
@section('main-content')
@php
if(isset($menu->id) )
{

    $show_edit_image = 1;
    $edit_image = $menu->image;
    $branchId = $menu->menuCategory->restaurantBranch->id;

}
else
{
    $branchId = "";
    $images = "";
    $edit_image = "";
}

     $img_url = asset('public'). '/Temp/'.$branchId.'/images/';
@endphp

<div class="breadcrumb">
    @if(isset($menu))
    <h1>Update Menu</h1>
    @else
    <h1>Create New Menu</h1>
    @endif

    <a type="button" class="btn  btn-primary m-1" href="{{url('list-menus')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-File-Horizontal-Text"></i>Back to List</a>

</div>

@if(count($errors) > 0)

<div class="alert alert-danger">
    <ul>
    @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
    @endforeach
    </ul>
</div>
@endif

@include('image-modal',['branchId'=>$branchId, 'img_url'=>$img_url])

<div class="separator-breadcrumb border-top"></div>
<div class="2-columns-form-layout">
    <div class="">
        <div class="row">
            <div class="col-lg-12">
                <!-- start card 3 Columns  Form Layout-->
                <div class="card">
                    <!--begin::form-->
                    <form  id="add-form" action = "{{isset($menu)?url('update-menu'):url('store-menu') }}" method = "POST" class="needs-validation" novalidate>
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{isset($menu)?$menu->id:''}}">
                        <div class="card-body">
                            <div class="card-title mb-3">Item Details</div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <button type="button" class="btn btn-success"  data-toggle="modal" data-target="#imageModal" style="{{isset($menu)?'' :'display:none' }};"><i class="nav-icon mr-2 i-File-Text--Image"></i>Upload Image </button>
                                </div>
                                @if(Auth::guard('admin_user')->User()->role_id == 1 )
                                <div class="form-group col-md-4">
                                    <label for="select_restaurant" class="ul-form__label">Search Restaurant:</label>
                                    <input type="text" class="typeahead form-control" autocomplete="off" placeholder="Search Restaurant" id="basic-url" aria-describedby="basic-addon3">
                                </div>
                                @endif

                                <div class="form-group col-md-4">
                                    <label for="select_branch" class="ul-form__label">Select Branch:</label>
                                    <select class="form-control" name="branch" id="branch" required >

                                        @if(isset($branch))
                                            @foreach ($branch as $item)
                                                <option value="{{$item->id}}">{{$item->name}} </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="select_branch" class="ul-form__label">Select Category:</label>
                                    <select class="form-control" name="category" id="category" required >

                                        @if(isset($category))
                                            @foreach ($category as $item)
                                                <option value="{{$item->id}}" {{isset($menu)&& $menu->menu_category_id == $item->id ? "Selected":""}}>{{$item->name}} </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="name" class="ul-form__label">Name:</label>
                                    <input type="text" class="form-control" name="name" value="{{isset($menu)?$menu->name:''}}"  id="name" placeholder="Enter menu name" required>
                                    <div class="invalid-tooltip">
                                        Please enter menu name
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="name" class="ul-form__label">Price:</label>
                                    <input type="number" class="form-control" name="price" value="{{isset($menu)?$menu->price:''}}"  id="price" placeholder="Enter menu price" required>
                                    <div class="invalid-tooltip">
                                        Please enter menu price
                                    </div>
                                </div>

{{--
                                <div class="form-group col-md-4">
                                    <label for="name" class="ul-form__label">Ingredient:</label>
                                    <input type="text" class="form-control" name="ingredient" value="{{isset($menu)?$menu->ingridient:''}}"  id="ingredient" placeholder="Enter menu ingredient">
                                    <div class="invalid-tooltip">
                                        Please enter menu ingredient
                                    </div>
                                </div> --}}

                                <div class="form-group col-md-4">
                                    <label for="name" class="ul-form__label">Description:</label>
                                    <input type="text" class="form-control" name="description" value="{{isset($menu)?$menu->description:''}}"  id="description" placeholder="Enter menu description">
                                    <div class="invalid-tooltip">
                                        Please enter menu description
                                    </div>
                                </div>

                            </div>



                            <div class="form-row">

                                {{-- <label class="switch switch-success mr-3">
                                   <span>Upload Facebook</span>
                                   <input type="checkbox" name="upload_fb" id="upload_fb" {{(isset($menu->upload_fb) && $menu->upload_fb==1)?'checked':''}}>
                                   <span class="slider"></span>
                                 </label> --}}

                                <label class="switch switch-success mr-3">
                                    <span>Ready Made</span>
                                    <input type="checkbox" name="ready_made" id="ready_made" {{(isset($menu->ready_made) && $menu->ready_made==1)?'checked':''}}>
                                    <span class="slider"></span>
                                </label>

                                <label class="switch switch-success mr-3">
                                    <span>Has Kitchen</span>
                                    <input type="checkbox" name="has_kitchen" id="has_kitchen" {{(isset($menu->kitchen_id) && $menu->kitchen_id>0)?'checked':''}}>
                                    <span class="slider"></span>
                                </label>


                            </div>

                            <div class="custom-separator"></div>

                            <div class="form-row">
                                <div id="image-div" class="col-md-4 form-group mb-3" style="{{isset($menu->image)?'' :'display:none' }}">
                                    <label for="Logo" class="ul-form__label mr-3">Image</label>
                                    <input class="form-control" type="hidden" name="image" value="{{ $edit_image }}"  id="image" readonly>
                                    <img id="menu-image" name="menu-image"  src="{{asset($img_url.$edit_image)}}" style="width: 150px;">
                                </div>
                            </div>

                            <div class="custom-separator"></div>
                            <div class="card-title mb-3">Prices & Sizes
                                <button class="btn btn-primary m-1 add-var" type="button" >Add Variation</button>
                            </div>
                            <p>Add variation if an item comes with different size and price.</p>
                            <p>You can sort a variation by drag and drop.</p>
                            <div class="table-responsive" id="vari-div">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Group</th>
                                            <th scope="col">Price</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="connected-sortable droppable-area1">
                                        @if(isset($menu->menuVariations))

                                            @foreach ($menu->menuVariations as $item)

                                                <tr id="{{$item->id}}" class="draggable-item">
                                                    <td>{{$item->sorting}}</td>

                                                    @if(count($item->choiceGroups)>0)

                                                    @php
                                                        $id_str = "";
                                                        $sort_str = "";
                                                        $name_str = "";
                                                        foreach ($item->choiceGroups as $ch_grp)
                                                        {
                                                            $name_str .=$ch_grp->name.",";
                                                            $id_str .=$ch_grp->id.",";
                                                            $sort_str .=$ch_grp->sorting.",";
                                                        }

                                                    @endphp

                                                    <td id="{{$sort_str}}">{{$item->name}}</td>
                                                    <td id="{{$id_str}}">{{$name_str}}</td>
                                                    @else
                                                    <td>{{$item->name}}</td>
                                                    <td>None</td>
                                                    @endif

                                                    <td>{{$item->price}}</td>
                                                    <td>
                                                        <a href="#" class="btn btn-warning edit-var" ><i class="nav-icon i-Pen-2"></i></a>
                                                        <button class="btn btn-danger" ><i class="nav-icon i-Close-Window"></i></button>
                                                    </td>
                                                </tr>
                                            @endforeach

                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="custom-separator"></div>
                            <div class="card-title mb-3">Choice Groups</div>
                            <p>Select Choice Group to display for customers against menu item</p>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col"></th>
                                            <th scope="col"></th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Choices</th>
                                        </tr>
                                    </thead>
                                    <tbody id="menu-choice-group" class="connected-sortable droppable-area1">
                                        @if(isset($menu_choice_group))

                                            @foreach ($menu_choice_group as $key => $item)
                                                <tr id="{{$item->id}}" class="draggable-item">
                                                    <td></td>
                                                    <td>

                                                        <label class="checkbox checkbox-success">
                                                            <input type="checkbox" {{$item->checked}} /><span class="checkmark"></span>
                                                            </label>
                                                    </td>
                                                    <td>{{$item->name}}</td>
                                                    <td>{{$item->choicess}}</td>
                                                </tr>
                                            @endforeach

                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="custom-separator"></div>

                        </div>

                        <div class="card-footer">
                            <div class="mc-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-right">
                                        <button class="btn btn-primary" type="submit" >{{isset($menu)?"Update":"Save"}}</button>
                                        <a class="btn btn-outline-secondary m-1" href={{url('list-menus')}}>Cancel</a>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                    <!-- end::form 3-->

                </div>
                <!-- end card 3-->
            </div>

        </div>
        <!-- end of main row -->
    </div>
</div>

{{-- modal for add variation --}}
<div class="modal fade " id="variation-modal" tabindex="-1" role="dialog" aria-labelledby="variation-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">"
                <h5 class="modal-title" id="variation-title">New Variation</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body" style="max-height: calc(100vh - 210px);overflow-y: auto;">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="col-form-label" for="var-name">Variation Name*:</label>
                        <input class="form-control" id="var-name" type="text" required/>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="col-form-label" for="message-text-2">Price*:</label>
                        <input class="form-control" id="var-price" type="number" required/>
                    </div>
                </div>
                <div class="custom-separator"></div>

                <div class="card-title mb-3">Variation Choice Groups</div>
                <p>Select Choice Group to display for customers against variation</p>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    {{-- <th scope="col"></th> --}}
                                    <th scope="col"></th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Choices</th>
                                </tr>
                            </thead>
                            <tbody id="var-choice-group" class="connected-sortable droppable-area1">
                                @if(isset($choice_groups))

                                    @foreach ($choice_groups as $key => $item)
                                        <tr id="{{$item->id}}" class="draggable-item">
                                            <td style="display:none">{{$key}}</td>
                                            <td>
                                                <label class="checkbox checkbox-success">
                                                    <input type="checkbox" /><span class="checkmark"></span>
                                                    </label>
                                            </td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->choicess}}</td>
                                        </tr>
                                    @endforeach

                                @endif
                            </tbody>
                        </table>
                    </div>


            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" type="button">Save</button>
            </div>
        </div>
    </div>
</div>
        {{-- end modal --}}
@endsection

@section('page-js')
<script src="{{asset('public/assets/js/vendor/dropzone.min.js')}}"></script>
<script type="text/javascript">
Dropzone.autoDiscover = false;
$(document).ready(function(){
    var branchId = $('#branch').val();
    var myDropzone = new Dropzone("#upload_image", {

        createImageThumbnails: true,
        addRemoveLinks: true,
        url: "{{url('saveImage')}}",
        headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
        init: function() {
            this.on("sending", function(file, xhr, formData){
                formData.append( 'branchId', branchId);
            })
        },

    });


    myDropzone.on("success", function(file)
    {
        var uploadedfile_name = "";

        if(file.xhr.response)
            uploadedfile_name = file.xhr.response;
        $('#image-div').show();
        $('#image').val(uploadedfile_name);
        $('#imageModal').modal('toggle');
        var branchID =$('#branch').val();
        document.getElementById('menu-image').src= '{{ asset('public') }}'+'/Temp/'+branchID+'/images/'+uploadedfile_name;

        $('#image-div').focus();
    });

    $(document).on("click", ".open-AddBookDialog", function () {
        var myBookId = $(this).data('id');
        $(".modal-body #bookId").val( myBookId );
        // As pointed out in comments,
        // it is unnecessary to have to manually call the modal.
        // $('#addBookDialog').modal('show');
    });


});

function getImageVal(val) {

    var branchID =$('#branch').val();
    const imageVals = val.src;
    const pieces = imageVals.split(/[\s/]+/);
    const imgVal = pieces[pieces.length - 1];

    var img_url = '{{ asset('public') }}';

    $('#image-div').show();
    $('#image').val(imgVal);
    document.getElementById('menu-image').src= img_url+'/Temp/'+branchID+'/images/'+imgVal;
    $('#imageModal').modal('toggle');
    $('#menu-image').focus();


}
</script>

{{-- drag and drop ui --}}
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
{{-- autocomplete input fiedl --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

@endsection

@section('bottom-js')

<script src="{{asset('public/assets/js/form.validation.script.js')}}"></script>
<script src="{{asset('resources/views/menus/js/menuform.js')}}"></script>

@endsection
