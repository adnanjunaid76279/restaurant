@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('public/assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('public/assets/styles/vendor/toastr.css')}}">
@endsection

@section('main-content')
  <div class="breadcrumb">
                <h1>Menu</h1>
                <ul>
                    <li>List</li>
                    <li>All</li>
                </ul>
              
            <a type="button" class="btn  btn-primary m-1" href="{{url('create-menu')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-Add"></i>Create</a>
                
            </div>
            
            <div class="separator-breadcrumb border-top"></div>
            <!-- end of row -->
            <div class="row mb-4">
               
            
                <div class="col-md-12 mb-4">
                    

                            <div class="table-responsive">
                                <table id="menu_pagination_table" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Deactive</th>
                                            <th>Price</th>
                                            <th>Category</th>
                                            <th>Restaurant</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                      
                                    </tbody>
                                </table>
                            </div>

                        
                   
                </div>
                <!-- end of col -->


            </div>
            <!-- end of row -->



@endsection

@section('page-js')

    <script src="{{asset('public/assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('public/assets/js/datatables.script.js')}}"></script>
    <script src="{{asset('public/assets/js/vendor/toastr.min.js')}}"></script>
    <script src="{{asset('public/assets/js/toastr.script.js')}}"></script>

    <script>
 
 $(document).ready(function(){
    var groupColumn = 3;
         $('#menu_pagination_table').DataTable( {
         "processing": true,
         "serverSide": true,
         "pageLength": 1000,
         "lengthMenu": [[50, 100, 500, 1000,-1], [50, 100, 500, 1000, "All"]],
         "ajax": "{{url('getmenus')}}",
         "columnDefs": [
            { "visible": false, "targets": groupColumn },{width: "30%", targets: [0]},{width: "10%", targets: [1]},{width: "10%", targets: [2]},{width: "20%", targets: [3,4]}
        ],
         "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr style="background-color: grey;color: #fff;"><td colspan="6">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
     } );
 });


 $(document).on("click",".menu_active",function() {
    var is_active = $(this).is(':checked') ? 1: 0;
    var menu_id = $(this).attr('data-menu-id');

    $.ajax({
            url: "{{url('enableMenu')}}",
            type: 'POST',
            data: { is_active ,menu_id},
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            success: function (response) 
            {
                toastr.success(response.msg,"Status", {
                    showMethod: "slideDown",
                    hideMethod: "slideUp",
                    timeOut: 2e3

                });
            }
    });
});


</script>
@endsection
