@extends('layouts.master')

@section('main-content')


@if(\Session::has('success'))
<div class="alert alert-success">
    <p>{{\Session::get('success')}}</p>
</div>
@endif


   <div class="breadcrumb">
                <h1>{{$menu->name}}</h1>
                <ul>
                    <li><a href="">Menu</a></li>
                    <li>View</li>
                </ul>
                <a type="button" class="btn  btn-primary m-1" href="{{url('create-menu')}}" style="position: right;right: 45px;"><i class="nav-icon mr-2 i-Add"></i>Create</a>
            
                <a type="button" class="btn  btn-primary m-1" href="{{url('list-menus')}}" style="position: absolute;right: 45px;"><i class="nav-icon mr-2 i-File-Horizontal-Text"></i>Back to List</a>

            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row">

                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="card-title mb-3">Item Details</div>  
                            <form ">
                                <div class="row">

                                    <div class="col-md-12 form-group mb-3">
                                        <img src="{{asset('/public/Temp/'.$menu->menuCategory->restaurantBranch->id.'/images/'.$menu->image)}}" width="150px" alt="{{$menu->name}}">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="firstName2">Name</label>
                                        <input type="text" class="form-control form-control-rounded" disabled value="{{$menu->name}}" >
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="firstName2">Restaurant Branch</label>
                                        <input type="text" class="form-control form-control-rounded" disabled value="{{$menu->menuCategory->restaurantBranch->name}}" >
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="firstName2">Price</label>
                                        <input type="text" class="form-control form-control-rounded" disabled value="{{$menu->price}}" >
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="firstName2">Description</label>
                                        <input type="text" class="form-control form-control-rounded" disabled value="{{$menu->description}}" >
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-6 form-group mb-3">
                                        <label class="switch switch-success mr-3">
                                            <span>Ready Made</span>
                                            <input type="checkbox" name="ready_made" disabled id="ready_made" {{(isset($menu->ready_made) && $menu->ready_made==1)?'checked':''}}>
                                            <span class="slider"></span>
                                        </label>

                                        <label class="switch switch-success mr-3">
                                            <span>Has Kitchen</span>
                                            <input type="checkbox" name="has_kitchen" disabled id="has_kitchen" {{(isset($menu->kitchen_id) && $menu->kitchen_id>0)?'checked':''}}>
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="custom-separator"></div>
                                <div class="card-title mb-3">Prices & Sizes</div>
                                <div class="table-responsive" id="vari-div">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr><th scope="col">Sr#</th>
                                                <th scope="col">Variation Name</th>
                                                <th scope="col">Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($menu->menuVariations))
                                            
                                                @foreach ($menu->menuVariations as $item)
                                                    <tr id="{{$item->id}}">
                                                        <td>{{$item->sorting}}</td>
                                                        <td>{{$item->name}}</td>
                                                        <td>{{$item->price}}</td>
                                                    </tr>
                                                @endforeach
                                            
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-12 text-right">
                                        <a type="button" class="btn btn-success" href="{{url('edit-menu/'.$menu->id)}}">
                                            <i class="nav-icon mr-2 i-Pen-2"></i>
                                            Edit
                                        </a>

                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>


@endsection
