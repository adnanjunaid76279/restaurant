
// drag and drop sortable
$( init );
function init() {
    $( ".droppable-area1" ).sortable({
        connectWith: ".connected-sortable",
        stack: '.connected-sortable tbody',
        stop: function() {
            $.map($(this).find('tr'), function(el) {
            //var id = el.id;
            var sorting = $(el).index();
            $(el).find('td:eq(0)').html(sorting+1);

            });  
          }
      }).disableSelection();
}

// modal pop by create button  of variation
$('.add-var').click(function(event){
    event.preventDefault();
    $('#variation-modal').modal('toggle');
    $('#var-name').val('');
    $('#var-price').val('');
    
});
var edit_var_ch_grp_tr = "";
// modal pop by edit  button  against variation
$('.edit-var').click(function(event){
    event.preventDefault();
    
    var tr = $(this).parent().parent();
    edit_var_ch_grp_tr = tr;
    $('#variation-title').html("Update Variation");
    $('#var-name').val(tr.find('td:eq(1)').text());
    $('#var-price').val(tr.find('td:eq(3)').text());
    // $('#var-name').prop('readonly', true);
    // $('#var-price').prop('readonly', true);

    var pre_ch_grp_ids = tr.find('td:eq(2)').attr('id');

    pre_ch_grp_ids = pre_ch_grp_ids.split(",");

    // selection for choice groups against variations
    var ii = 1;
    $('#var-choice-group > tr').each(function(index, tr) { 

        
        if(pre_ch_grp_ids.includes($(this).attr('id') ))
        {
            var is_choice_chk = $(this).find('.checkmark').css("background-color")=="rgb(76, 175, 80)";
            
            if(!is_choice_chk)
                $(this).find('.checkbox-success').trigger('click');
            
            $(this).find('checkbox').prop('checked', true);
            $(this).find('td:eq(0)').text(ii);
            console.log('trigger');
            ii += 1;
        }
        else
            // $(this).find('td:eq(0)').text(1000);
        
        ii += 1;

    });
    $('#variation-modal').modal('toggle');
    sortVarChocieGrpTable();
  
});

// modal save button for variation with choice groups
$('#variation-modal .btn-primary').click(function(event){
    event.preventDefault();
    var tr_id = "0";
    if(($('#variation-title').html()).trim() == "Update Variation")
    {   tr_id = edit_var_ch_grp_tr.attr('id');
        edit_var_ch_grp_tr.remove();
        edit_var_ch_grp_tr = "";
    }


    var var_name;
    var var_price;

    var_name = $('#var-name').val();
    var_price =  Number($('#var-price').val()).toFixed(2);
    if(var_name == "" || var_price == "")
    {
        alert("variation name or price fields are empty");
    }

    var choice_grp_name = "";
    var choice_grp_id = "";
    var choice_grp_sorting = "";
    // selection for choice groups against variations
    $('#var-choice-group > tr').each(function(index, tr) { 

        var is_choice_chk = $(this).find('.checkmark').css("background-color")=="rgb(76, 175, 80)";
        if(is_choice_chk)
        {
            choice_grp_sorting += $(this).find('td:eq(0)').text();
            choice_grp_name += $(this).find('td:eq(2)').text();
            choice_grp_id += $(this).attr('id');
            choice_grp_name += ",";
            choice_grp_id += ",";
            choice_grp_sorting += ",";
        }
    });

    if(choice_grp_name == "")
        choice_grp_name = "None";
    
    var sortt = $('#vari-div tbody tr:last-child').index();
    sortt = sortt ==-1 ?0:sortt+1;
    var row = '<tr id="'+tr_id+'">';
    row += '<td>'+Number(sortt+1)+'</td>';
    
    row += '<td id="'+choice_grp_sorting+'">' + var_name + '</td><td id="'+choice_grp_id+'">' + choice_grp_name + '</td>';
    row += '<td>' + var_price + '</td>';
    row += '<td><a href="#" class="btn btn-warning edit-var" ><i class="nav-icon i-Pen-2"></i></a><button class="btn btn-danger" ><i class="nav-icon i-Close-Window"></i></button></td>';
    row += '</tr>';
    $('#vari-div tbody').append(row);
    $('#variation-modal').modal('toggle');


});

// sort a choice group table against variatoin
function sortVarChocieGrpTable() {
    var table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById("var-choice-group");
    switching = true;
    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
      //start by saying: no switching is done:
      switching = false;
      rows = table.rows;
      /*Loop through all table rows (except the
      first, which contains table headers):*/
      for (i = 0; i < (rows.length - 1); i++) {
        //start by saying there should be no switching:
        shouldSwitch = false;
        /*Get the two elements you want to compare,
        one from current row and one from the next:*/
        x = rows[i].getElementsByTagName("TD")[0];
        y = rows[i + 1].getElementsByTagName("TD")[0];
        //check if the two rows should switch place:
        if (Number(x.innerHTML) > Number(y.innerHTML)) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
      if (shouldSwitch) {
        /*If a switch has been marked, make the switch
        and mark that a switch has been done:*/
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
      }
    }
  }

//form submit button
$("form").submit(function (event) {
    event.preventDefault();
    var formaction = $('#add-form').attr('action');
    var formdata = $('#add-form').serializeArray();
    formdata = setForm(formdata);

    var tableobj = [];
    
    $('#vari-div table > tbody  > tr').each(function(index, tr) { 
        
        var varid = $(this).attr('id');
        var sort_id = $(this).find('td:eq(0)').text();
        var var_name = $(this).find('td:eq(1)').text();
        var var_price = $(this).find('td:eq(3)').text();
        var choice_grp_ids = $(this).find('td:eq(2)').attr('id');
        var choice_grp_sortingg = $(this).find('td:eq(1)').attr('id');
        var choice_grp_arr = [];
        if(typeof choice_grp_ids != "undefined" && choice_grp_ids != "")
        {
            choice_grp_ids = choice_grp_ids.split(",");
            choice_grp_sortingg = choice_grp_sortingg.split(",");
            choice_grp_ids.forEach(function(val,index){
                if(val != "")
               {
                choice_grp_arr.push({"id": val,"sorting": choice_grp_sortingg[index]});
               }
            });
        }
        
        
        tableobj.push({
            "varid":    varid,
            "sorting":  sort_id,
            "variname":  var_name,
            "price": var_price,
            "choice_grp_ids":choice_grp_arr
        });
    });

    formdata.variation = tableobj;
    console.log(formdata);
    
    // filling of choice group ids against menu
    //var choice_grp_id = "";
    var choice_grp_id = [];
    
    $('#menu-choice-group > tr').each(function(index, tr) { 

        var is_choice_chk = $(this).find('.checkmark').css("background-color")=="rgb(76, 175, 80)";
        if(is_choice_chk)
        {
            choice_grp_id.push({"id": $(this).attr('id'),"sorting": index});
            
        }
    });
    
    formdata.menu_ch_grp_ids = choice_grp_id;
    
    $.ajax({
      type: "POST",
      url: formaction,
      data: formdata,
      dataType: "json",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    }).done(function (data) {
        var success_url = "";
        var location_path = formaction;//window.location.pathname;
        
        // success_url = window.location.origin;
        
    
        success_url += location_path.substr(0,location_path.lastIndexOf("/"));

        success_url +=  "/view-menu/" + data;
        
        window.location.href = success_url;
    });


    
  });

  $(document).on("click", ".btn-danger", function (event) {

    event.preventDefault();
    $(this).parent().parent().remove();
});


  function setForm(dd) {
    let data = {};
    for (let i = 0; i < dd.length; i++) {
      let k = dd[i].name
      if (dd[i].name) {
        data[k] = dd[i].value
      }
    }
    return data
  }

$('input.typeahead').typeahead({
    minLength: 3,
    source:function (query, process) {
        return $.ajax({
                    url: "{{url('searchRestaurantsByName')}}",
                    type: 'get',
                    data: { name: query },
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    success: function (result) 
                    {
                        var results = result.Data;
                        if(!results)
                            return false;
                        var resultList = results.map(function (item) {
                            var aItem = { id: item.id, name: item.name };
                            return aItem;
                        });

                        return process(resultList);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                    }
                });
    }

 });

 $(document).on('click','ul.typeahead li',function() {
     
    var data = $('ul.typeahead li.active').data().value;

    if(data != null && data != '' && data != undefined )
    {
        var tempItemId = data.id;
        var tempItemName = data.name;
           
        $.ajax({
            url: "{{url('getBranchesByRestaurantId')}}",
            type: 'get',
            data: { id: tempItemId },
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            success: function (result) 
            {
                var results = result.Data;
                if(results.length == 0)
                    return false;

                var branches = '';
                
                $.each(results,function(index,obj){
                    
                    branches += "<option value='"+obj.id+"'>"+obj.name+"</option>";
                    

                });

                $('#branch').html(branches);
                getCatByBranchId();
              

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });

    }    

});

$(".typeahead").on('keyup', function(e){

    if(e.which == 13) 
    {
 
      $('ul.typeahead li.active').trigger('click'); // for treiggering of click evetn of typhead for selec an item

    }
    
});

$('#price').on("keypress keyup blur",function (event) {
          
    $(this).val($(this).val().replace(/[^0-9\.]/g,''));
    if ((event.which != 46 || $('#price').val().indexOf('.') != -1) && ((event.which  < 48)  || event.which > 57)) {
    
        event.preventDefault();
    }
});


$(document).on('change','#branch',function() {
    getCatByBranchId();
});

function getCatByBranchId()
{
    $.ajax({
        url: '{{url("getCategoriesByBranchId")}}',
        type: 'get',
        data: { id: $('#branch').val() },
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        success: function (result) 
        {
            var results = result.Data;
            if(results.length == 0)
                return false;

            var categories = '';
            
            $.each(results,function(index,obj){
                
                categories += "<option value='"+obj.id+"'>"+obj.name+"</option>";
                

            });

            $('#category').html(categories);

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
}


