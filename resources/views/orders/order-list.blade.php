@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('public/assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('public/assets/styles/vendor/toastr.css')}}">
@endsection
@section('main-content')

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Order Details</h4>
         <button style="float:right;" data-dismiss="modal" class="btn btn-danger" ><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="container">
            <div class="row">
          
            </div>
            <table id="order_details" class="table">
                <thead>
                 </thead>
                   <tbody >
                  </tbody>
                <tfoot>
                </tfoot>           
            </table>
           
            
        </div>
     </div>
     
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<div class="breadcrumb">
<h1>Orders</h1>
<ul>
<li>List</li>
<li>All</li>
</ul>
</div>

<div class="separator-breadcrumb border-top"></div>
<div class="row mb-4">

<div class="col-md-12 mb-4">

<div class="row ml-1" >
<div class="col-sm">
<label for="">From</label>  <input class="form-control"  value="<?php echo date('Y-m-d'); ?>" type="date"  id="start_date">
</div>
<div class="col-sm">
<label for="">To </label><input  class="form-control"  value="<?php echo date('Y-m-d'); ?>" type="date"  id="end_date">
</div>
<div class="col-sm">
<label for="">Select Order Status </label>
<select class="form-control" id="order_status_id" aria-label="Default select example">
<option value="0" selected>All</option>
@foreach($order_status as $item)
<option value="{{$item->id}}">{{$item->name}}</option>
@endforeach`

</select>
</div>
<div class="col-sm">
<button id="search_button" class="mt-4 btn btn-danger">Search</button>
</div>
</div>
<div class="table-responsive mt-3">

<table id="orders_table" class="table table-striped table-dark" style="width:100%">
<thead>
<tr>
<th>Customer</th>
<th>Order No.</th>
<th>Amount</th>
<th>Date</th>
<th>Branch</th>
<th>Status</th>
<th>Change Status</th>
<th></th>
</tr>
</thead>

<tbody>

</tbody>
<tfoot>
<tr>


<th  style="text-align:left">Total:</th>
<th colspan="2" style="text-align:right">Total:</th>
</tr>
</tfoot>
</table>
</div>



</div>


</div>



@endsection

@section('page-js')

<script src="{{asset('public/assets/js/vendor/datatables.min.js')}}"></script>
<script src="{{asset('public/assets/js/datatables.script.js')}}"></script>
<script src="{{asset('public/assets/js/vendor/toastr.min.js')}}"></script>
<script src="{{asset('public/assets/js/toastr.script.js')}}"></script>
<script>

$(document).ready(function(){
    getData();
});

function getData() 
{   
    $order_status=$('#order_status_id').val();
    $start= $('#start_date').val();;
    $end=$('#end_date').val();
    table=  $('#orders_table').DataTable({
        "pageLength": 100,
        "processing": true,
        "serverSide": true,
        "destroy":true,
        "ajax": {
            "url": "get-orders",
            "data": {
                "start_date": $start,
                "end_date":$end,
                "order_status":$order_status,
            },
        },
        
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
            
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                i : 0;
            };
            
            // Total over all pages
            total = api
            .column( 2 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );
            total = parseFloat(total).toFixed(2);
            // Total over this page
            pageTotal = api
            .column( 2, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );
            
            // Update footer
            $( api.column( 2 ).footer() ).html(
                '  '+ total
            );
        }
        
    });
}
$('#search_button').click(function(){
    getData();
    
});
$( "#orders_table tbody" ).on( "click", ".order_detail", function(event) {
  var order_id= $( this ).val();
   var formData=new FormData();
   formData.append('order_id',order_id) 

    $.ajax({
  type: "post",
  url: "{{route('getOrderDetailsByOrderId')}}",
  headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
  },

  data: formData,
  dataType: 'json',
  contentType: false,
  cache: false,
  processData: false,
  success: function (response) 
  {
    $('#order_details thead ').empty();
    $('#order_details tbody ').empty();
    $('#order_details tfoot ').empty();

    var status_time_div = document.getElementById("exampleModal").getElementsByClassName("row")[0];

    var timeStatus = "";
    timeStatus += "<div class='col-md-2'><p>Recieved<br>" + convertToLocaDate(response.recieved_time) + "<br>"+ convertToLocaTime(response.recieved_time) + "</p></div>";
    timeStatus += "<div class='col-md-2'><p>Preparing<br>" + convertToLocaDate(response.preparing_time) + "<br>"+ convertToLocaTime(response.preparing_time) + "</p></div>";
    timeStatus += "<div class='col-md-2'><p>Ready<br>" + convertToLocaDate(response.ready_time) + "<br>"+ convertToLocaTime(response.ready_time) + "</p></div>";
    timeStatus += "<div class='col-md-2'><p>On Delivery<br>" + convertToLocaDate(response.dispatch_time) + "<br>"+ convertToLocaTime(response.dispatch_time) + "</p></div>";
    timeStatus += "<div class='col-md-2'><p>Delivered<br>" + convertToLocaDate(response.delivered_time) + "<br>"+ convertToLocaTime(response.delivered_time) + "</p></div>";
    
    status_time_div.innerHTML  = timeStatus;
    
    var order_tax_amount = response.tax_anount;
    var amount_paid = response.amount_paid;
    
    var amount_returned = response.amount_return;
    var discount = response.discount_amount;
    var delivery_charge = response.delivery_charge;
    var total_items = 0;
    var sub_total_price = 0;
    var order_details_thead = $('#order_details thead');
    var order_details_thead_append =`<tr><td style="font-weight:bold;">Customer</td><td>`+ 
    response.first_name+" "+ response.last_name 
    + `</td><td style="font-weight:bold;">Address</td><td>`
    + response.address + `</td></tr><tr><td style="font-weight:bold;"> Resource</td><td>`
    + response.resource + `</td><td style="font-weight:bold;">Order Type</td><td>`
    + response.order_type + `</td></tr><tr><td style="font-weight:bold;">Status</td><td>`
    + response.order_status + `</td><td style="font-weight:bold;">Order No</td><td>`
    + response.daily_order + `</td></tr><tr><td style="font-weight:bold;">Menu Item</td><td style="font-weight:bold;">Quantity</td><td style="font-weight:bold;">Price</td><td style="font-weight:bold;">Sub Total</td></tr>`
    
    order_details_thead.append(order_details_thead_append);
    
    var order_details_tbody = $("#order_details tbody");
      
      $.map(response.menu_details, function(elem)
      {
        var price = elem.price;
        var quantity = elem.quantity;
        var subtotal = (price*quantity).toFixed(2);
        order_details_tbody.append("<tr><td>"+elem.name+"</td><td>"+elem.quantity+"</td>   <td>"+elem.price+"</td><td class='sub_total'>"+subtotal+"</td >  </tr>  ");
      
      });
     
       $('#order_details .sub_total').each(function()
       {
        sub_total_price += parseFloat($(this).text());
        total_items++;
       });

       $("#total_items").append(total_items);
       $("#sub_total").append(sub_total_price.toFixed(2));
       
       var sub_total_price_value=sub_total_price.toFixed(2);
       var discount_value=discount;
       var discount_price=parseFloat(sub_total_price_value)-parseFloat(discount_value);
       var grand_total=parseFloat(discount_price)+parseFloat(delivery_charge)+parseFloat(order_tax_amount);
       $('#discount_price').append(discount_price.toFixed(2));
       var order_details_tfoot=$('#order_details tfoot')
       var order_details_tfoot_append=`<tr><th>Total Items</th><th>`+total_items+`</th><th>Sub Total</th><th>`+sub_total_price+`</th></tr> <tr><th><label for="discount">Discount</label></th><th>`+discount+`</th><th><label for="discount_price">Discount Price</label></th><th>`+discount_price+`</th></tr><tr><th>Tax Percentage</th><th>`+response.tax_percentage+'%'+`</th><th>Tax Amount</th><th>`+order_tax_amount+`</th></tr><tr><th><label for="delivery_charge">Delivery Charge</label></th><th>`+delivery_charge+`</th><th><label for="grand_total">Grand Total</label></th><th>`+grand_total.toFixed(2)+`</th></tr><tr><th><label for="amount_paid">Amount Paid</label></th><th>`+amount_paid+`</th><th><label for="amount_return">Change</label></th><th>`+amount_returned+`</th></tr>`;
        $('#order_details tfoot').append(order_details_tfoot_append);
         
      }})

      
       
});

  $( "#orders_table tbody" ).on( "change", ".order_status_list", function(event) {
      event.preventDefault();
      var formData = new FormData();
    
      var change_status_id= $( this ).val();
      var array=change_status_id.split(',')
      formData.append('orderID',array[1]);
      formData.append('statusId',array[0]);
      $.ajax({
      type: "post",
      url: "{{route('changeOrderstatus')}}",
      headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
      },
      data: formData,
      dataType: 'json',
      contentType: false,
      cache: false,
      processData: false,
      success: function (response) {
        toastr.success("updated successfully","Status", {
                    showMethod: "slideDown",
                    hideMethod: "slideUp",
                    timeOut: 2e3

                });
      $('#search_button').click();

      }
    })

});

function convertToLocaTime(server_date)
{
  if(server_date == null || server_date == "")
    return "";
  var date = new Date(server_date); 

  var hour = date.getHours();
  var minute = date.getMinutes();
  
  if( minute < 10)
    minute = "0"+minute;    
  
  return hour + ":" + minute;   
}

function convertToLocaDate(server_date)
{
  if(server_date == null || server_date == "")
    return "";
  var date = new Date(server_date); 
  var year = date.getFullYear();
  var month = date.getMonth()+1;
  var day = date.getDate();

  return day + "-" + month + "-" + year;
}


</script>
@endsection
