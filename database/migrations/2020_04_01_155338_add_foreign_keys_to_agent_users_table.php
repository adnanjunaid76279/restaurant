<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAgentUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('agent_users', function(Blueprint $table)
		{
			$table->foreign('agent_role_id', 'FK_agent_users_agent_roles')->references('id')->on('agent_roles')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('agent_users', function(Blueprint $table)
		{
			$table->dropForeign('FK_agent_users_agent_roles');
		});
	}

}
