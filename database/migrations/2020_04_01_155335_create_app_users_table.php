<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAppUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('app_users', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->integer('app_id')->nullable()->index('FK_app_users_app');
			$table->integer('user_id')->nullable()->index('FK_app_users_users');
			$table->string('fb_id', 200)->nullable();
			$table->text('device_id')->nullable();
			$table->dateTime('date_created')->nullable();
			$table->dateTime('date_updated')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('app_users');
	}

}
