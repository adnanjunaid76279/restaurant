<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddOnTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('add_on', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->text('name')->nullable();
			$table->bigInteger('price')->nullable();
			$table->text('description')->nullable();
			$table->integer('menu_id');
			$table->dateTime('date_created')->nullable();
			$table->dateTime('date_updated')->nullable();
			$table->integer('updatedby_id')->nullable();
			$table->integer('createdby_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('add_on');
	}

}
