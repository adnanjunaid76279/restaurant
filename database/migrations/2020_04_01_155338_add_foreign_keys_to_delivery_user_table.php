<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDeliveryUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('delivery_user', function(Blueprint $table)
		{
			$table->foreign('app_id', 'FK_delivery_user_app')->references('id')->on('app')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('delivery_user', function(Blueprint $table)
		{
			$table->dropForeign('FK_delivery_user_app');
		});
	}

}
