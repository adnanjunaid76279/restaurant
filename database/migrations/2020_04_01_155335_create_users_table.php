<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->text('login')->nullable();
			$table->text('password')->nullable();
			$table->integer('role_id')->nullable()->index('FK_users_roles');
			$table->text('first_name')->nullable();
			$table->text('last_name')->nullable();
			$table->text('phone_num')->nullable();
			$table->text('cell_num')->nullable();
			$table->text('fb_id')->nullable();
			$table->text('access_token')->nullable();
			$table->integer('expiry')->nullable();
			$table->dateTime('date_created')->nullable();
			$table->dateTime('date_updated')->nullable();
			$table->integer('updatedby_id')->nullable();
			$table->integer('createdby_id')->nullable();
			$table->string('gender', 50)->nullable();
			$table->dateTime('date_birth')->nullable();
			$table->string('email', 50)->nullable();
			$table->boolean('pos_user')->nullable();
			$table->integer('branchId')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
