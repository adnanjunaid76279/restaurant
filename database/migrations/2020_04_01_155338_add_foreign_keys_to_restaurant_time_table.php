<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRestaurantTimeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('restaurant_time', function(Blueprint $table)
		{
			$table->foreign('restaurant_branch_id', 'FK_restaurant_time_restaurant_branch')->references('id')->on('restaurant_branch')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('restaurant_time', function(Blueprint $table)
		{
			$table->dropForeign('FK_restaurant_time_restaurant_branch');
		});
	}

}
