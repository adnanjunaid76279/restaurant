<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBranchRatingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('branch_rating', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->integer('user_id')->nullable();
			$table->integer('restaurant_branch_id')->nullable();
			$table->integer('rating')->nullable();
			$table->dateTime('date_created')->nullable();
			$table->dateTime('date_updated')->nullable();
			$table->integer('updatedby_id')->nullable();
			$table->integer('createdby_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('branch_rating');
	}

}
