<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTownBlockTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('town_block', function(Blueprint $table)
		{
			$table->foreign('town_id', 'FK_town_block_town')->references('id')->on('town')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('town_block', function(Blueprint $table)
		{
			$table->dropForeign('FK_town_block_town');
		});
	}

}
