<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRestaurantBranchTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('restaurant_branch', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->text('name')->nullable();
			$table->text('address')->nullable();
			$table->integer('town_id')->nullable()->index('FK_restaurant_branch_town');
			$table->integer('is_takeaway')->nullable();
			$table->integer('is_delivery')->nullable();
			$table->integer('is_dinein')->nullable();
			$table->integer('is_freedelivery')->nullable();
			$table->bigInteger('likes')->nullable();
			$table->text('feedback')->nullable();
			$table->integer('restaurant_id')->nullable()->index('FK_restaurant_branch_restaurant');
			$table->text('phone_number')->nullable();
			$table->text('phone_number2')->nullable();
			$table->text('phone_number3')->nullable();
			$table->text('mobile_number')->nullable();
			$table->text('mobile_number2')->nullable();
			$table->text('mobile_number3')->nullable();
			$table->text('contact_person')->nullable();
			$table->text('contact_email')->nullable();
			$table->text('contact_number')->nullable();
			$table->text('logo')->nullable();
			$table->text('longitude')->nullable();
			$table->text('latitude')->nullable();
			$table->text('fb_post_id')->nullable();
			$table->text('description')->nullable();
			$table->boolean('have_category')->nullable();
			$table->boolean('order_taking')->nullable();
			$table->bigInteger('minimum_order_value')->nullable();
			$table->bigInteger('delivery_fee')->nullable();
			$table->dateTime('close_time')->nullable();
			$table->dateTime('open_time')->nullable();
			$table->dateTime('date_created')->nullable();
			$table->dateTime('date_updated')->nullable();
			$table->integer('updatedby_id')->nullable();
			$table->integer('createdby_id')->nullable();
			$table->string('currency', 10)->nullable();
			$table->integer('total_seats')->nullable();
			$table->integer('dailyorder_id')->nullable();
			$table->string('ntn_number', 50)->nullable();
			$table->bigInteger('tax_percent')->nullable();
			$table->boolean('tax_include')->nullable();
			$table->bigInteger('order_discount_percent')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('restaurant_branch');
	}

}
