<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessTokenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('access_token', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->text('access_token')->nullable();
			$table->dateTime('expiry_date')->nullable();
			$table->integer('restaurant_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('access_token');
	}

}
