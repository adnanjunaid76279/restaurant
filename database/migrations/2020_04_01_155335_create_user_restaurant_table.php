<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserRestaurantTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_restaurant', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->integer('user_id')->index('FK_user_branch_users');
			$table->integer('branch_id')->nullable();
			$table->integer('restaurant_id')->nullable()->index('FK_user_restaurant_restaurant');
			$table->dateTime('date_created')->nullable();
			$table->dateTime('date_updated')->nullable();
			$table->integer('updatedby_id')->nullable();
			$table->integer('createdby_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_restaurant');
	}

}
