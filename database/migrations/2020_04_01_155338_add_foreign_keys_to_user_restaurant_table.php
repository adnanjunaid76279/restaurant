<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserRestaurantTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_restaurant', function(Blueprint $table)
		{
			$table->foreign('user_id', 'FK_user_branch_users')->references('id')->on('admin_users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('restaurant_id', 'FK_user_restaurant_restaurant')->references('id')->on('restaurant')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_restaurant', function(Blueprint $table)
		{
			$table->dropForeign('FK_user_branch_users');
			$table->dropForeign('FK_user_restaurant_restaurant');
		});
	}

}
