<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMenuTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('menu', function(Blueprint $table)
		{
			$table->foreign('cuisine_id', 'FK_menu_cuisine')->references('id')->on('cuisine')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('menu_category_id', 'FK_menu_menu_category')->references('id')->on('menu_category')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('menu', function(Blueprint $table)
		{
			$table->dropForeign('FK_menu_cuisine');
			$table->dropForeign('FK_menu_menu_category');
		});
	}

}
