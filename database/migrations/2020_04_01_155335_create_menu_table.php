<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMenuTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menu', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->integer('menu_category_id')->nullable()->index('FK_menu_menu_category');
			$table->integer('cuisine_id')->nullable()->index('FK_menu_cuisine');
			$table->text('name')->nullable();
			$table->text('image')->nullable();
			$table->text('ingridient')->nullable();
			$table->text('description')->nullable();
			$table->bigInteger('likes')->nullable();
			$table->integer('rating')->nullable();
			$table->text('reviews')->nullable();
			$table->text('fb_post_id')->nullable();
			$table->decimal('price', 18)->nullable();
			$table->boolean('have_addone')->nullable();
			$table->dateTime('date_created')->nullable();
			$table->dateTime('date_updated')->nullable();
			$table->integer('updatedby_id')->nullable();
			$table->integer('createdby_id')->nullable();
			$table->boolean('add_image')->nullable();
			$table->boolean('is_deleted')->nullable();
			$table->boolean('is_deal')->nullable();
			$table->integer('kitchen_id')->nullable();
			$table->boolean('ready_made')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('menu');
	}

}
