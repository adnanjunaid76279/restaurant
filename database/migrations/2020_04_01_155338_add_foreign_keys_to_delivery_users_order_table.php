<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDeliveryUsersOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('delivery_users_order', function(Blueprint $table)
		{
			$table->foreign('delivery_user_id', 'FK_delivery_users_order_delivery_user')->references('id')->on('delivery_user')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('delivery_user_order_status_id', 'FK_delivery_users_order_delivery_user_order_status')->references('id')->on('delivery_user_order_status')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('order_id', 'FK_delivery_users_order_order')->references('id')->on('order')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('delivery_users_order', function(Blueprint $table)
		{
			$table->dropForeign('FK_delivery_users_order_delivery_user');
			$table->dropForeign('FK_delivery_users_order_delivery_user_order_status');
			$table->dropForeign('FK_delivery_users_order_order');
		});
	}

}
