<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAddressTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('address', function(Blueprint $table)
		{
			$table->foreign('address_type_id', 'FK_address_address_type')->references('id')->on('address_type')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('town_id', 'FK_address_town')->references('id')->on('town')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('user_id', 'FK_address_users')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('address', function(Blueprint $table)
		{
			$table->dropForeign('FK_address_address_type');
			$table->dropForeign('FK_address_town');
			$table->dropForeign('FK_address_users');
		});
	}

}
