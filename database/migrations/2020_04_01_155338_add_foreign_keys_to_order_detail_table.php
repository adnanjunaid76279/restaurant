<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOrderDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('order_detail', function(Blueprint $table)
		{
			$table->foreign('order_id', 'FK_order_detail_order')->references('id')->on('order')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('order_detail_status_id', 'FK_order_detail_order_detail_status')->references('id')->on('order_detail_status')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('order_detail', function(Blueprint $table)
		{
			$table->dropForeign('FK_order_detail_order');
			$table->dropForeign('FK_order_detail_order_detail_status');
		});
	}

}
