<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRestaurantTimeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('restaurant_time', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->integer('day_id')->nullable();
			$table->dateTime('open_time')->nullable();
			$table->dateTime('close_time')->nullable();
			$table->integer('restaurant_branch_id')->nullable()->index('FK_restaurant_time_restaurant_branch');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('restaurant_time');
	}

}
