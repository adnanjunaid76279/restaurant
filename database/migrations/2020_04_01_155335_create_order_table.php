<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->integer('user_id')->index('FK_order_users');
			$table->integer('restaurant_branch_id')->nullable();
			$table->text('tracking_id')->nullable();
			$table->dateTime('order_date')->nullable();
			$table->integer('status_id')->nullable()->index('FK_order_order_status');
			$table->dateTime('deliver_date')->nullable();
			$table->bigInteger('total')->nullable();
			$table->bigInteger('sub_total')->nullable();
			$table->integer('order_resource')->nullable()->comment('1=website,2=facebookapp,3=mobileapp');
			$table->dateTime('date_created')->nullable();
			$table->dateTime('date_updated')->nullable();
			$table->integer('updatedby_id')->nullable();
			$table->integer('createdby_id')->nullable();
			$table->integer('dailyorder_id')->nullable();
			$table->integer('useragent_id')->nullable();
			$table->integer('order_type_id')->nullable();
			$table->bigInteger('amount_paid')->nullable();
			$table->bigInteger('amount_return')->nullable();
			$table->bigInteger('discount_amount')->nullable();
			$table->decimal('discount_per', 18)->nullable();
			$table->boolean('order_edit')->nullable();
			$table->bigInteger('tax_percent')->nullable();
			$table->bigInteger('tax_amount')->nullable();
			$table->string('table_no', 50)->nullable();
			$table->integer('num_persons')->nullable();
			$table->boolean('tax_include')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order');
	}

}
