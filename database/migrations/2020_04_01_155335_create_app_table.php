<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAppTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('app', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->string('app_name', 200)->nullable();
			$table->integer('branch_id')->nullable()->index('FK_app_restaurant_branch');
			$table->text('server_key')->nullable();
			$table->string('sender_id', 50)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('app');
	}

}
