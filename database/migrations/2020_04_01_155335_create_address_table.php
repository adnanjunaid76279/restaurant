<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('address', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->integer('user_id')->nullable()->index('FK_address_users');
			$table->text('address1')->nullable();
			$table->text('address2')->nullable();
			$table->integer('address_type_id')->nullable()->index('FK_address_address_type');
			$table->integer('preffered_add')->nullable();
			$table->integer('town_id')->nullable()->index('FK_address_town');
			$table->string('longitude', 50)->nullable();
			$table->string('latitude', 50)->nullable();
			$table->integer('town_block_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('address');
	}

}
