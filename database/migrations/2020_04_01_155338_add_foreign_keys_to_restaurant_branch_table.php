<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRestaurantBranchTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('restaurant_branch', function(Blueprint $table)
		{
			$table->foreign('restaurant_id', 'FK_restaurant_branch_restaurant')->references('id')->on('restaurant')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('town_id', 'FK_restaurant_branch_town')->references('id')->on('town')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('restaurant_branch', function(Blueprint $table)
		{
			$table->dropForeign('FK_restaurant_branch_restaurant');
			$table->dropForeign('FK_restaurant_branch_town');
		});
	}

}
