<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTownTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('town', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->text('town_name')->nullable();
			$table->integer('city_id')->nullable()->index('FK_town_city');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('town');
	}

}
