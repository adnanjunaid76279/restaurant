<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCuisineTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cuisine', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->integer('restaurant_branch_id')->nullable()->index('FK_cuisine_restaurant_branch');
			$table->text('name')->nullable();
			$table->text('image')->nullable();
			$table->boolean('have_menu')->nullable();
			$table->dateTime('date_created')->nullable();
			$table->dateTime('date_updated')->nullable();
			$table->integer('updatedby_id')->nullable();
			$table->integer('createdby_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cuisine');
	}

}
