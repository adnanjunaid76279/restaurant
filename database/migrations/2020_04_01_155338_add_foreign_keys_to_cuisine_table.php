<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCuisineTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cuisine', function(Blueprint $table)
		{
			$table->foreign('restaurant_branch_id', 'FK_cuisine_restaurant_branch')->references('id')->on('restaurant_branch')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cuisine', function(Blueprint $table)
		{
			$table->dropForeign('FK_cuisine_restaurant_branch');
		});
	}

}
