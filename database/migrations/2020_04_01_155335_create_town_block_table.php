<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTownBlockTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('town_block', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->string('block_name', 100);
			$table->integer('town_id')->index('FK_town_block_town');
			$table->string('longitude', 50)->nullable();
			$table->string('latitude', 50)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('town_block');
	}

}
