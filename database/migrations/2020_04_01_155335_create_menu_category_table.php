<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMenuCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menu_category', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->integer('restaurant_branch_id')->nullable()->index('FK_menu_category_restaurant_branch');
			$table->text('name')->nullable();
			$table->text('image')->nullable();
			$table->bigInteger('likes')->nullable();
			$table->boolean('have_menu')->nullable();
			$table->dateTime('date_created')->nullable();
			$table->dateTime('date_updated')->nullable();
			$table->integer('updatedby_id')->nullable();
			$table->integer('createdby_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('menu_category');
	}

}
