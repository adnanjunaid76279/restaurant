<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeliveryDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('delivery_details', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->integer('user_id');
			$table->integer('town_id')->nullable();
			$table->text('address')->nullable();
			$table->text('mobile_num')->nullable();
			$table->text('phone_num')->nullable();
			$table->text('others')->nullable();
			$table->text('email')->nullable();
			$table->integer('order_id')->nullable();
			$table->dateTime('date_birth')->nullable();
			$table->string('gender', 50)->nullable();
			$table->text('instructions')->nullable();
			$table->string('latitude', 50)->nullable();
			$table->string('longitude', 50)->nullable();
			$table->integer('town_block_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('delivery_details');
	}

}
