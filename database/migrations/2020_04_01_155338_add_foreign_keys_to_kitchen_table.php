<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToKitchenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('kitchen', function(Blueprint $table)
		{
			$table->foreign('branch_id', 'FK_kitchen_restaurant_branch')->references('id')->on('restaurant_branch')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('kitchen', function(Blueprint $table)
		{
			$table->dropForeign('FK_kitchen_restaurant_branch');
		});
	}

}
