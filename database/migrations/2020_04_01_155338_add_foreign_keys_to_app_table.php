<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAppTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('app', function(Blueprint $table)
		{
			$table->foreign('branch_id', 'FK_app_restaurant_branch')->references('id')->on('restaurant_branch')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('app', function(Blueprint $table)
		{
			$table->dropForeign('FK_app_restaurant_branch');
		});
	}

}
