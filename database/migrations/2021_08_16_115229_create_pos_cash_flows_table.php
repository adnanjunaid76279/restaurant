<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePosCashFlowsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pos_cash_flows', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->decimal('dinein_sale', 18);
			$table->decimal('takeaway_sale', 18)->default(0.00);
			$table->decimal('delivery_sale', 18)->default(0.00);
			$table->text('cash_in_items', 65535);
			$table->text('cash_out_items', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pos_cash_flows');
	}

}
