<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKitchenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kitchen', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->string('kitchen_name', 50);
			$table->integer('branch_id')->index('FK_kitchen_restaurant_branch');
			$table->dateTime('date_created');
			$table->dateTime('date_updated')->nullable();
			$table->integer('createdby_id');
			$table->integer('updatedby_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kitchen');
	}

}
