<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_detail', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->integer('order_id')->index('FK_order_detail_order');
			$table->integer('menu_id')->nullable();
			$table->decimal('quantity', 18)->nullable();
			$table->decimal('price', 18)->nullable();
			$table->boolean('is_deleted')->nullable();
			$table->integer('order_detail_status_id')->nullable()->index('FK_order_detail_order_detail_status');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_detail');
	}

}
