<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeliveryUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('delivery_user', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->string('login', 50)->nullable();
			$table->string('password', 50)->nullable();
			$table->string('first_name', 50)->nullable();
			$table->string('last_name', 50)->nullable();
			$table->text('device_id')->nullable();
			$table->boolean('is_online')->nullable()->default(0);
			$table->integer('role_id')->nullable();
			$table->string('cell_num', 13)->nullable();
			$table->integer('branchId');
			$table->string('latitude', 50)->nullable();
			$table->string('longitude', 50)->nullable();
			$table->dateTime('date_created')->nullable();
			$table->dateTime('date_updated')->nullable();
			$table->dateTime('updatedby_id')->nullable();
			$table->dateTime('createdby_id')->nullable();
			$table->integer('app_id')->nullable()->index('FK_delivery_user_app');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('delivery_user');
	}

}
