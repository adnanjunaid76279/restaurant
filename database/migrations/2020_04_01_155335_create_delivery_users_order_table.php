<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeliveryUsersOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('delivery_users_order', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->integer('order_id')->index('FK_delivery_users_order_order');
			$table->integer('delivery_user_id')->index('FK_delivery_users_order_delivery_user');
			$table->integer('delivery_user_order_status_id')->index('FK_delivery_users_order_delivery_user_order_status');
			$table->dateTime('date_created');
			$table->dateTime('date_updated')->nullable();
			$table->integer('createdby_id');
			$table->integer('updatedby_id')->nullable();
			$table->string('start_latitude', 50)->nullable();
			$table->string('start_longitude', 50)->nullable();
			$table->string('current_latitude', 50)->nullable();
			$table->string('current_longitude', 50)->nullable();
			$table->string('end_latitude', 50)->nullable();
			$table->string('end_longitude', 50)->nullable();
			$table->boolean('order_in_progress')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('delivery_users_order');
	}

}
