<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReservationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reservation', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->integer('user_id')->nullable();
			$table->integer('restaurant_id')->nullable();
			$table->integer('branch_id')->nullable();
			$table->string('name', 200)->nullable();
			$table->string('email', 200)->nullable();
			$table->string('phone_num', 200)->nullable();
			$table->integer('total_seats')->nullable();
			$table->date('reservation_date')->nullable();
			$table->integer('reservation_source')->nullable();
			$table->time('from_time')->nullable();
			$table->time('to_time')->nullable();
			$table->integer('time_slot')->nullable();
			$table->boolean('is_canceled')->nullable();
			$table->text('reserved_tables')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reservation');
	}

}
