<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAppUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('app_users', function(Blueprint $table)
		{
			$table->foreign('app_id', 'FK_app_users_app')->references('id')->on('app')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('user_id', 'FK_app_users_users')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('app_users', function(Blueprint $table)
		{
			$table->dropForeign('FK_app_users_app');
			$table->dropForeign('FK_app_users_users');
		});
	}

}
