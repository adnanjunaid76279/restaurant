<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAgentUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('agent_users', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->string('name', 100)->nullable();
			$table->integer('branch_id')->nullable();
			$table->text('username')->nullable();
			$table->text('password')->nullable();
			$table->boolean('allowPos')->nullable();
			$table->dateTime('lastLogin')->nullable();
			$table->boolean('loggedin')->nullable();
			$table->boolean('category_flag')->nullable();
			$table->string('hcc_user_login', 50)->nullable();
			$table->string('hcc_user_password', 50)->nullable();
			$table->integer('agent_role_id')->nullable()->index('FK_agent_users_agent_roles');
			$table->string('hcc_server_ip', 50)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('agent_users');
	}

}
