<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRestaurantTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('restaurant', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->text('name')->nullable();
			$table->text('logo')->nullable();
			$table->text('owner')->nullable();
			$table->text('contact_person')->nullable();
			$table->text('contact_email')->nullable();
			$table->text('contact_number')->nullable();
			$table->boolean('have_branch')->nullable();
			$table->dateTime('date_created')->nullable();
			$table->dateTime('date_updated')->nullable();
			$table->integer('updatedby_id')->nullable();
			$table->integer('createdby_id')->nullable();
			$table->string('facebook_page_id', 50)->nullable();
			$table->text('facebook_page_name')->nullable();
			$table->text('facebook_app_secret')->nullable();
			$table->string('facebook_app_id', 50)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('restaurant');
	}

}
