<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTownTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('town', function(Blueprint $table)
		{
			$table->foreign('city_id', 'FK_town_city')->references('id')->on('city')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('town', function(Blueprint $table)
		{
			$table->dropForeign('FK_town_city');
		});
	}

}
