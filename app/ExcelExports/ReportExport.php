<?php

namespace App\ExcelExports;

use App\Services\Concrete\ReportService;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ReportExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $obj_rport;

    public function __construct($obj_rport)
    {
        $this->obj_rport = $obj_rport;
    }

    public function view(): View
    {
        $parms   = $this->obj_rport;
        switch($parms->report_name)
        {
            case 'consumed_menu';
                return view('reports.export-view.consumed_menu_view', compact('parms'));
            break;
            
            case 'sale_detail';
                return view('reports.export-view.sale_detail_view', compact('parms'));
            break;

            case 'rider_delivery';
                return view('reports.export-view.rider_delivery_view', compact('parms'));
            break;
        }
        
    }
   
}
