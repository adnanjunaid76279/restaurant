<?php

namespace App\Http\Controllers;

use App\Restaurant;
use App\RestaurantBranch;
use App\Services\Concrete\AgentUserService;
use App\Services\Concrete\RestaurantService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AgentUserController extends Controller
{
    protected $agent_user_service;
    protected $restaurant_service;

    public function __construct(AgentUserService  $agent_user_service,RestaurantService $restaurant_service)
    {
        $this->agent_user_service = $agent_user_service;
        $this->restaurant_service = $restaurant_service;
    }

    public function index(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->get('start');
        $length = $request->get('length');
        $search = (isset($request->search['value']))? $request->search['value'] : false;
        $login_user = Auth::guard('admin_user')->User();

        return $this->agent_user_service->getAgentUsersDatatableSource($draw,$start ,$length ,$search,$login_user);

    }

    public function create()
    {
        $login_user = Auth::guard('admin_user')->User();

        switch($login_user ->role_id)
        {
            case '1':

                $agent_roles = $this->agent_user_service->getAgentRoles();
                
                return view('agent-users.agent-user-form',compact('agent_roles'));
                
                break;

            case '2':

                $agent_roles = $this->agent_user_service->getAgentRoles();
                $branch =  $this->restaurant_service->getAllBranches($login_user);

                return view('agent-users.agent-user-form',compact('agent_roles','branch'));

                break;
            case '3':

                $branch[] =  $this->restaurant_service->getBranchById($login_user->userRestaurant->branch_id);

                return view('agent-users.agent-user-form',compact('branch'));

                break;

        }


    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'     =>'required',
            'role'     =>'required',
            'username' =>'required|email|unique:agent_users',
            'password' =>'required',
            'branch'   => 'required',
        ]);



        $obj = [

            'name'          =>  $request->name,
            'agent_role_id' =>  $request->role,
            'allowPos'      =>  isset($request->allowPos)?1:0,
            'username'      =>  $request->username,
            'password'      =>  $request->password,
            'branch_id'     =>  $request->branch
        ];

        $agent_user = $this->agent_user_service->save($obj);


        return redirect('view-agent-user/'.$agent_user->id);
    }

    public function show($id)
    {
        $agent_users = $this->agent_user_service->getById($id);

        if($agent_users)
            return view('agent-users.agent-user-view',compact('agent_users'));
        else
            return  redirect('list-agent-user');
    }

    public function edit($id)
    {

        $login_user = Auth::guard('admin_user')->User();

        $agent_users = $this->agent_user_service->getById($id);
        $agent_roles = $this->agent_user_service->getAgentRoles();

        switch($login_user ->role_id) {

            case '1':
                
                $branch[] = $this->restaurant_service->getBranchById($agent_users->branch_id);
                
                break ;

            case '2':
                
                $branch =  $this->restaurant_service->getBranchesByRestaurantId($login_user->userRestaurant->restaurant_id);

                break;

            case '3':
                
                $branch[] =  $this->restaurant_service->getBranchById($login_user->userRestaurant->branch_id);
                
                break;
        }

        return view('agent-users.agent-user-form', compact('agent_users', 'agent_roles', 'branch'));
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'name'     =>'required',
            'role'     =>'required',
            'username' =>'required',
            'password' =>'required',
            'branch'   =>'required',
        ]);

        $obj = [
            'id'           =>  $request->id,
            'name'         =>  $request->name,
            'agent_role_id'=>  $request->role,
            'allowPos'     =>  isset($request->allowPos)?1:0,
            'username'     =>  $request->username,
            'password'     =>  $request->password,
            'branch_id'    =>  $request->branch
        ];

        $this->agent_user_service->save($obj);

        return redirect('view-agent-user/'.$request->id)->with('success',$obj['username'].' Updated');

    }
}
