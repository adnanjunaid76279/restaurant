<?php

namespace App\Http\Controllers;
use App\ExcelExports\ReportExport;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Services\Concrete\ReportService;
use App\Services\Concrete\RestaurantService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use PDF;
use Maatwebsite\Excel\Facades\Excel;


class ReportController extends Controller
{

    // space where we declare services
    protected $report_service;
    protected $restaurant_service;
    
    /**
     * Constructor
    */
    public function __construct(ReportService $report_service,RestaurantService $restaurant_service)
    {
        // set the model
        $this->report_service  = $report_service;
        $this->restaurant_service  = $restaurant_service;
    }

// consumed menu report
    public function showMenuConsumedReport(Request $request)
    {
        $login_user = Auth::guard('admin_user')->User();
        
        $branches = $this->restaurant_service->getAllBranches($login_user);
        
        return view('reports.consumed_menu',compact('branches'));
        
    }

    public function consumedReport(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->get('start');
        $length = $request->get('length');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $branch = $request->get('branch');
        $search = false;
        $login_user = Auth::guard('admin_user')->User();
        return  $this->report_service->getConsumedMenuItemsByDateByBranch($draw,$start ,$length ,$search,$login_user,$start_date,$end_date,$branch);
        
    }

    public function exportConsumedReport(Request $request)
    {

        $request['draw'] = 1;
        $request['start'] = 0;
        $request['length'] = -1;

        // get data via consumedReport in current controller
        $parms = $this->consumedReport($request);
        $parms = json_decode($parms);
        // set further parms to transfer to report view
        $parms->start_date = $request->get('start_date');
        $parms->end_date = $request->get('end_date');
        $parms->report_name = "consumed_menu";  // report name for export expor tfunction

        // if export excel button is clicked
        if($request->has('export-excel'))
        {
            return Excel::download(new ReportExport($parms), 'consumed_report.xls');
        }

        // if export pdf is clicked
        $pdf = PDF::loadView('reports.export-view.consumed_menu_view', compact('parms'));
        $fileName =  'consumed_report-'.date('d-m-Y').'.'. 'pdf' ;
        return $pdf->setPaper('A4', 'portrait')->stream($fileName);
    }

// sale detail report
    public function showSaleDetailReport(Request $request)
    {
        $login_user = Auth::guard('admin_user')->User();
        
        $branches = $this->restaurant_service->getAllBranches($login_user);
        
        return view('reports.sale_detail',compact('branches'));
        
    }

    public function saleDetailReport(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->get('start');
        $length = $request->get('length');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $branch = $request->get('branch');
        $search = false;
        $login_user = Auth::guard('admin_user')->User();
        return  $this->report_service->getSaleDetailByDateByBranch($draw,$start ,$length ,$search,$login_user,$start_date,$end_date,$branch);
        
    }
    
    public function exportSaleDetailReport(Request $request)
    {

        $request['draw'] = 1;
        $request['start'] = 0;
        $request['length'] = -1;

        // get data via report in current controller
        $parms = $this->saleDetailReport($request);
        $parms = json_decode($parms);
        // set further parms to transfer to report view
        $parms->start_date = $request->get('start_date');
        $parms->end_date = $request->get('end_date');
        $parms->report_name = "sale_detail";

        // if export excel button is clicked
        if($request->has('export-excel'))
        {
            return Excel::download(new ReportExport($parms), 'sale_detail_report.xls');
        }

        // if export pdf is clicked
        $pdf = PDF::loadView('reports.export-view.sale_detail_view', compact('parms'));
        $fileName =  'sale_detail_report-'.date('d-m-Y').'.'. 'pdf' ;
        return $pdf->setPaper('A4', 'landscape')->stream($fileName);
    }

//================================ Rider Delivery Report Monthly ==========================

    public function showRiderMonthlyDeliveryReport(Request $request)
    {
        $login_user = Auth::guard('admin_user')->User();
        
        $branches = $this->restaurant_service->getAllBranches($login_user);
        $months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
        
        return view('reports.rider_delivery',compact('branches','months'));
        
    }

    public function riderMonthlyDeliveryReport(Request $request)
    {
        $login_user = Auth::guard('admin_user')->User();
        $month = $request->month != 0 ? $request->month : 9;
        $branch = $request->branch != 0 ? $request->branch : $request->branch  ;
        
        return $this->report_service->getSaleVsKiloMeterByMonth($login_user,$month,$branch);
                
    }


    public function exportRiderMonthlyDeliveryReport(Request $request)
    {
        $month = $request->get('month') !=0 ? $request->get('month'):1;
        
        // get data via report in current controller
        $parms = $this->riderMonthlyDeliveryReport($request);
        
        $rpt_date = date('Y-'.$month.'-d');

        // set further parms to transfer to report view
        $parms['month_name'] = date('F',strtotime($rpt_date));
        $parms['report_name'] = "rider_delivery";
        
        $parms = (object)$parms;
       
        // if export excel button is clicked
        if($request->has('export-excel'))
        {
            return Excel::download(new ReportExport($parms), 'rider_delivery_report.xls');
        }

        // if export pdf is clicked
        $pdf = PDF::loadView('reports.export-view.rider_delivery_view', compact('parms'));
        $fileName =  'rider_delivery_report-'.date('d-m-Y').'.'. 'pdf' ;
        return $pdf->setPaper('A4', 'landscape')->stream('rider_delivery_report.pdf');
    }


}