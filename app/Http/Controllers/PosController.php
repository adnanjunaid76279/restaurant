<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Services\Concrete\PosService;
use App\Services\Concrete\OrderService;
use App\Services\Concrete\RestaurantService;
use App\Services\Concrete\DeliveryDetailService;
use App\Services\Concrete\UserService;
use App\Services\Concrete\OrderDetailService;
use App\Services\Concrete\RiderService;
use App\Services\Concrete\MenuCategoryService;
use App\Services\Concrete\OmcOrderService;

class PosController extends Controller
{

    // space where we declare services
    protected $pos_service;
    protected $order_service;
    protected $restaurant_service;
    protected $delivery_detail_service;
    protected $user_service;
    protected $order_detail_service;
    protected $rider_service;
    protected $menu_category_service;
    protected $omc_order_service;
    protected $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(PosService $pos_service,OrderService $order_service,RestaurantService $restaurant_service,DeliveryDetailService $delivery_detail_service,UserService $user_service,
    OrderDetailService $order_detail_service,
    RiderService $rider_service,
    MenuCategoryService $menu_category_service,
    OmcOrderService $omc_order_service
    )
    {
        // set the model
        $this->pos_service      = $pos_service;
        $this->order_service    = $order_service;
        $this->restaurant_service   = $restaurant_service;
        $this->delivery_detail_service = $delivery_detail_service;
        $this->user_service = $user_service;
        $this->order_detail_service = $order_detail_service;
        $this->rider_service = $rider_service;
        $this->menu_category_service = $menu_category_service;
        $this->omc_order_service = $omc_order_service;
    }

    public function savePosUser(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];


        $validator = Validator::make($request->all(), [
            "branchId"      => "required",
            "first_name"    => "required",
            "cell_num"      => "required"
        ]);

        if ($validator->fails())
        {
            $response["Success"]   = false;
            $response["Status"] = 400;
            $response["ErrorMessage"] = $validator->errors();

            return response(
                $response,
                200
            );
        }


        $chk_user_exist = $this->user_service->getByContactBranchLoginTypeId($request->cell_num,$request->branchId,4); // search user by cell num,branch id ,login type ,current is pos

        if($chk_user_exist)
        {
            $obj_user = [
                "id"            => $chk_user_exist->id,
                'first_name'    => isset($request->first_name)?$request->first_name:$chk_user_exist->first_name,
                'last_name'     => isset($request->last_name)?$request->last_name:$chk_user_exist->last_name,
                'cell_num'      => isset($request->cell_num)?$request->cell_num:$chk_user_exist->cell_num,
                'date_birth'    => isset($request->date_birth)?$request->date_birth:$chk_user_exist->date_birth,
                'email'         => isset($request->email)?$request->email:$chk_user_exist->email,
                'branch_id'     => isset($request->branchId)?$request->branchId:$chk_user_exist->branch_id
            ];
        }
        else
        {
            $obj_user = [
                'first_name'    => $request->first_name,
                'last_name'     => $request->last_name??null,
                'cell_num'      => $request->cell_num,
                'date_birth'    => $request->date_birth??null,
                'email'         => $request->email,
                'branch_id'     => $request->branchId,
                'login_type_id' => 4
            ];
        }

        // db request via service
        $login = $this->user_service->save($obj_user);

        if($login)
        {
            $response["Data"] =  $login;
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "Error: creating user";

            return response(
                $response,
                200
            );

        }

        return $response;
    }

    // place pos order
    public function placeOrder(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        $rnd_number = rand(1,25000);
        $current = date('Ymdhis');
        $tracking_id = $current.'_'.$rnd_number ;

        // validate request
        $validator = Validator::make($request->all(), [
            "total" => "required"
        ]);

        if ($validator->fails())
        {
            $response["Success"]   = false;
            $response["Status"] = 400;
            $response["ErrorMessage"] = $validator->errors();

            return response(
                $response,
                200
            );
        }

        // check menu detail is missing or not

        if(!isset($request->menuItems))
        {
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "order detail is missing";

            return response(
                $response,
                200
            );
        }

        //check if user id does not exist then save user
        if(!isset($request->user_id))
        {
            $save_pos_user = new Request([

            "email"         => $request->delivery_details['email']??null,
            "first_name"    => $request->delivery_details['first_name']??null,
            "last_name"     => $request->delivery_details['last_name']??null,
            "cell_num"      => $request->delivery_details['mobile_no']??null,
            "address1"      => $request->delivery_details['Address']??null,
            "gender"        =>$request->delivery_details['gender']??null,
            "date_birth"    => $request->delivery_details['date_birth']??null,
            "town_id"       => $request->delivery_details['town_id']??null,
            "branchId"      => $request->branch_id??null
            ]);

            $response_user_request = $this->savePosUser($save_pos_user);
            $request->user_id = $response_user_request["Data"]->id;
        }

        $new_order = 1;

        if(isset($request->paymentLaterOrderId) && $request->paymentLaterOrderId != 0)
            $new_order = 0;

        $order_datetime = date("Y-m-d H:i:s", time() - date("Z"));

        $obj_order = [
        'total'              =>     $request->total,
        'sub_total'          =>     $request->sub_total,
        'amount_paid'        =>     is_numeric($request->amount_paid)?$request->amount_paid:0,
        'amount_return'      =>     is_numeric($request->amount_return)?$request->amount_return:0,
        'discount_per'       =>     is_numeric($request->discount_per)?$request->discount_per:0,
        'discount_amount'    =>     is_numeric($request->discount_amount)?$request->discount_amount:0,
        'tax_percent'        =>     is_numeric($request->tax_percent)?$request->tax_percent:0,
        'tax_amount'         =>     is_numeric($request->tax_amount)?$request->tax_amount:0,
        'delivery_charge'    =>     is_numeric($request->delivery_charge)?$request->delivery_charge:0,
        'service_charge'    =>      is_numeric($request->service_charge)?$request->service_charge:0,
        'status_id'          =>     isset($request->status_id)? $request->status_id:3,
        'order_type_id'      =>     $request->order_type_id,
        'order_resource'     =>     isset($request->order_resource)?$request->order_resource:4 ,
        'table_no'           =>     $request->table_no,
        'num_persons'        =>     $request->num_persons,
        'order_edit'         =>     isset($request->order_edit) ? 1:0,
        'tax_include'        =>     $request->tax_include == null ? false : true,
        'tax_on_original_amt'=>     $request->tax_on_original_amt == null ? false : true,
        'order_taker'        =>     $request->order_taker,
        'useragent_id'       =>     $request->order_taker_id,
        ];

        $obj_delivery_detail = [

            'address'       => $request->delivery_details['Address']??NULL,
            'mobile_num'    => $request->delivery_details['mobile_no'],
            'others'        => $request->delivery_details['others'],
            'phone_num'     => isset($request->delivery_details['phone_num'])??null,
            'town_id'       => $request->delivery_details['town_id'],
            'town_block_id' => isset($request->delivery_details['town_block_id'])?$request->delivery_details['town_block_id']:NULL,
            'email'         => $request->delivery_details['email']??NULL,
            'gender'        => $request->delivery_details['gender'],
            'date_birth'    => $request->delivery_details['date_birth'],
            'instructions'  => $request->delivery_details['instructions'],
            'latitude'      => $request->lat,
            'longitude'     => $request->lng
        ];


        $dailyorder_id = 0;
        // for order edit scenerio
        if($new_order)
        {
            // get branch
            $obj_branch = $this->restaurant_service->getBranchById($request->branch_id ?? 0);
            // order object
            $dailyorder_id = $obj_branch->dailyorder_id ?? 1;
            $obj_order['dailyorder_id'] = $dailyorder_id;
            $obj_order['createdby_id']  =  $request->login_user_id;
            $obj_order['restaurant_branch_id']  = $request->branch_id;
            $obj_order['tracking_id']  = $tracking_id;
            $obj_order['user_id']  = $request->user_id;
            $obj_order['order_date']  = $order_datetime;
            $obj_order['deliver_date']  = $order_datetime;

            // update daily order id in branch
            $obj_branch->dailyorder_id =    $dailyorder_id + 1;
            $this->restaurant_service->saveBranch($obj_branch->toArray());

            $obj_delivery_detail['user_id'] = $request->user_id;
        }
        else
        {
            $dailyorder_id =  $this->order_service->getById($request->paymentLaterOrderId)->dailyorder_id;
            $obj_order['updatedby_id']  =  $request->login_user_id;
            $obj_order['id']  = $request->paymentLaterOrderId;
            
            // get delivery detail by order id
            $pre_delivery_detail = $this->delivery_detail_service->getDetailByOrderIds([$obj_order['id']])->first();
            //delivery detail object
            $obj_delivery_detail['id'] = $pre_delivery_detail->id;
        }

        // save order
        $order_placed = $this->order_service->save($obj_order);

        if($new_order)
            $order_id = $order_placed['id'];
        else
            $order_id = $request->paymentLaterOrderId;

        //delivery detail object
        $obj_delivery_detail['order_id'] = $order_id;
        // save order delivery detail
        $this->delivery_detail_service->save($obj_delivery_detail);


        if($order_placed)
        {

            // check if user already has address
            $user_address = $this->delivery_detail_service->getAddressByUserId($request->user_id);
            if($user_address)
            {
                $user_address->address1 = isset($request->delivery_details['Address']) ? $request->delivery_details['Address'] : $user_address->address1 ;
                $user_address->longitude = isset($request->lng) ? $request->lng : $user_address->longitude ;
                $user_address->latitude = isset($request->lat) ? $request->lat : $user_address->latitude ;
                $user_address->town_id =isset($request->delivery_details['town_id']) ? $request->delivery_details['town_id'] : $user_address->town_id ;
                $user_address->town_block_id =isset($request->delivery_details['town_block_id']) ? $request->delivery_details['town_block_id'] : $user_address->town_block_id ;
                $user_address->address2 = isset($request->delivery_details['address2']) ? $request->delivery_details['address2'] : $user_address->address2 ;

                $this->delivery_detail_service->saveAddress($user_address->toArray());
            }
            else
            {   $order_delivery_add = [

                    'address1'  => isset($request->delivery_details['Address']) ?$request->delivery_details['Address']: null ,
                    'user_id'   => $obj_order['user_id'] ,
                    'longitude' => isset($request->lng)  ?? null ,
                    'latitude'  => isset($request->lat)  ?? null ,
                    'town_id'   =>   isset($request->delivery_details['town_id'])  ? $request->delivery_details['town_id']: null ,
                    'town_block_id'   =>   isset($request->delivery_details['town_block_id'])  ? $request->delivery_details['town_block_id']: null ,
                    'address2'  => isset($request->delivery_details['address2'])  ? $request->delivery_details['address2']:null ,
                    'address_type_id' => 5
                ];
                // save user address
                $this->delivery_detail_service->saveAddress($order_delivery_add);
            }

            // check if user already exist
            $user = $this->user_service->getById($request->user_id);
            if($user)
            {

                $user->phone_num = isset($request->delivery_details['phone_num']) ? $request->delivery_details['phone_num'] : $user->phone_num;
                $user->cell_num = isset($request->delivery_details['mobile_no']) ? $request->delivery_details['mobile_no'] : $user->cell_num;;
                $user->date_birth =  isset($request->delivery_details['date_birth']) ? $request->delivery_details['date_birth'] : $user->date_birth;
                $user->email = isset($request->delivery_details['email']) ? $request->delivery_details['email'] : $user->email;
                $user->gender = isset($request->delivery_details['gender']) ? $request->delivery_details['gender'] : $user->gender;

                $user->first_name = isset($request->delivery_details['first_name']) ? $request->delivery_details['first_name'] : $user->first_name;
                $user->last_name = isset($request->delivery_details['last_name']) ? $request->delivery_details['last_name'] : $user->last_name;
                $user->updatedby_id = $request->login_user_id;
                $this->user_service->save($user->toArray());
            }

            $order_detail = [];
            if(!$new_order)
            {
                // for check if any menu is deleted or not from previous detail
                $pre_order_detail = $this->order_detail_service->getOrderDetailByOrderIds([$order_id]);
                foreach($pre_order_detail as $pre_detail)
                {
                        $pre_detail['is_deleted'] = 1;
                        $this->order_detail_service->save($pre_detail->toArray());

                }
                // for check if any new menu is added in detail or not
                foreach($request->menuItems as $detail)
                {

                    $order_detail = [
                        'menu_id'   => $detail['menu_id'],
                        'order_id'  => $order_id,
                        'price'     => $detail['menu_price'],
                        'quantity'  => $detail['menu_qty'],
                        'menu_name' => $detail['menu_name'],
                        'order_detail_status_id'  => 2
                    ];

                    if(isset($detail['menu_var_obj']))
                    {

                        $order_detail['variation_name']    = $detail['menu_var_obj']['var_name'];
                        $order_detail['menu_variation_id'] = $detail['menu_var_obj']['var_id'];

                    }

                        $saved_od = $this->order_detail_service->save($order_detail);
                        $saved_od_id = $saved_od->id;

                    if(isset($detail['menu_var_obj']['var_choice_grp']))
                    {
                        foreach ($detail['menu_var_obj']['var_choice_grp'] as $key => $choice_grp) {

                            foreach ($choice_grp['choices'] as $key => $value) {

                                $od_choices = [
                                    'choice_name'     => $value['choice_name'],
                                    'price'           => $value['choice_price'],
                                    'choice_id'       => $value['choice_id'],
                                    'order_detail_id' => $saved_od_id
                                ];

                                $this->order_detail_service->saveDetailChoices($od_choices);
                            }
                        }
                    }

                    if(isset($detail['menu_choice_group']))
                    {
                        foreach ($detail['menu_choice_group'] as $key => $choice_grp) {

                            foreach ($choice_grp['choices'] as $key => $choice) {

                                $od_choices = [
                                    'choice_name'     => $choice['choice_name'],
                                    'price'           => $choice['choice_price'],
                                    'choice_id'       => $choice['choice_id'],
                                    'order_detail_id' => $saved_od_id
                                ];

                                $this->order_detail_service->saveDetailChoices($od_choices);
                            }
                        }
                    }
                }

            }
            else
            {   // for inserting new menu in detail
                foreach($request->menuItems as $detail)
                {
                    $order_detail = [
                    'menu_id'   => $detail['menu_id'],
                    'order_id'  => $order_id,
                    'price'     => $detail['menu_price'],
                    'quantity'  => $detail['menu_qty'],
                    'menu_name' => $detail['menu_name'],
                    'order_detail_status_id'  => 2
                    ];

                    if(isset($detail['menu_var_obj'])){

                        $order_detail['variation_name']    = $detail['menu_var_obj']['var_name'];
                        $order_detail['menu_variation_id'] = $detail['menu_var_obj']['var_id'];

                    }

                    $saved_od = $this->order_detail_service->save($order_detail);
                    $saved_od_id = $saved_od->id;

                    if(isset($detail['menu_var_obj']['var_choice_grp']))
                    {
                        foreach ($detail['menu_var_obj']['var_choice_grp'] as $key => $choice_grp) {

                            foreach ($choice_grp['choices'] as $key => $choice) {

                                $od_choices = [
                                    'choice_name'     => $choice['choice_name'],
                                    'price'           => $choice['choice_price'],
                                    'choice_id'       => $choice['choice_id'],
                                    'order_detail_id' => $saved_od_id
                                ];

                                $this->order_detail_service->saveDetailChoices($od_choices);
                            }
                        }
                    }

                    if(isset($detail['menu_choice_group']))
                    {
                        foreach ($detail['menu_choice_group'] as $key => $choice_grp) {

                            foreach ($choice_grp['choices'] as $key => $choice) {

                                $od_choices = [
                                    'choice_name'     => $choice['choice_name'],
                                    'price'           => $choice['choice_price'],
                                    'choice_id'       => $choice['choice_id'],
                                    'order_detail_id' => $saved_od_id
                                ];

                                $this->order_detail_service->saveDetailChoices($od_choices);
                            }
                        }
                    }
                }

            }

        }


        if($order_placed)
        {
            $response["Data"] =  ["trackingId" => $tracking_id,"dailyOrderCount" => $dailyorder_id + 1];

            if(!isset($request->order_edit) || $request->order_edit == 0)
                $this->order_service->insertErpInventoryTrans($order_id);

            if(isset($request->branch_id) && $request->branch_id == 1267)
                $this->omc_order_service->postOrder($order_id);
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "Order does not placed successfully";

            return response(
                $response,
                200
            );

        }

        return $response;
    }

    public function changeOrderstatus(Request $request)
    {
        // dd($request);
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // validate request
        $validator = Validator::make($request->all(), [
            "orderID" => "required",
            "statusId" => "required"
        ]);

        if ($validator->fails())
        {
            $response["Success"]   = false;
            $response["Status"] = 400;
            foreach ($validator->errors()->all() as $message) {
                $response["ErrorMessage"] .= $message;
            }

            return response(
                $response,
                200
            );
        }

        // get data from service
        $status = $this->order_service->changeOrderStatus($request->orderID,$request->statusId);

        if($status)
        {
            $response["Data"] = $status;
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "record does not found";

            return response(
                $response,
                200
            );

        }
        return $response;
    }

    public function changeOrderDetailStatus(Request $request)
    {
        // dd($request);
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // validate request
        $validator = Validator::make($request->all(), [
            "orderID" => "required",
            "statusId" => "required"
        ]);

        if ($validator->fails())
        {
            $response["Success"]   = false;
            $response["Status"] = 200;
            foreach ($validator->errors()->all() as $message) {
                $response["ErrorMessage"] .= $message;
            }

            return response(
                $response,
                200
            );
        }

        // get data from service, her $orderID is acttually order detail id
        $status = $this->order_detail_service->changeOrderDetailStatus($request->orderID,$request->statusId);

        if($status)
        {
            $this->order_service->changeOrdStatustoReadyViaAllDetailReady($request->orderID);
            $response["Data"] = $status;
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "Error occur during status update!";

            return response(
                $response,
                200
            );

        }
        return $response;
    }

    public function getOrderStatus()
    {
         // get data from service
        $status = $this->order_service->getOrderStatus();

        if($status)
        {
            $this->response["Data"] = $status;
        }
        else
        {
            $this->response["Success"]   = false;
            $this->response["ErrorMessage"] = "record does not found";

            return response(
                $this->response,
                200
            );

        }
        return $this->response;
    }

    public function getOrdersByBranchId(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // validate request
        $validator = Validator::make($request->all(), [
            "branchId" => "required"
        ]);

        if ($validator->fails())
        {
            $response["Success"]   = false;
            $response["Status"] = 400;
            $response["ErrorMessage"] = $validator->errors();

            return response(
                $response,
                200
            );
        }

        if(!$request->has('from') && !$request->has('end'))
        {
            // get data from service

            // $from  = date("Y-m-d H:i:s", strtotime('-24 hours', time()));
            $from  = date("Y-m-d 07:00:00");
            $end  = date("Y-m-d H:i:s");
            $orders = $this->order_service->getOrdersByBranchId($request->branchId,$from,$end,0, 50);
        }
        else if($request->has('from') && $request->has('end'))
        {
             // get data from service
             $orders = $this->order_service->getOrdersByBranchId($request->branchId,$request->from,$request->end,0,1000);
        }
        else
        {
            $response["Success"]   = false;
            $response["Status"] = 400;
            $response["ErrorMessage"] = "Invalid date range";

            return response(
                $response,
                400
            );
        }


        if($orders)
        {
            $response["Data"] = $orders;
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "record does not found";

            return response(
                $response,
                200
            );

        }
        return $response;
    }

    public function getOrdersByBranchIdAgentId(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // validate request
        $validator = Validator::make($request->all(), [
            "branchId" => "required",
            "agentId"  => "required"
        ]);

        if ($validator->fails())
        {
            $response["Success"]   = false;
            $response["Status"] = 400;
            $response["ErrorMessage"] = $validator->errors();

            return response(
                $response,
                400
            );
        }

        if(!$request->has('from') && !$request->has('end'))
        {
            // get data from service

            //  $from  = date("Y-m-d H:i:s", strtotime('-24 hours', time()));
            $from  = date("Y-m-d 07:00:00");
            $end  = date("Y-m-d H:i:s");
            $orders = $this->order_service->getOrdersByBranchIdAgentId($request->branchId,$request->agentId,$from,$end);
        }
        else if($request->has('from') && $request->has('end'))
        {
             // get data from service
             $orders = $this->order_service->getOrdersByBranchIdAgentId($request->branchId,$request->agentId,$request->from,$request->end);
        }
        else
        {
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "Invalid date range";

            return response(
                $response,
                400
            );
        }


        if($orders)
        {
            $response["Data"] = $orders;
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "no order available";

            return response(
                $response,
                200
            );

        }
        return $response;
    }

    public function getUnpaidOrdersByBranchId(Request $request)
    {

        // validate request
        $validator = Validator::make($request->all(), [
            "branchId" => "required"
        ]);

        if ($validator->fails())
        {
            $this->response["Success"]   = false;
            $this->response["Status"] = 400;
            $this->response["ErrorMessage"] = $validator->errors();

            return response(
                $this->response,
                200
            );
        }

        if(!$request->has('from') && !$request->has('end'))
        {
            // get data from service
            $from  = date("Y-m-d 07:00:00");
            $end  = date("Y-m-d H:i:s");
            $orders = $this->order_service->getUnpaidOrdersByBranchId($request->branchId,$from,$end,0, 50,"order.dailyorder_id");
        }
        else if($request->has('from') && $request->has('end'))
        {
             // get data from service
             $orders = $this->order_service->getUnpaidOrdersByBranchId($request->branchId,$request->from,$request->end,0,1000);
        }
        else
        {
            $this->response["Success"]   = false;
            $this->response["Status"] = 400;
            $this->response["ErrorMessage"] = "Invalid date range";

            return response(
                $this->response,
                400
            );
        }


        if($orders)
        {
            $this->response["Data"] = $orders;
        }
        else
        {

            $this->response["Success"]   = false;
            $this->response["Status"] = 401;
            $this->response["ErrorMessage"] = "record does not found";

            return response(
                $this->response,
                200
            );

        }
        return $this->response;
    }

    public function getUserOrdersByRestID(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // validate request
        $validator = Validator::make($request->all(), [
            "UserId" => "required",
            "restaurant_id" => "required"

        ]);

        if ($validator->fails())
        {
            $response["Success"]   = false;
            $response["Status"] = 400;
            $response["ErrorMessage"] = $validator->errors();

            return response(
                $response,
                200
            );
        }

        // get data from service
        $ordres = $this->order_service->getUserOrdersWithDetailByRestID($request->UserId,$request->restaurant_id);

        if($ordres)
        {
            $response["Data"] = $ordres;
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "record does not found";

            return response(
                $response,
                200
            );

        }
        return $response;
    }


    public function getUserByNumberandBranchId(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // validate request

        $validator = Validator::make($request->all(), [
            "branchid" => "required",
            "number"   => "required"

        ]);


        if ($validator->fails())
        {
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = $validator->errors();

            return response(
                $response,
                200
            );
        }

        // get data from service by contact and branch id

        $users = $this->user_service->getByContactBranchLoginTypeId($request->number,$request->branchid,4);
        if(isset($users->addresses))
        {
            $users->address         = $users->addresses->address1;
            $users->longitude       = $users->addresses->longitude;
            $users->town_id         = $users->addresses->town_id;
            $users->town_block_id   = $users->addresses->town_block_id;
            $users->latitude        = $users->addresses->latitude;
            $users->address_type_id = $users->addresses->address_type_id;

        }



        if($users)
        {
            $response["Data"] = $users;
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "record does not found";

            return response(
                $response,
                200
            );

        }
        return $response;
    }


    public function getCategoriesByBranchId(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // validate request
        $validator = Validator::make($request->all(), [
            "BranchID" => "required"
        ]);

        if ($validator->fails())
        {
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = $validator->errors();

            return response(
                $response,
                200
            );
        }

        // get data from service
        $category = $this->pos_service->getCategoriesByBranchId($request->BranchID);

        if($category)
        {
            $response["Data"] = $category;
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "record does not found";

            return response(
                $response,
                200
            );

        }
        return $response;
    }


    public function getTownsByCityId(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // validate request
        if (!$request->cityid)
        {
            $response["Success"]   = false;
            $response["Status"] = 400;
            $response["ErrorMessage"] = "cityid field is missing";

            return response(
                $response,
                400
            );
        }

        // get data from service
        $towns = $this->delivery_detail_service->getTownsByCityId($request->cityid);

        if($towns)
        {
            $response["Data"] = $towns;
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "record does not found";

            return response(
                $response,
                200
            );

        }
        return $response;
    }

    public function getTownBlocksByTownId(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // validate request
        if (!$request->townId)
        {
            $response["Success"]   = false;
            $response["Status"] = 400;
            $response["ErrorMessage"] = "cityid field is missing";

            return response(
                $response,
                400
            );
        }

        // get data from service
        $blocks = $this->delivery_detail_service->getTownBlocksByTownId($request->townId);

        if($blocks)
        {
            $response["Data"] = $blocks;
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "record does not found";

            return response(
                $response,
                200
            );

        }
        return $response;
    }

    public function getRestaurantBranchesNameAndId($restaurant_id)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // validate request
        if (!$restaurant_id)
        {
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "restaurant_id field is missing";

            return response(
                $response,
                200
            );
        }

        // get data from service
        $branches = $this->pos_service->getRestaurantBranchesNameAndId($restaurant_id);

        if($branches)
        {
            $response["Data"] = $branches;
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "record does not found";

            return response(
                $response,
                200
            );

        }

        return $response;
    }

    public function posLogin(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // validate request
        $validator = Validator::make($request->all(), [
            "uid" => "required",
            "pwd"  => "required",
        ]);

        if ($validator->fails())
        {
            $response["Success"]   = false;
            $response["Status"] = 400;
            $response["ErrorMessage"] = $validator->errors();

            return response(
                $response,
                200
            );
        }

        // authenticate request
        $login = $this->pos_service->posLogin($request->uid,$request->pwd);

        if($login)
        {
            $response["Data"] =  $login;
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "Invalid login detail";

            return response(
                $response,
                200
            );

        }

        return $response;
    }

    public function getActiveRidersByBranchId(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // validate request
        if (!$request->branchId)
        {
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "branchId field is missing";

            return response(
                $response,
                200
            );
        }

        // get data from service
        $riders = $this->rider_service->getActiverRiders($request->branchId);

        if($riders)
        {
            $response["Data"] = $riders;
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "record does not found";

            return response(
                $response,
                200
            );

        }
        return $response;
    }

    public function getDeliveryRoute(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // validate request
        if (!$request->order_id)
        {
            $response["Success"]   = false;
            $response["Status"] = 400;
            $response["ErrorMessage"] = "order_id field is missing";

            return response(
                $response,
                400
            );
        }

        $order_id = $request->order_id;

        // get order
        $order = $this->order_service->getById($order_id);

        if($order->order_type_id == 3)
        {
            // get delivery detail
            $delivery_details = $this->delivery_detail_service->getDetailByOrderIds([$order_id]);

            if (isset($delivery_details[0]->latitude)  && isset($delivery_details[0]->longitude))
            {
                // get branch
                $branch = $this->restaurant_service->getBranchById($order->restaurant_branch_id);

                if (isset($branch->latitude) && isset($branch->longitude))
                {
                    $response["Data"] = [
                                        "startLatitude" => $branch->latitude,
                                        "startLongitude" => $branch->longitude,
                                        "endLatitude" => $delivery_details[0]->latitude,
                                        "endLongitude" => $delivery_details[0]->longitude,

                                        ];
                }
                else
                {
                    $response["Success"]   = false;
                    $response["Status"] = 200;
                    $response["ErrorMessage"] = "Restaurant has no valid location!";

                }

            }
            else
            {
                $response["Success"]   = false;
                $response["Status"] = 200;
                $response["ErrorMessage"] = "Delivery detail has no valid location!!";
            }

        }
        else
        {
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "Order is not for delivery";
        }


        return $response;
    }

    // pos order tracking, between rider and destination
    public function posGetOrderTracking(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // validate request
        if (!$request->order_id)
        {
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "order_id field is missing";

            return response(
                $response,
                200
            );
        }

        $order_id = $request->order_id;

        // get order
        $order = $this->order_service->getById($order_id);

        if($order->order_type_id == 3)
        {
            // get delivery detail
            $delivery_details = $this->delivery_detail_service->getDetailByOrderIds([$order_id]);

            if (isset($delivery_details[0]->latitude)  && isset($delivery_details[0]->longitude))
            {
                // get branch
                $obj_rider_ord = $this->rider_service->getRiderOrderByOrderId($order_id);

                if ($obj_rider_ord && isset($obj_rider_ord->current_latitude) && isset($obj_rider_ord->current_longitude))
                {
                    $response["Data"] = [
                                        "startLatitude"     => $obj_rider_ord->current_latitude,
                                        "startLongitude"    => $obj_rider_ord->current_longitude,
                                        "endLatitude"       => $delivery_details[0]->latitude,
                                        "endLongitude"      => $delivery_details[0]->longitude,

                                        ];
                }
                else
                {
                    $response["Success"]   = false;
                    $response["Status"] = 200;
                    $response["ErrorMessage"] =  "Delivery user has not started delivery yet!";

                }

            }
            else
            {
                $response["Success"]   = false;
                $response["Status"] = 200;
                $response["ErrorMessage"] = "Delivery detail has no valid location!!";
            }

        }
        else
        {
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "Order is not for delivery";
        }


        return $response;
    }

    public function getAddressTypes()
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // get data from service
        $add_types = $this->delivery_detail_service->getAddressTypes();

        if($add_types)
        {
            $response["Data"] = $add_types;
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "record does not found";

            return response(
                $response,
                200
            );

        }
        return $response;
    }

    // get address by user id and address type id
    public function getAddressByUserAddType(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // validate request
         $validator = Validator::make($request->all(), [
            "userId"    => "required",
            "addTypeId" => "required"
        ]);

        if ($validator->fails())
        {
            $response["Success"]   = false;
            $response["Status"] = 400;
            foreach ($validator->errors()->all() as $message) {
                $response["ErrorMessage"] .= $message;
            }

            return response(
                $response,
                400
            );
        }

        // get data from service
        $adds = $this->delivery_detail_service->getAddressByUserIdAddTypeId($request->userId,$request->addTypeId);

        if($adds)
        {
            $response["Data"] = $adds;
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "record does not found";

            return response(
                $response,
                200
            );

        }
        return $response;
    }
    // insert pos order payment

    public function insertPosOrderPayment(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // validate request
        $validator = Validator::make($request->all(), [
            "paymentLaterOrderId" => "required"
        ]);

        if ($validator->fails())
        {
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = $validator->errors();

            return response(
                $response,
                200
            );
        }

        // db request via service
        $order = $this->order_service->getById($request->paymentLaterOrderId);


        if($order)
        {

            $order->total = $request->total;
            $order->sub_total = $request->sub_total;
            $order->amount_paid = $request->amount_paid;
            $order->amount_return = $request->amount_return;
            $order->discount_per = $request->discount_per;
            $order->discount_amount = $request->discount_amount;
            $order->tax_percent = $request->tax_percent;
            $order->tax_amount = $request->tax_amount;
            $order->tax_on_original_amt = $request->tax_on_original_amt;
            $order->tax_include = $request->tax_include;
            $order->delivery_charge = $request->del_chg;
            $order->service_charge = $request->ser_chg;
            $order->status_id = 2;
            $order->updatedby_id = $request->login_user_id;
            $order = $this->order_service->setOrerStatusTime($order->toArray());

            $response["Data"] =  $this->order_service->save($order);
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "Error: no record found!";

            return response(
                $response,
                200
            );

        }

        return $response;
    }


    public function updateBranchDailyCount(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // get data from service
        $status = $this->restaurant_service->updateBranchDailyCount();

        if($status)
        {
            $response["Data"] = $status;
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "record does not found";

            return response(
                $response,
                200
            );

        }
        return $response;
    }

//======================= Pos Cash Flow ===================================//

    public function getViewPosCashFlow(Request $request)
    {
        $login_user = Auth::guard('admin_user')->User();

        $branches = $this->restaurant_service->getAllBranches( $login_user);

        return view('pos.reconcile-drawer',compact('branches'));

    }

    public function getPosCashFlow(Request $request)
    {
        $login_user = Auth::guard('admin_user')->User();

        $branch = $this->restaurant_service->getBranchById($request->branch_id);

        // get data from service
        $pos_cash_data = $this->pos_service->getPosCashFlow($request->branch_id,$request->datee);

        $sales = $this->order_service->getOrderSumByTypeAndDateRange($request->datee,$request->datee,$login_user );



        return view('pos.reconcile-drawer-subview',compact('pos_cash_data','sales','branch'));

    }

    public function storePosCashFlow(Request $request)
    {
        $cashout = [];
        $cashin = [];
        $cash_detail = ["Couter1" => []];

        // cash in items
        foreach ($request->cashin as $value)
        {
            $cashin[$value["cashin_item"]] = $value["amt"];
        }

        // cash out items
        foreach ($request->cashout as $value)
        {
            $cashout[$value["cashout_item"]] = $value["amt"];
        }

        // cash detail or currency detail
        foreach ($request->cash_detail as $value)
        {
            $cash_detail["Counter1" ][$value["cash_value"]] = $value["qty"];

        }

        $obj = [];


        $obj["dinein_sale"]     = isset($request->sales[0]["amt"]) && $request->sales[0]["sale_item"] == "Dine in" ? $request->sales[0]["amt"]:0;
        $obj["takeaway_sale"]   = isset($request->sales[1]["amt"]) && $request->sales[1]["sale_item"] == "Take away" ? $request->sales[1]["amt"]:0;
        $obj["delivery_sale"]   = isset($request->sales[2]["amt"]) && $request->sales[2]["sale_item"] == "Delivery" ? $request->sales[2]["amt"]:0;

        $obj["cash_in_items"] = serialize($cashin);
        $obj["cash_out_items"] = serialize($cashout);
        $obj["cash_detail"] = serialize($cash_detail);
        $obj["branch_id"] = $request->branchid;

        return $this->pos_service->savePosCashFlow($obj);

    }
//============================= Variation and Choices

    public function getMenuVariationAndChoicesByMenuId(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // validate request
        $validator = Validator::make($request->all(), [
            "menu_id" => "required",
        ]);

        if ($validator->fails())
        {
            $response["Success"]   = false;
            $response["Status"] = 400;
            $response["ErrorMessage"] = $validator->errors();

            return response(
                $response,
                200
            );
        }

        // get data from service by menu id
        $data = $this->menu_category_service->getMenuVariationAndChoicesByMenuId($request->menu_id);

        if($data)
        {
            $response["Data"] = $data;
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "record does not found";

            return response(
                $response,
                200
            );

        }
        return $response;
    }


}

