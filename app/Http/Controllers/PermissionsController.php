<?php

namespace App\Http\Controllers;

use App\Services\Concrete\AdminUserService;
use App\Services\Concrete\RestaurantService;
use App\Models\Permission;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;

class PermissionsController extends Controller
{
    protected $admin_user_service;
    protected $restaurant_service;

    public function __construct(AdminUserService  $admin_user_service,RestaurantService $restaurant_service)
    {
        $this->admin_user_service = $admin_user_service;
        $this->restaurant_service=$restaurant_service;
    }
    public function index(Request $request)
    {
        // abort_if(Gate::denies('permission_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
        $draw   = $request->get('draw');
        $start  = $request->get('start');
        $length = $request->get('length');
        $search = (isset($request->search['value']))? $request->search['value'] : false;
        $login_user = Auth::guard('admin_user')->User();
        return $this->admin_user_service->getAdminUsersPermissionsDatatableSource($draw,$start ,$length ,$search,$login_user);
        // dd($permissions);
        // $permissions = Permission::all();
        // $i=0;
        // foreach($permissions as $item)
        // {

        //     $resp[$i][0] = $item->title;

        //     $edit_column    = "<a class='text-success mr-2' href='".route('permissions.edit',$item->id)."'><i title='Edit' class='nav-icon mr-2 i-Pen-2'></i>Edit</a>";
        //     $view_column    = "<a class='text-warning mr-2' href='".url('permissions.show',$item->id)."'><i title='View' class='nav-icon mr-2 i-Full-View-Window'></i>View</a>" ;
        //     $action_column =    $edit_column.$view_column ;

        //     $resp[$i][1] = $action_column;
        //     $i +=1;
        // }

        

        // return json_encode($data,true);

        return view('permissions.list-permissions', compact('permissions'));
    }

    public function create()
    {
        // abort_if(Gate::denies('permission_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('permissions.create');
    }

    public function store(Request $request)
    {
        $permission = Permission::create($request->all());

        return redirect('list-permissions');
    }

    public function edit($id)
    {
        // abort_if(Gate::denies('permission_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $permission = Permission::find($id);
        return view('permissions.create', compact('permission'));
    }

    public function update(Request $request, $id)
    {
        $permission = Permission::find($id);
        $permission->update($request->all());

        return redirect('list-permissions');
    }

    public function show($id)
    {
        // abort_if(Gate::denies('permission_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $permission = Permission::find($id);
        return view('permissions.view', compact('permission'));
    }

    public function destroy(Permission $permission)
    {
        abort_if(Gate::denies('permission_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $permission->delete();

        return back();
    }

    public function massDestroy(Request $request)
    {
        Permission::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
