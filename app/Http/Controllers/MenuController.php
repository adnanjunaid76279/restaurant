<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Services\Concrete\MenuCategoryService;
use App\Services\Concrete\RestaurantService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use App\Menu;
use DB;

class MenuController extends Controller
{

    // space where we declare services
    protected $menu_category_service;
    protected $restaurant_service;


    /**
     * Constructor
    */
    public function __construct(MenuCategoryService $menu_category_service,RestaurantService $restaurant_service)
    {
        // set the model
        $this->menu_category_service  = $menu_category_service;
        $this->restaurant_service  = $restaurant_service;
    
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->get('start');
        $length = $request->get('length');
        $search = (isset($request->search['value']))? $request->search['value'] : false;
        $login_user = Auth::guard('admin_user')->User();
        return $this->menu_category_service->getMenuDatatableSource($draw,$start ,$length ,$search,$login_user);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $login_user = Auth::guard('admin_user')->User();
        $menu_choice_group =  $this->menu_category_service->getChoiceGroups($login_user);
        $choice_groups =   $this->menu_category_service->getChoiceGroups($login_user);

        // selection criteria of menu with choice group
        foreach ($menu_choice_group as $value) 
            $value->checked = "";
        
        switch($login_user ->role_id)
        {
            case '1':
                return view('menus.menu-form');
            break;

            case '2':
                $branch =  $this->restaurant_service->getBranchesByRestaurantId($login_user->userRestaurant->restaurant_id);
                $category   =  $this->menu_category_service->getCategoriesByBranchId($branch[0]->id);
                return view('menus.menu-form',compact('branch','category','menu_choice_group','choice_groups'));

            break;

            case '3':
               
                $branch[] =  $this->restaurant_service->getBranchById($login_user->userRestaurant->branch_id);
                $category   =  $this->menu_category_service->getCategoriesByBranchId($login_user->userRestaurant->branch_id);
                return view('menus.menu-form',compact('branch','category','menu_choice_group','choice_groups'));
            
            break;
            
        }

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        $this->validate($request, [
            'name'      =>'required',
            'category'  =>'required',
            'price'     =>'required'
        ]);


        $branch_id = $request->branch;
        $category_id = $request->category;
        
        $validator = Validator::make($request->all() , [
            'name' => [
                'required',
                Rule::unique('menu')->where(function($query)use($category_id) {
                    return $query->where('menu_category_id', '=',$category_id);
                                
                }),
            'branch' =>'required'    
            ]
        ]);

        if ($validator->fails()) 
        {
            $response["Success"]   = false;
            $response["Status"] = 400;

            foreach ($validator->errors()->all() as $message) {
                $response["ErrorMessage"] .= $message;
            }
            
            return response(
                $response,
                200
            );
        }

        $obj = [
            'name'              => strtoupper($request->name),
            'menu_category_id'  => $request->category,
            'price'             => $request->price,
            'image'             => $request->image??null,
            'description'       => isset($request->description)?$request->description:null

        ];

        
        $menu = $this->menu_category_service->saveMenu($obj);
        $menu_id = $menu->id;
       
        
        // Menu Choice Group Save
        if(isset($request->menu_ch_grp_ids))
        {
            $menu_ch_grp_arr = $request->menu_ch_grp_ids;
            foreach ($menu_ch_grp_arr as $value) 
            {
                if($value == "")
                    continue;

                $obj_men_ch_grp = [
                    'menu_id' =>  $menu_id,
                    'choice_group_id'   =>  $value['id'],
                    'sorting'   =>  $value['sorting'],

                ];
                
                $menu_ch_grp = $this->menu_category_service->saveMenuChoiceGroup($obj_men_ch_grp);  // save menu_var_cho_grp
            }

        }
        //End Menu Choice Group Save
        

        if(isset($request->variation))
        {
            foreach ($request->variation as $key => $value) 
            {
                
               $obj_vari =  [
                    'name'     => $value['variname'],
                    'sorting'  => $value['sorting'],
                    'price'    => $value['price'],
                    'menu_id'  => $menu_id
        
                ];
                $menu_var = $this->menu_category_service->saveMenuVariation($obj_vari);
                $saved_menu_var_id = $menu_var->id;
                // Choice group against variation save
        
                $cho_grp_arr = isset($value['choice_grp_ids'])?$value['choice_grp_ids']:[];
               
                foreach ($cho_grp_arr as $value) 
                {
                    if($value == "")
                        continue;

                    $obj_men_var_ch_grp = [
                        'menu_variation_id' =>  $saved_menu_var_id,
                        'choice_group_id'   =>  $value['id'],

                    ];
                    
                    $menu_var_ch_grp = $this->menu_category_service->saveMenuVariationChoiceGroup($obj_men_var_ch_grp);  // save menu_var_cho_grp
                }
                //End Choice group against variation save
            }

            
        }

        return response(
            $menu ->id,
            200
        );

        // return view('menus.menu-view',compact('menu'))->with('success',$menu ->name.' Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
 
        $login_user = Auth::guard('admin_user')->User();
        $menu = $this->menu_category_service->getMenuByIdAndUserRole($id,$login_user );

        if($menu)
            return view('menus.menu-view',compact('menu','menu'));
        else
            return  redirect('list-menus');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $login_user = Auth::guard('admin_user')->User();
        $menu = $this->menu_category_service->getMenuByIdAndUserRole($id,$login_user );
        
        $category   =  $this->menu_category_service->getCategoriesByBranchId($menu->menuCategory->restaurantBranch->id);
        $branch[]     =  $this->restaurant_service->getBranchById($menu->menuCategory->restaurantBranch->id);
        $menu_choice_group =  $this->menu_category_service->getChoiceGroups($login_user);
        $choice_groups =   $this->menu_category_service->getChoiceGroups($login_user);;

           // selection criteria of menu with choice group
           foreach ($menu_choice_group as $value) {
            $value->checked = "";
            $value->sorting = 100000; // set a default value of sorting
            foreach ($menu->choiceGroups as $key => $grp) {
                
                if($value->id == $grp->id)
                {
                    $value->checked = "checked";
                    $value->sorting = $key+1;
                }            
            }
        }
        $menu_choice_group = collect($menu_choice_group)->sortBy('sorting')->toArray();

        return view('menus.menu-form',compact('menu','category','branch','menu_choice_group','choice_groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name'      =>'required',
            'category'  =>'required',
            'price'     =>'required'
        ]);
        
        $menu_id      = $request->id;
        $category_id  = $request->category;

        $validator = Validator::make($request->all() , [
            'name' => [
                'required',
                Rule::unique('menu')->where(function($query)use($menu_id,$category_id) {
                    return $query->where('menu_category_id', '=',$category_id)
                                 ->where('id', '<>',$menu_id );
                                
                }),
            'branch' =>'required'    
            ]
        ]);

        if ($validator->fails()) 
        {
            $response["Success"]   = false;
            $response["Status"] = 400;
            foreach ($validator->errors()->all() as $message) {
                $response["ErrorMessage"] .= $message;
            }
            
            return response(
                $response,
                200
            );
        }

        $obj = [
            'id'                => $request->id,
            'name'              => strtoupper($request->name),
            'menu_category_id'  => $request->category,
            'price'             => $request->price,
            'image'             => $request->image??null,
            'ready_made'        => isset($request->ready_made)?1:0,
            'kitchen_id'        => $request->kitchen??null,
            'ingridient'        => isset($request->ingredient)?$request->ingredient:null,
            'description'       => isset($request->description)?$request->description:null
        ];

        
        $menu = $this->menu_category_service->saveMenu($obj);

        $menu_id = $obj['id']; 
        
        // Menu Choice Group Save
        $this->menu_category_service->removeMenuChoiceByChoiceGroupId($menu_id); // remove menu choice group by menu id
        
        if(isset($request->menu_ch_grp_ids))
        {
            // $menu_ch_grp_arr = explode(',', $request->menu_ch_grp_ids);
            $menu_ch_grp_arr = $request->menu_ch_grp_ids;
            foreach ($menu_ch_grp_arr as $value) 
            {
                if($value == "")
                    continue;

                $obj_men_ch_grp = [
                    'menu_id' =>  $menu_id,
                    'choice_group_id'   =>  $value['id'],
                    'sorting'   =>  $value['sorting']

                ];
                
                $menu_ch_grp = $this->menu_category_service->saveMenuChoiceGroup($obj_men_ch_grp);  // save menu_var_cho_grp
            }

        }
        //End Menu Choice Group Save
        
        // Variation Save
        $this->menu_category_service->removeVariationByMenuId($menu_id);   // remove variatoins against menu
        
        if($request->variation)
        {
            foreach ($request->variation as $key => $value) {
                
                $obj_vari =  [
                    'id'       => 0,
                    'name'     => $value['variname'],
                    'sorting'  => $value['sorting'],
                    'price'    => $value['price'],
                    'menu_id'  => $menu_id
        
                ];
                $menu_var = $this->menu_category_service->saveMenuVariation($obj_vari);
                $saved_menu_var_id = $menu_var->id;
                // Choice group against variation save
               
                if(isset($value['choice_grp_ids']))
                {
                    foreach ($value['choice_grp_ids'] as $val) 
                    {
                        if($val == "")
                            continue;

                        $obj_men_var_ch_grp = [
                            'menu_variation_id' =>  $saved_menu_var_id,
                            'choice_group_id'   =>  $val['id'],
                            'sorting'           =>  $val['sorting'],

                        ];
                        
                        $menu_var_ch_grp = $this->menu_category_service->saveMenuVariationChoiceGroup($obj_men_var_ch_grp);  // save menu_var_cho_grp
                    }
                }
                //End Choice group against variation save
                
            }
        }
        //End Variation Save

        
       return response(
        $request->id,
        200
        );

        return  redirect('/view-menu/'.$request->id)->with('success',$obj['name'].' Updated');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function enableMenu(Request $request)
    {
        $this->validate($request, [
            'menu_id'    =>'required',
            'is_active'  =>'required'
        ]);

        $obj = [
            'id'         => $request->menu_id,
            'is_deleted' => $request->is_active

        ];

        $menu = $this->menu_category_service->saveMenu($obj);

        if($request->is_active)
            $msg = $menu->name." is deactive ";
        else
            $msg = $menu->name." is active ";

        return response(["msg" => $msg], 200);
    }

    /**
     * Get Categories by Branch Id
     *
     * @return \Illuminate\Http\Response
     */
    public function getCategoriesByBranchId(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        
        // validate request
        $this->validate($request, [
            'id'  =>'required'
        ]);

        $categories = $this->menu_category_service->getCategoriesByBranchId($request->id);
       
        if($categories)
        {
            $response["Data"] =  $categories;
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 422;
            $response["ErrorMessage"] = "record not found";
            
            return response(
                $response,
                422
            );    

        }
        
        return $response;
    }
}
