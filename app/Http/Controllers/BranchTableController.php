<?php

namespace App\Http\Controllers;


use App\BranchTable;
use App\Services\Concrete\RestaurantService;
use Illuminate\Http\Request;
use App\Services\Concrete\PosService;
use Illuminate\Support\Facades\Auth;

class BranchTableController extends Controller
{

    protected $pos_service;
    protected $restaurant_service;

    public function __construct(PosService $pos_service,RestaurantService $restaurant_service )
    {
        $this->pos_service = $pos_service;
        $this->restaurant_service  = $restaurant_service;

    }

    public function index(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->get('start');
        $length = $request->get('length');
        $search = (isset($request->search['value']))? $request->search['value'] : false;
        $login_user = Auth::guard('admin_user')->User();
        return $this->pos_service->getBranchTableDatatableSource($draw,$start ,$length ,$search,$login_user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $login_user = Auth::guard('admin_user')->User();

        switch($login_user->role_id)
        {
            case '1':
                return view('branch-tables\branch-table-form');
                break;

            case '2':
            case '3':

                $branch =  $this->restaurant_service->getAllBranches($login_user);
                return view('branch-tables\branch-table-form',compact('branch'));
                break;

        }

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate_items =  [
            'branch'           =>'required',
            'table_no'        =>'required',
        ];


        $this->validate($request,$validate_items);
        if($request->availability == "on"){
            $request->availability = 1;
        }else{
            $request->availability = 0;
        }

        $obj = [
            "branch_id"                   => $request->branch,
            "table_no"                  => $request->table_no,
            "description"              => $request->description,
            "availability"          => $request->availability,
            "capacity"           => $request->capacity,
        ];

        $branch_table = $this->pos_service->saveBranchTable($obj);


        return view('branch-tables.branch-table-view',compact('branch_table'))->with('success',$branch_table->table_no.' Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $login_user = Auth::guard('admin_user')->User();
        $branch_table = $this->pos_service->getBranchTableByIdAndUserRole($id,$login_user );

        if($branch_table)
            return view('branch-tables.branch-table-view',compact('branch_table'));
        else
            return  redirect('list-branch-tables');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $login_user = Auth::guard('admin_user')->User();
        $branch_table = $this->pos_service->getBranchTableByIdAndUserRole($id,$login_user );
        $branch[]     =  $this->restaurant_service->getBranchById($branch_table->branch_id);
        return view('branch-tables\branch-table-form',compact('branch_table','branch'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $validate_items =  [
            'branch'           =>'required',
            'table_no'        =>'required',
            'id'             =>'required'
        ];

        $this->validate($request,$validate_items);

        if($request->availability == "on"){
            $request->availability = 1;
        }else{
            $request->availability = 0;
        }

        $obj = [
            'id'                  => $request->id,
            "branch_id"                   => $request->branch,
            "table_no"                  => $request->table_no,
            "description"              => $request->description,
            "availability"          => $request->availability,
            "capacity"           => $request->capacity,
        ];

        $this->pos_service->saveBranchTable($obj);

        return  redirect('view-branch-table/'.$request->id)->with('success',$obj['table_no'].' Updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd($id);
        $branch_table = BranchTable::find($id);
        $branch_table->delete();
        return  redirect('list-branch-tables')->with('success',' Updated');
    }
}
