<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Services\Concrete\MenuCategoryService;
use App\Services\Concrete\RestaurantService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;


class MenuCategoryController extends Controller
{

    // space where we declare services
    protected $menu_category_service;
    protected $restaurant_service;


    /**
     * Constructor
    */
    public function __construct(MenuCategoryService $menu_category_service,RestaurantService $restaurant_service)
    {
        // set the model
        $this->menu_category_service  = $menu_category_service;
        $this->restaurant_service  = $restaurant_service;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->get('start');
        $length = $request->get('length');
        $search = (isset($request->search['value']))? $request->search['value'] : false;
        $login_user = Auth::guard('admin_user')->User();

        return  $this->menu_category_service->getDatatableSource($draw,$start ,$length ,$search,$login_user);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $login_user = Auth::guard('admin_user')->User();


        switch($login_user->role_id)
        {
            case '1':
                return view('menu-categories.category-form');
            break;

            case '2':
                $branch =  $this->restaurant_service->getAllBranches($login_user);
                return view('menu-categories.category-form',compact('branch'));

            break;

            case '3':

                $branch =  $this->restaurant_service->getAllBranches($login_user);
                return view('menu-categories.category-form',compact('branch'));
            break;

        }
    }

    /**
     * Search retaurants by name
     *
     * @return \Illuminate\Http\Response
     */
    public function searchRestaurantsByName(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        $this->validate($request, [
            'name'  =>'required'
        ]);


        $restaurants = $this->restaurant_service->searchRestaurantsByName($request->name);

        if($restaurants)
        {
            $response["Data"] =  $restaurants;
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 422;
            $response["ErrorMessage"] = "record not found";

            return response(
                $response,
                422
            );

        }

        return $response;

    }

     /**
     * Search retaurants by name
     *
     * @return \Illuminate\Http\Response
     */
    public function getBranchesByRestaurantId(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // validate request
        $this->validate($request, [
            'id'  =>'required'
        ]);

        $branches = $this->restaurant_service->getBranchesByRestaurantId($request->id);

        if($branches)
        {
            $response["Data"] =  $branches;
        }
        else
        {

            $response["Success"]   = false;
            $response["Status"] = 422;
            $response["ErrorMessage"] = "record not found";

            return response(
                $response,
                422
            );

        }

        return $response;
    }

    public function saveImage(Request $request)
    {

        if ($request->hasFile('file'))
        {

        $uploadedFile = $request->file('file');
        $filename = time().$uploadedFile->getClientOriginalName();


        $response = $request->file->move(public_path('/Temp/'.$request->branchId.'/images/'), $filename);

        // $response = Storage::disk('public')->putFileAs(
        //     'images/',
        //     $uploadedFile,
        //     $filename
        // );
        }

        if($response)
            return $filename;
        else
            return false;


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name'           =>'required',
            'branch'         =>'required'
        ]);

        $branch_id = $request->branch;
        $validator = Validator::make($request->all() , [
            'name' => [
                'required',
                Rule::unique('menu_category')->where(function($query)use($branch_id) {
                    return $query->where('restaurant_branch_id', '=',$branch_id);
                }),
            'branch' =>'required'
            ]
        ]);

        if ($validator->fails())
        {
            return redirect('create-category')
                        ->withErrors($validator)
                        ->withInput();
        }

        $obj = [
            'name'                  => $request->name,
            'restaurant_branch_id'  => $request->branch,
            'image'                 => $request->image??null,
            'is_active'             => true
        ];

        $category = $this->menu_category_service->saveCategory($obj);

        return view('menu-categories.category-view',compact('category'))->with('success',$category ->name.' Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $login_user = Auth::guard('admin_user')->User();

        $category = $this->menu_category_service->getCategoryByIdAndUserRole($id,$login_user );

        if($category)
            return view('menu-categories.category-view',compact('category','category'));
        else
            return  redirect('list-categories');


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $login_user = Auth::guard('admin_user')->User();
        
        $category = $this->menu_category_service->getCategoryByIdAndUserRole($id,$login_user);
        
        $branch[]     =  $this->restaurant_service->getBranchById($category->restaurantBranch->id);
        
        return view('menu-categories.category-form',compact('category','branch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name'           =>'required',
            'branch'         =>'required',
            'id'             =>'required'
        ]);

        $branch_id      = $request->branch;
        $category_id    = $request->id;
        $validator = Validator::make($request->all() , [
            'name' => [
                'required',
                Rule::unique('menu_category')->where(function($query)use($branch_id,$category_id) {
                    return $query->where('restaurant_branch_id', '=',$branch_id)
                                ->where('id', '<>',$category_id );

                }),
            'branch' =>'required'
            ]
        ]);

        if ($validator->fails())
        {
            return redirect('edit-category/'.$category_id )
                        ->withErrors($validator)
                        ->withInput();
        }

        $obj = [
            'id'                  => $request->id,
            'name'                  => $request->name,
            'restaurant_branch_id'  => $request->branch,
            'image'  => $request->image??null,

        ];

        $this->menu_category_service->saveCategory($obj);

        return  redirect('/view-category/'.$request->id)->with('success',$obj['name'].' Updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function enableCategory(Request $request)
    {
        $this->validate($request, [
            'cat_id'     =>'required',
            'is_active'  =>'required'
        ]);

        $obj = [
            'id'         => $request->cat_id,
            'is_active'  => $request->is_active,

        ];

        $menu_category = $this->menu_category_service->saveCategory($obj);

        if($request->is_active)
            $msg = $menu_category->name." is enabled ";
        else
            $msg = $menu_category->name." is disabled ";

        return response(["msg" => $msg], 200);
    }
}
