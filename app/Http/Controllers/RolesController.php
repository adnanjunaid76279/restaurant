<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use Gate;
use Illuminate\Http\Request;
use App\Services\Concrete\AdminUserService;
use App\Services\Concrete\RestaurantService;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class RolesController extends Controller
{
    protected $admin_user_service;
    protected $restaurant_service;

    public function __construct(AdminUserService  $admin_user_service,RestaurantService $restaurant_service)
    {
        $this->admin_user_service = $admin_user_service;
        $this->restaurant_service=$restaurant_service;
    }
    public function index(Request $request)
    {
        // abort_if(Gate::denies('permission_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
        // $draw   = $request->get('draw');
        // $start  = $request->get('start');
        // $length = $request->get('length');
        // $search = (isset($request->search['value']))? $request->search['value'] : false;
        // $login_user = Auth::guard('admin_user')->User();
        // return $this->admin_user_service->getAdminUsersRolesDatatableSource($draw,$start ,$length ,$search,$login_user);
        $roles = Role::all();

        return view('roles.index', compact('roles'));
        
    }

    public function create()
    {
        // abort_if(Gate::denies('role_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $permissions = Permission::all()->pluck('title', 'id');

        return view('roles.create', compact('permissions'));
    }

    public function store(Request $request)
    {
        $role = Role::create($request->all());
        $role->permissions()->sync($request->input('permissions', []));

        return redirect()->route('roles.index');
    }

    public function edit(Role $role)
    {
        // abort_if(Gate::denies('role_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $permissions = Permission::all()->pluck('title', 'id');

        $role->load('permissions');

        return view('roles.create', compact('permissions', 'role'));
    }

    public function update(Request $request, Role $role)
    {
        $role->update($request->all());
        $role->permissions()->sync($request->input('permissions', []));

        return redirect()->route('roles.index');
    }

    public function show(Role $role)
    {
        abort_if(Gate::denies('role_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $role->load('permissions');

        return view('roles.view', compact('role'));
    }

    public function destroy(Role $role)
    {
        abort_if(Gate::denies('role_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $role->delete();

        return back();
    }

    public function massDestroy(Request $request)
    {
        Role::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
