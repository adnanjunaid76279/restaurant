<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Services\Concrete\DeliveryUserService;
use App\Services\Concrete\RestaurantService;
use App\Services\Concrete\AdminUserService;



class DeliveryUserController extends Controller
{
    protected $delivery_user_service;
    protected $restaurant_service;
    protected $admin_user_service;


    public function __construct(AdminUserService  $admin_user_service,DeliveryUserService  $delivery_user_service,RestaurantService $restaurant_service)
    {
        $this->delivery_user_service = $delivery_user_service;
        $this->restaurant_service = $restaurant_service;
        $this->admin_user_service = $admin_user_service;
    }

    public function create()
    {
        $login_user = Auth::guard('admin_user')->User();
        $roles = $this->admin_user_service->getRolesByLoginUser($login_user);
        $branches =  $this->restaurant_service->getAllBranches($login_user);
        
        return view('delivery-users.delivery-users-form',compact('branches'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'firstname'        =>'required',
            'lastname'         =>'required',
            'login'            =>'unique:delivery_user,login',
            'password'         =>'required',
            'cell_num'         =>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:11|max:13',
            'branch'           =>'required',
        ]);

        $obj=[
            'first_name'    =>  $request->firstname,
            'last_name'     =>  $request->lastname,
            'cell_num'      =>  $request->cell_num,
            'login'         =>  $request->login,
            'password'      =>  $request->branch,
            'branchId'      =>  $request->branch
        ];

        $store= $this->delivery_user_service->save($obj);
        
        return redirect('view-rider/'.$store->id);  
    }
    
    public function index(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->get('start');
        $length = $request->get('length');
        $search = (isset($request->search['value']))? $request->search['value'] : false;
        $login_user = Auth::guard('admin_user')->User();

        return $this->delivery_user_service->getDeliveryUsersDatatableSource($draw,$start ,$length ,$search,$login_user );
    }

    public function edit($id)
    {
        $login_user = Auth::guard('admin_user')->User();
        $branches =  $this->restaurant_service->getAllBranches($login_user);
        
        $daily_user_service_data = $this->delivery_user_service->getDeiveryUserById($id);
        
        return view('delivery-users.delivery-users-form',compact('daily_user_service_data','branches'));

    }

    public function show($id)
    {
       $daily_user_service_data = $this->delivery_user_service->getDeiveryUserById($id);
       return view('delivery-users.delivery-users-view',compact('daily_user_service_data'));
    }

    public function update(Request $request)
    {
        
        $this->validate($request, [
            'id'            =>'required',
            'firstname'     =>'required',
            'lastname'      =>'required',
            'login'         =>'unique:delivery_user,login,',
            'password'      =>'required',
            'cell_num'      =>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:11|max:13',
            'branch'        =>'required',
        ]);

        $obj=[
             
            'id'            =>  $request->id,
            'first_name'    =>  $request->firstname,
            'last_name'     =>  $request->lastname,
            'cell_num'      =>  $request->cell_num,
            'login'         =>  $request->login,
            'password'      =>  $request->password,
            'branchId'      =>  $request->branch
        ];

        $this->delivery_user_service->save($obj);
        
        return redirect('view-rider/'.$request->id);  
    }


}
