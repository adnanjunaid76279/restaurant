<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Services\Concrete\AdminUserService;
use App\Services\Concrete\RestaurantService;


class AdminUserController extends Controller
{
    protected $admin_user_service;
    protected $restaurant_service;

    public function __construct(AdminUserService  $admin_user_service,RestaurantService $restaurant_service)
    {
        $this->admin_user_service = $admin_user_service;
        $this->restaurant_service=$restaurant_service;
    }
    
    public function index(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->get('start');
        $length = $request->get('length');
        $search = (isset($request->search['value']))? $request->search['value'] : false;
        $login_user = Auth::guard('admin_user')->User();
        
        return $this->admin_user_service->getAdminUsersDatatableSource($draw,$start ,$length ,$search,$login_user);
      
    }

    public function create()
    {
        $login_user = Auth::guard('admin_user')->User();
        $roles = $this->admin_user_service->getRolesByLoginUser($login_user);


        switch($login_user ->role_id)
        {
            case '1':
            case '3':
                return view('admin-users.admin-user-form',compact('roles'));
            break;

            case '2':
                $branches =  $this->restaurant_service->getAllBranches($login_user);
                return view('admin-users.admin-user-form',compact('branches','roles'));

            break;
            
        }


        return view('admin-users.admin-user-form',compact('roles'));
    }

    public function show($id)
    {
        $admin_users = $this->admin_user_service->getById($id);
        
        if($admin_users)
            return view('admin-users.admin-user-view',compact('admin_users'));
        else
            return  redirect('list-admin-users');
    }


    public function edit($id)
    {
        $login_user = Auth::guard('admin_user')->User();
        $admin_users = $this->admin_user_service->getById($id);
        $roles = $this->admin_user_service->getRolesByLoginUser($login_user);
        $branches =  $this->restaurant_service->getAllBranches($admin_users);


        return view('admin-users.admin-user-form',compact('admin_users','roles','branches'));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'role'          =>'required',
            'login'         =>'required|email|unique:admin_users',
            'firstname'     =>'required',
            'lastname'      =>'required',
            'cell_num'      =>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:11|max:13',
            'password'      =>'required',
        ]);

        $obj = [
          
            'login'         =>  $request->login,
            'first_name'    =>  $request->firstname,
            'last_name'     =>  $request->lastname,
            'cell_num'      =>  $request->cell_num,
            'password'      =>  $request->password,
            'role_id'       =>  $request->role,
            'createdby_id'  =>  Auth::guard('admin_user')->User()->id
        ];

        $admin_user = $this->admin_user_service->save($obj);
        

        if(Auth::guard('admin_user')->User()->role_id == 2)
            $request->restaurantid = Auth::guard('admin_user')->User()->userRestaurant->restaurant_id;

        if(Auth::guard('admin_user')->User()->role_id == 1)
            $request->restaurantid = $request->restaurantid;


        $user_restaurant = [
          
            'user_id'       =>  $admin_user->id,
            'branch_id'     =>  isset($request->branch) ? $request->branch : NULL,
            'restaurant_id' =>  $request->restaurantid ?? NULL,
            'createdby_id'  =>  Auth::guard('admin_user')->User()->id
        ];
      
        $data = $this->admin_user_service->saveUserRestaurant($user_restaurant);

        return redirect('view-admin-user/'.$admin_user->id);
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'id'        =>'required',
            'role'      =>'required',
            'login'     =>'required',
            'password'  =>'required',
            'firstname' =>'required',
            'lastname'  =>'required',
        ]);

        $obj = [
            'id'            =>  $request->id,
            'login'         =>  $request->login,
            'first_name'    =>  $request->firstname,
            'last_name'     =>  $request->lastname,
            'cell_num'      =>  $request->cell_num,
            'password'      =>  $request->password,
            'role_id'       =>  $request->role,
            'updatedby_id'  =>  Auth::guard('admin_user')->User()->id,
            'createdby_id'  =>  Auth::guard('admin_user')->User()->id
        ];

        $admin_user = $this->admin_user_service->save($obj);
        $admin_user = $this->admin_user_service->getById($obj['id']);


        $user_restaurant = [
          
            'id'            =>  $admin_user->userRestaurant->id,
            'user_id'       =>  $request->id,
            'branch_id'     =>  $request->branch ?? NULL,
           
            'updatedby_id'  =>  Auth::guard('admin_user')->User()->id,
            'createdby_id'  =>  Auth::guard('admin_user')->User()->id
        ];

        if(Auth::guard('admin_user')->User()->role_id == 1)
            $user_restaurant['restaurant_id'] = $request->restaurantid ?? NULL;
      
        $data = $this->admin_user_service->saveUserRestaurant($user_restaurant);


        return  redirect('view-admin-user/'.$request->id)->with('success',$obj['first_name'].' Updated');
    }
 


}
