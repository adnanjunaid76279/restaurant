<?php

namespace App\Http\Controllers;

use App\Services\Concrete\RestaurantService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Services\Concrete\UserService;
use App\Services\Concrete\AgentUserService;
use App\Services\Concrete\OrderService;
use App\Services\Concrete\ReportService;
use App\Notifications;
use DB;

class HomeController extends Controller
{

    protected $user_service;
    protected $agent_user_service;
    protected $order_service;
    protected $report_service;
    protected $restaurant_service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserService $user_service,
    OrderService $order_service,
    ReportService $report_service,
    RestaurantService $restaurant_service,AgentUserService $agent_user_service)
    {
        //$this->middleware('auth');
        $this->user_service = $user_service;
        $this->agent_user_service = $agent_user_service;
        $this->order_service = $order_service;
        $this->report_service = $report_service;
        $this->restaurant_service  = $restaurant_service;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $login_user = Auth::guard('admin_user')->User();
        $branches = $this->restaurant_service->getAllBranches($login_user);
        return view('dashboard.dashboardv1',compact('branches'));
    
    }

    public function getDashboardStatsByBranchId(Request $request){


        $branch_id = $request->id;

        $login_user         = Auth::guard('admin_user')->User();
        $today_date         = date('Y-m-d');
        $yesterday_date     = date('Y-m-d', strtotime('-1 day', strtotime($today_date)));
        $month_first_day    = date('Y-m-01', strtotime($today_date));
        $month_last_day     = date('Y-m-t', strtotime($today_date));
        $year_first_day     = date('Y-01-01', strtotime($today_date));
        $year_last_day      = date('Y-12-31', strtotime($today_date));
        $current_monday     = date("Y-m-d", strtotime('monday this week', strtotime($today_date)));
        $current_sunday     = date("Y-m-d", strtotime('sunday this week', strtotime($today_date)));
        $pre_monday         = date("Y-m-d", strtotime('monday this week', strtotime('-7 days',strtotime($today_date))));
        $pre_sunday         = date("Y-m-d", strtotime('sunday this week', strtotime('-7 days',strtotime($today_date))));



        $pos_orders = $this->order_service->getOrderCountByResourceAndDateRange($current_monday,$current_sunday,$branch_id,$login_user)->firstWhere('order_resource', '=', 4);
        $pos_orders = is_null($pos_orders)?0:$pos_orders->count;
        $mobile_orders = $this->order_service->getOrderCountByResourceAndDateRange($current_monday,$current_sunday, $branch_id,$login_user)->firstWhere('order_resource', '=', 3);
        $mobile_orders = is_null($mobile_orders)?0:$mobile_orders->count;
        $website_orders = $this->order_service->getOrderCountByResourceAndDateRange($current_monday,$current_sunday, $branch_id,$login_user)->firstWhere('order_resource', '=', 1);
        $website_orders = is_null($website_orders)?0:$website_orders->count;

        $avg_prepare_time = $this->order_service->getAveragePreparationTimeByDay($today_date, $branch_id,$login_user);


        $dashboard = [];

        $dashboard['Stats'] = [
            'todayusers'        => $this->user_service->getUsersCountByDate($today_date, $branch_id,$login_user),
            'yesterdayusers'    => $this->user_service->getUsersCountByDateRange('2016-01-01',$yesterday_date, $branch_id,$login_user),
            'todaysale'         => $this->order_service->getSaleSumByDate($today_date, $branch_id,$login_user ),
            'yesterdaysale'     => $this->order_service->getSaleSumByDateRange($yesterday_date,$today_date, $branch_id, $login_user),
            'weeksale'          => $this->order_service->getSaleSumByDateRange($current_monday,$current_sunday, $branch_id, $login_user),
            'preweeksale'       => $this->order_service->getSaleSumByDateRange($pre_monday,$pre_sunday, $branch_id, $login_user),
            'monthlysale'       => $this->order_service->getSaleSumByDateRange($month_first_day,$month_last_day, $branch_id, $login_user),
            'yearlysale'        => $this->order_service->getSaleSumByDateRange($year_first_day,$year_last_day, $branch_id, $login_user),
            'todayorders'       => $this->order_service->getOrderCountByDate($today_date, $branch_id, $login_user ),
            'yesterdayorders'   => $this->order_service->getOrderCountByDateRange($yesterday_date,$today_date, $branch_id, $login_user),
            'weekorders'        => $this->order_service->getOrderCountByDateRange($current_monday,$current_sunday, $branch_id, $login_user),
            'preweekorders'     => $this->order_service->getOrderCountByDateRange($pre_monday,$pre_sunday, $branch_id, $login_user),
            'monthlyorders'     => $this->order_service->getOrderCountByDateRange($month_first_day,$month_last_day, $branch_id, $login_user),
            'pos_orders'        => $pos_orders,
            'mobile_orders'     => $mobile_orders,
            'website_orders'    => $website_orders,
            'avg_prepare_time'  => $avg_prepare_time,
            'graph_saleweekday' => $this->report_service->getSaleByWeekDayMonthYear($login_user,$today_date,$branch_id),
            'graph_saleweeklyday'=> $this->report_service->getSaleByWeekDayMonth($login_user,$today_date,$branch_id),
            'graph_saletownwise'=> $this->report_service->getSaleByTownMonth($login_user,$today_date, $branch_id),
            'graph_salecategorywise'=> $this->report_service->getSaleByMenuCategoryMonth($login_user,$today_date, $branch_id),
            'list_topsaledays'=> $this->report_service->getSalesByTopSellingDay($login_user,$today_date, $branch_id),
            'graph_branch_orders_month'  =>$this->report_service->getOrderCountByBranchMonthYear($login_user,$today_date, $branch_id),
            'graph_branch_orders_week'  =>$this->report_service->getOrderCountByBranchWeekMonth($login_user,$today_date, $branch_id),
            'list_toporderusers'=> $this->report_service->getTopOrderedUsers($login_user,$today_date, $branch_id)
        ];

        $view = view('/dashboard/partials/partial-dashboard', compact('dashboard'))->render();

        return response($view);

    }
}