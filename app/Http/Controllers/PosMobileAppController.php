<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Services\Concrete\PosMobileAppService;
use App\Services\Concrete\OrderService;
use App\Services\Concrete\RestaurantService;
use App\Services\Concrete\DeliveryDetailService;
use App\Services\Concrete\UserService;
use App\Services\Concrete\OrderDetailService;

class PosMobileAppController extends Controller
{

    // space where we declare services
    protected $pos_mobile_service;
    protected $order_service;
    protected $restaurant_service;
    protected $delivery_detail_service;
    protected $user_service;
    protected $order_detail_service;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(PosMobileAppService $pos_mobile_service,OrderService $order_service,RestaurantService $restaurant_service,DeliveryDetailService $delivery_detail_service,UserService $user_service,
    OrderDetailService $order_detail_service
    )
    {
        // set the model
        $this->pos_mobile_service      = $pos_mobile_service;
        $this->restaurant_service      = $restaurant_service;
        $this->order_service    = $order_service;
        $this->delivery_detail_service = $delivery_detail_service;
        $this->user_service = $user_service;
        $this->order_detail_service = $order_detail_service;
    }
    
    
    public function login(Request $request)
    {   
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        
        // validate request
        $validator = Validator::make($request->all(), [
            "uid" => "required",
            "pwd"  => "required",
        ]);
        
        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = $validator->errors();
            
            return response(
                $response,
                200
            );
        }

        // authenticate request
        $login = $this->pos_mobile_service->posMobileAppLogin($request->uid,$request->pwd);
        
       
        if($login)
        {
            $response["Data"] =  $login[0];
            $response["Data"]["branchDetail"]   =  $login[1];
            $response["Data"]["categoryDetail"] =  $login[2];
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "Invalid login detail";
            
            return response(
                $response,
                200
            );    

        }
           
        return $response;
    }

    
    // public function placeMobileOrder(Request $request){
    //         $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
    //         $rnd_number = rand(1,25000);
    //         $current = date('Ymdhis');
    //         $tracking_id = $current.'_'.$rnd_number ;
    
    //         // validate request
    //         $validator = Validator::make($request->all(), [
    //             "branch_id" => "required"
    //         ]);
    
    //         if ($validator->fails())
    //         {
    //             $response["Success"]   = false;
    //             $response["Status"] = 400;
    //             $response["ErrorMessage"] = $validator->errors();
    
    //             return response(
    //                 $response,
    //                 200
    //             );
    //         }
    
    //         //check if user id does not exist then save user
    //         if(!isset($request->user_id))
    //         {
    //             $obj_user = [
    
    //             "email"         => $request->delivery_details['email'],
    //             "first_name"    => $request->delivery_details['first_name'],
    //             "last_name"     => $request->delivery_details['last_name'],
    //             "cell_num"      => $request->delivery_details['mobile_no'],
    //             // "address1"      => $request->delivery_details['Address'],
    //             "gender"        =>$request->delivery_details['gender'],
    //             "date_birth"    => $request->delivery_details['date_birth'],
    //             // "town_id"       => $request->delivery_details['town_id'],
    //             "appId"        =>   $request->app_id,
    //             "role_id"      =>   5,
    //             ];
    
    //             $inserted_user_id = $this->user_service->savePosUser($obj_user);
    //             $request->user_id = $inserted_user_id;
    //             $obj_user_address = [
    //                 "address1"          =>  $request->delivery_details['address'],
    //                 "address_type_id"   =>  !isset($request->delivery_details['addtype_id']) || $request->delivery_details['addtype_id'] == 0?3:$request->delivery_details['addtype_id'],
    //                 "user_id"           =>  $inserted_user_id,
    //                 "town_id"           =>  $request->delivery_details['town_id'],
    //                 "town_block_id"     =>  $request->delivery_details['town_block_id'],
    //                 "latitude"          =>  $request->delivery_details['latitude'],
    //                 "longitude"         =>  $request->delivery_details['longitude'],
    //             ];
    
    //             $this->delivery_detail_service->saveAddress($obj_user_address);      
            

    //         }
    
    //         $new_order = 1;    
    //         if(isset($request->paymentLaterOrderId) && $request->paymentLaterOrderId != 0)
    //             $new_order = 0;
    
    //         $order_datetime = date("Y-m-d H:i:s", time() - date("Z"));
    
    //         $obj_order = [
    //             'total'              =>     $request->total,
    //             'sub_total'          =>     $request->sub_total,
    //             'amount_paid'        =>     is_numeric($request->amount_paid)?$request->amount_paid:0,
    //             'amount_return'      =>     is_numeric($request->amount_return)?$request->amount_return:0,
    //             'discount_per'       =>     is_numeric($request->discount_per)?$request->discount_per:0,
    //             'discount_amount'    =>     is_numeric($request->discount_amount)?$request->discount_amount:0,
    //             'tax_percent'        =>     is_numeric($request->tax_percent)?$request->tax_percent:0,
    //             'tax_amount'         =>     is_numeric($request->tax_amount)?$request->tax_amount:0,
    //             'delivery_charge'    =>     is_numeric($request->del_chg)?$request->del_chg:0,
    //             'status_id'          =>     isset($request->status_id)? $request->status_id:3,
    //             'order_type_id'      =>     $request->order_type_id,
    //             'order_resource'     =>     isset($request->order_resource)?$request->order_resource:4 ,
    //             'table_no'           =>     $request->table_no,
    //             'num_persons'        =>     $request->num_persons,
    //             'order_edit'         =>     isset($request->order_edit) ? 1:0,
    //             'tax_include'        =>     $request->tax_include == null ? false : true,
    //             'order_taker'        =>     $request->order_taker,
    //             'useragent_id'       =>     $request->order_taker_id,
    //         ];

    //         $obj_delivery_detail = [
    //             'address'       => $request->delivery_details['Address']??NULL,
    //             'mobile_num'    => isset($request->delivery_details['mobile_no'])??null,
    //             'phone_num'     => isset($request->delivery_details['phone_num'])??null,
    //             'town_id'       => $request->delivery_details['town_id'],
    //             'email'         => $request->delivery_details['email']??NULL,
    //             'gender'        => $request->delivery_details['gender'],
    //             'date_birth'    => $request->delivery_details['date_birth'],
    //             'instructions'  => $request->delivery_details['instructions'],
    //             'latitude'      => $request->lat,
    //             'longitude'     => $request->lng
    //         ];
    //         $dailyorder_id = 0;
    //         if($new_order)
    //         {
    //             $obj_branch = $this->restaurant_service->getBranchById($request->branch_id ?? 0);
    //             $dailyorder_id = $obj_branch->dailyorder_id ?? 1;
    //             $obj_order['dailyorder_id'] = $dailyorder_id;
    //             $obj_order['createdby_id']  =  $request->login_user_id;
    //             $obj_order['restaurant_branch_id']  = $request->branch_id;
    //             $obj_order['tracking_id']  = $tracking_id;
    //             $obj_order['user_id']  = $request->user_id;
    //             $obj_order['order_date']  = $order_datetime;
    //             $obj_order['deliver_date']  = $order_datetime;
    //             $obj_order['useragent_id']  = $request->useragent_id;
    //             $obj_branch->dailyorder_id =    $dailyorder_id + 1;  // update daily order id in branch
    //             $this->restaurant_service->saveBranch($obj_branch->toArray());
    //             $obj_delivery_detail['user_id'] = $request->user_id;    
    //         }
    //         else
    //         {
    //             $dailyorder_id =  $this->order_service->getById($request->paymentLaterOrderId)->dailyorder_id;
    //             $obj_order['updatedby_id']  =  $request->login_user_id;
    //             $obj_order['id']  = $request->paymentLaterOrderId;
    //             $pre_delivery_detail = $this->delivery_detail_service->getDetailByOrderIds([$obj_order['id']])->first();
    //             $obj_delivery_detail['id'] = $pre_delivery_detail->id;
    //         }



    //         $order_placed = $this->order_service->save($obj_order);    
    //         if($new_order)
    //             $order_id = $order_placed['id'];
    //         else
    //             $order_id = $request->paymentLaterOrderId;    
    //         //delivery detail object
    //         $obj_delivery_detail['order_id'] = $order_id;
    //         // save order delivery detail
    //         $this->delivery_detail_service->save($obj_delivery_detail);
    //         if($order_placed)
    //         {    
    //             // check if user already has address
    //             $user_address = $this->delivery_detail_service->getAddressByUserId($request->user_id);
    //             if($user_address)
    //             {
    //                 $user_address->address1 = isset($request->delivery_details['Address']) ? $request->delivery_details['Address'] : $user_address->address1 ;
    //                 $user_address->longitude = isset($request->lng) ? $request->lng : $user_address->longitude ;
    //                 $user_address->latitude = isset($request->lat) ? $request->lat : $user_address->latitude ;
    //                 $user_address->town_id =isset($request->delivery_details['town_id']) ? $request->delivery_details['town_id'] : $user_address->town_id ;
    //                 $user_address->town_block_id =isset($request->delivery_details['town_block_id']) ? $request->delivery_details['town_block_id'] : $user_address->town_block_id ;
    //                 $user_address->address2 = isset($request->delivery_details['address2']) ? $request->delivery_details['address2'] : $user_address->address2 ;
    
    //                 $this->delivery_detail_service->saveAddress($user_address->toArray());
    //             }
    //             else
    //             {   $order_delivery_add = [
    
    //                     'address1'  => isset($request->delivery_details['Address']) ?$request->delivery_details['Address']: null ,
    //                     'user_id'   => $obj_order['user_id'] ,
    //                     'longitude' => isset($request->lng)  ?? null ,
    //                     'latitude'  => isset($request->lat)  ?? null ,
    //                     'town_id'   =>   isset($request->delivery_details['town_id'])  ? $request->delivery_details['town_id']: null ,
    //                     'town_block_id'   =>   isset($request->delivery_details['town_block_id'])  ? $request->delivery_details['town_block_id']: null ,
    //                     'address2'  => isset($request->delivery_details['address2'])  ? $request->delivery_details['address2']:null ,
    //                     'address_type_id' => 5
    //                 ];
    //                 // save user address
    //                 $this->delivery_detail_service->saveAddress($order_delivery_add);
    //             }
    
    //             // check if user already exist
    //             $user = $this->user_service->getById($request->user_id);
    //             if($user)
    //             {
    
    //                 $user->phone_num = isset($request->delivery_details['phone_num']) ? $request->delivery_details['phone_num'] : $user->phone_num;
    //                 $user->cell_num = isset($request->delivery_details['mobile_no']) ? $request->delivery_details['mobile_no'] : $user->cell_num;;
    //                 $user->date_birth =  isset($request->delivery_details['date_birth']) ? $request->delivery_details['date_birth'] : $user->date_birth;
    //                 $user->email = isset($request->delivery_details['email']) ? $request->delivery_details['email'] : $user->email;
    //                 $user->gender = isset($request->delivery_details['gender']) ? $request->delivery_details['gender'] : $user->gender;
    
    //                 $user->first_name = isset($request->delivery_details['first_name']) ? $request->delivery_details['first_name'] : $user->first_name;
    //                 $user->last_name = isset($request->delivery_details['last_name']) ? $request->delivery_details['last_name'] : $user->last_name;
    //                 $user->updatedby_id = $request->login_user_id;
    //                 $this->user_service->save($user->toArray());
    //             }
    
    //             $order_detail = [];
    //             if(!$new_order)
    //             {
    //                 // for check if any menu is deleted or not from previous detail
    //                 $pre_order_detail = $this->order_detail_service->getOrderDetailByOrderIds([$order_id]);
    //                 foreach($pre_order_detail as $pre_detail)
    //                 {
    //                         $pre_detail['is_deleted'] = 1;
    //                         $this->order_detail_service->save($pre_detail->toArray());
    
    //                 }
    //                 // for check if any new menu is added in detail or not
    //                 foreach($request->order_detail as $detail)
    //                 {
    
    //                     $order_detail = [
    //                         'menu_id'   => $detail['menu_id'],
    //                         'order_id'  => $order_id,
    //                         'price'     => $detail['menu_price'],
    //                         'quantity'  => $detail['menu_qty'],
    //                         'menu_name' => $detail['menu_name'],
    //                         'order_detail_status_id'  => 2,
    //                         'menu_variation_id' => isset($detail['menu_variation_id'])?$detail['menu_variation_id']:null,
    //                         'variation_name'    => isset($detail['variation_name'])?$detail['variation_name']:null
                        
    //                     ];
    
    //                         $saved_od = $this->order_detail_service->save($order_detail);
    //                         $saved_od_id = $saved_od->id;

    //                         if(isset($detail['choices']))
    //                         {
    //                             foreach ($detail['choices'] as $key => $value) {
    //                                 $od_choices = [
    //                                     'choice_name'     => $value['choice_name'],
    //                                     'price'           => $value['price'],
    //                                     'choice_id'       => $value['choice_id'],
    //                                     'order_detail_id' => $saved_od_id
    //                                 ];
            
    //                                 $this->order_detail_service->saveDetailChoices($od_choices);
    //                             }
    //                         }
    //                 }
    
    //             }
    //             else
    //             {   // for inserting new menu in detail
    //                 foreach($request->order_detail as $detail)
    //                 {
    //                     $order_detail = [
                        
    //                     'menu_id'   => $detail['id'],
    //                     'order_id'  => $order_id,
    //                     'price'     => $detail['price'],
    //                     'quantity'  => $detail['quantity'],
    //                     'order_detail_status_id'  => 2,
    //                     'menu_variation_id' => isset($detail['menu_variation_id'])?$detail['menu_variation_id']:NULL,
    //                     'variation_name'    => isset($detail['variation_name'])?$detail['variation_name']:NULL,
    //                     'menu_name'         => isset($detail['menu_name'])?$detail['menu_name']:$detail['id'],
                    
    //                     ];

    
    //                     $saved_od = $this->order_detail_service->save($order_detail);
    //                     $saved_od_id = $saved_od->id;
    
    //                     if(isset($detail['choices']))
    //                     {
    //                         foreach ($detail['choices'] as $key => $value) {
    //                             $od_choices = [
    //                                 'choice_name'     => $value['choice_name'],
    //                                 'price'           => $value['price'],
    //                                 'choice_id'       => $value['choice_id'],
    //                                 'order_detail_id' => $saved_od_id
    //                             ];

    //                             $this->order_detail_service->saveDetailChoices($od_choices);
    //                         }
    //                     }
    //                 }
    
    //             }
    
    //         }
    //         if($order_placed)
    //         {
    //             $response["Data"] =  ["trackingId" => $tracking_id,"dailyOrderCount" => $dailyorder_id];
    //         }
    //         else
    //         {
    
    //             $response["Success"]   = false;
    //             $response["Status"] = 401;
    //             $response["ErrorMessage"] = "Order does not placed successfully";
    
    //             return response(
    //                 $response,
    //                 200
    //             );
    
    //         }
    //         return $response;
    // }

    public function placeOrder(Request $request)
    {   
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        $rnd_number = rand(1,25000);
        $current = date('Ymdhis');
        $tracking_id = $current.'_'.$rnd_number ;
        
        // validate request
        $validator = Validator::make($request->all(), [
            "branch_id"     => "required",
            "order_detail"  => "required|array|min:1",
            "total"         => "numeric|min:0|not_in:0",
            "sub_total"     => "numeric|min:0|not_in:0"
        ]);
        
        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 400;
            $response["ErrorMessage"] = $validator->errors();
            
            return response(
                $response,
                200
            );
        }

        if (!isset($request->user_id))
        {
            $obj_user = [
                "id"           =>   $request->user_id,
                "role_id"      =>   5,
                "first_name"   =>   $request->delivery_details['first_name'],
                "last_name"    =>   $request->delivery_details['last_name'],
                "cell_num"     =>   $request->delivery_details['mobile_no'],
                "gender"       =>   $request->delivery_details['gender'],
                "date_birth"   =>   $request->delivery_details['date_birth'],
                "email"        =>   $request->delivery_details['email'],
                "appId"        =>   $request->app_id
            ];

            $inserted_user_id = $this->user_service->savePosUser($obj_user);
            $request->user_id = $inserted_user_id;
            
            $obj_user_address = [
                "address1"          =>  $request->delivery_details['address'],
                "address_type_id"   =>  !isset($request->delivery_details['addtype_id']) || $request->delivery_details['addtype_id'] == 0?3:$request->delivery_details['addtype_id'],
                "user_id"           =>  $inserted_user_id,
                "town_id"           =>  $request->delivery_details['town_id'],
                "town_block_id"     =>  $request->delivery_details['town_block_id'],
                "latitude"          =>  $request->delivery_details['latitude'],
                "longitude"         =>  $request->delivery_details['longitude'],
            ];

            $this->delivery_detail_service->saveAddress($obj_user_address);      
        }

        $new_order = 1;
        if(isset($request->paymentLaterOrderId) && $request->paymentLaterOrderId != 0)
            $new_order = 0;
    
        $order_datetime = date("Y-m-d H:i:s", time() - date("Z"));
        
        $obj_order = [
        'total'              =>     $request->total,
        'sub_total'          =>     $request->sub_total,
        'amount_paid'        =>     is_numeric($request->amount_paid)?$request->amount_paid:0,
        'amount_return'      =>     is_numeric($request->amount_return)?$request->amount_return:0,
        'discount_per'       =>     is_numeric($request->discount_per)?$request->discount_per:0,
        'discount_amount'    =>     is_numeric($request->discount_amount)?$request->discount_amount:0,
        'tax_percent'        =>     is_numeric($request->tax_percent)?$request->tax_percent:0,
        'tax_amount'         =>     is_numeric($request->tax_amount)?$request->tax_amount:0,
        'status_id'          =>     isset($request->status_id) && $request->status_id!=0 ? $request->status_id:3,
        'order_type_id'      =>     $request->order_type_id,
        'order_resource'     =>     isset($request->order_resource)?$request->order_resource:5,  // Order Resource for Tablet (Tab).
        'table_no'           =>     $request->table_no,
        'num_persons'        =>     $request->num_persons,
        'order_edit'         =>     isset($request->order_edit) ? $request->order_edit:0,
        'tax_include'        =>     $request->tax_include == null ? false : true,
        'service_charge'    =>     is_numeric($request->service_charge)?$request->service_charge:0

        ];

        $obj_delivery_detail = [

            'address'       => $request->delivery_details['address'],
            'mobile_num'    => $request->delivery_details['mobile_no'],
            'phone_num'     => isset($request->delivery_details['phone_num'])??null,
            'town_id'       => $request->delivery_details['town_id'],
            'email'         => $request->delivery_details['email'],
            'gender'        => $request->delivery_details['gender'],
            'date_birth'    => $request->delivery_details['date_birth'],
            'instructions'  => $request->delivery_details['instructions'],
            'latitude'      => $request->lat,
            'longitude'     => $request->lng
        ];


        $dailyorder_id = 0;
        // for order edit scenerio
        if($new_order)
        {   
            // get branch
            $obj_branch = $this->restaurant_service->getBranchById($request->branch_id ?? 0);
            // order object
            $dailyorder_id = $obj_branch->dailyorder_id ?? 1;
            $obj_order['dailyorder_id'] = $dailyorder_id;
            $obj_order['createdby_id']  =  $request->login_user_id;
            $obj_order['restaurant_branch_id']  = $request->branch_id;
            $obj_order['tracking_id']  = $tracking_id;
            $obj_order['useragent_id']  = $request->useragent_id;
            $obj_order['user_id']  = $request->user_id;
            $obj_order['order_date']  = $order_datetime;
            $obj_order['deliver_date']  = $order_datetime;

            // update daily order id in branch
            $obj_branch->dailyorder_id =    $dailyorder_id+1; 
            $this->restaurant_service->saveBranch($obj_branch->toArray());
            
            $obj_delivery_detail['user_id'] = $request->user_id;
        }
        else
        {
            $dailyorder_id =  $this->order_service->getById($request->paymentLaterOrderId)->dailyorder_id;
            $obj_order['updatedby_id']  =  $request->login_user_id;
            $obj_order['id']  = $request->paymentLaterOrderId;
            
            // get delivery detail by order id
            $pre_delivery_detail = $this->delivery_detail_service->getDetailByOrderIds([$obj_order['id']])->first();
            //delivery detail object 
            $obj_delivery_detail['id'] = $pre_delivery_detail->id;
        }

        // save order
        $order_placed = $this->order_service->save($obj_order);

        if($new_order)
            $order_id = $order_placed['id'];
        else
            $order_id = $request->paymentLaterOrderId;

        //delivery detail object 
        $obj_delivery_detail['order_id'] = $order_id;
        // save order delivery detail
        $this->delivery_detail_service->save($obj_delivery_detail);
        

        if($order_placed)
        {
            
            // check if user already has address
            $user_address = $this->delivery_detail_service->getAddressByUserId($request->user_id);
            if($user_address)
            {
                $user_address->address1 = isset($request->delivery_details['address']) ? $request->delivery_details['address'] : $user_address->address1 ;
                $user_address->longitude = isset($request->lng) ? $request->lng : $user_address->longitude ;
                $user_address->latitude = isset($request->lat) ? $request->lat : $user_address->latitude ;
                $user_address->town_id =isset($request->delivery_details['town_id']) ? $request->delivery_details['town_id'] : $user_address->town_id ;
                $user_address->address2 = isset($request->delivery_details['address2']) ? $request->delivery_details['address2'] : $user_address->address2 ;

                $this->delivery_detail_service->saveAddress($user_address->toArray());
            }
            else
            {   $order_delivery_add = [
                
                    'address1'  => isset($request->delivery_details['address']) ?$request->delivery_details['address']: null ,
                    'user_id'   => $obj_order['user_id'] ,
                    'longitude' => isset($request->u)  ?? null ,
                    'latitude'  => isset($request->latitude)  ?? null ,
                    'town_id'   =>   isset($request->delivery_details['town_id'])  ? $request->delivery_details['town_id']: null ,
                    'address2'  => isset($request->delivery_details['address2'])  ? $request->delivery_details['address2']:null ,
                    'address_type_id' => 5
                ];
                // save user address
                $this->delivery_detail_service->saveAddress($order_delivery_add);
            }

            // check if user already exist
            $user = $this->user_service->getById($request->user_id);
            if($user)
            {
                
                $user->phone_num = isset($request->delivery_details['phone_num']) ? $request->delivery_details['phone_num'] : $user->phone_num;
                $user->cell_num = isset($request->delivery_details['mobile_no']) ? $request->delivery_details['mobile_no'] : $user->cell_num;;
                $user->date_birth =  isset($request->delivery_details['date_birth']) ? $request->delivery_details['date_birth'] : $user->date_birth;
                $user->email = isset($request->delivery_details['email']) ? $request->delivery_details['email'] : $user->email;
                $user->gender = isset($request->delivery_details['gender']) ? $request->delivery_details['gender'] : $user->gender;

                $user->first_name = isset($request->delivery_details['first_name']) ? $request->delivery_details['first_name'] : $user->first_name;
                $user->last_name = isset($request->delivery_details['last_name']) ? $request->delivery_details['last_name'] : $user->last_name;
                $user->updatedby_id = $request->login_user_id;
                $this->user_service->save($user->toArray());
            }

            $order_detail = [];
            if(!$new_order)
            {    
                // for check if any menu is deleted or not from previous detail
                $pre_order_detail = $this->order_detail_service->getOrderDetailByOrderIds([$order_id]);
                foreach($pre_order_detail as $pre_detail)
                {   
                        $pre_detail['is_deleted'] = 1;
                        $this->order_detail_service->save($pre_detail->toArray());
                    
                }
                
                // for check if any new menu is added in detail or not 
                foreach($request->order_detail as $detail)
                {
                   
                    $order_detail = [
                        'menu_id'   => $detail['id'],  
                        'order_id'  => $order_id,
                        'price'     => $detail['price'],
                        'quantity'  => $detail['quantity'],
                        'order_detail_status_id'  => 2,
                        'menu_variation_id' => isset($detail['menu_variation_id'])?$detail['menu_variation_id']:NULL,
                        'variation_name'    => isset($detail['variation_name'])?$detail['variation_name']:NULL,
                        'menu_name'         => isset($detail['menu_name'])?$detail['menu_name']:$detail['id'],
                    ];
                        
                    $saved_od = $this->order_detail_service->save($order_detail);
                    $saved_od_id = $saved_od->id; 

                    if(isset($detail['choices']))
                    {
                        foreach ($detail['choices'] as $key => $value) {
                            $od_choices = [
                                'choice_name'     => $value['choice_name'],
                                'price'           => $value['price'],
                                'choice_id'       => $value['choice_id'],
                                'order_detail_id' => $saved_od_id
                            ];
    
                            $this->order_detail_service->saveDetailChoices($od_choices);
                        }
                    }
                   
                }
            }
            else
            {  // for inserting new menu in detail 
                foreach($request->order_detail as $detail)
                {
                   
                    $order_detail = [
                        'menu_id'   => $detail['id'],
                        'order_id'  => $order_id,
                        'price'     => $detail['price'],
                        'quantity'  => $detail['quantity'],
                        'order_detail_status_id'  => 2,
                        'menu_variation_id' => isset($detail['menu_variation_id'])?$detail['menu_variation_id']:NULL,
                        'variation_name'    => isset($detail['variation_name'])?$detail['variation_name']:NULL,
                        'menu_name'         => isset($detail['menu_name'])?$detail['menu_name']:$detail['id'],
                    ];
                        
                    $saved_od = $this->order_detail_service->save($order_detail);
                    $saved_od_id = $saved_od->id; 

                    if(isset($detail['choices']))
                    {
                        foreach ($detail['choices'] as $key => $value) {
                            $od_choices = [
                                'choice_name'     => $value['choice_name'],
                                'price'           => $value['price'],
                                'choice_id'       => $value['choice_id'],
                                'order_detail_id' => $saved_od_id
                            ];
    
                            $this->order_detail_service->saveDetailChoices($od_choices);
                        }
                    }
                   
                }

            }

        }

        
        if($order_placed)
        {
            $orderInfo = $this->order_service->getPosMobileOrdersByBranchId($order_placed['restaurant_branch_id'],null,null, 0, 1,"order.id","DESC" );
            $response["Data"] =  ["trackingId" => $tracking_id,"dailyOrderCount" => $dailyorder_id, "orderInfo" => $orderInfo['data'] ];
       
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "Order does not placed successfully";
            
            return response(
                $response,
                401
            );    

        }
        
        return $response;
    }
    
    public function getPosMobileAppUserByContactAppId(Request $request)
    {   
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        
        // validate request
        $validator = Validator::make($request->all(), [
            "appId"     => "required",
            "contactno" => "required"

        ]);

        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 400;
            foreach ($validator->errors()->all() as $message) {
                $response["ErrorMessage"] .= $message;
            }
            
            return response(
                $response,
                200
            );
        }

        // get data from service by contact and app id
        $user = $this->user_service->getByContactAppId($request->contactno,$request->appId);
        
        if($user)
        {$user['dateBirth'] = $user['date_birth']; unset($user['date_birth']);
        $user['userId'] = $user['id'];unset($user['id']);
        $user['name'] = $user['first_name'];unset($user['first_name']);
        $user['contact'] = $user['cell_num'];unset($user['cell_num']);unset($user['phone_num']);
        $user['blockId'] = $user['townBlockId'];unset($user['townBlockId']);

        $orders = $this->order_service->getUserOrdersWithDetailByBranchId($user['userId'],$request->branchId);
        if(!$orders)
            $orders  = [];
        
        foreach ($orders as $order) 
        {
            foreach ($order['orderDetail'] as $detail) 
            {    
                unset($detail['delivery_details']);
            }
            $order['branchId'] = $order['restaurant_branch_id'];unset($order['restaurant_branch_id']);
            $order['subTotal'] = $order['sub_total'];unset($order['sub_total']);
            $order['statusId'] = $order['status_id'];unset($order['status_id']);
        }
        $user['userOrders'] = $orders;
    }
        if($user)
        {
            $response["Data"] = $user;
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "user dose not exist!";
            
            return response(
                $response,
                200
            );    

        }
        return $response;
    }

    public function getOrderByTrackId(Request $request){
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        
        $validator = Validator::make($request->all(), [
            "trackId" => "required",
            "branch_id" => "required"
        ]);

        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 400;
            foreach ($validator->errors()->all() as $message) {
                $response["ErrorMessage"] .= $message;
            }
            
            return response(
                $response,
                200
            );
        }
        $orders = $this->order_service->getOrderByTrackId($request->trackId, $request->branch_id);

        $response["Data"] = $orders;
        
        return $response;
    }
    
    
    public function getOrdersByBranchId(Request $request)
    {   
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200, "grand_Total"=> null];

        // validate request
        $validator = Validator::make($request->all(), [
            "branchId" => "required"
        ]);

        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 400;
            foreach ($validator->errors()->all() as $message) {
                $response["ErrorMessage"] .= $message;
            }
            
            return response(
                $response,
                200
            );
        }

        if(!$request->has('start') && !$request->has('end'))
        {
            // get data from service
            
            $start_date  = date("Y-m-d 07:00:00");
            $end_date    = date("Y-m-d H:i:s");
            $orders      = $this->order_service->getPosMobileOrdersByBranchId($request->branchId,$start_date,$end_date,0,50);
        }
        else if($request->has('start') && $request->has('end'))
        {
            // get data from service
            $orders = $this->order_service->getPosMobileOrdersByBranchId($request->branchId,$request->start,$request->end);
        }
        else
        {
            $response["Success"]        = false;
            $response["Status"]         = 400;
            $response["ErrorMessage"]   = "Invalid date range";
            
            return response(
                $response,
                200
            );
        }


        if($orders)
        {
            $response["Data"] = $orders["data"];
            $response["grand_Total"] = $orders["total_Sale"];
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "record does not found";
            
            return response(
                $response,
                200
            );    

        }
        return $response;
    }
    
}

