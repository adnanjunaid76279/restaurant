<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Services\Concrete\WebAppService;
use App\Services\Concrete\OrderService;
use App\Services\Concrete\RestaurantService;
use App\Services\Concrete\DeliveryDetailService;
use App\Services\Concrete\UserService;
use App\Services\Concrete\OrderDetailService;
use App\Services\Concrete\MenuCategoryService;
use App\Services\Concrete\OmcOrderService;

class WebAppController extends Controller
{

    // space where we declare services
    protected $webapp_service;
    protected $user_service;
    protected $order_service;
    protected $restaurant_service;
    protected $delivery_detail_service;
    protected $order_detail_service;
    protected $menu_category_service;
    protected $omc_order_service;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(WebAppService $webapp_service,UserService $user_service,
    OrderService $order_service,RestaurantService $restaurant_service,DeliveryDetailService $delivery_detail_service,
    OrderDetailService $order_detail_service,MenuCategoryService $menu_category_service,
    OmcOrderService $omc_order_service
    )
    {
        // set the model
        $this->webapp_service = $webapp_service;
        $this->user_service = $user_service;
        $this->order_service = $order_service;
        $this->restaurant_service = $restaurant_service;
        $this->delivery_detail_service = $delivery_detail_service;
        $this->order_detail_service = $order_detail_service;
        $this->menu_category_service = $menu_category_service;
        $this->omc_order_service = $omc_order_service;
       
    }
    
    // get web main data
    public function getMainData(Request $request)
    {   
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        
        // validate request
        $validator = Validator::make($request->all(), [
            "branch_id" => "required",   // just need branch_id and app_id ,fb_id and user_id is optional
            "app_id" => "required"   
        ]);



        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = $validator->errors();
            
            return response(
                $response,
                200
            );
        }

        if (isset($request->user_id) && $request->user_id != 0)
            $user_id = $request->user_id;
        else
            $user_id  = 0;  


        if ($user_id == 0 && isset($request->fb_id))
        {
            $user_id =  $this->user_service->getUserIdByFbIdAppId($request->fb_id,$request->app_id);
        }
   

        // db request via service
        $main_data = $this->webapp_service->getMainData($user_id ,$request->branch_id);
       
        if($main_data)
        {
            $response["Data"] =  $main_data;
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "Error: getting restaurant data";
            
            return response(
                $response,
                200
            );    

        }
           
        return $response;
    }
    
    // save web user via app id,fb id
    public function saveWebUser(Request $request)
    {   
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        
        // validate request
        $validator = Validator::make($request->all(), [
            "social_app_id" => 'required',
            "branch_id"     => "required",
            "login_type_id" => "required",
            "first_name"    => "required",
        ]);
        
        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 400;
            foreach ($validator->errors()->all() as $message) {
                $response["ErrorMessage"] .= $message;
            }
            
            return response(
                $response,
                200
            );
        }
        
       $obj_user = [
            'first_name'    => $request->first_name,
            'last_name'     => isset($request->last_name)?$request->last_name:null,
            'branch_id'     => $request->branch_id,
            'social_app_id' => $request->social_app_id,
            'login_type_id' => $request->login_type_id,
            'email'         => isset($request->email) ? $request->email : NULL
        ];

        // db request via service
        $user = $this->user_service->getBySocialAppId($request->social_app_id);

        if(!$user)
        {
            $save_user = $this->user_service->save($obj_user);
        }
        else
        {
            $obj_user['id'] = $user->id;
            // db request via service
            $save_user = $this->user_service->save($obj_user);
        }

        if($save_user)
        {
            $response["Data"] =  $save_user;
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "Error: saving user";
            
            return response(
                $response,
                200
            );    

        }
           
        return $response;
    }

    // place web order
    public function placeOrder(Request $request)
    {   
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        
        // validate request
        $validator = Validator::make($request->all(), [
            "user_id" => "required"
        ]);
        
        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 400;
            $response["ErrorMessage"] = $validator->errors();
            
            return response(
                $response,
                200
            );
        }

        // creating tracking id
        $rnd_number = rand(1,25000);
        $current = date('Ymdhis');
        $tracking_id = $current.'_'.$rnd_number ;

   
        $order_datetime = date("Y-m-d H:i:s", time() - date("Z"));
        
        
        $obj_order = [
            'user_id'            =>     $request->user_id,
            'total'              =>     $request->total,
            'tracking_id'        =>     $tracking_id,
            'sub_total'          =>     $request->sub_total,
            'status_id'          =>     isset($request->status_id)? $request->status_id:1,
            'tax_percent'        =>     is_numeric($request->tax_percent)?$request->tax_percent:0,
            'tax_amount'         =>     is_numeric($request->tax_amount)?$request->tax_amount:0,
            'order_date'         =>     $order_datetime,
            'deliver_date'       =>     $order_datetime,
            'order_type_id'      =>     isset($request->order_type_id)?$request->order_type_id:3,
            'order_resource'     =>     1 ,
            'date_created'       =>     $order_datetime,
            'order_edit'         =>     0,
            'restaurant_branch_id'  => $request->branch_id,
            'createdby_id'      =>     $request->user_id,
            'payment_type'      =>     isset($request->payment_type)?$request->payment_type:"cod",
            'online_pay_order_id' =>   isset($request->online_pay_order_id)?$request->online_pay_order_id:null,
            'payment_gateway'     =>   isset($request->payment_gateway) ? $request->payment_gateway:null,
        
        ];
        
        $dailyorder_id = 0;
        // get branch
        $obj_branch = $this->restaurant_service->getBranchById($request->branch_id ?? 0);

        if(!$obj_branch)
        {   
            $response["Success"]   = false;
            $response["Status"] = 404;
            $response["ErrorMessage"] = "restaurant was not found";
            
            return response(
                $response,
                200
            );
        }

        $dailyorder_id = $obj_branch->dailyorder_id ?? 1;
        
        $obj_order['dailyorder_id'] = $dailyorder_id;
        
        // save order
        $order_placed = $this->order_service->save($obj_order);
     
        // update daily order id in branch
        $obj_branch->dailyorder_id =    $dailyorder_id+1; 
        $this->restaurant_service->saveBranch($obj_branch->toArray());

        $obj_delivery_detail = [

            'address'       => $request->delivery_details['address'],
            'mobile_num'    => $request->delivery_details['mobile_no'],
            'phone_num'     => isset($request->delivery_details['phone_num'])??null,
            'town_id'       => $request->delivery_details['town_id'],
            'town_block_id' => $request->delivery_details['town_block_id'],           
            'email'         => $request->delivery_details['email'],
            'gender'        => $request->delivery_details['gender'],
            'date_birth'    => isset($request->delivery_details['date_birth'])?$request->delivery_details['date_birth']:null,
            'instructions'  => $request->delivery_details['instructions'],
            'latitude'      => $request->latitude,
            'longitude'     => $request->longitude,
            'user_id'       => $request->user_id,
            'order_id'      => $order_placed['id']
        ];

        // save order delivery detail
        $this->delivery_detail_service->save($obj_delivery_detail);
        

        if($order_placed)
        {
            
            // check if user already has address
            $user_address = $this->delivery_detail_service->getAddressByUserId($request->user_id);
            if($user_address)
            {
                $user_address->address1 = isset($request->delivery_details['Address']) ? $request->delivery_details['Address'] : $user_address->address1 ;
                $user_address->longitude = isset($request->lng) ? $request->lng : $user_address->longitude ;
                $user_address->latitude = isset($request->lat) ? $request->lat : $user_address->latitude ;
                $user_address->town_id = isset($request->delivery_details['town_id']) ? $request->delivery_details['town_id'] : $user_address->town_id ;
                $user_address->town_block_id = isset($request->delivery_details['town_block_id']) ? $request->delivery_details['town_block_id'] : $user_address->town_block_id ;
                $user_address->address2 = isset($request->delivery_details['address2']) ? $request->delivery_details['address2'] : $user_address->address2 ;
                $user_address->id = $user_address->id ;
                
                $this->delivery_detail_service->saveAddress($user_address->toArray());
            }
            else
            {   $order_delivery_add = [
                
                    'address1'  => isset($request->delivery_details['Address']) ?$request->delivery_details['Address']: null ,
                    'user_id'   => $obj_order['user_id'] ,
                    'longitude' => isset($request->lng) ? $request->lng : null ,
                    'latitude'  => isset($request->lat)  ? $request->lat : null ,
                    'town_id'   => isset($request->delivery_details['town_id'])  ? $request->delivery_details['town_id']: null ,
                    'address2'  => isset($request->delivery_details['address2'])  ? $request->delivery_details['address2']:null ,
                    'address_type_id' => 5
                ];
                // save user address
                $this->delivery_detail_service->saveAddress($order_delivery_add);
            }

            // check if user already exist
            $user = $this->user_service->getById($request->user_id);

            if($user)
            {
                
                $user->phone_num = isset($request->delivery_details['phone_num']) ? $request->delivery_details['phone_num'] : $user->phone_num;
                $user->cell_num = isset($request->delivery_details['mobile_no']) ? $request->delivery_details['mobile_no'] : $user->cell_num;;
                $user->date_birth =  isset($request->delivery_details['date_birth']) ? $request->delivery_details['date_birth'] : $user->date_birth;
                $user->email = isset($request->delivery_details['email']) ? $request->delivery_details['email'] : $user->email;
                $user->gender = isset($request->delivery_details['gender']) ? $request->delivery_details['gender'] : $user->gender;

                $user->first_name = isset($request->delivery_details['first_name']) ? $request->delivery_details['first_name'] : $user->first_name;
                $user->last_name = isset($request->delivery_details['last_name']) ? $request->delivery_details['last_name'] : $user->last_name;
                $user->updatedby_id = $request->login_user_id;
                $this->user_service->save($user->toArray());
            }

            $order_detail = [];
           
            // for inserting new menu in detail 
            foreach($request->order_detail as $detail)
            {
                $order_detail = [
                'menu_id'   => $detail['id'],
                'menu_name' => $detail['menu_name'],
                'order_id'  => $order_placed['id'],
                'price'     => $detail['price'],
                'quantity'  => $detail['quantity'],
                'order_detail_status_id'  => 2
                ];

                $this->order_detail_service->save($order_detail);
            }

        }

        
        if($order_placed)
        {
            $response["Data"] = $tracking_id;

            if(isset($request->branch_id) && $request->branch_id == 1267)
                $this->omc_order_service->postOrder($order_placed['id']);
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "Order does not placed successfully";
            
            return response(
                $response,
                200
            );    

        }
        
        return $response;
    }

    // WebOrderHistory by user id and branch id
    public function getOrderHistory(Request $request)
    {   
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];

        // validate request
        $validator = Validator::make($request->all(), [
            "branchId" => "required",
            "userId" => "required"
            
        ]);

        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 400;
            $response["ErrorMessage"] = $validator->errors();
            
            return response(
                $response,
                200
            );
        }

        $orders = $this->order_service->getUserOrdersWithDetailByBranchId($request->userId,$request->branchId);
        
        if($orders)
        {
            $response["Data"] = $orders;
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "record does not found";
            
            return response(
                $response,
                200
            );    

        }
        return $response;
    }

    // rate dish on web by dish id,user id,rating
    public function rateDish(Request $request)
    {   
         $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
         
         // validate request
         $validator = Validator::make($request->all(), [
            "dish_id" => "required",
            "user_id" => "required",
            "rating"  => "required"
         ]);
         
         if ($validator->fails()) 
         {   
             $response["Success"]   = false;
             $response["Status"] = 400;
             $response["ErrorMessage"] = $validator->errors();
             
             return response(
                 $response,
                 400
             );
         }
        
 
         // db request via service  
         $save_rating = $this->menu_category_service->rateMenu($request->rating,$request->user_id,$request->dish_id);
        
         if($save_rating)
         {
             $response["Data"] =  $save_rating;
         }
         else
         {
             
             $response["Success"]   = false;
             $response["Status"] = 401;
             $response["ErrorMessage"] = "Error: saving rating";
             
             return response(
                 $response,
                 401
             );    
 
         }
            
         return $response;
    }

    // user sign up
    public function userSignUp(Request $request)
    {   
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        
        $cell_num = $request->cell_num;
        $email  = $request->email;
        $branch_id  = $request->branch_id;
        
        // validate request
        $validator = Validator::make($request->all(), [
            "first_name"    => "required",
            "last_name"     => "required", 
            "email"         => ["required",
                Rule::unique('users')->where(function ($query)use($email,$cell_num,$branch_id) {
                    $query->where('email',$email)->where('cell_num',$cell_num)->where('branch_id',$branch_id);
                }),
            ], 
            "password"      => "required|min:6", 
            "cell_num"      => "required",
            "branch_id"     => "required"
        ]);

        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 200;
            foreach ($validator->errors()->all() as $message) {
                $response["ErrorMessage"] .= $message;
            }
            return response(
                $response,
                200
            );
        }

        $obj = [
            "first_name"    => $request->first_name,
            "last_name"     => $request->last_name,
            "email"         => $request->email,
            "password"      => $request->password,
            "cell_num"      => $request->cell_num,
            "date_birth"    => $request->date_birth,
            "branch_id"     => $request->branch_id,
            "login_type_id" => 5
        ];
        
        $saved_obj =  $this->user_service->save($obj);
        
        if($saved_obj)
        {
            $response["Data"] =  $saved_obj;
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "Error: user not saved!";
            
            return response(
                $response,
                401
            );    

        }
            
        return $response;
    }

    //  user login with username and password 
    public function userLogin(Request $request)
    {   
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        
        // validate request
        $validator = Validator::make($request->all(), [
            'email'     => 'required',
            'password'  => 'required|min:6',
            'branch_id' => 'required'
        ]);

        
        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 200;
            
            foreach ($validator->errors()->all() as $message) {
                $response["ErrorMessage"] .= $message;
            }
            
            return response(
                $response,
                200
            );
        }

        $user = $this->user_service->getUserByEmaliPasswordBranchId($request->email,$request->password,$request->branch_id);

        if($user)
        {
            $response["Data"] =  $user;
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "Error: Invalid Credentials!";
            
            return response(
                $response,
                401
            );    

        }
            
        return $response;
    }

    //
    public function updatePaymentStatusToPaidByOnlinePayOrderId(Request $request)
    {   
        
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        
        // validate request
        $validator = Validator::make($request->all(), [
            'online_pay_order_id'  => 'required'
        ]);

        
        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 200;
            
            foreach ($validator->errors()->all() as $message) {
                $response["ErrorMessage"] .= $message;
            }
            
            return response(
                $response,
                200
            );
        }
        
        $order = $this->order_service->getByOnlinePayOrderId($request->online_pay_order_id);
        
        
        // $order->amount_paid = $order->total;
        // $order->updatedby_id = $order->createdby_id;
        // $this->order_service->save($order->toArray());
        

       
        if($order)
        {
            $order->amount_paid = $order->total;
            $order->amount_return = 0;
            $order->status_id = 3;
            $order->updatedby_id = $order->createdby_id;
            $order = $this->order_service->setOrerStatusTime($order->toArray());

            $response["Data"] =  $this->order_service->save($order);
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 401;
            $response["ErrorMessage"] = "Error: no record found!";
            
            return response(
                $response,
                200
            );    

        }
            
        return $response;
    }
}
