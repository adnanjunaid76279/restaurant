<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Restaurant;
use App\Services\Concrete\RestaurantService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Requests\CreateRestaurantRequest;

class RestaurantController extends Controller
{
    // space where we declare services
    protected $restaurant_service;
    
     /**
     * Constructor
    */
    public function __construct(RestaurantService $restaurant_service)
    {
        $this->restaurant_service  = $restaurant_service;    
    }
  
    public function index(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->get('start');
        $length = $request->get('length');
        $search = (isset($request->search['value']))? $request->search['value'] : false;
        $login_user = Auth::guard('admin_user')->User();
        
        return $this->restaurant_service->getRestaurantDatatableSource($draw,$start ,$length ,$search,$login_user);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('restaurants.restaurant-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRestaurantRequest $request)
    {
        
        $obj = [
        "name"              => $request->name,
        "owner"             => $request->owner_name,
        "logo"              => isset($request->logo)?$request->logo:null,
        "contact_person"    => $request->contact_person,
        "contact_email"     => $request->contact_email,
        "contact_number"    => $request->contact_number,
        "createdby_id"      => Auth::guard('admin_user')->User()->id
        ];

        
        $restaurant = $this->restaurant_service->save($obj);
        
        return view('restaurants.restaurant-view',compact('restaurant'))->with('success',$restaurant->name.' Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $restaurant = Restaurant::find($id);
        
        return view('restaurants.restaurant-view',compact('restaurant'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $restaurant = Restaurant::find($id);
        return view('restaurants.restaurant-form',compact('restaurant','restaurant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(Restaurant::where('id','=',$request->id)->exists())
        {
            $this->validate($request, [
                'id'            =>'required',
                'name'          =>'required',
                'owner_name'    =>'required',
                'contact_person'=>'required',
                'contact_number'=>'required',
                'contact_email' =>'required'
            ]);

            $restaurant_id = $request->id;
            $validator = Validator::make($request->all() , [
                'name' => [
                    'required',
                    Rule::unique('restaurant')->where(function($query)use($restaurant_id) {
                        return $query->where('id', '<>',$restaurant_id);                          
                    })
                ]
            ]);
    
            if ($validator->fails()) 
            {
                return redirect('edit-restaurant/'.$restaurant_id)
                            ->withErrors($validator)
                            ->withInput();
            }

            $obj = [
                'id'                => $request->id,
                'name'              => $request->name,
                'owner'             => $request->owner_name,
                'logo'              => isset($request->logo)?$request->logo:null,
                'contact_person'    => $request->contact_person,
                'contact_email'     => $request->contact_email,
                'contact_number'    => $request->contact_number,
                'updatedby_id'=> 1
                ];
            
            $restaurant = $this->restaurant_service->save($obj);

            return  redirect('/view-restaurant/'.$request->id);

        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
    /**
     * get litst of restaurants
     */
    public function getRestaurants(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');
        
        $search = (isset($request->search['value']))? $request->search['value'] : false;

        if($search)
        {
            $restaurants = Restaurant::select('name','owner','contact_person','contact_email','contact_number')->Where('name', 'like', '%' . $search . '%')->orderBy('id', 'desc')->offset($start)->limit($length)->get();
            $recordsFiltered = $restaurants->count();
            $recordsTotal = count($restaurants);
        }
        else
        {
            $restaurants = Restaurant::select('name','owner','contact_person','contact_email','contact_number')->orderBy('id', 'desc')->offset($start)->limit($length)->get();
            $recordsFiltered = Restaurant::all()->count();
            $recordsTotal = count($restaurants);
        }
       



        $resp = array();
        $i = 0;
        foreach($restaurants as $item)
        {

            $resp[$i][0] = $item->name; 
            $resp[$i][1] = $item->owner; 
            $resp[$i][2] = $item->contact_person; 
            $resp[$i][3] = $item->contact_email; 
            $resp[$i][4] = $item->contact_number;
            $i +=1; 
        }
  
        $data = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' =>  $recordsFiltered,
            'data' =>  $restaurants
        );


        return json_encode($data,true);
    }
    
}
