<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Services\Concrete\MenuCategoryService;
use App\Services\Concrete\RestaurantService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;


class ChoiceGroupController extends Controller
{

    // space where we declare services
    protected $menu_category_service;
    protected $restaurant_service;


    /**
     * Constructor
    */
    public function __construct(MenuCategoryService $menu_category_service,RestaurantService $restaurant_service)
    {
        // set the model
        $this->menu_category_service  = $menu_category_service;
        $this->restaurant_service  = $restaurant_service;
    
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $login_user = Auth::guard('admin_user')->User();

        $draw   = $request->get('draw');
        $start  = $request->get('start');
        $length = $request->get('length');
        $search = (isset($request->search['value']))? $request->search['value'] : false;
        return $this->menu_category_service->getChoiceGroupDatatableSource($draw,$start ,$length ,$search,$login_user);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $login_user = Auth::guard('admin_user')->User();
        
        switch($login_user ->role_id)
        {
            case '1':
                return view('choice-groups.choice-group-form');
            break;

            case '2':

                $branch =  $this->restaurant_service->getBranchesByRestaurantId($login_user->userRestaurant->restaurant_id);
                
                return view('choice-groups.choice-group-form',compact('branch'));

            break;

            case '3':
               
                $branch[] =  $this->restaurant_service->getBranchById($login_user->userRestaurant->branch_id);
                
                return view('choice-groups.choice-group-form',compact('branch'));
            
            break;
            
        }
        return view('choice-groups.choice-group-form');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'name'      =>'required',
            'min_choice'=>'required',
            'max_choice'=>'required',
            'choice'    =>'required'
        ]);

        $obj = [
            'name'         => $request->name,
            'branch_id'    => $request->branch,
            'min_choices'  => $request->min_choice,
            'max_choices'  => $request->max_choice,
            'sorting'      => 100
        ];
        
        $saved_choice_group = $this->menu_category_service->saveChoiceGroup($obj);
        $choice_group_id = $saved_choice_group->id;
        if($request->choice)
        {
            foreach ($request->choice as $key => $value) {
                
               $obj_choice =  [
                    'name'             => $value['choicename'],
                    'sorting'          => $value['sorting'],
                    'price'            => $value['price'],
                    'choice_group_id'  => $choice_group_id
        
                ];
                $this->menu_category_service->saveChoice($obj_choice);
            }
        }
       
        redirect('/view-choice-group/'.$request->id)->with('success',$saved_choice_group ->name.' Created');
        return response(
            $saved_choice_group ->id,
            200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $choice_group = $this->menu_category_service->getChoiceGroupById($id);

        if($choice_group)
            return view('choice-groups.choice-group-view',compact('choice_group'));
        else
            return  redirect('list-choicegroups');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $choice_group = $this->menu_category_service->getChoiceGroupById($id);
        $branch[]     =  $this->restaurant_service->getBranchById($choice_group->branch_id);

        return view('choice-groups.choice-group-form',compact('choice_group','branch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name'      =>'required',
            'min_choice'=>'required',
            'max_choice'=>'required',
            'choice'    =>'required'
        ]);
        
        $obj = [
            'id'           => $request->id,
            'name'         => $request->name,
            'min_choices'  => $request->min_choice,
            'max_choices'  => $request->max_choice,
            'branch_id'    => $request->branch
        ];
        
        $saved_choice_group = $this->menu_category_service->saveChoiceGroup($obj);
        $choice_group_id = $obj['id'];


        $this->menu_category_service->removeChoiceByChoiceGroupId($choice_group_id);

        if($request->choice)
        {
            foreach ($request->choice as $key => $value) {
   
                $obj_choice =  [
                    'id'               => 0,
                    'name'             => $value['choicename'],
                    'sorting'          => $value['sorting'],
                    'price'            => $value['price'],
                    'choice_group_id'  => $choice_group_id
        
                ];
                $this->menu_category_service->saveChoice($obj_choice);
            }
        }

        
       return response(
        $request->id,
        200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get Categories by Branch Id
     *
     * @return \Illuminate\Http\Response
     */
    public function getCategoriesByBranchId(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        
        // validate request
        $this->validate($request, [
            'id'  =>'required'
        ]);

        $categories = $this->menu_category_service->getCategoriesByBranchId($request->id);
       
        if($categories)
        {
            $response["Data"] =  $categories;
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 422;
            $response["ErrorMessage"] = "record not found";
            
            return response(
                $response,
                422
            );    

        }
        
        return $response;
    }
}
