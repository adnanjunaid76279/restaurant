<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
// use Illuminate\Validation\Validator;
use App\Models\AdminUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    //use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        // $this->middleware('guest:admin_user')->except('logout');
        if(Auth::guard('admin_user')->check()) 
        {
            return redirect()->intended('/home');
        }
    }

    public function showadminUserLoginForm()
    {
        return view('/auth/login');
    }

    public function adminUserLogin(Request $request)
    {
       
        $request->validate([
            'email'    => 'required',
            'password' => 'required|min:4'
        ]);

        
        $remember_me = $request->has('remember') ? true : false; // save Remember_Token

        $user = AdminUsers::where('login','=',$request->email)->where('password','=',$request->password)->first();
       
        if($user)
        {

            Auth::guard('admin_user')->login($user,$remember_me);
            
        // dd(Auth::guard('admin_user')->getSession());
            // return Auth::guard('admin_user')->User()->userRestaurant->branch_id;
            return redirect()->intended('/home');
        }
        return back()->withInput($request->only('login', 'remember'));
    }

    
    public function adminUserLogout(Request $request) 
    {
        Auth::guard('admin_user')->logout();
        return redirect('/login');
    }


}
