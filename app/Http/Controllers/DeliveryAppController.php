<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Services\Concrete\RiderService;
use App\Services\Concrete\RestaurantService;
use App\Services\Concrete\FirebaseService;
use App\Services\Concrete\OrderService;

class DeliveryAppController extends Controller
{

    // space where we declare services
    protected $rider_service;
    protected $restaurant_service;
    protected $firebase_service;
    protected $order_service;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(
        RiderService $rider_service,
        RestaurantService $restaurant_service ,
        FirebaseService $firebase_service,
        OrderService $order_service    
    )
    {
        // set the model
        $this->rider_service        = $rider_service;
        $this->restaurant_service   = $restaurant_service;
        $this->firebase_service     = $firebase_service;
        $this->order_service        = $order_service;
    }
    
    
    public function riderLogin(Request $request)
    {   
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        
        // validate request
        $validator = Validator::make($request->all(), [
            "uid" => "required",
            "pwd"  => "required",
        ]);
        
        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = $validator->errors();
            
            return response(
                $response,
                200
            );
        }

        // authenticate request
        $login = $this->rider_service->login($request->uid,$request->pwd);
        
        if($login)
        {
            $login = $this->rider_service->login($request->uid,$request->pwd);
            $branch = $this->restaurant_service->getBranchById($login->branchId);

            $login->restaurant_id = $branch->restaurant->id;
            $login->restaurant_name = $branch->restaurant->name;
            $login->branch_name = $branch->name;
           

            $response["Data"]["objRider"] = $login;
            $response["Data"]["listOrders"] = $this->rider_service->getActiveDeliveryOrdersByRiderId($login->id);

        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "Invalid login detail";
            
            return response(
                $response,
                200
            );    

        }
           
        return $response;
    }

    public function updateRider(Request $request)
    {   
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        // validate request
        $validator = Validator::make($request->all(), [
            "deviceId"   => "required",
            "latitude"   => "required",
            "longitude"  => "required",
            "userId"     => "required"  
        ]);
        
        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = $validator->errors();
            
            return response(
                $response,
                400
            );
        }



        $rider = $this->rider_service->getById($request->userId);

        $obj = [
            'id'                => $rider->id,
            'device_id'         => $request->deviceId,
            'latitude'          => isset($rider->latitude)?$request->latitude:$rider->latitude,
            'longitude'         => isset($rider->longitude)?$request->longitude:$rider->longitude
        ];

        $save_data = $this->rider_service->saveRider($obj);
        
        if($response)
        {
            $response["Data"] = $save_data;
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "record not found";
            
            return response(
                $response,
                200
            );    

        }

        return $response;

    }

    public function updatePassword(Request $request)
    {   
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        // validate request
        $validator = Validator::make($request->all(), [
            "new_password"  => "required",
            "first_name"    => "required",
            "last_name"     => "required",
            "old_password"  => "required",
            "userId"        => "required"  

        ]);
        
        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 400;
            $response["ErrorMessage"] = $validator->errors();
            
            return response(
                $response,
                400
            );
        }

        $rider = $this->rider_service->getById($request->userId);

        if($rider->password != $request->old_password)
        {
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "old password is not correct!";
            return $response;
        }

        $obj = [
            'id'                => $rider->id,
            'first_name'        => $request->first_name,
            'last_name'         => $request->last_name,
            'password'          => $request->new_password
        ];

        $save_data = $this->rider_service->saveRider($obj);
        
        if($response)
        {
            $response["Data"] = $save_data;
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "record not found";
            
            return response(
                $response,
                200
            );    

        }

        return $response;

    }

    public function isRiderAvailable(Request $request)
    {   
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        // validate request
        $validator = Validator::make($request->all(), [
            "status"  => "required",
            "userId"  => "required"  
        ]);
        
        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 400;
            $response["ErrorMessage"] = $validator->errors();
            
            return response(
                $response,
                400
            );
        }

        $rider = $this->rider_service->getById($request->userId);

        $obj = [
            'id'               => $rider->id,
            'is_online'        => $request->status == "true"?1:0
        ];
     
        $save_data = $this->rider_service->saveRider($obj);
        
        if($response)
        {
            $response["Data"] = $save_data;
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "record not found";
            
            return response(
                $response,
                200
            );    

        }

        return $response;

    }

    public function assignOrderToRider(Request $request)
    {   
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        // validate request
        $validator = Validator::make($request->all(), [
            "deliveryUserId"    => "required",
            "orderId"           => "required"  
        ]);
        
        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 400;
            $response["ErrorMessage"] = $validator->errors();
            
            return response(
                $response,
                400
            );
        }
     
        $order_exist = $this->rider_service->getRiderOrderByOrderIdRiderId($request->deliveryUserId,$request->orderId);
        
        if(!$order_exist)
        {
            $obj = [
                'order_id'                             => $request->orderId,
                'delivery_user_id'                     => $request->deliveryUserId,
                'delivery_user_order_status_id'        => 2
            ];
            $save_data = $this->rider_service->saveRiderOrder($obj);

            $order = $this->rider_service->getDeliveryOrderByOrderId($request->orderId);
            $order = $order[0];
           
            $rider = $this->rider_service->getById($request->deliveryUserId);
         
            $title = "New Delivery Order";
            $message = "New Delivery Order with OrderNo." . $order["orderNo"] . " need to be delivered";
            $server_key = $rider->server_key;
            $sender_id = $rider->sender_id;
            $device_id = $rider->device_id;
            
            $fire_base = $this->firebase_service->sendMessageByDeviceId($device_id,$server_key,$sender_id,$message,$title,$order);
          
        }
        else
        {
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "Order is already assigned!";

            return $response;
        }


        
        if($fire_base)
        {
            $response["Data"] = $fire_base;
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "record not found";
            
            return response(
                $response,
                200
            );    

        }

        return $response;

    }

    // get activer delivery orders by rider
    public function getActiveDeliveryOrders(Request $request)
    {   
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => [], "Status" => 200];
        // validate request
        $validator = Validator::make($request->all(), [
            "deliveryUserId"   => "required"
        ]);
        
        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = $validator->errors();
            
            return response(
                $response,
                200
            );
        }

        $orders = $this->rider_service->getActiveDeliveryOrdersByRiderId($request->deliveryUserId);

        
        if($orders)
        {
            $response["Data"] = $orders;
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "record not found";
            
            return response(
                $response,
                200
            );    

        }

        return $response;

    }

    // start delivery order
    public function startDeliveryOrder(Request $request)
    {
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        // validate request
        $validator = Validator::make($request->all(), [
        "orderId"           => "required",
        "start_latitude"    => "required",
        "start_longitude"   => "required",
        "userId"            => "required"
        ]);
        
        if ($validator->fails()) 
        {
            $response["Success"]   = false;
            $response["Status"] = 400;
            $response["ErrorMessage"] = $validator->errors();
            
            return response(
                $response,
                200
            );
        }

        $order = $this->rider_service->getRiderOrderByOrderIdRiderId($request->userId,$request->orderId);

        
        if($order)
        {
            
            if(!isset($request->start_latitude) || !isset($request->start_longitude))
            {
                $response["Success"]   = false;
                $response["Status"] = 200;
                $response["ErrorMessage"] = "starting latitude or start longitude are null!";
            }
            else
            {
                $order->start_latitude = $request->start_latitude;
                $order->start_longitude = $request->start_longitude;
                $order->current_latitude = $request->start_latitude;
                $order->current_longitude = $request->start_longitude;
                $order->order_in_progress = 1;
                
                // update status of order to Out for delivery in order table
                $this->order_service->changeOrderStatus($order->order_id,6);

                $response["Data"] = $this->rider_service->saveRiderOrder($order->toArray());
            }
            
            
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "record not found";
            
            return response(
                $response,
                200
            );    

        }

        return $response;

    }

    // end delivery order
    public function endDeliveryOrder(Request $request)
    {   
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        // validate request
        $validator = Validator::make($request->all(), [
        "orderId"         => "required",
        "end_latitude"    => "required",
        "end_longitude"   => "required",
        "userId"          => "required"
        ]);
        
        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = $validator->errors();
            
            return response(
                $response,
                200
            );
        }

        $order = $this->rider_service->getRiderOrderByOrderIdRiderId($request->userId,$request->orderId);
        
        if($order && $order->order_in_progress)
        {
            
            if(!isset($request->end_latitude) || !isset($request->end_longitude))
            {
                $response["Success"]   = false;
                $response["Status"] = 200;
                $response["ErrorMessage"] = "Current latitude or longitude are null!";
            }
            else
            {
                $order->end_latitude = $request->end_latitude;
                $order->end_longitude = $request->end_longitude;
                $order->delivery_user_order_status_id = 3;   // order closed
                $order->order_in_progress = 0;

                // update status of order to Delivered in order table
                $this->order_service->changeOrderStatus($order->order_id,2);

                //====== Calculate distance travel by rider    
                
                // Google API key
                $apiKey = env('GOOGLE_API_KEY');

            
                $branch = $this->restaurant_service->getBranchById($order->order->restaurant_branch_id);
            
                $addressFrom =  $order->start_latitude.",".$order->start_longitude;//"Main Boulevard Gulberg, Block H Gulberg 2, Lahore, Punjab";
                $addressTo   = $order->end_latitude.",".$order->end_longitude;//"Johar View Phase II, BOR Society, Lahore, Punjab, Pakistan";
                
                $geocodeFrom = file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=".$addressFrom."&destinations=".$addressTo."&sensor=false&key=".$apiKey);
            
                $outputFrom = json_decode($geocodeFrom);
            
                $save_data = $outputFrom;
                
                if($save_data->rows[0]->elements[0]->status != "ZERO_RESULTS")
                {
                    $distance = $save_data->rows[0]->elements[0]->distance->text;
                    $distance = explode(" ",$distance);
                    $del_dist = str_replace(",","",$distance[0]);
                    
                    if(trim($distance[1]) != "km")
                        $del_dist = $del_dist /1000; 
                    $order->travel_distance = $del_dist;      
                }
        

                $response["Data"] = $this->rider_service->saveRiderOrder($order->toArray());
            }
            
            
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "record not found";
            
            return response(
                $response,
                200
            );    

        }

        return $response;

    }

    // update delivery order which is on the way
    public function updateOnwayDeliveryOrder(Request $request)
    {   
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => null, "Status" => 200];
        // validate request
        $validator = Validator::make($request->all(), [
        "orderId"             => "required",
        "current_latitude"    => "required",
        "current_longitude"   => "required",
        "userId"              => "required"
        ]);
        
        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = $validator->errors();
            
            return response(
                $response,
                200
            );
        }

        $order = $this->rider_service->getRiderOrderByOrderIdRiderId($request->userId,$request->orderId);
        
        if($order)
        {
            
            if(!isset($request->current_latitude) || !isset($request->current_longitude))
            {
                $response["Success"]   = false;
                $response["Status"] = 200;
                $response["ErrorMessage"] = "Current latitude or longitude are null!";
            }
            else
            {
                $order->current_latitude = $request->current_latitude;
                $order->current_longitude = $request->current_longitude;
               
                $response["Data"] = $this->rider_service->saveRiderOrder($order->toArray());
            }
            
            
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "record not found";
            
            return response(
                $response,
                200
            );    

        }

        return $response;

    }

    
    // get orders of rider
    public function getDeliveryOrderHistory(Request $request)
    {   
        $response = ["Success" => true,"ErrorMessage" => null,"Data" => [], "Status" => 200];
        // validate request
        $validator = Validator::make($request->all(), [
            "deliveryUserId"   => "required"
        ]);
        
        if ($validator->fails()) 
        {   
            $response["Success"]   = false;
            $response["Status"] = 400;
            $response["ErrorMessage"] = $validator->errors();
            
            return response(
                $response,
                400
            );
        }

        $orders = $this->rider_service->getActiveDeliveryOrdersByRiderId($request->deliveryUserId,3); // get orders with close status against rider

        
        if($orders)
        {
            $response["Data"] = $orders;
        }
        else
        {
            
            $response["Success"]   = false;
            $response["Status"] = 200;
            $response["ErrorMessage"] = "record not found";
            
            return response(
                $response,
                200
            );    

        }

        return $response;

    }

}
