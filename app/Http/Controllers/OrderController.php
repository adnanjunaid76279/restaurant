<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Concrete\OrderService;
use Illuminate\Support\Facades\Auth;


class OrderController extends Controller
{
    protected $order_service;
     

    /**
    * Constructor
   */
   public function __construct(OrderService $order_service)
   {
       $this->order_service  = $order_service;
   }
    public function index()
    {
        $order_status = $this->order_service->getOrderStatus();
        
        return view('orders.order-list',compact('order_status'));
    }

    public function getorders(Request $request)
    {
      
        $login_user = Auth::guard('admin_user')->User();
        $order_status=$request->get('order_status');
        $start_date=$request->get('start_date');
        $end_date=$request->get('end_date');
        $draw   = $request->get('draw');
        $start  = $request->get('start');
        $length = $request->get('length');
        $search = (isset($request->search['value']))? $request->search['value'] : false;
       
        return $this->order_service->getOrdersDatatableSource($draw,$start ,$length ,$search,$start_date,$end_date,$order_status,$login_user);
    }
    
    public function getOrderDetailsByOrderId(Request $request)
    {
        $order_id = $request->order_id;       
        $order_details = $this->order_service->getOrderDetailsByOrderId($order_id);
        
        return response()->json($order_details);
    }
 
}
