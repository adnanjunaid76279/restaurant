<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\RestaurantBranch;
use App\Services\Concrete\RestaurantService;
use App\Services\Concrete\DeliveryDetailService;
use Illuminate\Support\Facades\File;

class RestaurantBranchController extends Controller
{

    // space where we declare services
    protected $restaurant_service;
    protected $delivery_detail_service;

        /**
     * Constructor
    */
    public function __construct(RestaurantService $restaurant_service,DeliveryDetailService $delivery_detail_service)
    {
        $this->restaurant_service  = $restaurant_service;
        $this->delivery_detail_service  = $delivery_detail_service;
    }

    public function index(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->get('start');
        $length = $request->get('length');
        $search = (isset($request->search['value']))? $request->search['value'] : false;
        $login_user = Auth::guard('admin_user')->User();

        return $this->restaurant_service->getBranchesDatatableSource($draw,$start ,$length ,$search,$login_user);

    }

    public function create()
    {
        $towns = $this->delivery_detail_service->getTownsByCityId(2);

        return view('restaurant-branches.branch-form',compact('towns'));
    }

    public function store(Request $request)
    {

        $validate_items =  [
            'branch_name'           =>'required',
            'contact_person'        =>'required',
            'contact_number'        =>'required',
            'contact_email'         =>'required',
            'branch_contact1'       =>'required',
            'latitude_input'        =>'required',
            'longitude_input'       =>'required',
            'location'              =>'required',
            'address'               =>'required',
            'currency'              =>'required',
            'open_time'             =>'required',
            'close_time'            =>'required',
            'minimum_order_value'   =>'required',
            'tax_percent'           =>'required',
            'town'                  =>'required'
        ];

        if(Auth::guard('admin_user')->User()->role_id == 1)
            $validate_items['restaurant_id'] =  'required';

        $this->validate($request,$validate_items);

        $obj = [
        "name"                   => $request->branch_name,
        "contact_person"         => $request->contact_person,
        "contact_number"         => $request->contact_number,
        "contact_email"          => $request->contact_email,
        "phone_number"           => $request->branch_contact1,
        "phone_number2"          => isset($request->branch_contact2)?$request->branch_contact2:null,
        "phone_number3"          => isset($request->branch_contact3)?$request->branch_contact3:null,
        "mobile_number"          => isset($request->branch_contact4)?$request->branch_contact4:null,
        "longitude"              => $request->longitude_input,
        'latitude'               => $request->latitude_input,
        'address'                => $request->address,
        'town_id'                => $request->town,
        'currency'               => $request->currency,
        "open_time"              => $request->open_time,
        "close_time"             => $request->close_time,
        "minimum_order_value"    => $request->minimum_order_value,
        "tax_percent"            => $request->tax_percent,
        "logo"                   => isset($request->logo)?$request->logo:null,
        "is_takeaway"            => isset($request->take_away)?1:0,
        "is_delivery"            => isset($request->delivery)?1:0,
        "is_dinein"              => isset($request->dinein)?1:0,
        "is_freedelivery"        => isset($request->free_delivery)?1:0,
        "tax_include"            => isset($request->tax_inclusive)?1:0,
        "order_taking"           => isset($request->order_allow)?1:0,
        "strn_number"             => isset($request->strn_number)?$request->strn_number:null,
        "ntn_number"             => isset($request->ntn_number)?$request->ntn_number:null,
        "description"            => isset($request->description)?$request->description:null,
        "createdby_id"           => Auth::guard('admin_user')->User()->id
        ];

        if(Auth::guard('admin_user')->User()->role_id == 2)
            $obj['restaurant_id'] = Auth::guard('admin_user')->User()->userRestaurant->restaurant_id;
        elseif(Auth::guard('admin_user')->User()->role_id == 1)
            $obj['restaurant_id'] = $request->restaurant_id;



        $branch = $this->restaurant_service->saveBranch($obj);

        if($branch)
        {
            if (! File::exists(public_path('/Temp')."/".$branch->id)) {
                File::makeDirectory(public_path('/Temp')."/".$branch->id);
            }
        }
        return view('restaurant-branches.branch-view',compact('branch'))->with('success',$branch->name.' Created');
    }

    public function show($id)
    {
        $login_user = Auth::guard('admin_user')->User();

        switch($login_user->role_id)
        {
            case '1':
                $branch =  RestaurantBranch::where('id','=',$id)->first();
            break;
            case '2':
                $branch =  RestaurantBranch::where('id','=',$id)->where('restaurant_id','=',$login_user->userRestaurant->restaurant_id)->first();
            break;
        }

        if($branch)
            return view('restaurant-branches.branch-view',compact('branch'));
        else
            return  redirect('list-restaurant-branches');

    }

    public function edit($id)
    {
        $login_user = Auth::guard('admin_user')->User();

        switch($login_user->role_id)
        {
            case '1':

                $branch =  RestaurantBranch::where('id','=',$id)->first();
                $towns = $this->delivery_detail_service->getTownsByCityId(2);

                break;

            case '2':
                $towns = $this->delivery_detail_service->getTownsByCityId(2);
                $branch =  RestaurantBranch::where('id','=',$id)->where('restaurant_id','=',$login_user->userRestaurant->restaurant_id)->first();

            break;
        }


        if($branch)
            return view('restaurant-branches.branch-form',compact('branch','towns'));
        else
            return  redirect('list-restaurant-branches');

    }

    public function update(Request $request)
    {
        if(RestaurantBranch::where('id','=',$request->id)->exists())
        {
            $this->validate($request, [
                'id'                    =>'required',
                'branch_name'           =>'required',
                'contact_person'        =>'required',
                'contact_number'        =>'required',
                'contact_email'         =>'required',
                'branch_contact1'       =>'required',
                'latitude_input'        =>'required',
                'longitude_input'       =>'required',
                'location'              =>'required',
                'address'               =>'required',
                'currency'              =>'required',
                'open_time'             =>'required',
                'close_time'            =>'required',
                'minimum_order_value'   =>'required',
                'tax_percent'           =>'required',
                'town'                  =>'required'
            ]);

            $obj = [
                'id'                    => $request->id,
                'name'                  => $request->branch_name,
                'contact_person'        =>  $request->contact_person,
                'contact_number'        =>  $request->contact_number,
                'contact_email'         => $request->contact_email,
                'phone_number'          => $request->branch_contact1,
                'phone_number2'         => isset($request->branch_contact2)?$request->branch_contact2:null,
                'phone_number3'         => isset($request->branch_contact3)?$request->branch_contact3:null,
                'mobile_number'         => isset($request->branch_contact4)?$request->branch_contact4:null,
                'longitude'             => $request->longitude_input,
                'latitude'              => $request->latitude_input,
                'address'               => $request->address,
                'town_id'               => $request->town,
                'currency'              => $request->currency,
                'open_time'             => $request->open_time,
                'close_time'            => $request->close_time,
                'minimum_order_value'   => $request->minimum_order_value,
                'tax_percent'           => $request->tax_percent,
                'logo'                  => isset($request->logo)?$request->logo:null,
                'is_takeaway'           => isset($request->take_away)?1:0,
                'is_delivery'           => isset($request->delivery)?1:0,
                'is_dinein'             => isset($request->dinein)?1:0,
                'is_freedelivery'       =>isset($request->free_delivery)?1:0,
                'tax_include'           => isset($request->tax_inclusive)?1:0,
                'order_taking'          => isset($request->order_allow)?1:0,
                "strn_number"             => isset($request->strn_number)?$request->strn_number:null,
                'ntn_number'            => isset($request->ntn_number)?$request->ntn_number:null,
                'description'           => isset($request->description)?$request->description:null,
                'updatedby_id'          => Auth::guard('admin_user')->User()->id

            ];

            if(Auth::guard('admin_user')->User()->role_id == 2)
                $obj['restaurant_id'] = Auth::guard('admin_user')->User()->userRestaurant->restaurant_id;
            elseif(Auth::guard('admin_user')->User()->role_id == 1)
                $obj['restaurant_id'] = $request->restaurant_id;

            $this->restaurant_service->saveBranch($obj);

            return  redirect('/view-branch/'.$request->id)->with('success',$obj['name'].' Updated');

        }
    }

    public function destroy($id)
    {
        //
    }

    // Restuarant Branh Waiter Functionality

    public function getBranchWaiters(Request $request){

        $draw   = $request->get('draw');
        $start  = $request->get('start');
        $length = $request->get('length');
        $search = (isset($request->search['value']))? $request->search['value'] : false;
        $login_user = Auth::guard('admin_user')->User();

        return $this->restaurant_service->getBrancheWaitersDatatableSource($draw,$start ,$length ,$search,$login_user);

    }

    public function createBranchWaiters(){

        $login_user = Auth::guard('admin_user')->User();

        switch($login_user->role_id)
        {
            case '1':
                return view('branch-waiters.branch-waiter-form');
                break;

            case '2':
            case '3':

                $branch =  $this->restaurant_service->getAllBranches($login_user);
                return view('branch-waiters.branch-waiter-form',compact('branch'));
                break;

        }

    }

    public function storeBranchWaiters(Request $request){

        $validate_items =  [
            'branch'           =>'required',
            'name'        =>'required',
        ];

        $this->validate($request,$validate_items);

        if($request->is_active == "on"){
            $request->is_active = 1;
        }else{
            $request->is_active = 0;
        }

        $obj = [
            "branch_id"           => $request->branch,
            "name"                => $request->name,
            "is_active"           => $request->is_active
        ];

        $branch_waiter = $this->restaurant_service->saveBranchWaiter($obj);


        return view('branch-waiters.branch-waiter-view',compact('branch_waiter'))->with('success',$branch_waiter->name.' Created');

    }

    public function showBranchWaiters($id){

        $login_user = Auth::guard('admin_user')->User();

        $branch_waiter = $this->restaurant_service->getBranchWaiterByBranchId($id,$login_user );

        if($branch_waiter)
            return view('branch-waiters.branch-waiter-view',compact('branch_waiter'));
        else
            return  redirect('list-branch-waiters');
    }

    public function editBranchWaiters($id){

        $login_user = Auth::guard('admin_user')->User();
        $branch_waiter = $this->restaurant_service->getBranchWaiterByBranchId($id,$login_user );
        $branch[]     =  $this->restaurant_service->getBranchById($branch_waiter->branch_id);
        return view('branch-waiters\branch-waiter-form',compact('branch_waiter','branch'));

    }

    public function updateBranchWaiters(Request $request){


        $validate_items =  [
            'branch'       =>'required',
            'name'         =>'required',
            'id'           =>'required'
        ];

        $this->validate($request,$validate_items);

        if($request->is_active == "on"){
            $request->is_active = 1;
        }else{
            $request->is_active = 0;
        }

        $obj = [
            'id'                => $request->id,
            "branch_id"         => $request->branch,
            "name"              => $request->name,
            "is_active"         => $request->is_active
        ];

        $this->restaurant_service->saveBranchWaiter($obj);

        return  redirect('view-branch-waiter/'.$request->id)->with('success',$obj['name'].' Updated');

    }

    // Restaurant Branch Waiter Functions Ends
}
