<?php namespace App\Services\Abstractt;

interface IDeliveryDetailService
{
    public function getDetailByOrderIds($order_ids);
    public function save($obj);
    public function saveAddress($obj);
    public function getAddressByUserId($user_id);
    public function getTownsByCityId($city_id);
    public function getTownBlocksByTownId($town_id);
    public function getAddressTypes();
    public function getAddressByUserIdAddTypeId($user_id,$add_type_id);  // get address by add type id and user id

}