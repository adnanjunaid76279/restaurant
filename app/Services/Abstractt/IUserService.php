<?php namespace App\Services\Abstractt;

interface IUserService
{
    public function getById($user_id);
    public function getUserdetailById($user_id);
    public function save($obj);         // save user
    public function saveAppUser($obj);  // save app user
    public function getByContactAppId($contactno,$app_id);
    public function savePosUser($obj);  //used for creating user in pos
    public function saveWebUser($obj);  //used for creating user in Web
    public function saveMobileUser($obj);  //used for creating user in Mobile
    public function getByContact($contact);
    public function getUserIdByFbIdAppId($fb_id,$app_id);
    public function getAppUserByFbIdAppIdUserId($app_id,$user_id,$fb_id);
    public function getDeliveryUserActiveOrderByOrderId($order_id);   // get delivery user order by order id
    public function getByContactBranchLoginTypeId($contactno,$branch_id,$login_type_id);
    public function getBySocialAppId($social_app_id);
    public function getUserByEmaliPasswordBranchId($email,$pwd,$branch_id);

    //used dashboard in admin pannel
    public function getUsersCountByDate($datee,$branch_id,$login_user=null);
    public function getUsersCountByDateRange($start_date,$end_date,$branch_id,$login_user=null);

  
}
