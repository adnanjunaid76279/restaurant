<?php namespace App\Services\Abstractt;

interface IRestaurantService
{
    public function getBranchesDatatableSource($draw,$start,$length,$search,$login_user);
    public function getRestaurantDatatableSource($draw,$start,$length,$search,$login_user);
    public function getBranchById($id);
    public function save($obj); // save restaurant
    public function saveBranch($obj);  // save branch
    public function getAllRestaurants();
    public function getAllBranches($login_user=null);
    public function getBranchesByRestaurantId($id);
    public function searchRestaurantsByName($name);
    public function getBranchRatingsByBranchId($branch_id);
    public function addRemoveFavBranch($fav,$user_id,$branch_id);   // add remove a branch in favourite
    public function updateBranchDailyCount();   // update daily order count in of branch

    // Branch Waiters //
    public function getBrancheWaitersDatatableSource($draw,$start ,$length ,$search,$login_user);
    public function saveBranchWaiter($obj);
    public function getBranchWaiterByBranchId($id,$login_user );

}
