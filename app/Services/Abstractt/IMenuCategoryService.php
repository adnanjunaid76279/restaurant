<?php namespace App\Services\Abstractt;

interface IMenuCategoryService 
{
    public function getAll();
    public function getDatatableSource($draw,$start,$length,$search,$login_user);
    public function getMenuDatatableSource($draw,$start,$length,$search,$login_user);
    public function getChoiceGroupDatatableSource($draw,$start,$length,$search,$login_user);
    public function saveCategory($obj);
    public function saveMenu($obj);
    public function saveMenuVariation($obj);
    public function saveChoiceGroup($obj);   // save choice group
    public function saveMenuVariationChoiceGroup($obj);// save menu variation vs choice group
    public function saveMenuChoiceGroup($obj);  //save menu vs choice group
    public function saveChoice($obj); // save choice
    public function getCategoryById($id);
    public function getCategoryByIdAndUserRole($id,$login_user);
    public function getMenuById($id);
    public function getMenuByBranchId($branch_id);
    public function getMenuByIdAndUserRole($id,$login_user);
    public function getCategoriesByBranchId($branch_id);
    public function getCategoriesWithMenuByBranchIdUserId($branch_id,$user_id);
    public function getFavMenusByUserId($user_id);
    public function getFavMenusByUserIdBranchId($user_id,$branch_id);
    public function getMenuRatingsByBranchId($branch_id);
    public function getTopMenuRatingByBranchId($branch_id);

    public function rateMenu($rating,$user_id,$menu_id);    // rate a menu by userid and menuid
    public function addRemoveFavMenu($fav,$user_id,$menu_id); // addremove menu in favourite list

    public function createMenuItemOnErp($menu_id); // create menu item on erp side

    public function removeVariationByMenuId($menu_id); //remove menu variation by menu id
    public function removeChoiceByChoiceGroupId($choice_group_id);  // remove choices by choice group id
    public function removeMenuVarChoiceGrpByMenuId($menu_id);  //  remove menu_variation_choice_group by menu id
    public function removeMenuChoiceByChoiceGroupId($menu_id);        // remove menu choice group by menu id

    public function getMenuVariationAndChoicesByMenuId($menu_id); // get menu variation and choices by menu id
    public function getChoiceGroupById($choice_group_id); // get choice group by id
    public function getChoiceGroups($login_user=null); // get choice group by login user or all
}