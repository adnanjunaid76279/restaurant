<?php namespace App\Services\Abstractt;

interface IPosService
{
    public function posLogin($username,$password);
    public function getRestaurantBranchesNameAndId($restaurant_id);
    public function getCategoriesByBranchId($branch_id,$user_id=0);
    public function getPosCashFlow($branch_id,$datee);
    public function savePosCashFlow($obj);
    public function getBranchTableDatatableSource($draw,$start,$length,$search,$login_user);
    public function saveBranchTable($obj);
    public function getBranchTableByIdAndUserRole($id,$login_user);
}
