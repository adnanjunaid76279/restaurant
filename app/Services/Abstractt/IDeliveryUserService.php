<?php namespace App\Services\Abstractt;
interface IDeliveryUserService
{
 public function getDeliveryUsersDatatableSource($draw,$start ,$length ,$search,$login_user);
 public function getDeiveryUserById($id);
 public function save($obj);
}