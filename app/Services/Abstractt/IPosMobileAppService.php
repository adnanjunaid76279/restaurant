<?php namespace App\Services\Abstractt;

interface IPosMobileAppService
{
    public function posMobileAppLogin($username,$password);
    public function getRestaurantBranchesNameAndId($restaurant_id);
    public function getCategoriesByBranchId($branch_id,$user_id=0);

}