<?php namespace App\Services\Abstractt;

interface IAgentUserService
{
    public function getByBranchId($branch_id);
    public function getById($id);
    public function getAgentUsersDatatableSource($draw,$start ,$length ,$search,$login_user);
    public function getAgentRoles();    // get all roles
    public function save($obj);
    public function getRoleById($id);

}
