<?php namespace App\Services\Abstractt;

interface IFirebaseService
{
    public function sendMessageByDeviceId($device_id,$server_key,$sender_id,$message,$title,$image);
}