<?php namespace App\Services\Abstractt;

interface IReportService
{
    // dashboard
    public function getSaleByWeekDayMonthYear($login_user,$today_date,$branch_id);
    public function getSaleByWeekDayMonth($login_user,$today_date,$branch_id);
    public function getSaleByTownMonth($login_user,$today_date,$branch_id);
    public function getSaleByMenuCategoryMonth($login_user,$today_date,$branch_id);
    public function getSalesByTopSellingDay($login_user,$today_date,$branch_id);
    public function getOrderCountByBranchMonthYear($login_user,$today_date,$branch_id);
    public function getOrderCountByBranchWeekMonth($login_user,$today_date,$branch_id);
    public function getTopOrderedUsers($login_user,$today_date,$branch_id);

    //reports
    public function getConsumedMenuItemsByDateByBranch($draw,$start ,$length ,$search,$login_user,$start_date,$end_date,$branch=null);
    public function getSaleDetailByDateByBranch($draw,$start ,$length ,$search,$login_user,$start_date,$end_date,$branch=null);
    public function getSaleVsKiloMeterByMonth($login_user,$month,$branch);
}
