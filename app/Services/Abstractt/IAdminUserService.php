<?php namespace App\Services\Abstractt;

interface IAdminUserService
{
    public function getAdminUsersDatatableSource($draw,$start ,$length ,$search,$login_user);
    public function save($obj);
    public function saveUserRestaurant($obj);  // save object of user restaurant
    public function getById($id);
    public function getRoles();    // get all roles
    public function getRolesByLoginUser($login_user);    // get all roles of admin user by login user
    public function getRoleById($id);

      //Permission
      public function getAdminUsersPermissionsDatatableSource($draw,$start ,$length ,$search,$login_user);
      //Roles
      public function getAdminUsersRolesDatatableSource($draw,$start ,$length ,$search,$login_user);
}