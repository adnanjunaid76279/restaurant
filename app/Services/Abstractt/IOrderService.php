<?php namespace App\Services\Abstractt;

interface IOrderService
{
    public function getUserOrdersWithDetailByRestID($user_id,$restaurant_id);
    public function getUserOrdersWithDetailByBranchId($user_id,$branch_id);
    public function getMobileUserOrdersWithDetailByBranchId($user_id,$branch_id); // mobile order history
    public function getOrdersByBranchId($branch_id,$from_date=null,$end_date=null, $from=null, $limit=300, $order_by="id", $sorting="ASC");
    public function getOrdersByBranchIdAgentId($branch_id,$agent_id,$from_date=null,$end_date=null); // get orders by branch id and agent id for kitchen screen
    public function getPosMobileOrdersByBranchId($branch_id,$from_date=null,$end_date=null,  $from=null, $limit=300, $order_by="id", $sorting="ASC"); // pos mobile app
    public function getOrderStatus();
    public function save($obj_order);
    public function getById($id);
    public function getByOnlinePayOrderId($id);
    public function changeOrderStatus($order_id,$status_id);
    public function setOrerStatusTime($obj); // set order status time via status id
    public function changeOrdStatustoReadyViaAllDetailReady($order_detail_id); // change order status to ready if all detail items' status is ready
    public function insertErpInventoryTrans($order_id);  // inventory entry in external cherryberry ERP
    public function getOrderByTrackId($id,$branch_id);
    // Dashboard
    public function getSaleSumByDate($datee,$branch_id,$login_user=null);
    public function getSaleSumByDateRange($start_date,$branch_id,$end_date,$login_user=null);
    public function getOrderCountByDate($datee,$branch_id,$login_user=null);
    public function getOrderCountByDateRange($start_date,$end_date,$branch_id,$login_user=null);
    public function getOrderCountByResourceAndDateRange($start_date,$end_date,$branch_id,$login_user=null);
    public function getOrderSumByTypeAndDateRange($start_date,$end_date,$login_user=null);
    public function getOrdersDatatableSource($draw,$start ,$length ,$search,$start_date,$end_date,$order_status,$login_user);
    public function getAveragePreparationTimeByDay($datee, $branch_id,$login_user=null);

    //orders
    public function getOrderDetailsByOrderId($order_id);

}
