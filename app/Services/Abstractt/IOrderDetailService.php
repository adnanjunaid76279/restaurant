<?php namespace App\Services\Abstractt;

interface IOrderDetailService
{
    public function getOrderDetailByOrderIds($restaurant_id);
    public function getOrderDetailByAgentId($agent_id);
    public function save($obj);
    public function saveDetailChoices($obj);
    public function changeOrderDetailStatus($ord_detail_id,$status_id);

}