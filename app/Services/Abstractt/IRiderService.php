<?php namespace App\Services\Abstractt;

interface IRiderService
{
    public function getActiverRiders($branch_id);
    public function login($username,$password);
    public function getActiveDeliveryOrdersByRiderId($rider_id,$status_id=2);
    public function getById($rider_id);
    public function saveRider($obj);    
    public function getRiderOrderByOrderIdRiderId($rider_id,$order_id); // get data from delivery users order table
    public function saveRiderOrder($obj); // save rider order
    public function getDeliveryOrderByOrderId($order_id);  // get delivery order by order id 
    public function getRiderOrderByOrderId($order_id);   // get data from delivery users order table by order
    
}
