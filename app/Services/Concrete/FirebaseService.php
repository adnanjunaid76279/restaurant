<?php namespace App\Services\Concrete;

use App\Repository\Repository;
use App\Services\Abstractt\IFirebaseService;


class FirebaseService  implements IFirebaseService
{
    protected $firebase_url ;
    
    public function __construct()
    {
       // set the url
       $this->firebase_url = env('FIREBASE_URL');
    }

    // send firebase message to a device
    public function sendMessageByDeviceId($device_id,$server_key,$sender_id,$message,$title,$image)
    {
        $msg = array("message" => $message,"title" => $title);
       
        $data = array("to" => $device_id, "notification" => array( "title" => $title, "body" => $message,"sound" => "Enabled","image" => $image));
        $data_string = json_encode($data);

        $headers = array (
                'Authorization: key=' . $server_key,
                'Sender: id=' . $sender_id,
                'Content-Type: application/json'
        );
        
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $this->firebase_url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $data_string );

        $result = curl_exec ( $ch );
        curl_close ( $ch );
        $result = json_decode($result);
   
        if(!$result)
            return false;
        
        return $result; 


    }    

}