<?php namespace App\Services\Concrete;

use Illuminate\Support\Facades\Http;
use App\Repository\Repository;
use App\Services\Abstractt\IMenuCategoryService;
use App\Models\MenuCategory;
use App\Models\Menu;
use App\Models\MenuFav;
use App\Models\MenuRating;
use App\Models\MenuVariation;
use App\Models\Choice;
use App\Models\ChoiceGroup;
use App\Models\MenuVariationChoiceGroup;
use App\Models\MenuChoiceGroup;
use DB;


class MenuCategoryService  implements IMenuCategoryService
{

    protected $model_menu_category;
    protected $model_menu;
    protected $model_menu_fav;
    protected $model_menu_rating;
    protected $model_menu_variation;
    protected $model_choice_group;
    protected $model_choice;
    protected $model_menu_var_choice_grp;
    protected $model_menu_choice_grp;
    
    public function __construct()
    {
       // set the model
       $this->model_menu_category = new Repository(new MenuCategory);
       $this->model_menu = new Repository(new Menu);
       $this->model_menu_fav = new Repository(new MenuFav);
       $this->model_menu_rating = new Repository(new MenuRating);
       $this->model_menu_variation = new Repository(new MenuVariation);
       $this->model_choice_group = new Repository(new ChoiceGroup);
       $this->model_choice = new Repository(new Choice);
       $this->model_menu_var_choice_grp = new Repository(new MenuVariationChoiceGroup);
       $this->model_menu_choice_grp = new Repository(new MenuChoiceGroup);
    }

    // get by id
    public function getCategoryById($id)
    {
        $category = $this->model_menu_category->getModel()::where('is_active',true)->where('id',$id)->first();

        if(!$category)
            return false;

        return $category;

    }

    // get by id and user role
    public function getCategoryByIdAndUserRole($id,$login_user)
    {
        switch($login_user->role_id)
        {
            case '1':
                $category = $this->model_menu_category->getModel()::select('menu_category.*')
                    ->where('menu_category.id','=',$id)
                    ->where('menu_category.is_active',true)->first();
            break;

            case '2':
                $category = $this->model_menu_category->getModel()::
                select('menu_category.*')
                ->join('restaurant_branch', 'menu_category.restaurant_branch_id', '=', 'restaurant_branch.id')
                ->where('menu_category.id','=',$id)
                ->where('menu_category.is_active',true)
                ->where('restaurant_branch.restaurant_id','=',$login_user->userRestaurant->restaurant_id)->first();
            break;

            case '3':
                $category = $this->model_menu_category->getModel()::select('menu_category.*')
                        ->where('menu_category.is_active',true)
                        ->where('menu_category.id','=',$id)
                        ->where('restaurant_branch_id','=',$login_user->userRestaurant->branch_id)->first();
            break;

        }



        if(!$category)
             return false;

        return $category;

    }

    // get categories by branch id
    public function getCategoriesByBranchId($branch_id)
    {
        $categories = $this->model_menu_category->getModel()::where('restaurant_branch_id','=',$branch_id)->where('menu_category.is_active',true)->orderby('name')->get();

        if(!$categories)
            return false;

        return $categories;
    }

    // get menu by id
    public function getMenuById($id)
    {
        $menu = $this->model_menu->find($id);

        if(!$menu)
            return false;

            return $menu;
    }
     // get menu by branch id
    public function getMenuByBranchId($branch_id)
    {
        $menu = $this->model_menu->getModel()::select('menu.*')
                ->join('menu_category', 'menu_category.id', '=', 'menu.menu_category_id')
                ->where('menu_category.restaurant_branch_id','=',$branch_id)
                ->orderBy('menu.id','DESC')
                ->get();

        if(!$menu)
            return false;

        return $menu;
    }

    // get menu by id and user role
    public function getMenuByIdAndUserRole($id,$login_user)
    {

        switch($login_user->role_id)
        {
            case '1':
                $menu = $this->model_menu->getModel()::
                    select('menu.*')
                    ->join('menu_category', 'menu.menu_category_id', '=', 'menu_category.id')
                    ->join('restaurant_branch', 'menu_category.restaurant_branch_id', '=', 'restaurant_branch.id')
                    ->where('menu.id','=',$id)->first();
            break;

            case '2':
                $menu = $this->model_menu->getModel()::
                    select('menu.*')
                    ->join('menu_category', 'menu.menu_category_id', '=', 'menu_category.id')
                    ->join('restaurant_branch', 'menu_category.restaurant_branch_id', '=', 'restaurant_branch.id')
                    ->where('restaurant_branch.restaurant_id','=',$login_user->userRestaurant->restaurant_id)
                    ->where('menu.id','=',$id)
                    ->first();
                   
            break;

            case '3':
                $menu = $this->model_menu->getModel()::select('menu.*')
                        ->join('menu_category', 'menu.menu_category_id', '=', 'menu_category.id')
                        ->where('menu_category.restaurant_branch_id','=',$login_user->userRestaurant->branch_id)
                        ->where('menu.id','=',$id)
                        ->first();
            break;

        }



        if(!$menu)
            return false;

            return $menu;

    }

    // save category
    public function saveCategory($obj)
    {

        if(isset($obj['id']) && $obj['id'] > 0)
        {
            $obj['date_updated'] = date("Y-m-d H:i:s");
            $this->model_menu_category->update($obj,$obj['id']);
            $saved_obj = $this->model_menu_category->find($obj['id']);

        }
        else
        {
            $obj['date_created'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_menu_category->create($obj);
        }

        if(!$saved_obj)
            return false;

            return $saved_obj;
    }

    // save menu
    public function saveMenu($obj)
    {

        if(isset($obj['id']) && $obj['id'] > 0)
        {
            $obj['date_updated'] = date("Y-m-d H:i:s");
            $this->model_menu->update($obj,$obj['id']);
            $saved_obj = $this->model_menu->find($obj['id']);
        }
        else
        {
            $obj['date_created'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_menu->create($obj);

            // call function to create menu item on erp side
            $this->createMenuItemOnErp($saved_obj->id);
        }

        if(!$saved_obj)
            return false;

            return $saved_obj;
    }

    // get all categories
    public function getAll()
    {
        $category = $this->model_menu_category->all();

        if(!$category)
            return false;

            return $category;
    }

    // get menu category datatable source
    public function getDatatableSource($draw,$start,$length,$search,$login_user)
    {

        switch($login_user->role_id)
        {
            case '2':

               $wh = " WHERE restaurant_branch.restaurant_id =  ".$login_user->userRestaurant->restaurant_id;

            break;

            case '3':

                $wh = " WHERE restaurant_branch.id =  ".$login_user->userRestaurant->branch_id;

            break;

            default:

                $wh = " WHERE 1 ";

        }


        $limit = " LIMIT ".$length." OFFSET ".$start;
        $raw_qry = "";

        if($search)
        {
            $wh = $wh." AND (menu_category.name LIKE \"%" . $search. "%\" OR restaurant_branch.name LIKE \"%" . $search. "%\" ) ";
            $raw_qry = "SELECT menu_category.id,menu_category.name,menu_category.is_active,restaurant_branch.name as branch
                        FROM menu_category
                        JOIN restaurant_branch on menu_category.restaurant_branch_id = restaurant_branch.id
                        ".$wh." Order BY menu_category.name asc";

            // total records
            $raw_qry_count = "SELECT count(*) as total_category FROM menu_category WHERE is_active = 1 ";
            $records_total = DB::select( DB::raw($raw_qry_count) );
            $recordsTotal = $records_total[0]->total_category;

            // filtered or search records
            $categories = DB::select( DB::raw($raw_qry) );
            $recordsFiltered  = count($categories);

            // actual records return to client
            $categories = DB::select( DB::raw($raw_qry.$limit) );

        }
        else
        {
            $raw_qry = "SELECT menu_category.id,menu_category.name,menu_category.is_active,restaurant_branch.name as branch
            FROM menu_category
            JOIN restaurant_branch on menu_category.restaurant_branch_id = restaurant_branch.id
            ".$wh."  Order BY restaurant_branch.name asc,menu_category.name asc";
            $categories = DB::select( DB::raw($raw_qry) );
            $recordsTotal = $recordsFiltered = count($categories);
            $categories = DB::select( DB::raw($raw_qry. $limit) );

        }

        $resp = array();
        $i = 0;
        foreach($categories as $item)
        {

            $resp[$i][0] = $item->name;
            $resp[$i][1] = '<label class="switch switch-success mr-3" >
                            <input type="checkbox" data-cat-id="'.$item->id.'" class="cat_active" '.($item->is_active ? "checked":"").' data-toggle="tooltip" data-placement="bottom" title="Tooltip" data-original-title="Tooltip on bottom">
                            <span class="slider"></span>
                            </label>';

            $edit_column    = "<a class='btn btn-success mr-2' data-toggle='tooltip' data-placement='bottom'  data-original-title='Tooltip on bottom' href='".url('/edit-category')."/".$item->id."'><i title='Edit' class='nav-icon mr-2 i-Pen-2'></i>Edit</a>";
            $view_column    = "<a class='btn btn-warning mr-2' href='".url('/view-category')."/".$item->id."'><i title='View' class='nav-icon mr-2 i-Full-View-Window'></i>View</a>" ;
            $action_column =    $edit_column.$view_column;

            $resp[$i][2] = $item->branch;
            $resp[$i][3] = $action_column;
            $i +=1;
        }

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' =>  $recordsFiltered,
            'data' =>  $resp
        );

        return json_encode($data,true);

    }

    // get menu datatable source
    public function getMenuDatatableSource($draw,$start,$length,$search,$login_user)
    {

        switch($login_user->role_id)
        {
            case '2':

               $wh = " AND restaurant_branch.restaurant_id =  ".$login_user->userRestaurant->restaurant_id;

            break;

            case '3':

                $wh = " AND restaurant_branch.id =  ".$login_user->userRestaurant->branch_id;

            break;

            default:

                $wh = " AND 1 ";

        }


        $limit = " LIMIT ".$length." OFFSET ".$start;
        $raw_qry = "";

        if($search)
        {
            $wh = $wh." AND (menu.name LIKE \"%" . $search. "%\" OR menu_category.name LIKE \"%" . $search. "%\" OR restaurant_branch.name LIKE \"%" . $search. "%\" ) ";
            $raw_qry = "SELECT menu.id,menu.price,menu.name,menu.is_deleted,menu_category.name as category,restaurant_branch.name as branch
                        FROM menu
                        JOIN menu_category on menu.menu_category_id = menu_category.id
                        JOIN restaurant_branch on menu_category.restaurant_branch_id = restaurant_branch.id
                        WHERE 1 ".$wh." Order BY menu.name asc";

            // total records
            $raw_qry_count = "SELECT count(*) as total_menu FROM menu JOIN menu_category on menu.menu_category_id = menu_category.id
                            JOIN restaurant_branch on menu_category.restaurant_branch_id = restaurant_branch.id
                            WHERE 1 ".$wh;
            $records_total = DB::select( DB::raw($raw_qry_count) );
            $recordsTotal = $records_total[0]->total_menu;

            // filtered or search records
            $menus = DB::select( DB::raw($raw_qry) );
            $recordsFiltered  = count($menus);

            // actual records return to client
            $menus = DB::select( DB::raw($raw_qry.$limit) );

        }
        else
        {
            $raw_qry = "SELECT menu.id,menu.price,menu.is_deleted,menu.name,menu_category.name as category,restaurant_branch.name as branch
            FROM menu
            JOIN menu_category on menu.menu_category_id = menu_category.id
            JOIN restaurant_branch on menu_category.restaurant_branch_id = restaurant_branch.id
            WHERE  1 ".$wh."  Order BY menu_category.name asc";
            $menus = DB::select( DB::raw($raw_qry) );
            $recordsTotal = $recordsFiltered = count($menus);
            $menus = DB::select( DB::raw($raw_qry. $limit) );

        }

        $resp = array();
        $i = 0;
        foreach($menus as $item)
        {

            $resp[$i][0] = $item->name;
            $resp[$i][1] = '<label class="switch switch-success mr-3" >
            <input type="checkbox" data-menu-id="'.$item->id.'" class="menu_active" '.($item->is_deleted == 1 ? "checked":"").' data-toggle="tooltip" data-placement="bottom" title="Tooltip" data-original-title="Tooltip on bottom">
            <span class="slider"></span>
            </label>';
            $resp[$i][2] = $item->price;
            $resp[$i][3] = $item->category;
            $resp[$i][4] = $item->branch;

            $edit_column    = "<a class='btn btn-success mr-2' href='".url('/edit-menu')."/".$item->id."'><i title='Edit' class='nav-icon mr-2 i-Pen-2'></i>Edit</a>";
            $view_column    = "<a class='btn btn-warning mr-2' href='".url('/view-menu')."/".$item->id."'><i title='View' class='nav-icon mr-2 i-Full-View-Window'></i>View</a>" ;
            $delete_column  = "<a class='btn btn-danger mr-2' href='".url('/delete-menu')."/".$item->id."'><i title='View' class='nav-icon mr-2 i-Full-View-Window'></i>Delete</a>" ;
            $action_column =    $edit_column.$view_column.$delete_column ;

            $resp[$i][5] = $action_column;
            $i +=1;
        }

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' =>  $recordsFiltered,
            'data' =>  $resp
        );

        return json_encode($data,true);

    }

    // get categories with menu by branch id and user id
    public function getCategoriesWithMenuByBranchIdUserId($branch_id,$user_id)
    {
        $branch_categories = $this->model_menu_category->getModel()::where('restaurant_branch_id','=',$branch_id)
        ->where('is_active',true)
        ->orderby('id', 'DESC')
        ->get();
        
        $fav_menus =  $this->getFavMenusByUserId($user_id);
        $branch_menus =  $this->getMenuByBranchId($branch_id);
        $menu_ratings =  $this->getMenuRatingsByBranchId($branch_id);

        foreach($branch_categories as $item)
        {
            $category_menu = [];
            foreach ($branch_menus as $key => $value)
            {
                if($item->id == $value->menu_category_id)
                {
                    $rating =  (object)[
                        'ratedby'       =>  $menu_ratings->where('menu_id','=',$value->id)->count(),
                        'user_rating'   =>  $menu_ratings->where('menu_id','=',$value->id)->where('user_id','=',$user_id)->pluck('rating')->first() ?? 0,
                        'rating'        =>  $menu_ratings->where('menu_id','=',$value->id)->avg('rating')??0

                    ];

                    $category_menu[]=[
                        "id"            => $value->id,
                        "name"          => $value->name,
                        "price"         => $value->price,
                        "image"         => isset($value->image)?$value->image:(isset($item->image)?$item->image:(isset($item->restaurantBranch->logo)?$item->restaurantBranch->logo:"findurmeal_logo_rv.png")),
                        "ingridient"   => $value->ingridient,
                        "description"   => $value->description,
                        "isFav"         => $fav_menus->where('menu_id','=',$value->id)->where('user_id','=',$user_id)->first() ?? false,
                        "rating"        => $rating,
                        "addOn"           =>[]


                    ];
                }
            }

            $categories[]=[
                "id"            => $item->id,
                "name"          => $item->name,
                "branchName"    => $item->restaurantBranch->name,
                "image"         => isset($item->image)?$item->image:$item->restaurantBranch->logo,
                "menuList"      => $category_menu

            ];
        }

        // $categories =

        if(!isset($categories) || !$categories)
            return false;

        return $categories;
    }

    // get fav menu by user id
    public function getFavMenusByUserId($user_id)
    {
        $fav_menus = $this->model_menu_fav->getModel()::where('user_id','=',$user_id)->get();

        if(!$fav_menus)
            return false;

        return $fav_menus;
    }

    // get fav menu by user id and branch id
    public function getFavMenusByUserIdBranchId($user_id,$branch_id)
    {
        $fav_menus = $this->model_menu_fav->getModel()::select('menu_fav.menu_id','menu_fav.id','menu_fav.user_id')
                    ->join('menu', 'menu.id', '=', 'menu_fav.menu_id')
                    ->join('menu_category', 'menu_category.id', '=', 'menu.menu_category_id')
                    ->where('menu_category.restaurant_branch_id','=',$branch_id)
                    ->where('menu_fav.user_id','=',$user_id)
                    ->get();

        if(!$fav_menus)
            return false;

        return $fav_menus;
    }

    // get menu ratings by branch id
    public function getMenuRatingsByBranchId($branch_id)
    {
        $menu_ratings = $this->model_menu_rating->getModel()::select('menu_rating.*')
                        ->join('menu', 'menu.id', '=', 'menu_rating.menu_id')
                        ->join('menu_category', 'menu_category.id', '=', 'menu.menu_category_id')
                        ->where('menu_category.restaurant_branch_id','=',$branch_id)
                        ->get();

        if(!$menu_ratings)
            return false;

        return $menu_ratings;
    }

    // get top menus rating by branch id
    public function getTopMenuRatingByBranchId($branch_id)
    {
        $top_rated_menus = $this->model_menu->getModel()::select('menu.id',DB::raw('AVG(menu_rating.rating) as rating'),'menu.name','menu.description','menu.ingridient','menu.price','menu.image','menu.date_created')
                        ->join('menu_category', 'menu.menu_category_id', '=', 'menu_category.id')
                        ->join('menu_rating', 'menu_rating.menu_id', '=', 'menu.id')
                        ->where('menu_category.restaurant_branch_id','=',$branch_id)
                        ->groupBy('menu.id','menu.name','menu.description','menu.ingridient','menu.price','menu.image','menu.date_created')
                        ->orderBy('rating', 'DESC')
                        ->orderBy('date_created', 'DESC')
                        ->get();

        if(!$top_rated_menus)
            return false;

        return $top_rated_menus;
    }


    // rate a dish against user id ,if already have then replace it
    public function rateMenu($rating,$user_id,$menu_id)
    {
        $menu_rating = $this->model_menu_rating->getModel()::where('menu_id','=',$menu_id)->where('user_id','=',$user_id)->first();

        if($menu_rating)
        {
            $menu_rating->rating = $rating;
            $menu_rating->date_updated = date("Y-m-d H:i:s");

            $saved_obj = $this->model_menu_rating->update($menu_rating->toArray(),$menu_rating->id);
        }
        else
        {
            $obj_menu_rating = [
                "rating"        =>  $rating,
                "user_id"       =>  $user_id,
                "menu_id"       =>  $menu_id,
                "date_created"  =>  date("Y-m-d H:i:s"),
                "createdby_id"  =>  $user_id
            ];

            $saved_obj = $this->model_menu_rating->create($obj_menu_rating);
        }


        if(!$saved_obj)
            return false;

        return $saved_obj;
    }

    // addremove a dish against user id
    public function addRemoveFavMenu($fav,$user_id,$menu_id)
    {
        $menu_fav = $this->model_menu_fav->getModel()::where('menu_id','=',$menu_id)->where('user_id','=',$user_id)->first();
        $saved_obj = false;

        if(!$menu_fav && $fav)
        {
            $menu_fav = ["menu_id" => $menu_id,"user_id" => $user_id];

            $saved_obj = $this->model_menu_fav->create($menu_fav);
        }
        elseif($menu_fav && !$fav)
        {
            $saved_obj = $this->model_menu_fav->delete($menu_fav->id);
        }elseif($menu_fav && $fav)
        {
            $saved_obj = true;
        }


        if(!$saved_obj)
            return false;

        return $saved_obj;
    }

    public function createMenuItemOnErp($menu_id)
    {
        $menu = $this->getMenuById($menu_id);
        $erp_active = $menu->menuCategory->restaurantBranch->erp_active;
        $erp_url = $menu->menuCategory->restaurantBranch->erp_url;

        if($erp_active && isset($erp_url))
        {
            $api_url = $erp_url."api/menu/create";

            $response = Http::post($api_url, [
                'menu_id'  => $menu->id,
                'name'     => $menu->name
            ]);
            // $params = array();
  	        // $params["menu_id"] = $menu->id;
            // $params["name"] = $menu->name;

            // var_dump($this->postRequest($api_url,$params));
            // exit();
        }
    }

    public function postRequest($url,$data)
    {

        if(isset($url) && $url != '' && isset($data) && $data != '')
        {
            $data = json_encode($data);
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

            $response = curl_exec($curl);

            curl_close($curl);

            return $response;
        }
        else
            return false;

    }

    // get choice group datatable source
    public function getChoiceGroupDatatableSource($draw,$start,$length,$search,$login_user)
    {
        $wh = " 1 ";
        switch($login_user->role_id)
        {
            case '2':

            $wh .= " AND restaurant_branch.restaurant_id =  ".$login_user->userRestaurant->restaurant_id;

            break;

            case '3':
                
                $wh .= " AND restaurant_branch.id =  ".$login_user->userRestaurant->branch_id;

    
            break;

            default:
                
                $wh .= " AND 1 ";
            
        
        }

        $limit = " LIMIT ".$length." OFFSET ".$start;
        $raw_qry = "";
        

        if($search)
        {
            $wh = $wh." AND (choice_groups.name LIKE \"%" . $search. "%\" ) ";
            $raw_qry = "SELECT choice_groups.id,choice_groups.name,GROUP_CONCAT(choices.name) as choicess,count(choices.id) as choice_count
                        FROM choice_groups 
                        Join restaurant_branch on restaurant_branch.id = choice_groups.branch_id
                        LEFT JOIN choices ON choice_groups.id = choices.choice_group_id
                        WHERE ".$wh." GROUP BY choice_groups.id,choice_groups.name Order BY choice_groups.sorting ";

            // total records
            $raw_qry_count = "SELECT count(*) as totall FROM choice_groups Join restaurant_branch on restaurant_branch.id = choice_groups.branch_id 
                            WHERE ".$wh;
            $records_total = DB::select( DB::raw($raw_qry_count) );
            $recordsTotal = $records_total[0]->totall;

            // filtered or search records
            $choice_groups = DB::select( DB::raw($raw_qry) );
            $recordsFiltered  = count($choice_groups);

            // actual records return to client
            $choice_groups = DB::select( DB::raw($raw_qry.$limit) );
            
        }
        else
        {    
            $raw_qry = "SELECT choice_groups.id,choice_groups.name,GROUP_CONCAT(choices.name) as choicess,count(choices.id) as choice_count
                FROM choice_groups 
                Join restaurant_branch on restaurant_branch.id = choice_groups.branch_id
                LEFT JOIN choices ON choice_groups.id = choices.choice_group_id
                WHERE ".$wh."  GROUP BY choice_groups.id,choice_groups.name Order BY choice_groups.sorting ";

            $choice_groups = DB::select( DB::raw($raw_qry) );
            $recordsTotal = $recordsFiltered = count($choice_groups);
            $choice_groups = DB::select( DB::raw($raw_qry. $limit) );
        }

        $resp = array();
        $i = 0;
        foreach($choice_groups as $item)
        {

            $resp[$i][0] = $item->name;
            $resp[$i][1] = $item->choicess;
            $resp[$i][2] = $item->choice_count;  
        
            $view_column    = "<a class='text-warning mr-2' href='".url('/view-choice-group')."/".$item->id."'><i title='View' class='nav-icon mr-2 i-Full-View-Window'></i>View</a>" ;
            $edit_column    = "<a class='text-success mr-2' href='".url('/edit-choice-group')."/".$item->id."'><i title='Edit' class='nav-icon mr-2 i-Pen-2'></i>Edit</a>";
            $action_column = $edit_column.$view_column; 
            
            $resp[$i][3] = $action_column;
            $i +=1; 
        }

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' =>  $recordsFiltered,
            'data' =>  $resp
        );

        return json_encode($data,true);

    }

    
    // save menu variation
    public function saveMenuVariation($obj)
    {
        
        if(isset($obj['id']) && $obj['id'] > 0)
        {   
            $saved_obj = $this->model_menu_variation->update($obj,$obj['id']);

        }
        else
        {   
            $saved_obj = $this->model_menu_variation->create($obj);
        }
        
        if(!$saved_obj)
            return false;
        
            return $saved_obj;    
    }

    // save choice group
    public function saveChoiceGroup($obj)
    {
        
        if(isset($obj['id']) && $obj['id'] > 0)
        {   
            $saved_obj = $this->model_choice_group->update($obj,$obj['id']);
        }
        else
        {   
            $saved_obj = $this->model_choice_group->create($obj);
        }
        
        if(!$saved_obj)
            return false;
        
            return $saved_obj;    
    }

    // save menu variation vs choice group
    public function saveMenuVariationChoiceGroup($obj)
    { 
        $saved_obj = $this->model_menu_var_choice_grp->create($obj);
        
        if(!$saved_obj)
            return false;
        
            return $saved_obj;    
    }

    // save menu vs choice group
    public function saveMenuChoiceGroup($obj)
    { 
        $saved_obj = $this->model_menu_choice_grp->create($obj);
        
        if(!$saved_obj)
            return false;
        
            return $saved_obj;    
    }

    // save choices
    public function saveChoice($obj)
    {
        
        if(isset($obj['id']) && $obj['id'] > 0)
        {   
            $saved_obj = $this->model_choice->update($obj,$obj['id']);
        }
        else
        {   
            $saved_obj = $this->model_choice->create($obj);
        }
        
        if(!$saved_obj)
            return false;
        
            return $saved_obj;    
    }

    // remove variation by menu id
    public function removeVariationByMenuId($menu_id)
    {
        
        return $this->model_menu_variation->getmodel()::where('menu_id',$menu_id)->delete();

    }
    
    // remove menu_variation_choice_group by menu id
    public function removeMenuVarChoiceGrpByMenuId($menu_id)
    {
        
        return $this->model_menu_var_choice_grp->getmodel()::where('menu_id',$menu_id)->delete();

    }

    // remove choices by choice group id
    public function removeChoiceByChoiceGroupId($choice_group_id)
    {
        
        return $this->model_choice->getmodel()::where('choice_group_id',$choice_group_id)->delete();

    }

    // remove menu choice group by menu id
    public function removeMenuChoiceByChoiceGroupId($menu_id)
    {
        
        return $this->model_menu_choice_grp->getmodel()::where('menu_id',$menu_id)->delete();

    }

    // get menu variation and choices by menu id
    public function getMenuVariationAndChoicesByMenuId($menu_id)
    {   
        $men_var_ch_grp = $this->model_menu_variation->getmodel()::with(['choiceGroups' => function($query){
            $query->with('choices');
        }])->where('menu_id',$menu_id)->orderBy('sorting')->get();

        $menu_ch_grp = $this->model_menu_choice_grp->getmodel()::with(['choiceGroup' => function($query){
            $query->with('choices');
        }])->where('menu_id',$menu_id)->orderBy('sorting')->get();
        
        $obj = (object)[
            "variations" => $men_var_ch_grp ,
            "menu_choice_groups" => $menu_ch_grp,
        ];

        return  $obj ;

    }


    // get menu variation and choices by menu id
    public function getChoiceGroupById($choice_group_id)
    {   
        return $this->model_choice_group->with('choices')->with('restaurantBranch')->find($choice_group_id);  

    }

    // get menu variation and choices by menu id
    public function getChoiceGroups($login_user=null)
    {   
        
        $wh = " 1 ";
        switch($login_user->role_id)
        {
            case '2':

            $wh .= " AND restaurant_branch.restaurant_id =  ".$login_user->userRestaurant->restaurant_id;

            break;

            case '3':
                
                $wh .= " AND restaurant_branch.id =  ".$login_user->userRestaurant->branch_id;

    
            break;

            default:
                
                $wh .= " AND 1 ";
            
        
        }
        
        $raw_qry = "SELECT choice_groups.id,choice_groups.name,GROUP_CONCAT(choices.name) as choicess,count(choices.id) as choice_count
                        FROM choice_groups 
                        LEFT JOIN choices ON choice_groups.id = choices.choice_group_id
                        JOIN restaurant_branch on restaurant_branch.id = choice_groups.branch_id
                        WHERE $wh
                        GROUP BY choice_groups.id,choice_groups.name Order BY choice_groups.sorting";

        return DB::select( DB::raw($raw_qry) );

    }



}
