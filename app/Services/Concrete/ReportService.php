<?php namespace App\Services\Concrete;

use App\Repository\Repository;
use App\Services\Abstractt\IReportService;
use App\Models\Order;
use DB;
use App\Models\DeliveryUser;
use App\Models\DeliveryUsersOrder;


class ReportService  implements IReportService
{

    protected $model_order;
    protected $model_rider_order;
    protected $model_rider;


    public function __construct()
    {
       // set the model
        $this->model_order = new Repository(new Order);
        $this->model_rider = new Repository(new DeliveryUser);
        $this->model_rider_order = new Repository(new DeliveryUsersOrder);

    }

    // get order by id
    public function getSaleByWeekDayMonthYear($login_user,$today_date,$branch_id)
    {
        $year = date('Y',strtotime($today_date));
        $month = date('m',strtotime($today_date));
        $day = date('d',strtotime($today_date));

        $wh = "";
        switch ($login_user->role_id)
        {
            case '1':
                if($branch_id != null){
                    $wh = "AND `restaurant_branch`.`id` = " .$branch_id ;
                }else{
                    $wh = "";
                }
            break;
            case '2':
                if($branch_id != null){
                    $wh = "AND `restaurant_branch`.`id` = " .$branch_id ;
                }else{
                    $wh= "AND `restaurant_branch`.`restaurant_id` =".$login_user->userRestaurant->restaurant_id ;
                }

            break;
            case '3':
            case '4':

                $wh= "AND `order`.`restaurant_branch_id` =". $login_user->userRestaurant->branch_id ;

            break;
            default:

        }

        $sqlQuery = "SELECT Month(`order`.`order_date`) monthh_num,DAYOFWEEK(`order`.`order_date`) weekday_num,sum(`order`.`total`) amount
                            FROM `order`
                            Join restaurant_branch on `restaurant_branch`.`id` = `order`.`restaurant_branch_id`
                            WHERE Year(`order`.`order_date`) = $year  and `order`.`status_id` <> 8 ".$wh."
                            group by DAYOFWEEK(`order`.`order_date`),Month(`order`.`order_date`)
                            order by Month(`order`.`order_date`) asc,DAYOFWEEK(`order`.`order_date`) asc";

        $data = DB::select(DB::raw($sqlQuery));
        $response[] = ["name" => "Monday","data" => [0,0,0,0,0,0,0,0,0,0,0,0]];
        $response[] = ["name" => "Tuesday","data" => [0,0,0,0,0,0,0,0,0,0,0,0]];
        $response[] = ["name" => "Wednesday","data" => [0,0,0,0,0,0,0,0,0,0,0,0]];
        $response[] = ["name" => "Thursday","data" => [0,0,0,0,0,0,0,0,0,0,0,0]];
        $response[] = ["name" => "Friday","data" => [0,0,0,0,0,0,0,0,0,0,0,0]];
        $response[] = ["name" => "Saturday","data" => [0,0,0,0,0,0,0,0,0,0,0,0]];
        $response[] = ["name" => "Sunday","data" => [0,0,0,0,0,0,0,0,0,0,0,0]];

        // $month = [""]
        foreach($data as $val)
        {
            switch ($val->weekday_num)
            {
                case '1':
                    $response[6]["data"][$val->monthh_num-1] = $val->amount;
                break;

                case '2':
                    $response[0]["data"][$val->monthh_num-1] = $val->amount;
                break;

                case '3':
                    $response[1]["data"][$val->monthh_num-1] = $val->amount;
                break;

                case '4':
                    $response[2]["data"][$val->monthh_num-1] = $val->amount;
                break;

                case '5':
                    $response[3]["data"][$val->monthh_num-1] = $val->amount;
                break;

                case '6':
                    $response[4]["data"][$val->monthh_num-1] = $val->amount;
                break;

                case '7':
                    $response[5]["data"][$val->monthh_num-1] = $val->amount;
                break;

            }
        }

        if(!$response)
            return false;

            return $response;
    }

    public function getSaleByWeekDayMonth($login_user,$today_date,$branch_id)
    {
        $year = date('Y',strtotime($today_date));
        $month = date('m',strtotime($today_date));
        $day = date('d',strtotime($today_date));
        $wh = "";

        switch ($login_user->role_id)
        {
            case '1':
                if($branch_id != null){
                    $wh = " AND restaurant_branch.id = ".$branch_id ;

                }else{
                   $wh = " ";
                }

            break;

            case '2':
                if($branch_id != null){
                    $wh = " AND restaurant_branch.id = ".$branch_id ;
                }else{
                    $wh = " AND restaurant_branch.restaurant_id = ".$login_user->userRestaurant->restaurant_id ;
                }

            break;
            case '3':
            case '4':
                    $wh = " AND order.restaurant_branch_id = ".$login_user->userRestaurant->branch_id ;
            break;

            default:

        }

         $sqlQuery = "SELECT FLOOR((DayOfMonth(Date(`order`.`order_date`))-1)/7)+1  week_num,DAYOFWEEK(`order`.`order_date`) weekday_num,SUM(`order`.`total`) amount
                        FROM `order`
                        Join restaurant_branch on `restaurant_branch`.`id` = `order`.`restaurant_branch_id`
                        WHERE YEAR(`order`.`order_date`) = $year AND `order`.`status_id` <> 8 AND MONTH(`order`.`order_date`) = $month ".$wh."
                        GROUP BY DATE(`order`.`order_date`),week_num,DAYOFWEEK(`order`.`order_date`),MONTH(`order`.`order_date`)
                        ORDER BY week_num DESC";

        $data = DB::select(DB::raw($sqlQuery));

        $response[] = ["name" => "Monday","data" => [0,0,0,0,0]];
        $response[] = ["name" => "Tuesday","data" => [0,0,0,0,0]];
        $response[] = ["name" => "Wednesday","data" => [0,0,0,0,0]];
        $response[] = ["name" => "Thursday","data" => [0,0,0,0,0]];
        $response[] = ["name" => "Friday","data" => [0,0,0,0,0]];
        $response[] = ["name" => "Saturday","data" => [0,0,0,0,0]];
        $response[] = ["name" => "Sunday","data" => [0,0,0,0,0]];

        foreach($data as $val)
        {
            switch ($val->weekday_num)
            {
                case '1':
                    $response[6]["data"][$val->week_num-1] = $val->amount;
                break;

                case '2':
                    $response[0]["data"][$val->week_num-1] = $val->amount;
                break;

                case '3':
                    $response[1]["data"][$val->week_num-1] = $val->amount;
                break;

                case '4':
                    $response[2]["data"][$val->week_num-1] = $val->amount;
                break;

                case '5':
                    $response[3]["data"][$val->week_num-1] = $val->amount;
                break;

                case '6':
                    $response[4]["data"][$val->week_num-1] = $val->amount;
                break;

                case '7':
                    $response[5]["data"][$val->week_num-1] = $val->amount;
                break;

            }

        }

        if(!$response)
            return false;

        return $response;
    }

    public function getSaleByTownMonth($login_user,$today_date,$branch_id)
    {
        $year = date('Y',strtotime($today_date));
        $month = date('m',strtotime($today_date));
        $day = date('d',strtotime($today_date));

        $wh = "";
        switch ($login_user->role_id)
        {
            case '1':
                if($branch_id != null){
                    $wh = "AND `restaurant_branch`.`id` = " .$branch_id ;
                }else{
                    $wh = "";
                }
                break;
            case '2':
                if($branch_id != null){
                    $wh = "AND `restaurant_branch`.`id` = " .$branch_id ;
                }else{
                    $wh= "AND `restaurant_branch`.`restaurant_id` =".$login_user->userRestaurant->restaurant_id ;
                }

                break;
            case '3':
            case '4':
                $wh= "AND `order`.`restaurant_branch_id` =". $login_user->userRestaurant->branch_id ;

                break;
            default:

        }

        $sqlQuery = "SELECT town.town_name,SUM(`order`.`total`) amount
                FROM `order`
                Join delivery_details on `order`.id = delivery_details.order_id
                Join town on delivery_details.town_id = town.id
                Join restaurant_branch on `restaurant_branch`.`id` = `order`.`restaurant_branch_id`
                WHERE `order`.`status_id` <> 8 AND MONTH(`order`.`order_date`) = $month ".$wh."
                GROUP BY town_name
                ORDER BY amount DESC
                LIMIT 5";

        $data = DB::select(DB::raw($sqlQuery));

        $response[] = ["labels" => [],"data" => []];


        foreach($data as $val)
        {
            $response[0]["labels"][] = $val->town_name;
            $response[0]["data"][]   = (float)$val->amount;
        }

        if(!$response)
            return false;

        return $response;
    }

    public function getSaleByMenuCategoryMonth($login_user,$today_date,$branch_id)
    {
        $year = date('Y',strtotime($today_date));
        $month = date('m',strtotime($today_date));
        $day = date('d',strtotime($today_date));

        $wh = "";
        switch ($login_user->role_id)
        {
            case '1':
                if($branch_id != null){
                    $wh = "AND `restaurant_branch`.`id` = " .$branch_id ;
                }else{
                    $wh = "";
                }
                break;
            case '2':
                if($branch_id != null){
                    $wh = "AND `restaurant_branch`.`id` = " .$branch_id ;
                }else{
                    $wh= "AND `restaurant_branch`.`restaurant_id` =".$login_user->userRestaurant->restaurant_id ;
                }

                break;
            case '3':
            case '4':
                $wh= "AND `order`.`restaurant_branch_id` =". $login_user->userRestaurant->branch_id ;

                break;
            default:

        }


        $sqlQuery = "SELECT menu_category.name,SUM(`order`.`total`) amount
                        FROM `order`
                        Join order_detail on `order`.id = order_detail.order_id
                        Join menu on `order_detail`.menu_id = menu.id
                        Join menu_category on `menu`.menu_category_id = menu_category.id
                        Join restaurant_branch on `restaurant_branch`.`id` = `order`.`restaurant_branch_id`
                        WHERE `order`.`status_id` <> 8 AND MONTH(`order`.`order_date`) = $month AND (order_detail.is_deleted is null OR order_detail.is_deleted = 0) ".$wh."
                        GROUP BY menu_category.name
                        ORDER BY amount DESC
                LIMIT 5";

        $data = DB::select(DB::raw($sqlQuery));

        $response[] = ["labels" => [],"data" => []];


        foreach($data as $val)
        {
            $response[0]["labels"][] = $val->name;
            $response[0]["data"][]   = (float)$val->amount;
        }

        if(!$response)
            return false;

        return $response;
    }


    public function getSalesByTopSellingDay($login_user,$today_date,$branch_id)
    {
        $year = date('Y',strtotime($today_date));
        $month = date('m',strtotime($today_date));
        $day = date('d',strtotime($today_date));

        $total_amount = 0;

        $wh = "";
        switch ($login_user->role_id)
        {
            case '1':
                if($branch_id != null){
                    $wh = "AND `restaurant_branch`.`id` = " .$branch_id ;
                }else{
                    $wh = "";
                }
                break;
            case '2':
                if($branch_id != null){
                    $wh = "AND `restaurant_branch`.`id` = " .$branch_id ;
                }else{
                    $wh= "AND `restaurant_branch`.`restaurant_id` =".$login_user->userRestaurant->restaurant_id ;
                }

                break;
            case '3':
            case '4':
                $wh= "AND `order`.`restaurant_branch_id` =". $login_user->userRestaurant->branch_id ;

                break;
            default:

        }

        $sqlQuery = "SELECT DAYNAME(`order`.`order_date`) weekday_name,SUM(`order`.`total`) amount
                                FROM `order`
                                Join restaurant_branch on `restaurant_branch`.`id` = `order`.`restaurant_branch_id`
                                WHERE  `order`.`status_id` <> 8 AND MONTH(`order`.`order_date`) = $month ".$wh."
                                GROUP BY weekday_name
                                ORDER BY amount DESC";

        $data = DB::select(DB::raw($sqlQuery));
        $response = [];
        foreach($data as $val)
        {
            $total_amount +=  (float)$val->amount;
        }

        foreach($data as $val)
        {
            switch($val->weekday_name)
            {
                case "Sunday":
                    $day_color = "background-color: #4caf50!important;";
                break;

                case "Monday":
                    $day_color = "background-color: #f44336!important;";
                break;

                case "Tuesday":
                    $day_color = "background-color: #ffc107!important;";
                break;

                case "Wednesday":
                    $day_color = "background-color: #003473!important;";
                break;

                case "Thursday":
                    $day_color = "background-color: rgb(0, 227, 150)!important;";
                break;

                case "Friday":
                    $day_color = "background-color: rgb(27, 153, 139)!important;;";
                break;

                case "Saturday":
                    $day_color = "background-color: rgb(119, 93, 208)!important;";
                break;
            }

            $response[] = (object)[
                "amount"       => (float)$val->amount,
                "percent"      => number_format((float)$val->amount/$total_amount * 100,1),
                "weekday_name"  => $val->weekday_name,
                "day_color"    =>  $day_color

            ];

        }


        if(!$response)
            return [];

        return $response;
    }

    // get order count by branchMonthYear
    public function getOrderCountByBranchMonthYear($login_user,$today_date,$branch_id)
    {
        $year = date('Y',strtotime($today_date));
        $month = date('m',strtotime($today_date));
        $day = date('d',strtotime($today_date));

        $wh = "";
        switch ($login_user->role_id)
        {
            case '1':
                if($branch_id != null){
                    $wh = "AND `restaurant_branch`.`id` = " .$branch_id ;
                }else{
                    $wh = "";
                }
                break;
            case '2':
                if($branch_id != null){
                    $wh = "AND `restaurant_branch`.`id` = " .$branch_id ;
                }else{
                    $wh= "AND `restaurant_branch`.`restaurant_id` =".$login_user->userRestaurant->restaurant_id ;
                }

                break;
            case '3':
            case '4':
                $wh= "AND `order`.`restaurant_branch_id` =". $login_user->userRestaurant->branch_id ;

                break;
            default:

        }

        $sqlQuery = "SELECT MONTH(`order`.`order_date`) month_name,restaurant_branch.name as branch,count(`order`.`id`) order_num
                                FROM `order`
                                Join restaurant_branch on `restaurant_branch`.`id` = `order`.restaurant_branch_id
                                WHERE `order`.`status_id` <> 8 AND Year(`order`.`order_date`) = $year ".$wh."
                                GROUP BY month_name,branch
                                ORDER BY branch ASC,month_name ASC";

        $data = DB::select(DB::raw($sqlQuery));

        $response = [];
        $current_branch = "";
        $tmp_count = -1;
        foreach($data as  $val)
        {
            if($current_branch == "" || $current_branch !=  $val->branch)
            {
                $current_branch = $val->branch;
                $response[] = [ "name" => $current_branch,
                    "data"    => [0,0,0,0,0,0,0,0,0,0,0,0]
                    ];
                $tmp_count +=1;
            }

            $response[$tmp_count]["data"][$val->month_name - 1]  = $val->order_num;

        }


        if(!$response)
            return false;

        return $response;

    }

    // get order count by branch Week Month
    public function getOrderCountByBranchWeekMonth($login_user,$today_date,$branch_id)
    {
        $year = date('Y',strtotime($today_date));
        $month = date('m',strtotime($today_date));
        $day = date('d',strtotime($today_date));

        $wh = "";
        switch ($login_user->role_id)
        {
            case '1':
                if($branch_id != null){
                    $wh = "AND `restaurant_branch`.`id` = " .$branch_id ;
                }else{
                    $wh = "";
                }
                break;
            case '2':
                if($branch_id != null){
                    $wh = "AND `restaurant_branch`.`id` = " .$branch_id ;
                }else{
                    $wh= "AND `restaurant_branch`.`restaurant_id` =".$login_user->userRestaurant->restaurant_id ;
                }

                break;
            case '3':
            case '4':
                $wh= "AND `order`.`restaurant_branch_id` =". $login_user->userRestaurant->branch_id ;

                break;
            default:

        }

        $sqlQuery = "SELECT FLOOR((DayOfMonth(Date(`order`.`order_date`))-1)/7)+1 week_num,
                            restaurant_branch.name as branch,count(`order`.`id`) order_num
                            FROM `order`
                            Join restaurant_branch on `restaurant_branch`.`id` = `order`.restaurant_branch_id
                            WHERE YEAR(`order`.`order_date`) = $year AND `order`.`status_id` <> 8 AND MONTH(`order`.`order_date`) = $month ".$wh."
                            GROUP BY week_num,branch
                            ORDER BY branch ASC,week_num DESC";

        $data = DB::select(DB::raw($sqlQuery));


        $response = [];
        $current_branch = "";
        $tmp_count = -1;
        foreach($data as  $val)
        {
            if($current_branch == "" || $current_branch !=  $val->branch)
            {
                $current_branch = $val->branch;
                $response[] = [ "name" => $current_branch,
                    "data"    => [0,0,0,0,0]
                    ];
                $tmp_count +=1;
            }

            $response[$tmp_count]["data"][$val->week_num-1]  = $val->order_num;

        }


        if(!$response)
            return false;

            return $response;
    }

    // get  users top order
    public function getTopOrderedUsers($login_user,$today_date,$branch_id)
    {
        $year = date('Y',strtotime($today_date));
        $month = date('m',strtotime($today_date));
        $day = date('d',strtotime($today_date));

        $wh = "";
        switch ($login_user->role_id)
        {
            case '1':
                if($branch_id != null){
                    $wh = "AND `restaurant_branch`.`id` = " .$branch_id ;
                }else{
                    $wh = "";
                }
                break;
            case '2':
                if($branch_id != null){
                    $wh = "AND `restaurant_branch`.`id` = " .$branch_id ;
                }else{
                    $wh= "AND `restaurant_branch`.`restaurant_id` =".$login_user->userRestaurant->restaurant_id ;
                }

                break;
            case '3':
            case '4':
                $wh= "AND `order`.`restaurant_branch_id` =". $login_user->userRestaurant->branch_id ;

                break;
            default:

        }

        $sqlQuery = "SELECT users.id,users.first_name,users.last_name,Count(`order`.`id`) order_num
                        FROM `order`
                        Join users on `order`.user_id = users.id
                        Join restaurant_branch on `restaurant_branch`.`id` = `order`.restaurant_branch_id
                        WHERE `order`.`status_id` <> 8 AND MONTH(`order`.`order_date`) = $month ".$wh."
                        GROUP BY users.first_name,users.last_name,users.id
                        ORDER BY order_num DESC
                        limit 5";

        $data = DB::select(DB::raw($sqlQuery));
        $response = [];
        $total_orders = 0;
        foreach($data as $val)
        {
            $total_orders +=  (float)$val->order_num;
        }

        foreach($data as $key => $val)
        {
            switch($key)
            {
                case "1":
                    $day_color = "background-color: #4caf50!important;";
                break;

                case "2":
                    $day_color = "background-color: #f44336!important;";
                break;

                case "3":
                    $day_color = "background-color: #ffc107!important;";
                break;

                case "4":
                    $day_color = "background-color: #003473!important;";
                break;

                case "5":
                    $day_color = "background-color: rgb(0, 227, 150)!important;";
                break;

                case "6":
                    $day_color = "background-color: rgb(27, 153, 139)!important;;";
                break;

                case "0":
                    $day_color = "background-color: rgb(119, 93, 208)!important;";
                break;
            }

            $response[] = (object)[
                "order_num"       => (float)$val->order_num,
                "percent"      => number_format((float)$val->order_num/$total_orders * 100,1),
                "user_name"    => isset($val->last_name)?$val->first_name." ".$val->last_name:$val->first_name,
                "day_color"    =>  $day_color

            ];

        }

        if(!$response)
            return [];

        return $response;

    }


    // get consumed menu by branch,date range
    public function getConsumedMenuItemsByDateByBranch($draw,$start ,$length ,$search,$login_user,$start_date,$end_date,$branch=null)
    {
        $limit = "";
        switch($login_user->role_id)
        {
            case '2':

                $wh = " AND restaurant_branch.restaurant_id = ".$login_user->userRestaurant->restaurant_id;

            break;

            case '3':
            case '4':

                $wh = " AND restaurant_branch.id = ".$login_user->userRestaurant->branch_id;

            break;

            default:

                $wh = " AND 1 ";

        }

        if(isset($branch) && $branch != 0 )
            $wh .= " AND restaurant_branch.id = $branch ";

        if($length != '-1')
            $limit = " LIMIT ".$length." OFFSET ".$start;

        $raw_qry = "";

        if($search)
        {

        }
        else
        {
            $raw_qry = "SELECT menu.name as menuitem,restaurant_branch.name as branch,sum(order_detail.quantity) as qty,sum((order_detail.quantity * order_detail.price) -(IFNULL(order.discount_per,0)/100*order_detail.quantity * order_detail.price)) as amt
            FROM menu
            Left Join order_detail on menu.id = order_detail.menu_id and order_detail.order_detail_status_id = 2 and (order_detail.is_deleted is null or order_detail.is_deleted = false)
            Join `order` on `order`.id = order_detail.order_id
            Join `restaurant_branch` on `restaurant_branch`.`id` = `order`.`restaurant_branch_id`
            WHERE 1  $wh AND Date(`order`.`order_date`) BETWEEN  '$start_date' AND '$end_date' AND `order`.`status_id` <> 8
            group by menu.name,branch";

            $qry_result = DB::select( DB::raw($raw_qry) );
            $recordsTotal = $recordsFiltered = count($qry_result);
            $qry_result = DB::select( DB::raw($raw_qry. $limit) );
        }

        $resp = array();
        $i = 0;
        foreach($qry_result as $item)
        {
            $resp[$i][0] = $item->menuitem;
            $resp[$i][1] = $item->qty;
            $resp[$i][2] = $item->branch;
            $resp[$i][3] = number_format($item->amt,2);
            $i +=1;
        }

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' =>  $recordsFiltered,
            'data' =>  $resp
        );

        return json_encode($data,true);

    }

    // get sale detail by branch,date range
    public function getSaleDetailByDateByBranch($draw,$start ,$length ,$search,$login_user,$start_date,$end_date,$branch=null)
    {
        $limit = "";

        $wh = [];

        switch($login_user->role_id)
        {
            case '2':

                $wh = [
                    ['restaurant_branch.restaurant_id', '=', $login_user->userRestaurant->restaurant_id]
                ];
            break;

            case '3':
            case '4':
                
                $wh[] = ['restaurant_branch.id', '=', $login_user->userRestaurant->branch_id];
            break;

            default:

                $wh = " AND 1 ";

        }

        
        if(isset($branch) && $branch != 0 )
            $wh = [
                ['order.order_date', '>=', $start_date." 00:00:00"],
                ['order.order_date', '<=', $end_date." 23:59:59"],
                ['restaurant_branch.id','=',$branch]
            ];
        else
        {
            $wh[] = ['order.order_date', '>=', $start_date." 00:00:00"];
           
            $wh[] = ['order.order_date', '<=', $end_date." 23:59:59"];
           
        }

        if($length != '-1')
            $limit = " LIMIT ".$length." OFFSET ".$start;

        $raw_qry = "";

        if($search)
        {

        }
        else
        {
            $qry_result =  $this->model_order->getModel()::join('restaurant_branch','order.restaurant_branch_id', '=', 'restaurant_branch.id')
            ->where($wh)->where('status_id','<>',8)
            ->get();

            $recordsTotal = $recordsFiltered = $qry_result->count();
            $length = $length == '-1' ?$recordsTotal:$length;

            $qry_result = $this->model_order->getModel()::join('restaurant_branch','order.restaurant_branch_id', '=', 'restaurant_branch.id')
            ->select(['order.id as orderid','order.dailyorder_id as dailyorderid','order.*', 'restaurant_branch.*'])
            ->where($wh)
            ->where('status_id','<>',8)
            ->skip($start)
            ->take($length)
            ->orderby('order.id')
            ->get();

        }

        $resp = array();
        $i = 0;
        foreach($qry_result as $item)
        {
            $resp[$i][0] = $item->order_date;
            $resp[$i][1] = $item->dailyorderid;
            $resp[$i][2] = isset($item->user->last_name)?$item->user->first_name." ".$item->user->last_name:$item->user->first_name ;
            $resp[$i][3] = $item->type->type;
            $resp[$i][4] = $item->resource->resource_name;
            $resp[$i][5] = $item->status->name;
            $resp[$i][6] = number_format(abs(strtotime($item->recieved_time) - strtotime($item->delivered_time)) / 60,2);
            $resp[$i][7] = $item->tax_amount;
            $resp[$i][8] = $item->discount_amount;
            $resp[$i][9] = $item->sub_total;
            $resp[$i][10] = $item->total;


            $i +=1;
        }

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' =>  $recordsFiltered,
            'data' =>  $resp
        );

        return json_encode($data,true);

    }


    //======================   Rider Reports  ================

    public function getSaleVsKiloMeterByMonth($login_user,$month,$branch)
    {

        switch($login_user->role_id)
        {
            case '2':

                $wh = " AND restaurant_branch.restaurant_id = ".$login_user->userRestaurant->restaurant_id;
                $where_conditions = [
                    ['restaurant_branch.restaurant_id', '=', $login_user->userRestaurant->restaurant_id]
                ];
            break;

            case '3':
            case '4':

                $wh = " AND restaurant_branch.id = ".$login_user->userRestaurant->branch_id;
                $where_conditions = [
                    ['restaurant_branch.id', '=', $login_user->userRestaurant->branch_id]
                ];
            break;

            default:

                $wh = " AND 1 ";

        }

        $wh .= " AND month(od.order_date) = $month AND year(od.order_date)=".date('Y');

        $raw_qry = "SELECT date(od.`order_date`) od_date,delivery_user.id AS rider_id,sum(delivery_users_order.travel_distance) as km,delivery_user.first_name,Sum(IFNULL(od.total,0)) as amt
                    FROM `delivery_user`
                    Join delivery_users_order on delivery_user.id = delivery_users_order.delivery_user_id
                    Join restaurant_branch on  delivery_user.branchId = `restaurant_branch`.`id`
                    LEFT Join `order` as od on od.id = delivery_users_order.order_id
                    WHERE 1 $wh
                    GROUP BY delivery_user.first_name , date(od.`order_date`),delivery_user.id
                    ORDER BY date(od.`order_date`),delivery_user.first_name";

        $riders = $this->model_rider->getModel()::Join('restaurant_branch','restaurant_branch.id','=','delivery_user.branchId')
                    ->select('delivery_user.id','delivery_user.first_name','delivery_user.last_name','restaurant_branch.name as branch')
                    ->where($where_conditions)->get();


        $qry_result = DB::select( DB::raw($raw_qry) );
        $current_date = date('Y-'.$month.'-d');
        $month_days = date('t',strtotime($current_date));

        $response = [];
        for($j = 1; $j <= $month_days ; $j++ )
        {
            $response["month_days"][] = $j;
        }

        foreach($riders as $rider)
        {
            $response[$rider->id] = [];
            $response[$rider->id] = ["rider_name" => $rider->first_name];



            for($i = 1; $i <= $month_days ; $i++ )
            {
                $datee = date('Y-'.$month.'-'.$i);
                $day_index = date('d',strtotime($datee));
                $response[$rider->id][$day_index] = ["sale_amt" => 0,"km" => 0];


                foreach($qry_result as $item)
                {

                    if(strtotime($item->od_date) ==  strtotime($datee) && isset($response[$item->rider_id]))
                    {
                        $response[$item->rider_id][$day_index]["sale_amt"] = $item->amt;
                        $response[$item->rider_id][$day_index]["km"] = $item->km;

                    }
                }
            }
        }

        $response['branch_name'] = $riders[0]->branch;
        return $response;
    }
}
