<?php namespace App\Services\Concrete;

use Illuminate\Database\Eloquent\Model;

use App\Services\Abstractt\IOmcOrderService;
use DB;


class OmcOrderService  implements IOmcOrderService
{

    public function __construct()
    {
       // set the model
    }
    
    public function postOrder($order_id)
    {

        // get order detail
        $detail_service = new OrderDetailService();
        $order_details  = $detail_service->getOrderDetailByOrderIds([$order_id]);

        // get orders
        $order_service = new OrderService();
        $order  = $order_service->getById($order_id);

        // get delivery detail
        $delivery_detail_service = new DeliveryDetailService();
        $delivery_details = $delivery_detail_service->getDetailByOrderIds([$order_id]);


        $cmobile = $delivery_details[0]->mobile_num;
        $cname = $order->user->first_name; 
        $acategory = $delivery_details[0]->addtype_id == 3 ?"House" : 
         ($delivery_details[0]->addtype_id == 4 ? "FLAT" : ($delivery_details[0]->addtype_id == 5) ? "Office" :"House");
        $ano = "";
        $afloor = "";
        $astreet = $delivery_details[0]->address??"";
        $ablock = $delivery_details[0]->townBlock->block_name;
        $area = strtoupper($delivery_details[0]->town->town_name);
        $ahint = $delivery_details[0]->instructions??"";
        $acity = "";
        $datetm = (string)$order->order_date;
        $profileflag = 0;



        // get area query
        $areas = DB::connection('sqlsrv')->table('AREAS')->where("AName",$area)->get();

        if($areas->count() == 0)
        {
            DB::connection('sqlsrv')->table('AREAS')->insert([
                'AName' => $area
            ]);
        }

        // to check if customer already exist or not

        $customers = DB::connection('sqlsrv')->table('CUSTOMERS')->where("CMOBILE",$cmobile)->where("AAREA",$area)->get();

        if($customers->count() > 0)
        {
            $cust_id = $customers[0]->CUSID;
        }

     
        if($customers->count() == 0)
        {
              // to get id of last customer
        
            $last_cust_id = DB::connection('sqlsrv')->table('CUSTOMERS')->max("CUSID") ?? 0;
            
            $cust_id = $last_cust_id + 1;

            DB::connection('sqlsrv')->table('CUSTOMERS')->insert([
                'CUSID'         => $cust_id,
                'CMOBILE'       => $cmobile,
                'CNAME'         => $cname,
                'ACATEGORY'     => $acategory,
                'ANo'           => $ano,
                'AFLOOR'        => $afloor,
                'ASTREET'       => $astreet,
                'ABLOCK'        => $ablock,
                'AAREA'         => $area,
                'AHINT'         => $ahint,
                'ACITY'         => $acity,
                'DATETM'        => $datetm,
                'ProfileFlag'   => $profileflag
            ]);
        }
        else
        {
            DB::connection('sqlsrv')->table('CUSTOMERS')
              ->where('CUSID', $cust_id)
              ->update([
                'CNAME'         => $cname,
                'ACATEGORY'     => $acategory,
                'ASTREET'       => $astreet,
                'ABLOCK'        => $ablock,
                'AHINT'         => $ahint,
                'DATETM'        => $datetm,
                ]);
        }

        //============= order insertion ==================

        $last_order_id = DB::connection('sqlsrv')->table('ORDERS')->max("OrderNo") ?? 0;
            
        $order_id = $last_order_id + 1;

        foreach($order_details as $item){
            
            $orders[] = [
                'CUSID'     => $cust_id,
                'OrderNo'   => $order_id,
                'IID'       => $item->menu->external_menu_id,
                'QTY'       => $item->quantity,
                'RMKS'      => $ahint ??"",
                'ServiceTp' => 0,
                'DateTm'    => $order->order_date,
                'OrderSq'   => 0,
                'PMode'     => 4
            ];
        
        }

        DB::connection('sqlsrv')->table('ORDERS')->insert($orders);

      
        DB::connection("sqlsrv")->statement("exec ORDERVERIFY @ONo=".$order_id);

    }
}    
