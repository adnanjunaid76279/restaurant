<?php namespace App\Services\Concrete;
use App\Services\Abstractt\IAgentUserService;
use App\Repository\Repository;
use App\Models\AgentUsers;
use App\Models\AgentRoles;
use DB;




class AgentUserService  implements IAgentUserService
{

    protected $model_agent_user;
    protected $model_agent_roles;

    public function __construct()
    {
       // set the model
        $this->model_agent_user = new Repository(new AgentUsers);
        $this->model_agent_roles= new Repository(new AgentRoles);

    }

    // get by branch id
    public function getByBranchId($branch_id)
    {
        $agent_users = $this->model_agent_user->getModel()::where('branch_id', $branch_id)->get();

        if(!$agent_users)
            return false;

        return $agent_users;

    }

    // get Agent User by id
    public function getById($id)
    {
        $agent_user = $this->model_agent_user->find($id);

        if(!$agent_user)
            return false;

        return $agent_user;

    }

    //datatable for Agent users
    public function getAgentUsersDatatableSource($draw,$start ,$length ,$search,$login_user)
    {

        $wh = "";
        switch ($login_user['role_id']) {


            case '2':

                $wh = " AND restaurant_branch.restaurant_id =  ".$login_user->userRestaurant->restaurant_id;

                break;

            case '3':

                $wh = " AND restaurant_branch.id =  ".$login_user->userRestaurant->branch_id;

                break;

            default:

                $wh = " AND 1 ";
        }

        $limit = " LIMIT ".$length." OFFSET ".$start;
        $raw_qry = "";

        if($search)
        {
            $wh .= " AND (agent_users.username LIKE \"%" . $search. "%\" OR agent_roles.role_name LIKE \"%" . $search. "%\" OR restaurant_branch.name LIKE \"%". $search . "%\")";
            $raw_qry =" SELECT agent_users.username,agent_users.id,agent_users.branch_id, agent_roles.role_name,restaurant_branch.name as branch, agent_users.allowPos
                        FROM agent_users
                        JOIN agent_roles on agent_roles.id = agent_users.agent_role_id
                        JOIN restaurant_branch on restaurant_branch.id = agent_users.branch_id ".$wh." Order BY agent_users.username asc";

            $raw_qry_count = "SELECT count(*) as total_menu
                              FROM agent_users
                              JOIN restaurant_branch on restaurant_branch.id = agent_users.branch_id
                              JOIN agent_roles on agent_roles.id = agent_users.agent_role_id".$wh;

            $records_total = DB::select( DB::raw($raw_qry_count) );
            $recordsTotal = $records_total[0]->total_menu;

            // filtered or search records
            $result = DB::select( DB::raw($raw_qry) );
            $recordsFiltered  = count($result);

            // actual records return to client
            $result = DB::select( DB::raw($raw_qry.$limit) );

        }
        else
        {

            $raw_qry ="SELECT agent_users.username,agent_users.id,agent_roles.role_name,restaurant_branch.name as branch, agent_users.allowPos
                        FROM agent_users
                        JOIN agent_roles on agent_roles.id = agent_users.agent_role_id
                        JOIN restaurant_branch on restaurant_branch.id = agent_users.branch_id
                        ".$wh." Order BY agent_users.username asc";

            $raw_qry_count = "SELECT count(*) as total_menu FROM agent_users JOIN restaurant_branch on restaurant_branch.id = agent_users.branch_id JOIN agent_roles on agent_roles.id = agent_users.agent_role_id".$wh;
            $records_total = DB::select( DB::raw($raw_qry_count) );
            $recordsTotal = $records_total[0]->total_menu;

            // filtered or search records
            $result = DB::select( DB::raw($raw_qry) );
            $recordsFiltered  = count($result);

            // actual records return to client
            $result = DB::select( DB::raw($raw_qry.$limit) );

        }
        $resp = array();
        $i = 0;
        foreach($result as $item)
        {
            $resp[$i][0] = $item->username;
            $resp[$i][1] = $item->branch;
            $resp[$i][2] = $item->role_name;
            if($item->allowPos == 1){
            $resp[$i][3] = "Active";
            }else{
                $resp[$i][3] = "Inactive";
            }

            $edit_column    = "<a class='text-success mr-2' href='".url('/edit-agent-user')."/".$item->id."'><i title='Add' class='nav-icon mr-2 i-Pen-2'></i>Edit</a>";
            $view_column    = "<a class='text-warning mr-2' href='".url('/view-agent-user')."/".$item->id."'><i title='View' class='nav-icon mr-2 i-Full-View-Window'></i>View</a>" ;
            $action_column =    $edit_column.$view_column ;

            $resp[$i][4] = $action_column;
            $i +=1;
        }

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' =>  $recordsFiltered,
            'data' =>  $resp
        );

        return json_encode($data,true);


    }

    // get all Agent roles
    public function getAgentRoles()
    {
        $agent_roles = $this->model_agent_roles->all();

        if(!$agent_roles)
            return false;

        return $agent_roles;

    }

    // To Save/Update Agent User
    public function save($obj)
    {
        if(isset($obj['id']) && $obj['id'] > 0)
        {
            $saved_obj = $this->model_agent_user->update($obj,$obj['id']);
        }
        else
        {
            $saved_obj = $this->model_agent_user->create($obj);
        }

        if(!$saved_obj)
            return false;

        return $saved_obj;
    }

    //get role by id
    public function getRoleById($id)
    {
        $role = $this->model_roles->find($id);

        if(!$role)
            return false;

        return $role;
    }

}
