<?php namespace App\Services\Concrete;

use Illuminate\Database\Eloquent\Model;
use App\Repository\Repository;
use App\Services\Abstractt\IRiderService;
use App\Models\DeliveryUser;
use App\Models\DeliveryUsersOrder;
use App\Models\Order;

class RiderService  implements IRiderService
{
    
    protected $model_rider_order;
    protected $model_rider;
    protected $model_order;
    
    
    public function __construct()
    {
       // set the model
       $this->model_rider = new Repository(new DeliveryUser);
       $this->model_rider_order = new Repository(new DeliveryUsersOrder);
       $this->model_order = new Repository(new Order);
    }

       // save rider
    public function saveRider($obj)
    {
        
        if(isset($obj['id']) && $obj['id'] > 0)
        {   
            $obj['date_updated'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_rider->update($obj,$obj['id']);
        }
        else
        {   
            $obj['date_created'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_rider->create($obj);
        }
        
        if(!$saved_obj)
            return false;
        
        return $saved_obj;    
    }

    // save rider order
    public function saveRiderOrder($obj)
    {
        
        if(isset($obj['id']) && $obj['id'] > 0)
        {   
            $obj['date_updated'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_rider_order->update($obj,$obj['id']);
        }
        else
        {   
            $obj['createdby_id'] = 10;
            $obj['date_created'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_rider_order->create($obj);
        }
        
        if(!$saved_obj)
            return false;
        
        return $saved_obj;    
    }
    
    public function getActiverRiders($branch_id)
    {
        $riders = $this->model_rider->getModel()::where('branchId', $branch_id)->where('is_online', 1)->get();
        
        if(!$riders)
            return false;
            
        return $riders; 
    }

    public function getById($rider_id)
    {
        $rider = $this->model_rider->getModel()::
            join('restaurant_branch','restaurant_branch.id', '=', 'delivery_user.branchId')
            ->select('delivery_user.*','restaurant_branch.delivery_app_server_key as server_key','restaurant_branch.delivery_app_sender_id as sender_id')
            ->where('delivery_user.id','=',$rider_id)
            ->first();

        if(!$rider)
            return false;
            
        return $rider; 
        
    }
    // get data from delivery users order table by rider and order
    public function getRiderOrderByOrderIdRiderId($rider_id,$order_id)
    {
        $order = $this->model_rider_order
        ->getModel()::
        join('order','order.id','=','delivery_users_order.order_id')
        ->where('delivery_user_id',$rider_id)
        ->where('order_id',$order_id)
        ->select('delivery_users_order.*', 'order.status_id as order_status')
        ->first();
        if(!$order)
            return false;
            
        return $order; 
        
    }

    // get data from delivery users order table by order
    public function getRiderOrderByOrderId($order_id)
    {
        $order = $this->model_rider_order->getModel()::where('order_id',$order_id)->first();
        
        if(!$order)
            return false;
            
        return $order; 
        
    }

    public function login($username,$password)
    {   

        $rider = $this->model_rider->getModel()::where([['login', '=', $username],['password', '=', $password]])->firstorFail();
        
        if(!$rider)
            return false;

        return $rider;  
    }

    // get active delivery order, order by status id
    public function getActiveDeliveryOrdersByRiderId($rider_id,$status_id=2)
    {
        $orders  =   $this->model_order->getModel()::join('delivery_users_order','order.id', '=', 'delivery_users_order.order_id')
                    ->join('users','users.id','=','order.user_id')
                    ->join('order_status','order_status.id','=','order.status_id')
                    ->select(['order.id as orderid','order_status.name as orderstatus','order.*','delivery_users_order.*','order.createdby_id as oc_id','users.*'])
                    ->where('delivery_users_order.delivery_user_order_status_id','=',$status_id) 
                    ->where('delivery_users_order.delivery_user_id','=',$rider_id)   
                    ->where('order.status_id','<>',8)
                    ->orderBy('orderid', 'DESC')
                    ->get();
        
        $branch_id = $orders ?? $orders[0]->restaurant_branch_id;            
                    
        $order_ids = $orders->pluck('orderid')->toArray();
        
        // // get order detail
        $detail_service = new OrderDetailService();
        $order_details  = $detail_service->getOrderDetailByOrderIds($order_ids);
        
        // // get delivery detail
        $delivery_detail_service = new DeliveryDetailService();
        $delivery_details = $delivery_detail_service->getDetailByOrderIds($order_ids);

        $response = [];

        foreach($orders as $order)
        {   
            $details = [];
            $order_detail = $order_details->where('order_id','=',$order->orderid);
            $delivery_details   =  $delivery_details->where('order_id','=',$order->orderid)->first();
   
            foreach($order_detail as $detail)
            {
                $details[] = [
                    'menu_name' => $detail->menu_name ,
                    'price' => $detail->price ,
                    'quantity' => $detail->quantity
                ];
            }
           
            $response[] = [

                'orderId'                    =>  $order->orderid,
                'orderStatus'                =>  $order->orderstatus,
                'user_name'                  =>  isset($order->last_name)?$order->first_name." ".$order->last_name:$order->first_name,
                'orderDate'                  =>  $order->order_date,
                'deliverDate'                =>  $order->deliver_date,
                'total'                      =>  $order->total,
                "orderNo"                    =>  $order->dailyorder_id,
                "order_in_progress"          =>  $order->order_in_progress == 1?"true":"false",

                "orderDetail"           =>  $details,
                "deliveryDetails"      =>  $delivery_details
                
            ];
        }
        
        if(!$response)
            return [];
            
        return $response; 
    }

    public function getDeliveryOrderByOrderId($order_id)
    {
        $orders  =   $this->model_order->getModel()::join('delivery_users_order','order.id', '=', 'delivery_users_order.order_id')
                    ->join('users','users.id','=','order.user_id')
                    ->join('order_status','order_status.id','=','order.status_id')
                    ->select(['order.id as orderid','order_status.name as orderstatus','order.*','delivery_users_order.*','order.createdby_id as oc_id','users.*'])
                    ->where('delivery_users_order.delivery_user_order_status_id','=',2)   
                    ->where('order.status_id','<>',8)
                    ->where('order.id','=',$order_id)
                    ->orderBy('orderid', 'DESC')
                    ->get();
        
        $branch_id = $orders[0]->restaurant_branch_id;            
                    
        $order_ids = $orders->pluck('orderid')->toArray();
        
        // // get order detail
        $detail_service = new OrderDetailService();
        $order_details  = $detail_service->getOrderDetailByOrderIds($order_ids);
        
        // // get delivery detail
        $delivery_detail_service = new DeliveryDetailService();
        $delivery_details = $delivery_detail_service->getDetailByOrderIds($order_ids);

        $response = [];

        foreach($orders as $order)
        {   
            $details = [];
            $order_detail = $order_details->where('order_id','=',$order->orderid);
            $delivery_details   =  $delivery_details->where('order_id','=',$order->orderid)->first();
   
            foreach($order_detail as $detail)
            {
                $details[] = [
                    'menu_name' => $detail->menu_name ,
                    'price' => $detail->price ,
                    'quantity' => $detail->quantity
                ];
            }
           
            $response[] = [

                'orderId'                    =>  $order->orderid,
                'orderStatus'                =>  $order->orderstatus,
                'user_name'                  =>  isset($order->last_name)?$order->first_name." ".$order->last_name:$order->first_name,
                'orderDate'                  =>  $order->order_date,
                'deliverDate'                =>  $order->deliver_date,
                'total'                      =>  $order->total,
                "orderNo"                    =>  $order->dailyorder_id,
                "order_in_progress"          =>  $order->order_in_progress == 1?"true":"false",

                "orderDetail"           =>  $details,
                "deliveryDetails"      =>  $delivery_details,
                "notificationId"        => 1
                
            ];
        }
        
        if(!$response)
            return false;
            
        return $response; 
    }



}