<?php namespace App\Services\Concrete;

use Illuminate\Database\Eloquent\Model;
use App\Repository\Repository;
use App\Services\Abstractt\IRestaurantService;
use App\Models\RestaurantBranch;
use App\Models\Restaurant;
use App\Models\BranchRating;
use App\Models\BranchFav;
use App\Models\BranchWaiter;
use DB;


class RestaurantService  implements IRestaurantService
{

    protected $model_branch;
    protected $model_restaurant;
    protected $model_branch_rating;
    protected $model_branch_fav;
    protected $model_branch_waiter;


    public function __construct()
    {
       // set the model
       $this->model_branch = new Repository(new RestaurantBranch);
       $this->model_restaurant = new Repository(new Restaurant);
       $this->model_branch_rating = new Repository(new BranchRating);
       $this->model_branch_fav = new Repository(new BranchFav);
       $this->model_branch_waiter = new Repository(new BranchWaiter);
    }

    // get all restaurants
    public function getAllRestaurants($login_user=null)
    {
        if(isset($login_user))
        {
            switch($login_user->role_id)
            {
                case '1':
                    $restaurant = $this->model_restaurant->all();

                break;

                case '2':

                    $restaurant = $this->model_restaurant->getmodel()::where('id',$login_user->userRestaurant->restaurant_id)->get();

                break;

                default:

                    $restaurant = $this->model_restaurant->all();
            }
        }
        else
            $restaurant = $this->model_restaurant->all();

        if(!$restaurant)
            return false;

            return $restaurant;
    }

    // get restaurant datatable source
    public function getRestaurantDatatableSource($draw,$start,$length,$search,$login_user)
    {

        switch($login_user->role_id)
        {
            case '2':

                $wh = " AND id = ".$login_user->userRestaurant->restaurant_id;

            break;

            default:

                $wh = " AND 1 ";

        }

        $limit = " LIMIT ".$length." OFFSET ".$start;
        $raw_qry = "";

        if($search)
        {
            $wh = $wh." AND (name LIKE \"%" . $search. "%\" OR contact_email LIKE \"%" . $search. "%\" OR owner LIKE \"%" . $search. "%\" OR contact_person LIKE \"%" . $search. "%\" OR contact_number LIKE \"%" . $search. "%\") ";
            $raw_qry = "SELECT id,name,owner,contact_person,contact_email,contact_number
                        FROM restaurant
                        WHERE 1 ".$wh." Order BY id asc";
            // total records
            $raw_qry_count = "SELECT count(*) as total FROM restaurant
                            WHERE 1".$wh;
            $records_total = DB::select( DB::raw($raw_qry_count) );
            $recordsTotal = $records_total[0]->total;

            // filtered or search records
            $restaurants = DB::select( DB::raw($raw_qry) );
            $recordsFiltered  = count($restaurants);

            // actual records return to client
            $restaurants = DB::select( DB::raw($raw_qry.$limit) );

        }
        else
        {
            $raw_qry = "SELECT id,name,owner,contact_person,contact_email,contact_number
                        FROM restaurant
                        WHERE 1 ".$wh." Order BY id asc";

            $restaurants = DB::select( DB::raw($raw_qry) );
            $recordsTotal = $recordsFiltered = count($restaurants);
            $restaurants = DB::select( DB::raw($raw_qry. $limit) );
        }

        $resp = array();
        $i = 0;
        foreach($restaurants as $item)
        {
            $resp[$i][0] = $item->name;
            $resp[$i][1] = $item->owner;
            $resp[$i][2] = $item->contact_person;
            $resp[$i][3] = $item->contact_email;
            $resp[$i][4] = $item->contact_number;

            $edit_column    = "<a class='text-success mr-2' href='".url('/edit-restaurant')."/".$item->id."'><i title='Edit' class='nav-icon mr-2 i-Pen-2'></i>Edit</a>";
            $view_column    = "<a class='text-warning mr-2' href='".url('/view-restaurant')."/".$item->id."'><i title='View' class='nav-icon mr-2 i-Full-View-Window'></i>View</a>" ;
            $action_column =    $edit_column.$view_column ;

            $resp[$i][5] = $action_column;
            $i +=1;
        }

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' =>  $recordsFiltered,
            'data' =>  $resp
        );

        return json_encode($data,true);

    }

    // get branches datatable source
    public function getBranchesDatatableSource($draw,$start,$length,$search,$login_user)
    {

        switch($login_user->role_id)
        {
            case '2':

            $wh = " AND restaurant_branch.restaurant_id =  ".$login_user->userRestaurant->restaurant_id;

            break;

            case '3':

                $wh = " AND restaurant_branch.id =  ".$login_user->userRestaurant->branch_id;

            break;

            default:

                $wh = " AND 1 ";

        }

        $limit = " LIMIT ".$length." OFFSET ".$start;
        $raw_qry = "";

        if($search)
        {
            $wh = $wh." AND (name LIKE \"%" . $search. "%\" OR phone_number LIKE \"%" . $search. "%\" OR contact_person LIKE \"%" . $search. "%\" ) ";
            $raw_qry = "SELECT id,name,phone_number,contact_person
                        FROM restaurant_branch
                        WHERE 1 ".$wh." Order BY id asc";

            // total records
            $raw_qry_count = "SELECT count(*) as total_branch FROM restaurant_branch
                            WHERE 1".$wh;
            $records_total = DB::select( DB::raw($raw_qry_count) );
            $recordsTotal = $records_total[0]->total_branch;

            // filtered or search records
            $branches = DB::select( DB::raw($raw_qry) );
            $recordsFiltered  = count($branches);

            // actual records return to client
            $branches = DB::select( DB::raw($raw_qry.$limit) );

        }
        else
        {
            $raw_qry = "SELECT id,name,phone_number,contact_person
            FROM restaurant_branch
            WHERE 1 ".$wh."  Order BY id desc";
            $branches = DB::select( DB::raw($raw_qry) );
            $recordsTotal = $recordsFiltered = count($branches);
            $branches = DB::select( DB::raw($raw_qry. $limit) );
        }

        $resp = array();
        $i = 0;
        foreach($branches as $item)
        {

            $resp[$i][0] = $item->name;
            $resp[$i][1] = $item->phone_number;
            $resp[$i][2] = $item->contact_person;

            $edit_column    = "<a class='text-success mr-2' href='".url('/edit-branch')."/".$item->id."'><i title='Edit' class='nav-icon mr-2 i-Pen-2'></i>Edit</a>";
            $view_column    = "<a class='text-warning mr-2' href='".url('/view-branch')."/".$item->id."'><i title='View' class='nav-icon mr-2 i-Full-View-Window'></i>View</a>" ;
            $action_column =    $edit_column.$view_column ;

            $resp[$i][3] = $action_column;
            $i +=1;
        }

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' =>  $recordsFiltered,
            'data' =>  $resp
        );

        return json_encode($data,true);

    }

    // get all branches
    public function getAllBranches($login_user=null)
    {
        if(isset($login_user))
        {
            switch ($login_user->role_id) {

                case '2':
                    $branch = $this->model_branch->getModel()::where('restaurant_id',$login_user->userRestaurant->restaurant_id)->get();

                    break;

                case '3':
                case '4':
                    $branch = $this->model_branch->getModel()::where('id',$login_user->userRestaurant->branch_id)->get();
                break;

                default:
                    $branch = $this->model_branch->all();
                break;
            }
        }
        else
            $branch = $this->model_branch->all();


        if(!$branch)
            return false;

        return $branch;
    }

    // get by id
    public function getBranchById($id)
    {

       $branch = $this->model_branch->find($id);
        if(!$branch)
            return false;

        return $branch;
    }

    // search branch by name
    public function searchBranchesByName($name)
    {
        $branches = $this->model_branch->getModel()::where('name', 'LIKE', '%'.$name.'%')->get();

        if(!$branches)
            return [];

        return $branches;

    }

    // search retaurant by name
    public function searchRestaurantsByName($name)
    {
        $restaurants = $this->model_restaurant->getModel()::where('name','LIKE','%'.$name.'%')
                    ->get();

        if(!$restaurants)
            return [];

        return $restaurants;
    }

    // get branches by restaurant id
    public function getBranchesByRestaurantId($id)
    {
        $branches = $this->model_branch->getModel()::where('restaurant_id','=',$id)->get();

        if(!$branches)
            return false;

            return $branches;
    }

    // save restaurant
    public function save($obj)
    {

        if(isset($obj['id']) && $obj['id'] > 0)
        {
            $obj['date_updated'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_restaurant->update($obj,$obj['id']);
        }
        else
        {
            $obj['date_created'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_restaurant->create($obj);
        }

        if(!$saved_obj)
            return false;

        return $saved_obj;
    }

    // save branch
    public function saveBranch($obj)
    {

        if(isset($obj['id']) && $obj['id'] > 0)
        {
            $obj['date_updated'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_branch->update($obj,$obj['id']);
        }
        else
        {
            $obj['date_created'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_branch->create($obj);
        }

        if(!$saved_obj)
            return false;

            return $saved_obj;
    }

    // get all branch ratings by branch id
    public function getBranchRatingsByBranchId($branch_id)
    {
        $branch_ratings = $this->model_branch_rating->getModel()::where('restaurant_branch_id','=',$branch_id)->get();

        if(!$branch_ratings)
            return false;

        return $branch_ratings;
    }

    // addremove a branch against user id
    public function addRemoveFavBranch($fav,$user_id,$branch_id)
    {
        $branch_fav = $this->model_branch_fav->getModel()::where('restaurant_branch_id','=',$branch_id)->where('user_id','=',$user_id)->first();
        $saved_obj = false;

        if(!$branch_fav && $fav)
        {
            $branch_fav = ["restaurant_branch_id" => $branch_id,"user_id" => $user_id];

            $saved_obj = $this->model_branch_fav->create($branch_fav);
        }
        elseif($branch_fav && !$fav)
        {
            $saved_obj = $this->model_branch_fav->delete($branch_fav->id);

        }elseif($branch_fav && $fav)
        {
            $saved_obj = true;
        }


        if(!$saved_obj)
            return false;

        return $saved_obj;
    }

    // update daily order count in of branch
    public function updateBranchDailyCount()
    {

        $saved_obj = $this->model_branch->getModel()::query()->update(['dailyorder_id' => 1]);

        if(!$saved_obj)
            return false;

            return $saved_obj;
    }


    // Branch Waiters Functions .. .. //

    public function getBrancheWaitersDatatableSource($draw,$start ,$length ,$search,$login_user){


        switch($login_user->role_id)
        {
            case '1':
                $wh = "";
                break;

            case '2':

                $wh = "Where restaurant_branch.restaurant_id =  ".$login_user->userRestaurant->restaurant_id;

                break;

            case '3':

                $wh = "Where restaurant_branch.id =  ".$login_user->userRestaurant->branch_id;

                break;

            default:

                $wh = "";

        }


        $limit = " LIMIT ".$length." OFFSET ".$start;

        $raw_qry = "";

        if($search)
        {
            $wh = $wh." AND (branch_waiters.name LIKE \"%" . $search. "%\" OR restaurant_branch.name LIKE \"%" . $search. "%\" ) ";

            $raw_qry = "SELECT branch_waiters.id,branch_waiters.name,branch_waiters.is_active, restaurant_branch.name as branch
                        FROM branch_waiters
                        JOIN restaurant_branch on branch_waiters.branch_id = restaurant_branch.id
                        ".$wh." Order BY branch_waiters.branch_id ";

            // total records
            $raw_qry_count = "SELECT count(*) as total_branch_waiters FROM branch_waiters
                            JOIN restaurant_branch on branch_waiters.branch_id = restaurant_branch.id
                            WHERE 1".$wh;

            $records_total = DB::select( DB::raw($raw_qry_count) );
            $recordsTotal = $records_total[0]->total_branch_waiters;

            // filtered or search records
            $branch_waiters = DB::select( DB::raw($raw_qry) );

            $recordsFiltered  = count($branch_waiters);

            // actual records return to client

            $branch_waiters = DB::select( DB::raw($raw_qry.$limit) );

        }
        else
        {
            $raw_qry = "SELECT branch_waiters.id,branch_waiters.name,branch_waiters.is_active, restaurant_branch.name as branch
                        FROM branch_waiters
                        JOIN restaurant_branch on branch_waiters.branch_id = restaurant_branch.id
                        ".$wh." Order BY branch_waiters.branch_id ";


            $branch_waiters = DB::select( DB::raw($raw_qry) );
            $recordsTotal = $recordsFiltered = count($branch_waiters);

            $branch_waiters = DB::select( DB::raw($raw_qry. $limit) );

        }

        $resp = array();
        $i = 0;
        foreach($branch_waiters as $item)
        {
            $active = $item->is_active;
            if($active == 1 ){
                $active = "active";
            }else{
                $active = "disable";
            }

            $resp[$i][0] = $item->name;
            $resp[$i][1] = $item->branch;
            $resp[$i][2] = $active;

            $edit_column    = "<a class='text-success mr-2' href='".url('/edit-branch-waiter')."/".$item->id."'><i title='Edit' class='nav-icon mr-2 i-Pen-2'></i>Edit</a>";
            $view_column    = "<a class='text-warning mr-2' href='".url('/view-branch-waiter')."/".$item->id."'><i title='View' class='nav-icon mr-2 i-Full-View-Window'></i>View</a>" ;
            $action_column =    $edit_column.$view_column ;
            $resp[$i][3] = $action_column;

            $i +=1;
        }

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' =>  $recordsFiltered,
            'data' =>  $resp
        );

        return json_encode($data,true);

    }

    public function saveBranchWaiter($obj){

        if(isset($obj['id']) && $obj['id'] > 0)
        {
            $obj['updated_at'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_branch_waiter->update($obj,$obj['id']);
        }
        else
        {
            $obj['created_at'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_branch_waiter->create($obj);
        }

        if(!$saved_obj)
            return false;

        return $saved_obj;
    }

    public function getBranchWaiterByBranchId($id,$login_user ){
        $branch_waiter = null;

        switch($login_user->role_id)
        {
            case '1':
                $branch_waiter = $this->model_branch_waiter->getModel()::where('branch_waiters.id','=',$id)
                    ->select('branch_waiters.*','restaurant.name as restaurant')
                    ->join('restaurant_branch', 'branch_waiters.branch_id', '=', 'restaurant_branch.id')
                    ->join('restaurant','restaurant_branch.id','=','restaurant.id')
                    ->first();
                break;

            case '2':
                $branch_waiter = $this->model_branch_waiter->getModel()::
                select('branch_waiters.*')
                    ->join('restaurant_branch', 'branch_waiters.branch_id', '=', 'restaurant_branch.id')
                    ->where('restaurant_branch.restaurant_id','=',$login_user->userRestaurant->restaurant_id)
                    ->where('branch_waiters.id','=',$id)
                    ->first();

                break;

            case '3':
                $branch_waiter = $this->model_branch_waiter->getModel()::select('branch_waiters.*')
                    ->join('restaurant_branch', 'branch_waiters.branch_id', '=', 'restaurant_branch.id')
                    ->where('branch_waiters.branch_id','=',$login_user->userRestaurant->branch_id)
                    ->where('branch_waiters.id','=',$id)
                    ->first();
                break;

        }

        if(!$branch_waiter)
            return false;

        return $branch_waiter;

    }

    // .. .. End Branch Waiter Functions //
}
