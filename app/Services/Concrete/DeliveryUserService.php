<?php namespace App\Services\Concrete;

use App\Repository\Repository;
use App\Services\Abstractt\IDeliveryUserService;
use App\Models\DeliveryUser;
use App\Models\RestaurantBranch;
use DB;

class DeliveryUserService implements IDeliveryUserService
{
    protected $model_delivery_user;
    protected $model_restaurant_branch;

    public function __construct()
    {
       $this->model_delivery_user=new Repository(new DeliveryUser);
       $this->model_restaurant_branch=new Repository(new RestaurantBranch);
       
    }
     

 
    //find Delivery User by id
    public function getDeiveryUserById($id)
    {
        $response=$this->model_delivery_user->find($id);
        if(!$response)
            return false;
        return $response;
    }
    
    public function getDeliveryUsersDatatableSource($draw,$start ,$length ,$search,$login_user)
    {

        $wh = "";

        $limit = " LIMIT ".$length." OFFSET ".$start;
        $raw_qry = "";

        switch ($login_user['role_id']) {
            
            case '2': // restaurent owner
                
                $wh = " WHERE restaurant_branch.restaurant_id=".$login_user->userRestaurant->restaurant_id;
                
                break;
            
            case '3':// branch owner
                $wh = " WHERE restaurant_branch.id=".$login_user->userRestaurant->branch_id;
                return false;
            
            
                break;
            default:
                # code...
                break;
        }
        if($search)
        {
            $wh .= " AND (delivery_user.first_name LIKE \"%" . $search. "%\" OR delivery_user.last_name LIKE \"%" . $search. "%\" OR delivery_user.login LIKE \"%" . $search. "%\" OR delivery_user.password LIKE \"%" . $search. "%\" OR restaurant_branch.name LIKE \"%" . $search. "%\" OR delivery_user.cell_num LIKE \"%" . $search. "%\" ) ";
            $raw_qry ="SELECT delivery_user.id,delivery_user.first_name,delivery_user.last_name,delivery_user.login,delivery_user.password,restaurant_branch.name,delivery_user.cell_num from delivery_user
            JOIN restaurant_branch on restaurant_branch.id=delivery_user.branchId ".$wh." Order BY delivery_user.first_name asc";

            $raw_qry_count = "SELECT count(*) as totall FROM delivery_user  JOIN restaurant_branch on restaurant_branch.id=delivery_user.branchId".$wh;
            
            $records_total = DB::select( DB::raw($raw_qry_count) );
            $recordsTotal = $records_total[0]->totall;

            // filtered or search records
            $result = DB::select( DB::raw($raw_qry) );
            $recordsFiltered  = count($result);

            // actual records return to client
            $result = DB::select( DB::raw($raw_qry.$limit) );

        }
        else
        {
            
            $raw_qry ="SELECT delivery_user.id,delivery_user.first_name,delivery_user.last_name,delivery_user.login,delivery_user.password,restaurant_branch.name,delivery_user.cell_num from delivery_user
            JOIN restaurant_branch on restaurant_branch.id=delivery_user.branchId ".$wh." Order BY delivery_user.first_name asc";

            $raw_qry_count = "SELECT count(*) as totall FROM delivery_user  JOIN restaurant_branch on restaurant_branch.id=delivery_user.branchId".$wh;
            $records_total = DB::select( DB::raw($raw_qry_count) );
            $recordsTotal = $records_total[0]->totall;

            // filtered or search records
            $result = DB::select( DB::raw($raw_qry) );
            $recordsFiltered  = count($result);
            // actual records return to client
            $result = DB::select( DB::raw($raw_qry.$limit) );

        }
        $resp = array();
        $i = 0;

        foreach($result as $item)
        {
            $resp[$i][0] = $item->first_name;

            $resp[$i][1] = $item->last_name;
            $resp[$i][2] = $item->cell_num;
            $resp[$i][3] = $item->name;
            $resp[$i][4] = $item->login;

            $edit_column    = "<a class='text-success mr-2' href='".url('edit-rider')."/".$item->id."'><i title='Add' class='nav-icon mr-2 i-Pen-2'></i>Edit</a>";
            $view_column    = "<a class='text-warning mr-2' href='".url('view-rider')."/".$item->id."'><i title='View' class='nav-icon mr-2 i-Full-View-Window'></i>View</a>" ;
            $action_column =    $edit_column.$view_column ;

            $resp[$i][5] = $action_column;
            $i +=1;
        }

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' =>  $recordsFiltered,
            'data' =>  $resp
        );

        return json_encode($data,true);
    }

   public function save($obj)
   {
    
       if(isset($obj['id']) && $obj['id'] > 0)
       {
           $obj['date_updated'] = date("Y-m-d H:i:s");
           $saved_obj = $this->model_delivery_user->update($obj,$obj['id']);
       }
       else
       {
           $obj['date_created'] = date("Y-m-d H:i:s");

           $saved_obj = $this->model_delivery_user->create($obj);
       }

       if(!$saved_obj)
           return false;

           return $saved_obj;
   }

   
}