<?php namespace App\Services\Concrete;

use App\Models\BranchTable;
use App\Models\BranchWaiter;
use Illuminate\Database\Eloquent\Model;
use App\Repository\Repository;
use App\Services\Abstractt\IPosService;
use App\Models\AgentUsers;
use App\Models\RestaurantBranch;
use App\Models\Restaurant;
use App\Models\Town;
use App\Models\City;
use App\Models\State;
use App\Models\Country;
use App\Models\AgentRoles;
use App\Models\App;
use App\Models\TownBlock;
use App\Models\MenuFav;
use App\Models\Menu;
use App\Models\MenuCategory;
use App\Models\MenuRating;
use App\Models\Users;
use App\Models\AppUsers;
use App\Models\PosCashFlow;
use DB;


class PosService  implements IPosService
{

    protected $model_agent_user;
    protected $model_agent_role;
    protected $model_branch;
    protected $model_restaurant;
    protected $model_town;
    protected $model_city;
    protected $model_state;
    protected $model_country;
    protected $model_appp;
    protected $model_townblock;
    protected $model_menu_fav;
    protected $model_menu;
    protected $model_menu_category;
    protected $model_menu_rating;
    protected $model_users;
    protected $model_app_users;
    protected $model_pos_cash_flow;
    protected $model_branch_table;
    protected $model_branch_waiter;


    public function __construct()
    {
       // set the model
       $this->model_agent_user = new Repository(new AgentUsers);
       $this->model_agent_role = new Repository(new AgentRoles);
       $this->model_app_users = new Repository(new AppUsers);
       $this->model_users = new Repository(new Users);
       $this->model_branch = new Repository(new RestaurantBranch);
       $this->model_restaurant = new Repository(new Restaurant);
       $this->model_town = new Repository(new Town);
       $this->model_city = new Repository(new City);
       $this->model_state = new Repository(new State);
       $this->model_country = new Repository(new Country);
       $this->model_app = new Repository(new App);
       $this->model_townblock = new Repository(new TownBlock);
       $this->model_menu_fav = new Repository(new MenuFav);
       $this->model_menu = new Repository(new Menu);
       $this->model_menu_category = new Repository(new MenuCategory);
       $this->model_menu_rating = new Repository(new MenuRating);
       $this->model_pos_cash_flow = new Repository(new PosCashFlow);
       $this->model_branch_table = new Repository(new BranchTable);
       $this->model_branch_waiter = new Repository(new BranchWaiter);

    }


    // Login Service
    public function posLogin($username,$password)
    {

        $agent_user_model = $this->model_agent_user->getModel();
        $agent_user = $agent_user_model::where([['username', '=', $username],['password', '=', $password]])->firstorFail();
        if(!$agent_user)
            return false;
        $agent_role = $this->model_agent_role->find($agent_user->agent_role_id);
        $branch     = $this->model_branch->find($agent_user->branch_id);
        $restaurant = $this->model_restaurant->find($branch->restaurant_id);
        $town = $this->model_town->find($branch->town_id);
        $city = $this->model_city->find($town->city_id);
        $state = $this->model_state->find($city->state_id);
        $country = $this->model_country->find($state->country_id);
        $waiter_list= $this->model_branch_waiter->getModel()::select('branch_waiters.id','branch_waiters.name')->where('branch_id','=',$branch->id)->where('is_active','=','1')->get();
        // $appp = $this->model_app->getModel()::where('branch_id','=',$branch->id)->where('app_name','LIKE','%pos%')->firstorFail();

       return  [
                "id"            => $agent_user->id,
                "branch_id"     => $branch->id,
                "username"      => $agent_user->username,
                "name"          => $agent_user->name,
                "password"      => $agent_user->userpasswordname,
                "allowPos"      => $agent_user->allowPos,
                "lastLogin"     => $agent_user->lastLogin,
                "loggedin"      => $agent_user->loggedin,
                "agent_role_id" => $agent_user->agent_role_id,
                "hcc_user_login" =>$agent_user->hcc_user_login,
                "hcc_user_password" => $agent_user->hcc_user_password,
                "hcc_server_ip"  => $agent_user->hcc_server_ip,
                "restaurant_name"=> $restaurant->name,
                "rolename"      => $agent_role->role_name,
                "branch_name"   => $branch->name,
                "restaurant_id" => $restaurant->id,
                "city_id"       => $city->id,
                "country_id"    => $country->id,
                "state_id"      => $state->id,
                "town_id"       => $town->id,
                "restaurantCity"=> $city->city_name,
                "restaurantTown"=> $town->town_name,
                "restaurantCountry"=> $country->country_name,
                "restaurantState"=> $state->state_name,
                // "app_name"      => $appp->app_name,
                // "app_id"        => $appp->id,
                "ntn_number"    => $branch->ntn_number,
                "tax_include"   => $branch->tax_include,
                "tax_percent"   => $branch->tax_percent,
                "currency"      => $branch->currency,
                "order_discount_percent"=> $branch->order_discount_percent,
                "waiter_list"   => $waiter_list??[],
                "tax_on_original_amt"   => $branch->tax_on_original_amt,
                "lstCategories" => null,
            ];
    }

    // Get Branches name and id
    public function getRestaurantBranchesNameAndId($restaurant_id)
    {
        $web_app_service = new WebAppService();

        $branches = $this->model_branch->getModel()::where('restaurant_id', '=', $restaurant_id)->with('restaurant')->get()->map(function($branch)use($web_app_service){
            return [
                'id' => $branch->id,
                'name' => $branch->name,
                'logo' => isset($branch->logo)?$branch->logo:(isset($branch->restaurant->logo)?$branch->restaurant->logo:"findurmeal_logo_rv.png"),
                'restaurant_name' => $branch->restaurant->name,
                'latitude'        => $branch->latitude,
                'longitude'       => $branch->longitude,
                'address'         => $branch->address,
                'contact_email'   => $branch->contact_email,
                'contact_number'  => $branch->contact_number,
                'open_time'       => $branch->open_time,
                'close_time'      => $branch->close_time,
                'restaurant_open' => $web_app_service->isBetween(gmdate("Y-m-d H:i:s"), $branch->open_time, $branch->close_time),
            ];
        });


        if(!$branches)
            return false;

            return $branches;
    }

    //get Categories by branch Id
    public function getCategoriesByBranchId($branch_id,$user_id=0)
    {
        // list of favourite menu by user id
        $lst_menu_fav = $this->model_menu_fav->getModel()::where('user_id', '=', $user_id)->get();

        // list of menu rating by branch id
        $lst_menu_rating = $this->model_menu_rating->getModel()::join('menu', 'menu.id', '=', 'menu_rating.menu_id')
                            ->join('menu_category', 'menu.menu_category_id', '=', 'menu_category.id')
                            ->where('menu_category.restaurant_branch_id', '=', $branch_id)
                            ->where('menu_category.is_active', true)
                            ->select('menu_rating.id','menu_rating.menu_id','menu_rating.user_id','menu_rating.rating')
                            ->get();

        // list of menu by branch id

        $lst_menu_category = $this->model_menu_category
                ->with(['restaurantBranch',
                    'menus' => function($query){
                        $query->where('is_deleted',0)->orwhere('is_deleted',null)->orderBy('name', 'ASC') ;
                    },
                    'menus.choiceGroups.choices',
                    'menus.menuVariations.choiceGroups.choices'
                ])
                // ->where('menu_category.id',4598)
                ->where('menu_category.is_active',1)
                ->where('menu_category.restaurant_branch_id',$branch_id)
                ->get();


        // $lst_menu = $this->model_menu->getModel()::where('is_deleted','0')
        //         ->orwhere('is_deleted',null)
        //         ->whereHas('menuCategory',function($category)use($branch_id){
        //             $category->where('restaurant_branch_id', '=', $branch_id)->with('restaurantBranch');
        //         })
        //         ->with([
        //             'menuVariations' => function($query)
        //                 {
        //                     $query->with([
        //                         'choiceGroups' => function($query)
        //                             {
        //                                 $query->with('choices');
        //                             }
        //                     ]);
        //                 },
        //             'choiceGroups' => function($query){
        //                     $query->with('choices');
        //                 }
        //             ])
        //         ->orderBy('name', 'ASC')
        //         ->get();


                // ->with([
                //     'menuVariations' => function($query)
                //         {
                //             $query->with([
                //                 'choiceGroups' => function($query)
                //                     {
                //                         $query->with('choices');
                //                     }
                //             ]);
                //         },
                //     'choiceGroups' => function($query){
                //             $query->with('choices');
                //         }
                //     ])
                // ->orderBy('name', 'ASC')
                // ->get();

        // list of menu categoryby branch id
        // $lst_menu_category = $this->model_menu_category->getModel()::where('menu_category.restaurant_branch_id', '=', $branch_id)
        //                     ->where('menu_category.is_active', true)
        //                     ->orderBy('id', 'DESC')
        //                     ->get();
        // RETURN ARRAY TO BE FILLED
        $response = [];

        foreach($lst_menu_category as $category )
        {
            $menu = [];

            foreach($category->menus as $val )
            {
                // if($val->menu_category_id == $category->id)
                // {
                    $rating =  (object)[
                        'ratedby'       =>  $lst_menu_rating->where('menu_id','=',$val->id)->count(),
                        'user_rating'   =>  $lst_menu_rating->where('menu_id','=',$val->id)->where('user_id','=',$user_id)->pluck('rating')->first() ?? 0,
                        'rating'        =>  $lst_menu_rating->where('menu_id','=',$val->id)->avg('rating')??0

                    ];


                    $menu[] = (object)[
                        'id'            => $val->id,
                        'name'          => $val->name,
                        'menu_variations'=> $val->menuVariations,
                        'choice_group'  => $val->choiceGroups,
                        'price'         => $val->price,
                        'image'         => isset($val->image)?$val->image:(isset($category->image)?$category->image:(isset($category->restaurantBranch->logo)?$category->restaurantBranch->logo:"findurmeal_logo_rv.png")),
                        'description'   => $val->description,
                        'ingridient'    => $val->ingridient ,
                        'likes'         => $val->likes,
                        'reviews'       => $val->reviews,
                        'restbranch_id' => $category->restaurantBranch->id,
                        'isFav'         => $lst_menu_fav->where('menu_id','=',$val->id)->where('user_id','=',$user_id)->first() ?? false,
                        'fb_post_id'    => $val->fb_post_id,
                        'order_taking'  => $category->restaurantBranch->order_taking,
                        'branch_name'   => $category->restaurantBranch->name,
                        'branch_logo'   => $category->restaurantBranch->logo,
                        'currency'      => $category->restaurantBranch->currency,
                        'rating'        => $rating


                    ];
                // }
            }

            $response[] = (object)[

                    'id'                => $category->id,
                    'name'              => $category->name,
                    'image'             => isset($category->image)?$category->image:(isset($category->restaurantBranch->logo)?$category->restaurantBranch->logo:"findurmeal_logo_rv.png"),
                    'restbranch_id'     => $category->restaurant_branch_id,
                    'restbranch_name'   => $category->restaurantBranch->name,
                    'restbranch_address'=> $category->restaurantBranch->address,
                    'contact_person'    => $category->restaurantBranch->contact_person,
                    'contact_number'    => $category->restaurantBranch->contact_number,
                    'restaurant_name'   => $category->restaurantBranch->restaurant->name,
                    'branch_name'       => $category->restaurantBranch->name,
                    'branch_logo'       => $category->restaurantBranch->logo,
                    'restaurant_id'     => $category->restaurantBranch->restaurant_id,
                    'Menu'              => $menu
            ];
        }

        if(!$response)
            return false;

            return $response;
    }

    // Get pos cash flow
    public function getPosCashFlow($branch_id,$datee)
    {
        $pos_cash_flow = $this->model_pos_cash_flow->getModel()::where('branch_id',$branch_id)
        ->whereDate('date_created','=',$datee)
        ->first();


        if(!$pos_cash_flow)
            return [];

        return $pos_cash_flow;
    }

    // save pos cash flow
    public function savePosCashFlow($obj)
    {

        if(isset($obj['id']) && $obj['id'] > 0)
        {
            $obj['date_updated'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_pos_cash_flow->update($obj,$obj['id']);
        }
        else
        {
            $obj['date_created'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_pos_cash_flow->create($obj);
        }

        if(!$saved_obj)
            return false;

            return $saved_obj;
    }


    public function getBranchTableDatatableSource($draw,$start,$length,$search,$login_user)
    {

        switch($login_user->role_id)
        {
            case '1':
                $wh = "";
                break;

            case '2':

                $wh = "Where restaurant_branch.restaurant_id =  ".$login_user->userRestaurant->restaurant_id;

                break;

            case '3':

                $wh = "Where restaurant_branch.id =  ".$login_user->userRestaurant->branch_id;

                break;

            default:

                $wh = " ";

        }


        $limit = " LIMIT ".$length." OFFSET ".$start;
        $raw_qry = "";

        if($search)
        {
            $wh = $wh." AND (branch_tables.table_no LIKE \"%" . $search. "%\" OR branch_tables.description LIKE \"%" . $search. "%\" OR restaurant_branch.name LIKE \"%" . $search. "%\" ) ";
            $raw_qry = "SELECT branch_tables.id,branch_tables.table_no,branch_tables.description,branch_tables.availability as enable, branch_tables.capacity, restaurant_branch.name as branch
                        FROM branch_tables
                        JOIN restaurant_branch on branch_tables.branch_id = restaurant_branch.id
                        ".$wh." Order BY branch_tables.table_no ";

            // total records
            $raw_qry_count = "SELECT count(*) as total_branch_tables FROM branch_tables
                            JOIN restaurant_branch on branch_tables.branch_id = restaurant_branch.id
                            WHERE 1".$wh;

            $records_total = DB::select( DB::raw($raw_qry_count) );
            $recordsTotal = $records_total[0]->total_branch_tables;

            // filtered or search records
            $branch_tables = DB::select( DB::raw($raw_qry) );

            $recordsFiltered  = count($branch_tables);

            // actual records return to client

            $branch_tables = DB::select( DB::raw($raw_qry.$limit) );

        }
        else
        {
            $raw_qry = "SELECT branch_tables.id,branch_tables.table_no,branch_tables.description,branch_tables.availability as enable, branch_tables.capacity, restaurant_branch.name as branch
                        FROM branch_tables
                        JOIN restaurant_branch on branch_tables.branch_id = restaurant_branch.id
                        ".$wh." Order BY branch_tables.table_no ";


            $branch_tables = DB::select( DB::raw($raw_qry) );
            $recordsTotal = $recordsFiltered = count($branch_tables);

            $branch_tables = DB::select( DB::raw($raw_qry. $limit) );

        }

        $resp = array();
        $i = 0;
        foreach($branch_tables as $item)
        {

            $resp[$i][0] = $item->branch;
            $resp[$i][1] = $item->table_no;
            $resp[$i][2] = $item->description;
            $resp[$i][3] = $item->enable;
            $resp[$i][4] = $item->capacity;

            $edit_column    = "<a class='text-success mr-2' href='".url('/edit-branch-table')."/".$item->id."'><i title='Edit' class='nav-icon mr-2 i-Pen-2'></i>Edit</a>";
            $view_column    = "<a class='text-warning mr-2' href='".url('/view-branch-table')."/".$item->id."'><i title='View' class='nav-icon mr-2 i-Full-View-Window'></i>View</a>" ;
//            $delete_column  = "<a class='text-danger mr-2' href='".url('/delete-branch-table')."/".$item->id."'><i title='View' class='nav-icon mr-2 i-Full-View-Window'></i>Delete</a>" ;
            $action_column =    $edit_column.$view_column ;
//            $action_column =    $edit_column.$view_column.$delete_column ;

            $resp[$i][5] = $action_column;
            $i +=1;
        }

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' =>  $recordsFiltered,
            'data' =>  $resp
        );

        return json_encode($data,true);

    }


    public function saveBranchTable($obj){
        //        dd($obj);
        if(isset($obj['id']) && $obj['id'] > 0)
        {
            $obj['updated_at'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_branch_table->update($obj,$obj['id']);
        }
        else
        {
            $obj['created_at'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_branch_table->create($obj);
        }
//        dd($saved_obj);

        if(!$saved_obj)
            return false;

        return $saved_obj;
    }

    public function getBranchTableByIdAndUserRole($id,$login_user){
        //  dd($id);
        switch($login_user->role_id)
        {
            case '1':
                $branch_table = $this->model_branch_table->getModel()::
                select('branch_tables.*')
                    ->join('restaurant_branch', 'branch_tables.branch_id', '=', 'restaurant_branch.id')
                    ->where('branch_tables.id','=',$id)->first();
                break;

            case '2':
                $branch_table = $this->model_branch_table->getModel()::
                select('branch_tables.*')
                    ->join('restaurant_branch', 'branch_tables.branch_id', '=', 'restaurant_branch.id')
                    ->where('restaurant_branch.restaurant_id','=',$login_user->userRestaurant->restaurant_id)
                    ->where('branch_tables.id','=',$id)
                    ->first();

                break;

            case '3':
                $branch_table = $this->model_branch_table->getModel()::select('branch_tables.*')
                    ->join('restaurant_branch', 'branch_tables.branch_id', '=', 'restaurant_branch.id')
                    ->where('branch_tables.branch_id','=',$login_user->userRestaurant->branch_id)
                    ->where('branch_tables.id','=',$id)
                    ->first();
                break;

        }

//        dd($branch_table);


        if(!$branch_table)
            return false;

        return $branch_table;
    }
}
