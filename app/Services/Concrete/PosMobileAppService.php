<?php namespace App\Services\Concrete;

use Illuminate\Database\Eloquent\Model;
use App\Repository\Repository;
use App\Services\Abstractt\IPosMobileAppService;
use App\Models\AgentUsers;
use App\Models\RestaurantBranch;
use App\Models\Restaurant;
use App\Models\Town;
use App\Models\City;
use App\Models\State;
use App\Models\Country;
use App\Models\AgentRoles;
use App\Models\App;
use App\Models\TownBlock;
use App\Models\MenuFav;
use App\Models\Menu;
use App\Models\MenuCategory;
use App\Models\MenuRating;
use App\Models\Users;
use App\Models\AppUsers;


class PosMobileAppService  implements IPosMobileAppService
{

    protected $model_agent_user;
    protected $model_agent_role;
    protected $model_branch;
    protected $model_restaurant;
    protected $model_town;
    protected $model_city;
    protected $model_state;
    protected $model_country;
    protected $model_appp;
    protected $model_townblock;
    protected $model_menu_fav;
    protected $model_menu;
    protected $model_menu_category;
    protected $model_menu_rating;
    protected $model_users;
    protected $model_app_users;


    public function __construct()
    {
       // set the model
       $this->model_agent_user = new Repository(new AgentUsers);
       $this->model_agent_role = new Repository(new AgentRoles);
       $this->model_app_users = new Repository(new AppUsers);
       $this->model_users = new Repository(new Users);
       $this->model_branch = new Repository(new RestaurantBranch);
       $this->model_restaurant = new Repository(new Restaurant);
       $this->model_town = new Repository(new Town);
       $this->model_city = new Repository(new City);
       $this->model_state = new Repository(new State);
       $this->model_country = new Repository(new Country);
       $this->model_app = new Repository(new App);
       $this->model_townblock = new Repository(new TownBlock);
       $this->model_menu_fav = new Repository(new MenuFav);
       $this->model_menu = new Repository(new Menu);
       $this->model_menu_category = new Repository(new MenuCategory);
       $this->model_menu_rating = new Repository(new MenuRating);

    }


    // Login Service
    public function posMobileAppLogin($username,$password)
    {

        $agent_user_model = $this->model_agent_user->getModel();
        $agent_user = $agent_user_model::where([['username', '=', $username],['password', '=', $password]])->first();


        if(!$agent_user)
            return NULL;

        $agent_role = $this->model_agent_role->find($agent_user->agent_role_id);
        $branch     = $this->model_branch->find($agent_user->branch_id);

        $branch_id = $agent_user->branch_id;

        // list of menu categoryby branch id
        $lst_menu_category = $this->model_menu_category->getModel()::where('menu_category.restaurant_branch_id', '=', $branch_id)
        ->where('menu_category.is_active', true)
        ->orderBy('id', 'DESC')
        ->get();

        // list of menu by branch id
        $lst_menu = $this->model_menu->getModel()::where('is_deleted','0')
                ->orwhere('is_deleted',null)
                ->whereHas('menuCategory',function($category)use($branch_id){
                    $category->where('restaurant_branch_id', '=', $branch_id)->where('is_active',true);})
                ->with([
                    'menuVariations' => function($query)
                        {
                            $query->with([
                                'choiceGroups' => function($query)
                                    {
                                        $query->with('choices');
                                    }
                            ]);
                        },
                    'choiceGroups' => function($query){
                            $query->with('choices');
                        }
                    ])
                ->orderBy('name', 'ASC')
                ->get();


        $town = $this->model_town->find($branch->town_id);
        $city = $this->model_city->find($town->city_id);
        $state = $this->model_state->find($city->state_id);
        $country = $this->model_country->find($state->country_id);

        $category_list = [];

        foreach ($lst_menu_category as $category) {

            if(!$category->is_active)
                continue;

            $menu_list = [];

            foreach ($lst_menu as $menu) {

                if($menu->is_deleted)
                continue;

                if($menu->menu_category_id == $category->id)
                {
                    $menu_list[] = [
                        "id"    =>  $menu->id,
                        "name"  =>  $menu->name,
                        "price" =>  $menu->price,
                        "image" =>  !isset($menu->image)?(!isset($category->image)?"findurmeal_logo_rv.png":$category->image):$menu->image,
                        "ingridient"  =>  $menu->ingridient,
                        "description" =>  $menu->description,
                        'menu_variations'=> $menu->menuVariations,
                        'choice_group'   => $menu->choiceGroups,

                    ];
                }
            }

            $category_list[] = [
                "id"        =>  $category->id,
                "name"      =>  $category->name,
                "image"     =>  $category->image??"findurmeal_logo_rv.png",
                "menuList"  =>  collect($menu_list)->sortBy('name')->values()->toArray()
            ];
        }
        $category_list =  collect($category_list)->sortByDesc('id')->values()->toArray();

        // $appp = $this->model_app->getModel()::where('branch_id','=',$branch->id)->where('app_name','LIKE','%pos%')->firstorFail();

       return  [
           [
                "userId"        => $agent_user->id,
                "username"      => $agent_user->username,
                "roleId"        => $agent_role->id,
                "roleName"      => $agent_role->role_name,
                "branchId"      => $branch->id,
                "strnNumber"     => $branch->strn_number,
                "ntnNumber"     => $branch->ntn_number,
                // "app_name"      => $appp->app_name,
                // "appId"         => $appp->id,
                "taxInclude"    => $branch->tax_include??false,
                "taxPercent"    => $branch->tax_percent??0,
                "townId"        => $branch->town_id??0
            ],
            [
                "id"            => $branch->id,
                "address"       => $branch->address,
                "order_taking"  => $branch->order_taking??false,
                "logo"          => $branch->logo??"findurmeal_logo_rv.png",
                "latitude"      => $branch->latitude,
                "longitude"     => $branch->longitude,
                "contact_number"=> $branch->contact_number,
                "name"          => $branch->name,
                "currency"      => $branch->currency,
                "tax_percent"   => !isset($branch->tax_include) || $branch->tax_include == false?$branch->tax_percent:"0.00",
                "town_id"       => $branch->town_id??0,
                "city_id"       => $city->id,
                "country_id"    => $country->id,
                "state_id"      => $state->id,
            ],

            $category_list
        ];
    }

    // Get Branches name and id
    public function getRestaurantBranchesNameAndId($restaurant_id)
    {
        $branches = $this->model_branch->getModel()::where('restaurant_id', '=', $restaurant_id)->with('restaurant')->get()->map(function($branch){
            return [
                'id' => $branch->id,
                'name' => $branch->name,
                'logo' => isset($branch->logo)?$branch->logo:(isset($branch->restaurant->logo)?$branch->restaurant->logo:"findurmeal_logo_rv.png"),
                'restaurant_name' => $branch->restaurant->name,

            ];
        });

        if(!$branches)
            return false;

            return $branches;
    }

    //get Categories by branch Id
    public function getCategoriesByBranchId($branch_id,$user_id=0)
    {
        // list of favourite menu by user id
        $lst_menu_fav = $this->model_menu_fav->getModel()::where('user_id', '=', $user_id)->get();

        // list of menu rating by branch id
        $lst_menu_rating = $this->model_menu_rating->getModel()::join('menu', 'menu.id', '=', 'menu_rating.menu_id')
                            ->join('menu_category', 'menu.menu_category_id', '=', 'menu_category.id')
                            ->where('menu_category.restaurant_branch_id', '=', $branch_id)
                            ->select('menu_rating.id','menu_rating.menu_id','menu_rating.user_id','menu_rating.rating')
                            ->get();

        // list of menu by branch id
        $lst_menu = $this->model_menu->getModel()::where('is_deleted','0')
                ->orwhere('is_deleted',null)
                ->whereHas('menuCategory',function($category)use($branch_id){
                    $category->where('restaurant_branch_id', '=', $branch_id);})->orderBy('name', 'ASC')->get();

        // list of menu categoryby branch id
        $lst_menu_category = $this->model_menu_category->getModel()::where('menu_category.restaurant_branch_id', '=', $branch_id)
                            ->orderBy('id', 'DESC')
                            ->get();
        // RETURN ARRAY TO BE FILLED
        $response = [];

        foreach($lst_menu_category as $category )
        {
            $menu = [];

            foreach($lst_menu as $val )
            {
                if($val->menu_category_id == $category->id)
                {
                    $rating =  (object)[
                        'ratedby'       =>  $lst_menu_rating->where('menu_id','=',$val->id)->count(),
                        'user_rating'   =>  $lst_menu_rating->where('menu_id','=',$val->id)->where('user_id','=',$user_id)->pluck('rating')->first() ?? 0,
                        'rating'        =>  $lst_menu_rating->where('menu_id','=',$val->id)->avg('rating')??0

                    ];


                    $menu[] = (object)[
                        'id'            => $val->id,
                        'name'          => $val->name,
                        'price'         => $val->price,
                        'image'         => isset($val->image)?$val->image:(isset($category->image)?$category->image:(isset($category->restaurantBranch->logo)?$category->restaurantBranch->logo:"findurmeal_logo_rv.png")),
                        'description'   => $val->description,
                        'ingridient'    => $val->ingridient ,
                        'likes'         => $val->likes,
                        'reviews'       => $val->reviews,
                        'restbranch_id' => $category->restaurantBranch->id,
                        'isFav'         => $lst_menu_fav->where('menu_id','=',$val->id)->where('user_id','=',$user_id)->first() ?? false,
                        'fb_post_id'    => $val->fb_post_id,
                        'order_taking'  => $category->restaurantBranch->order_taking,
                        'branch_name'   => $category->restaurantBranch->name,
                        'branch_logo'   => $category->restaurantBranch->logo,
                        'currency'      => $category->restaurantBranch->currency,
                        'rating'        => $rating


                    ];
                }
            }

            $response[] = (object)[

                    'id'                => $category->id,
                    'name'              => $category->name,
                    'image'             => isset($category->image)?$category->image:(isset($category->restaurantBranch->logo)?$category->restaurantBranch->logo:"findurmeal_logo_rv.png"),
                    'restbranch_id'     => $category->restaurant_branch_id,
                    'restbranch_name'   => $category->restaurantBranch->name,
                    'restbranch_address'=> $category->restaurantBranch->address,
                    'contact_person'    => $category->restaurantBranch->contact_person,
                    'contact_number'    => $category->restaurantBranch->contact_number,
                    'restaurant_name'   => $category->restaurantBranch->restaurant->name,
                    'branch_name'       => $category->restaurantBranch->name,
                    'branch_logo'       => $category->restaurantBranch->logo,
                    'restaurant_id'     => $category->restaurantBranch->restaurant_id,
                    'Menu'              => $menu
            ];
        }

        if(!$response)
            return false;

            return $response;
    }




}
