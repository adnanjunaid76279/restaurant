<?php namespace App\Services\Concrete;

use App\Repository\Repository;
use App\Services\Abstractt\IUserService;
use App\Models\Users;
use App\Models\AppUsers;
use App\Models\DeliveryUsersOrder;
use Illuminate\Support\Facades\DB;


class UserService  implements IUserService
{

    protected $model_user;
    protected $model_app_user;
    protected $model_delivery_user_order;

    public function __construct()
    {
       // set the model
       $this->model_user = new Repository(new Users);
       $this->model_app_user = new Repository(new AppUsers);
       $this->model_delivery_user_order = new Repository(new DeliveryUsersOrder);

    }


    // save user
    public function save($obj)
    {
        if(isset($obj['id']) && $obj['id'] > 0)
        {
            $obj['date_updated'] = date("Y-m-d H:i:s");
            $obj['updatedby'] = $obj['first_name'];
            $saved_obj = $this->model_user->update($obj,$obj['id']);

            if($saved_obj)
                $saved_obj = $this->model_user->find($obj['id']);
        }
        else
        {
            $obj['date_created'] = date("Y-m-d H:i:s");
            $obj['createdby'] = $obj['first_name'];
            $saved_obj = $this->model_user->create($obj);
            if($saved_obj)
                $saved_obj = $this->model_user->find($saved_obj['id']);
        }

        if(!$saved_obj)
            return false;

        return $saved_obj;
    }

    // save app user
    public function saveAppUser($obj)
    {
        if(isset($obj['id']) && $obj['id'] > 0)
        {
            $obj['date_updated'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_app_user->update($obj,$obj['id']);
        }
        else
        {
            $obj['date_created'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_app_user->create($obj);
        }

        if(!$saved_obj)
            return false;

            return $saved_obj;
    }

    // save pos user
    public function savePosUser($obj)
    {
        $user = $this->getByContactAppId($obj['cell_num'],$obj['appId']);

        if (!$user)
        {
            $get_userbycontact = $this->getByContact($obj['cell_num']);

            if ($get_userbycontact)
            {
                $obj['id'] = $get_userbycontact->id;
            }
            else
            {
                $obj['id'] = $this->save($obj)->id;
            }

            $obj_app_user = [
            'app_id' => $obj['appId'],
            'user_id' => $obj['id'],
            'fb_id' => isset($obj['fb_id'])?$obj['fb_id']:null
            ];
            $this->model_app_user->create($obj_app_user);

        }
        else
            $obj['id'] = $user->id;

        if(!$obj['id'] )
            return false;

            return $obj['id'] ;
    }

    // get user by id
    public function getById($user_id)
    {
        $user = $this->model_user->find($user_id);

        if(!$user)
            return false;

        return $user;

    }


    // get user by id
    public function getByContact($contact)
    {
        $user = $this->model_user->getModel()::where('cell_num','=',$contact)->orwhere('phone_num','=',$contact)->first();
        if(!$user)
            return false;

        return $user;

    }

    // get app user by app id,user id,fb id
    public function getAppUserByFbIdAppIdUserId($app_id,$user_id,$fb_id)
    {
        $app_user = $this->model_app_user->getModel()::where('user_id','=',$user_id)
        ->where('fb_id','=',$fb_id)
        ->where('app_id','=',$app_id)
        ->first();


        if(!$app_user)
            return false;

        return $app_user;

    }

     // get user by contact and app id
    public function getByContactAppId($contactno,$app_id)
    {
        $users  =   $this->model_user->getModel()::where(function($query)use($contactno)
                    {
                        $query->where('cell_num', '=', $contactno)
                            ->orwhere('phone_num', '=', $contactno);
                    })->leftjoin('app_users', function($join)use($app_id)
                    {
                        $join->on('users.id', '=', 'app_users.user_id')->where('app_users.app_id', '=', $app_id);
                    })
                    ->leftJoin('address', 'users.id', '=', 'address.user_id')
                    ->leftJoin('town', 'town.id', '=', 'address.town_id')
                    ->leftJoin('town_block', 'town_block.id', '=', 'address.town_block_id')
                    ->leftJoin('city', 'city.id', '=', 'town.city_id')
                    ->leftJoin('state', 'state.id', '=', 'city.state_id')
                    ->leftJoin('country', 'country.id', '=', 'state.country_id')
                    ->select('users.id',
                    'users.email',
                    'users.first_name',
                    'users.last_name',
                    'users.phone_num',
                    'users.cell_num',
                    'users.gender',
                    'users.date_birth',
                    'address.address1 as address',
                    'town.id as townId',
                    'address.latitude',
                    'address.longitude',
                    'town_block.id as townBlockId'
                    )
                    ->orderBy('address.id', 'DESC')
                    ->first();
        if(!$users)
            return false;

        return $users;
    }


    // get user id by fb id and app id
    public function getUserIdByFbIdAppId($fb_id,$app_id)
    {
        $user_id  =   $this->model_app_user->getModel()::where(function($query)use($fb_id,$app_id)
                    {
                        $query->where('app_users.fb_id', '=', $fb_id)
                            ->where('app_users.app_id', '=', $app_id);
                    })->Join('users', 'users.id', '=', 'app_users.user_id')
                    ->select('users.id')
                    ->first();


        if(!$user_id)
            return false;

        return $user_id->id;
    }

    // get  users by date
    public function getUsersCountByDate($datee,$branch_id,$login_user=null)
    {
        // dd($login_user->userRestaurant->branch_id);
        $wh = "";

        switch ($login_user->role_id)
        {
            case '1':
                if($branch_id != null){
                    $wh = "AND restaurant_branch.id = ".$branch_id ;

                }else{
                    $wh = "";
                }

                break;

            case '2':
                if($branch_id != null){
                    $wh = " AND restaurant_branch.id = ".$branch_id ;
                }else{
                    $wh = " AND restaurant_branch.restaurant_id = ".$login_user->userRestaurant->restaurant_id ;
                }

                break;
            case '3':
            case '4':
                
                $wh = " AND restaurant_branch.id = ".$login_user->userRestaurant->branch_id ;
                
                break;

            default:
                $wh = " ";
        }


        $user = "select count(*) as total
                 from `users`
                 inner join `restaurant_branch` on `users`.`branch_id` = `restaurant_branch`.`id`
                 where date(`users`.`date_created`) = '".$datee."' ".$wh." " ;


        $data = DB::select(DB::raw($user));
        
        // dd($data);

        $user_count= $data[0]->total;

        if(!$user_count)
            return 0;

        return $user_count;

    }

    // get users count by date range
    public function getUsersCountByDateRange($start_date,$end_date,$branch_id,$login_user=null)
    {
        $wh = "";

        switch ($login_user->role_id)
        {
            case '1':
                if($branch_id != null){
                    $wh = "AND restaurant_branch.id = ".$branch_id ;

                }else{
                    $wh = " ";
                }

                break;

            case '2':
                if($branch_id != null){
                    $wh = " AND restaurant_branch.id = ".$branch_id ;
                }else{
                    $wh = " AND restaurant_branch.restaurant_id = ".$login_user->userRestaurant->restaurant_id ;
                }

                break;
            case '3':
            case '4':
                $wh = " AND restaurant_branch.id = ".$login_user->userRestaurant->branch_id ;
                break;

            default:
                $wh = " ";
        }

        $user = "select count(*) as total
                 from `users`
                 inner join `restaurant_branch` on `users`.`branch_id` = `restaurant_branch`.`id`
                 where date(`users`.`date_created`) >= '".$start_date."' and date(`users`.`date_created`) <= '".$end_date."' ".$wh." " ;

        $data = DB::select(DB::raw($user));

        $user_count= $data[0]->total;

        if(!$user_count)
            return 0;

        return $user_count;

    }

    // save website user
    public function saveWebUser($obj)
    {

        $user_id = $this->getUserIdByFbIdAppId($obj['fb_id'],$obj['appId']);

        if(!$user_id)
        {
            $user_id = $this->save($obj)->id;

            $obj_app_user = [
                'fb_id'       => $obj['fb_id'],
                'app_id'      => $obj['appId'],
                'user_id'     => $user_id
            ];

            $this->saveAppUser($obj_app_user );
        }
        else
        {
            $obj_app_user = [
                'fb_id'       => $obj['fb_id'],
                'app_id'      => $obj['appId'],
                'user_id'     => $user_id
            ];

            $temp_app_user = $this->getAppUserByFbIdAppIdUserId($obj['appId'],$user_id,$obj['fb_id']);
            $obj_app_user['id'] = $temp_app_user->id ;

            $this->saveAppUser($obj_app_user);

        }

        $objUser = $this->getUserdetailById($user_id);
        $objUser->appId = $obj['appId'];

        if(!$objUser )
             return false;

        return $objUser ;
    }

    // save mobile user
    public function saveMobileUser($obj)
    {

        $user_id = $this->getUserIdByFbIdAppId($obj['fb_id'],$obj['app_id']);

        if(!$user_id)
        {
            $user_id = $this->save($obj)->id;

            $obj_app_user = [
                'fb_id'       => $obj['fb_id'],
                'app_id'      => $obj['app_id'],
                'device_id'   => $obj['device_id'],
                'user_id'     => $user_id
            ];

            $this->saveAppUser($obj_app_user );
        }
        else
        {
            $obj_app_user = [
                'fb_id'       => $obj['fb_id'],
                'app_id'      => $obj['app_id'],
                'device_id'   => $obj['device_id'],
                'user_id'     => $user_id
            ];

            $temp_app_user = $this->getAppUserByFbIdAppIdUserId($obj['app_id'],$user_id,$obj['fb_id']);
            $obj_app_user['id'] = $temp_app_user->id ;

            $this->saveAppUser($obj_app_user);

        }

        $objUser = $this->getUserdetailById($user_id);
        $objUser->name = isset($objUser->last_name)?$objUser->first_name. " ".$objUser->last_name:$objUser->first_name ;
        $objUser->address = $objUser->address1;
        $objUser->userId = $objUser['id'];
        $objUser->appId = $obj['app_id'];


        if(!$objUser )
             return false;

        return $objUser ;
    }

    // get user detail by id
    public function getUserdetailById($user_id)
    {
        $user = $this->model_user->getModel()::where('users.id','=',$user_id)
                ->leftJoin('address', 'users.id', '=', 'address.user_id')
                ->leftJoin('town', 'town.id', '=', 'address.town_id')
                ->leftJoin('town_block', 'town_block.town_id', '=', 'town.id')
                ->leftJoin('city', 'town.city_id', '=', 'city.id')
                ->leftJoin('state', 'city.state_id', '=', 'state.id')
                ->leftJoin('country', 'state.country_id', '=', 'country.id')
                ->select('users.id',
                            'users.first_name',
                            'users.last_name',
                            'users.cell_num as contact',
                            'users.fb_id as fbId',
                            'users.email',
                            'users.gender',
                            'users.date_birth as dateBirth',
                            'city.id as cityId',
                            'town_block.id as blockId',
                            'address.town_id as townId',
                            'address.address1',
                            'city.city_name',
                            'town.town_name'
                            )
                ->orderBy('address.id', 'DESC')
                ->first();

        if(!$user)
            return false;

        return $user;

    }


    // get delivery user order by order id
    public function getDeliveryUserActiveOrderByOrderId($order_id)
    {
        $del_user_order = $this->model_delivery_user_order->getModel()::where('delivery_users_order.order_id','=',$order_id)
        ->where('delivery_user_order_status_id','=',2)->first();

        if(!$del_user_order)
            return false;

        return $del_user_order;

    }


    // get user by contact,branch id,login type id
    public function getByContactBranchLoginTypeId($contactno,$branch_id,$login_type_id)
    {
        $users  =   $this->model_user->getModel()::with(['addresses' => function($query){
            $query->orderBy('id', 'desc')->first();
        }])->where(function($query)use($contactno)
                    {
                        $query->where('cell_num', '=', $contactno)
                            ->orwhere('phone_num', '=', $contactno);
                    })
                    ->where('branch_id',$branch_id)
                    ->where('login_type_id',$login_type_id)->first();
        if(!$users)
            return false;

        return $users;
    }


    // get user by social app id
    public function getBySocialAppId($social_app_id)
    {
        $user = $this->model_user->getmodel()::where('social_app_id',$social_app_id)->first();

        if(!$user)
            return false;

        return $user;

    }

    // authenticate user
    public function getUserByEmaliPasswordBranchId($email,$pwd,$branch_id)
    {
        $user = $this->model_user->getmodel()::where('email',$email)->where('password',$pwd)->where('branch_id',$branch_id)->first();

        if(!$user)
            return false;

        return $user;

    }


}
