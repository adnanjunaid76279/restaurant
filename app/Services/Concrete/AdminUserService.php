<?php namespace App\Services\Concrete;
use App\Services\Abstractt\IAdminUserService;
use Illuminate\Database\Eloquent\Model;
use App\Repository\Repository;
use App\Models\AdminUsers;
use App\Models\Roles;
use App\Models\UserRestaurant;
use App\Models\Restaurant;
use App\Models\RestaurantBranch;
use DB;
use Illuminate\Support\Facades\Auth;

class AdminUserService implements IAdminUserService
{
    protected $model_admin_user;
    protected $model_roles;
    protected $model_user_restaurant;
    protected $model_restaurant;
    protected $model_restaurant_branch;
    public function __construct()
    {
       // set the model
       $this->model_admin_user = new Repository(new AdminUsers);
       $this->model_roles = new Repository(new Roles);
       $this->model_user_restaurant = new Repository(new UserRestaurant);
       $this->model_restaurant = new Repository(new Restaurant);
       $this->model_restaurant_branch=new Repository(new RestaurantBranch);
    }
     //datatable for admin users
    public function getAdminUsersDatatableSource($draw,$start ,$length ,$search,$login_user)
    {

        $wh = "";
        switch ($login_user['role_id']) {
            
            case '2': // restaurent owner
               
                    $wh = " WHERE user_restaurant.restaurant_id=".$login_user->userRestaurant->restaurant_id;
                
                break;
           
           case '3':// branch owner
                $wh = " WHERE user_restaurant.user_id=".$login_user['id'];
              return false;
            
                break;
           
           default:
               # code...
               break;
       }
     
        $limit = " LIMIT ".$length." OFFSET ".$start;
        $raw_qry = "";
        
        if($search)
        {
            $wh .= " AND (admin_users.first_name LIKE \"%" . $search. "%\" OR admin_users.last_name LIKE \"%" . $search. "%\" OR admin_users.login LIKE \"%" . $search. "%\" OR roles.role_name LIKE \"%" . $search. "%\" OR admin_users.cell_num LIKE \"%" . $search. "%\" ) ";
            $raw_qry =" SELECT admin_users.first_name,admin_users.id,admin_users.last_name,admin_users.login,roles.role_name,admin_users.cell_num
                        FROM admin_users
                        JOIN roles on roles.id = admin_users.role_id
                        JOIN user_restaurant on user_restaurant.user_id = admin_users.id ".$wh." Order BY admin_users.first_name asc";

            $raw_qry_count = "SELECT count(*) as total_menu FROM admin_users  JOIN user_restaurant on user_restaurant.user_id = admin_users.id JOIN roles on roles.id = admin_users.role_id".$wh;
            
                        $records_total = DB::select( DB::raw($raw_qry_count) );
            $recordsTotal = $records_total[0]->total_menu;

            // filtered or search records
            $result = DB::select( DB::raw($raw_qry) );
            $recordsFiltered  = count($result);

            // actual records return to client
            $result = DB::select( DB::raw($raw_qry.$limit) );

        }
        else
        {
           
            $raw_qry ="SELECT admin_users.first_name,admin_users.id,admin_users.last_name,admin_users.login,roles.role_name,admin_users.cell_num
                        FROM admin_users
                        JOIN roles on roles.id = admin_users.role_id
                        JOIN user_restaurant on user_restaurant.user_id = admin_users.id ".$wh." Order BY admin_users.first_name asc";

            $raw_qry_count = "SELECT count(*) as total_menu FROM admin_users  JOIN user_restaurant on user_restaurant.user_id = admin_users.id JOIN roles on roles.id = admin_users.role_id".$wh;
            $records_total = DB::select( DB::raw($raw_qry_count) );
            $recordsTotal = $records_total[0]->total_menu;

            // filtered or search records
            $result = DB::select( DB::raw($raw_qry) );
            $recordsFiltered  = count($result);

            // actual records return to client
            $result = DB::select( DB::raw($raw_qry.$limit) );

        }
        $resp = array();
        $i = 0;
         foreach($result as $item)
         {
             $resp[$i][0] = $item->first_name."".$item->last_name;

             $resp[$i][1] = $item->login;
             $resp[$i][2] = $item->role_name;
             $resp[$i][3] = $item->cell_num;

             $edit_column    = "<a class='text-success mr-2' href='".url('/edit-admin-user')."/".$item->id."'><i title='Add' class='nav-icon mr-2 i-Pen-2'></i>Edit</a>";
             $view_column    = "<a class='text-warning mr-2' href='".url('/view-admin-user')."/".$item->id."'><i title='View' class='nav-icon mr-2 i-Full-View-Window'></i>View</a>" ;
             $action_column =    $edit_column.$view_column ;

             $resp[$i][4] = $action_column;
             $i +=1;
         }

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' =>  $recordsFiltered,
            'data' =>  $resp
        );

        return json_encode($data,true);


    }
  //datatable for Permissions
  public function getAdminUsersPermissionsDatatableSource($draw,$start ,$length ,$search,$login_user)
  {

      $wh = "";
      
   
      $limit = " LIMIT ".$length." OFFSET ".$start;
      $raw_qry = "";
      
      if($search)
      {
          $wh .= " AND (permissions.title LIKE \"%" . $search. "%\" ) ";
          $raw_qry =" SELECT * FROM permissions ".$wh." Order BY id asc";

          $raw_qry_count = "SELECT count(*) as total_permission FROM permissions".$wh;
          
                      $records_total = DB::select( DB::raw($raw_qry_count) );
          $recordsTotal = $records_total[0];

          // filtered or search records
          $result = DB::select( DB::raw($raw_qry) );
          $recordsFiltered  = count($result);

          // actual records return to client
          $result = DB::select( DB::raw($raw_qry.$limit) );

      }
      else
      {
         
        $raw_qry =" SELECT * FROM permissions".$wh." Order BY id asc";

          $raw_qry_count = "SELECT count(*) as total_permission FROM permissions";
          $records_total = DB::select( DB::raw($raw_qry_count) );
          $recordsTotal = $records_total[0];

          // filtered or search records
          $result = DB::select( DB::raw($raw_qry) );
          $recordsFiltered  = count($result);

          // actual records return to client
          $result = DB::select( DB::raw($raw_qry) );

      }
      $resp = array();
      $i = 0;
       foreach($result as $item)
       {
           $resp[$i][0] = $item->title;

           $edit_column    = "<a class='text-success mr-2' href='".url('/edit-permission')."/".$item->id."'><i title='Add' class='nav-icon mr-2 i-Pen-2'></i>Edit</a>";
           $view_column    = "<a class='text-warning mr-2' href='".url('/show-permission')."/".$item->id."'><i title='View' class='nav-icon mr-2 i-Full-View-Window'></i>View</a>" ;
           $action_column =    $edit_column.$view_column ;

           $resp[$i][1] = $action_column;
           $i +=1;
       }

      $data = array(
          'draw' => $draw,
          'recordsTotal' => $recordsTotal,
          'recordsFiltered' =>  $recordsFiltered,
          'data' =>  $resp
      );

      return json_encode($data,true);


  }
  //datatable for Permissions
  public function getAdminUsersRolesDatatableSource($draw,$start ,$length ,$search,$login_user)
  {

      $wh = "";
      
   
      $limit = " LIMIT ".$length." OFFSET ".$start;
      $raw_qry = "";
      
      if($search)
      {
          $wh .= " AND (roles.role_name LIKE \"%" . $search. "%\" ) ";
          $raw_qry =" SELECT * FROM roles ".$wh." Order BY id asc";

          $raw_qry_count = "SELECT count(*) as total_roles FROM roles".$wh;
          
                      $records_total = DB::select( DB::raw($raw_qry_count) );
          $recordsTotal = $records_total[0];

          // filtered or search records
          $result = DB::select( DB::raw($raw_qry) );
          $recordsFiltered  = count($result);

          // actual records return to client
          $result = DB::select( DB::raw($raw_qry.$limit) );

      }
      else
      {
         
        $raw_qry =" SELECT * FROM roles".$wh." Order BY id asc";

          $raw_qry_count = "SELECT count(*) as total_roles FROM roles";
          $records_total = DB::select( DB::raw($raw_qry_count) );
          $recordsTotal = $records_total[0];

          // filtered or search records
          $result = DB::select( DB::raw($raw_qry) );
          $recordsFiltered  = count($result);

          // actual records return to client
          $result = DB::select( DB::raw($raw_qry) );

      }
      $resp = array();
      $i = 0;
       foreach($result as $item)
       {
           $resp[$i][0] = $item->role_name;
           foreach($item->permissions as $key => $item){
            $resp[$i][1]="<span class='badge badge-info'>$item->title</span>";
           }
           

           $edit_column    = "<a class='text-success mr-2' href='".url('/edit-role')."/".$item->id."'><i title='Add' class='nav-icon mr-2 i-Pen-2'></i>Edit</a>";
           $view_column    = "<a class='text-warning mr-2' href='".url('/show-role')."/".$item->id."'><i title='View' class='nav-icon mr-2 i-Full-View-Window'></i>View</a>" ;
           $action_column =    $edit_column.$view_column ;

           $resp[$i][2] = $action_column;
           $i +=1;
       }

      $data = array(
          'draw' => $draw,
          'recordsTotal' => $recordsTotal,
          'recordsFiltered' =>  $recordsFiltered,
          'data' =>  $resp
      );

      return json_encode($data,true);


  }
    // get user by id
    public function getById($id)
    {
        $admin_user = $this->model_admin_user->find($id);

        if(!$admin_user)
            return false;

        return $admin_user;

    }
  
    //get role by id
    public function getRoleById($id)
    {
        $role = $this->model_roles->find($id);

        if(!$role)
            return false;

        return $role;
    }

    // get all roles
    public function getRoles()
    {
        $roles = $this->model_roles->all();

        if(!$roles)
            return false;

        return $roles;

    }
  
    // get admin user roles by login user
    public function getRolesByLoginUser($login_user)
    {   
        switch ($login_user['role_id']) {
            case '1':
                $roles = $this->model_roles->getModel()->get();
            break;

            case '2':
                $roles = $this->model_roles->getModel()::where('id','>=','2')->where('id','<=','4')->get();
            break;

            case '3':
                $roles = $this->model_roles->getModel()::where('id','=','4')->get();
            break;
        }


        if(!$roles)
            return false;

        return $roles;

    }

    public function save($obj)
    {

        if(isset($obj['id']) && $obj['id'] > 0)
        {
            $obj['date_updated'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_admin_user->update($obj,$obj['id']);
        }
        else
        {
            $obj['date_created'] = date("Y-m-d H:i:s");

            $saved_obj = $this->model_admin_user->create($obj);
        }

        if(!$saved_obj)
            return false;

            return $saved_obj;
    }


    public function saveUserRestaurant($obj)
    {

        if(isset($obj['id']) && $obj['id'] > 0)
        {
            $obj['date_updated'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_user_restaurant->update($obj,$obj['id']);
        }
        else
        {
            $obj['date_created'] = date("Y-m-d H:i:s");

            $saved_obj = $this->model_user_restaurant->create($obj);
        }

        if(!$saved_obj)
            return false;

            return $saved_obj;
    }
}
