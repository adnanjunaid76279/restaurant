<?php namespace App\Services\Concrete;

use App\Repository\Repository;
use App\Services\Abstractt\IDeliveryDetailService;
use App\Models\DeliveryDetails;
use App\Models\Address;
use App\Models\TownBlock;
use App\Models\Town;
use App\Models\AddressType;

class DeliveryDetailService  implements IDeliveryDetailService
{
    
    protected $model_delivery_detail;
    protected $model_address;
    protected $model_townblock;
    protected $model_town;
    protected $model_addtype;
 

    public function __construct()
    {
       // set the model
       $this->model_delivery_detail = new Repository(new DeliveryDetails);
       $this->model_address = new Repository(new Address);
       $this->model_townblock = new Repository(new TownBlock);
       $this->model_town = new Repository(new Town);
       $this->model_addtype = new Repository(new AddressType);
       
       
    }
    
    public function getDetailByOrderIds($order_ids)
    {
        $detail = $this->model_delivery_detail->getModel()::whereIn('order_id', $order_ids)->get();
        
        if(!$detail)
            return false;
            
        return $detail; 
        
    }
    
    // save detail
    public function save($obj)
    {
        
        if(isset($obj['id']) && $obj['id'] > 0)
        {   
            $saved_obj = $this->model_delivery_detail->update($obj,$obj['id']);
        }
        else
        {   
            $saved_obj = $this->model_delivery_detail->create($obj);
        }
        
        if(!$saved_obj)
            return false;
        
            return $saved_obj;    
    }

    // save address
    public function saveAddress($obj)
    {
        
        if(isset($obj['id']) && $obj['id'] > 0)
        {   
            $saved_obj = $this->model_address->update($obj,$obj['id']);
        }
        else
        {   
            $saved_obj = $this->model_address->create($obj);
        }
        
        if(!$saved_obj)
            return false;
        
            return $saved_obj;    
    }

    // get address by user id
    public function getAddressByUserId($user_id)
    {
       
        $address = $this->model_address->getModel()::where('user_id', $user_id)->orderBy('id', 'DESC')->first();
        
        if(!$address)
            return false;
            
        return $address; 
        
    }

    // get towns by city id
    public function getTownsByCityId($city_id)
    {
        $towns = $this->model_town->getModel()::where('city_id', '=', $city_id)->orderBy('town_name', 'ASC')->get();
        
        if(!$towns)
            return false;
        
            return $towns;    
    }

    // get town blockes by town id
    public function getTownBlocksByTownId($town_id)
    {
        $blocks = $this->model_townblock->getModel()::where('town_id', '=', $town_id)->orderBy('block_name', 'ASC')->get();
        
        if(!$blocks)
            return false;
        
            return $blocks;    
    }


    // get types of address
    public function getAddressTypes()
    {
        $add_types = $this->model_addtype->getModel()->get();
        
        if(!$add_types)
            return false;
        
            return $add_types;    
    }

    // get types of address
    public function getAddressByUserIdAddTypeId($user_id,$add_type_id)
    {
        $add = $this->model_address->getModel()::where('user_id',$user_id)->where('address_type_id',$add_type_id)->orderBy('id', 'DESC')->first();
        
        if(!$add)
            return false;
        
            return $add;    
    }

    
}