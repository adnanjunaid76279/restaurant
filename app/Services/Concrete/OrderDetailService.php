<?php namespace App\Services\Concrete;

use App\Repository\Repository;
use App\Services\Abstractt\IOrderDetailService;
use App\Models\OrderDetail;
use App\Models\OrderDetailChoices;
use DB;


class OrderDetailService  implements IOrderDetailService
{
    
    protected $model_order_detail;
    protected $model_order_detail_choices;

    public function __construct()
    {
       // set the model
       $this->model_order_detail = new Repository(new OrderDetail);
       $this->model_order_detail_choices = new Repository(new OrderDetailChoices);
    }

    // order details by order idss
    public function getOrderDetailByOrderIds($order_ids)
    {
       
        $detail  =   $this->model_order_detail->getModel()::
                    select(DB::raw('order_detail.id as id,order_detail.order_id,menu.id as menu_id,menu.name as menu_name,order_detail.quantity,order_detail.price,order_detail.order_detail_status_id,order_detail.variation_name,order_detail.menu_variation_id,menu.price as menu_original_price'))
                    ->join('menu','menu.id', '=', 'order_detail.menu_id')
                    ->where(function ($query) {
                        $query->whereNull('order_detail.is_deleted')
                              ->orWhere('order_detail.is_deleted', '=', '0');
                    })
                    ->whereIn('order_detail.order_id', $order_ids)
                    ->get();

                    return $detail;
        if(!$detail)
            return false;
            
        return $detail; 
        
    }

    // order details by agent id
    public function getOrderDetailByAgentId($agent_id)
    {
       
        $detail  =   $this->model_order_detail->getModel()::
                    select(DB::raw('order_detail.id as id,order_detail.order_id,menu.id as menu_id,menu.name as menu_name,order_detail.quantity,order_detail.price'))
                    ->join('menu','menu.id', '=', 'order_detail.menu_id')
                    ->join('agent_kitchen','agent_kitchen.kitchen_id','=','menu.kitchen_id')
                    ->where(function ($query) {
                        $query->whereNull('order_detail.is_deleted')
                              ->orWhere('order_detail.is_deleted', '=', '0');
                    })
                    ->whereIn('order_detail.order_id', $order_ids)
                    ->get();

                    return $detail;
        if(!$detail)
            return false;
            
        return $detail; 
        
    }

    // save order detail
    public function save($obj)
    {
        
        if(isset($obj['id']) && $obj['id'] > 0)
        {   
            $obj['date_updated'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_order_detail->update($obj,$obj['id']);
        }
        else
        {   
            $obj['date_created'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_order_detail->create($obj);
        }
        
        if(!$saved_obj)
            return false;
        
            return $saved_obj;    
    }

    // save order detail choices
    public function saveDetailChoices($obj)
    {
        
        if(isset($obj['id']) && $obj['id'] > 0)
        {   
            //$obj['date_updated'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_order_detail_choices->update($obj,$obj['id']);
        }
        else
        {   
        // $obj['date_created'] = date("Y-m-d H:i:s");
            $saved_obj = $this->model_order_detail_choices->create($obj);
        }
        
        if(!$saved_obj)
            return false;
        
            return $saved_obj;    
    } 

    // change order status of order detail
    public function changeOrderDetailStatus($ord_detail_id,$status_id)
    {
    
        $order_detail = $this->model_order_detail->find($ord_detail_id);
        
        if(!$order_detail)
            return false;
        
        $order_detail->order_detail_status_id = $status_id;
        
        $this->save($order_detail->toArray());

        return $order_detail;     
    }
    
   
    
}