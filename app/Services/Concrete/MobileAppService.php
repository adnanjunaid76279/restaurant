<?php namespace App\Services\Concrete;

use App\Services\Abstractt\IMobileAppService;


class MobileAppService  implements IMobileAppService
{
    
    // protected $model_agent_user;
        
    public function __construct()
    {
       // set the model
       //$this->model_agent_user = new Repository(new AgentUsers);
    }
    
    // get main data
    public function getMainData($user_id=null,$branch_id)
    {   
        $restaurant_service = new RestaurantService();
        $menu_category_service = new MenuCategoryService();

        $branch = $restaurant_service->getBranchById($branch_id);
        $branch_rating = $restaurant_service->getBranchRatingsByBranchId($branch_id);
        //$menu_categories = $menu_category_service->getCategoriesWithMenuByBranchIdUserId($branch_id,$user_id);
        $top_menu_rated = $menu_category_service->getTopMenuRatingByBranchId($branch_id);
        $lst_menu_rating = $menu_category_service->getMenuRatingsByBranchId($branch_id);
        $lst_menu_fav = $menu_category_service->getFavMenusByUserIdBranchId($user_id, $branch_id);
        $list_top_menu = [];
        
        foreach ($top_menu_rated as $menu) 
        {
            $rating =  (object)[
                'ratedby'       =>  $lst_menu_rating->where('menu_id','=',$menu->id)->count(),
                'user_rating'   =>  $lst_menu_rating->where('menu_id','=',$menu->id)->where('user_id','=',$user_id)->pluck('rating')->first() ?? 0,
                'rating'        =>  $lst_menu_rating->where('menu_id','=',$menu->id)->avg('rating')??0
                
            ];

            $list_top_menu[]=[
                "id"            => $menu->id,
                "name"          => $menu->name,
                "price"         => $menu->price,
                "image"         => isset($menu->image)?$menu->image:$branch->logo,
                "description"   => $menu->description,
                "ingridient"   => $menu->ingridient,
                "isFav"         => $lst_menu_fav->where('menu_id','=',$menu->id)->first() ?? false,
                "rating"        => $rating,
                "addOn"           =>[]
            ];
        }
        if($branch)
        {
            $main_data = [
                "id"                => $branch->id,
                "name"              => $branch->name,
                "address"           => $branch->address,
                "is_delivery"       => $branch->is_delivery,
                "is_dinein"         => $branch->is_dinein,
                "is_freedelivery"   => $branch->is_freedelivery,
                "order_taking"      => $branch->order_taking,
                "is_takeaway"       => $branch->is_takeaway,
                "tax_include"       => $branch->tax_include,
                "tax_percent"       => $branch->tax_percent,
                "order_taking"      => $branch->order_taking??$branch->order_taking,
                "logo"              => $branch->logo??"findurmeal_logo_rv.png",
                "latitude"          => $branch->latitude,
                "longitude"         => $branch->longitude,
                "town_id"           => $branch->town_id,
                "minimum_order_value" => $branch->minimum_order_value,
                "mobile_number"     => $branch->mobile_number,
                "mobile_number2"    => $branch->mobile_number2,
                "mobile_number3"    => $branch->mobile_number3,
                "phone_number"      => $branch->phone_number,
                "phone_number2"     => $branch->mobile_number2,
                "phone_number3"     => $branch->mobile_number3,
                "contact_number"    => $branch->contact_number,
                "contact_email"     => $branch->contact_email,
                "name"              => $branch->name,
                "delivery_fee"      => $branch->delivery_fee,
                "currency"          => $branch->currency,
                "restaurant_open"   => $this->isBetween(gmdate("Y-m-d H:i:s"), $branch->open_time, $branch->close_time),
                "open_time"         => date('h:i a',strtotime($branch->open_time)),
                "close_time"        => date('h:i a',strtotime($branch->close_time))
            ];
             
       
            $main_data["topDishList"] = $list_top_menu;

        }

        return $main_data ;
    }

    public function isBetween($utc_time, $start_time, $end_time)
    {   
        $time_of_day = date("H:i:s",strtotime($utc_time));
        $start_time  = date("H:i:s",strtotime($start_time));
        $end_time    = date("H:i:s",strtotime($end_time));


        if ($time_of_day  == $start_time) return true;
        if ($time_of_day == $end_time) return true;

        if ($start_time <= $end_time)
            return ($time_of_day >= $start_time && $time_of_day <= $end_time);
        else
            return !($time_of_day >= $end_time && $time_of_day <= $start_time);
    }
    
}