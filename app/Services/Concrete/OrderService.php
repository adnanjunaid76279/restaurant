<?php namespace App\Services\Concrete;

use Illuminate\Support\Facades\Http;

use App\Repository\Repository;

use App\Services\Abstractt\IOrderService;

use App\Models\Order;

use App\Models\OrderStatus;

use App\Models\OrderDetail;

use DB;


class OrderService  implements IOrderService

{



    protected $model_order;

    protected $model_order_status;

    protected $model_order_detail;





    public function __construct()

    {

       // set the model

       $this->model_order = new Repository(new Order);

       $this->model_order_status = new Repository(new OrderStatus);

       $this->model_order_detail = new Repository(new OrderDetail);





    }



    //get orders datatable

    public function getOrdersDatatableSource($draw,$start ,$length ,$search,$start_date,$end_date,$order_status,$login_user)

    {

        $order_status_list=$this->model_order_status->all();



        $wh= "";

        switch ($login_user['role_id']) {



            case '2': // restaurent owner



                $wh.= " WHERE restaurant_branch.restaurant_id=".$login_user->userRestaurant->restaurant_id;



                break;



           case '3':// branch owner



                $wh.= " WHERE restaurant_branch.id=".$login_user->userRestaurant->branch_id;

                break;



           default:

               # code...

               break;

       }



        if($order_status != 0)

        {

            $wh.=" AND `order`.`status_id` ="."'$order_status'";

        }



        if($start_date != null && $end_date != null )

        {

            $wh .= " AND Date(`order`.`order_date`) BETWEEN "." Date('$start_date') "." AND " ." Date('$end_date')";

        }



        $limit = " LIMIT ".$length." OFFSET ".$start;

        $raw_qry = "";



        if($search)

        {

            $wh .= " AND (users.first_name LIKE \"%" . $search. "%\" OR users.last_name LIKE \"%" . $search. "%\" OR `order`.`id` LIKE \"%" . $search. "%\" OR `order`.`sub_total` LIKE \"%" . $search. "%\" OR `order`.`order_date` LIKE \"%" . $search. "%\" OR restaurant_branch.name LIKE \"%" . $search. "%\" OR order_status.name LIKE \"%" . $search. "%\" ) ";



            $raw_qry = "SELECT users.first_name,restaurant_branch.name,users.last_name,`order`.`dailyorder_id` as order_no,`order`.`sub_total` as amount,`order`.`order_date` as order_date,order_status.name as status ,`order`.`id` as orderid ,`order`.`status_id` as order_status  FROM users

                    JOIN `order` ON `order`.`user_id`=users.id

                    JOIN order_status on order_status.id=`order`.`status_id`

                    JOIN restaurant_branch ON restaurant_branch.id=`order`.`restaurant_branch_id` ".$wh." Order BY `order`.`id` asc";



           $raw_qry_count = "SELECT count(*) as totall FROM users

                            JOIN `order` ON `order`.`user_id`=users.id

                            JOIN order_status on order_status.id=`order`.`status_id`

                            JOIN restaurant_branch ON restaurant_branch.id=`order`.`restaurant_branch_id`"

                            .$wh;



            $records_total = DB::select( DB::raw($raw_qry_count) );

            $recordsTotal = $records_total[0]->totall;



            // filtered or search records

            $result = DB::select( DB::raw($raw_qry) );

            $recordsFiltered  = count($result);



            // actual records return to client

            $result = DB::select( DB::raw($raw_qry.$limit) );



        }

        else

        {

            $raw_qry = "SELECT  users.first_name,users.last_name,`order`.`dailyorder_id` as order_no,restaurant_branch.name as name,`order`.`sub_total` as amount,`order`.`order_date` as order_date,order_status.name as status ,`order`.`id` as orderid,`order`.`status_id` as order_status FROM users

            JOIN `order` ON `order`.`user_id`=users.id



            JOIN order_status on order_status.id=`order`.`status_id`

            JOIN restaurant_branch ON restaurant_branch.id=`order`.`restaurant_branch_id`".$wh." Order BY `order`.`id` asc";



            $raw_qry_count = "SELECT count(*) as totall FROM users JOIN `order` ON `order`.`user_id`=users.id



            JOIN order_status on order_status.id=`order`.`status_id`

            JOIN restaurant_branch ON restaurant_branch.id=`order`.`restaurant_branch_id`".$wh;



            $records_total = DB::select( DB::raw($raw_qry_count) );

            $recordsTotal = $records_total[0]->totall;



            // filtered or search records

            $result = DB::select( DB::raw($raw_qry) );



            $recordsFiltered  = count($result);



            // actual records return to client

            $result = DB::select( DB::raw($raw_qry.$limit) );



        }

        $resp = array();

        $i = 0;









        foreach($result as $item)

        {

            $resp[$i][0] = $item->first_name." ".$item->last_name;

            $resp[$i][1] = $item->order_no;

            $resp[$i][2] = $item->amount;

            $resp[$i][3] = $item->order_date;

            $resp[$i][4] = $item->name;

            $resp[$i][5] = $item->status;

            $order_id = $item->orderid;

            // $disable_order = $item->order_status == 2 ? "disabled":"";   // if order is delivered make its status dropdown disabled

            $disable_order = "";


            $order_status_string = " <select $disable_order name='order_status_list' class='form-control order_status_list'>";



            $order_details = "<button value='$order_id' data-toggle='modal' data-target='#exampleModal' class='btn btn-primary order_detail'>Details</button>";

            foreach($order_status_list as $items)

            {

                $chk  = $items->id == $item->order_status ? 'selected' : '';

                $order_status_string.=" <option  $chk  value=".$items["id"].",".$order_id."   >".$items["name"]."</option>";

            }



            $resp[$i][6] = $order_status_string;

            $resp[$i][7] = $order_details;

            $i +=1;

         }



        $data = array(

            'draw' => $draw,

            'recordsTotal' => $recordsTotal,

            'recordsFiltered' =>  $recordsFiltered,

            'data' =>  $resp

        );



        return json_encode($data,true);



    }



    // save order

    public function save($obj)
    {

        if(isset($obj['id']) && $obj['id'] > 0)
        {
            $obj['date_updated'] = date("Y-m-d H:i:s");

            $saved_obj = $this->model_order->update($obj,$obj['id']);

        }
        else
        {

            $obj['date_created'] = date("Y-m-d H:i:s");

            $saved_obj = $this->model_order->create($obj);

        }



        if(!$saved_obj)

            return false;



            return $saved_obj;

    }



    public function setOrerStatusTime($obj)

    {



        switch($obj['status_id'])

        {

            case '2':  // delivered

                $obj['delivered_time'] = date("Y-m-d H:i:s");



                if(!isset($obj['recieved_time']))

                {

                    $obj['recieved_time'] = $obj['delivered_time'];

                }



                if(!isset($obj['preparing_time']))

                {

                    $obj['preparing_time'] = $obj['delivered_time'];

                }



                if(!isset($obj['ready_time']))

                {

                    $obj['ready_time'] = $obj['delivered_time'];

                }



                if(!isset($obj['dispatch_time']))

                {

                    $obj['dispatch_time'] = $obj['delivered_time'];

                }



            break;



            case '3':  // Recieved



                $obj['recieved_time'] = date("Y-m-d H:i:s");



                break;



            case '4':  // Preparing



                $obj['preparing_time'] = date("Y-m-d H:i:s");



                if(!isset($obj['recieved_time']))

                {

                    $obj['recieved_time'] = $obj['preparing_time'];

                }



                break;



            case '7':  // Ready

                $obj['ready_time'] = date("Y-m-d H:i:s");



                if(!isset($obj['recieved_time']))

                {

                    $obj['recieved_time'] = $obj['ready_time'];

                }



                if(!isset($obj['preparing_time']))

                {

                    $obj['preparing_time'] = $obj['ready_time'];

                }



                break;



            case '6':  // Out for delivery

                $obj['dispatch_time'] = date("Y-m-d H:i:s");



                if(!isset($obj['recieved_time']))

                {

                    $obj['recieved_time'] = $obj['dispatch_time'];

                }



                if(!isset($obj['preparing_time']))

                {

                    $obj['preparing_time'] = $obj['dispatch_time'];

                }



                if(!isset($obj['ready_time']))

                {

                    $obj['ready_time'] = $obj['dispatch_time'];

                }



                break;



            case '8':  // Cancel

                $obj['cancel_time'] = date("Y-m-d H:i:s");

            break;



        }



        return $obj;

    }



    // get order by id

    public function getById($id)

    {

        $order = $this->model_order->find($id);



        if(!$order)

            return false;



            return $order;

    }



    // get order by online_pay_order_id

    public function getByOnlinePayOrderId($id)

    {

        $order = $this->model_order->getModel()::where('online_pay_order_id',$id)->first();



        if(!$order)

            return false;



        return $order;

    }

    // change order status
    public function changeOrderStatus($order_id,$status_id)
    {

        $order = $this->model_order->find($order_id);

        $user = $order->user;

        $branch = $order->restaurantBranch;

     
        if(!$order)

            return false;

        if(isset($user->device_id) && isset($branch->firebase_server_key) && isset($branch->firebase_sender_id))
        {

            $device_id = $user->device_id;

            $server_key = $branch->firebase_server_key;

            $sender_id = $branch->firebase_sender_id;

            //$message = "Order Status";

            $title = $branch->name;

            $message = $this->getOrderDetailString($order,$branch,$status_id);

            $img_url = "https://admin.cherryberryrms.com/public/Temp/1219/images/".$branch->logo;

            //=============== alert notifi ation


            $firebase_service = new FirebaseService(); 

            $firebase_service->sendMessageByDeviceId($device_id,$server_key,$sender_id,$message,$title,$img_url); // ($device_id,$server_key,$sender_id,$message,$title,$data)

            

            //==================================================

        }



        //=============== order status change

        $order->status_id = $status_id;

        $order = $this->setOrerStatusTime($order->toArray());

        $this->save($order);

        //==================================================



       



        return $order;

    }



    // get order status

    public function getOrderStatus()

    {

        $status = $this->model_order_status->getModel()->orderby('priority')->get();



        if(!$status)

            return false;



            return $status;

    }



    // get sale sum by date

    public function getSaleSumByDate($datee,$branch_id,$login_user=null)
      {

        $wh = "";

        switch ($login_user->role_id)
        {

            case '1':

                if($branch_id != null){

                    $wh = "AND restaurant_branch.id = ".$branch_id ;



                }else{

                    $wh = " ";
                }

                break;

            case '2':

                if($branch_id != null){

                    $wh = " AND restaurant_branch.id = ".$branch_id ;

                }else{

                    $wh = " AND restaurant_branch.restaurant_id = ".$login_user->userRestaurant->restaurant_id ;

                }
                break;

            case '3':
            case '4':

                $wh = " AND order.restaurant_branch_id = ".$login_user->userRestaurant->branch_id ;
                break;

            default:

                $wh = " ";

        }



        $sale_sum = "SELECT SUM(IFNULL(order.total , 0)) as total FROM `order`

                        JOIN restaurant_branch on `order`.`restaurant_branch_id` = `restaurant_branch`.`id`

                        WHERE DATE(`order`.`order_date`) >= '".$datee."'

                        AND `order`.`status_id` <> 8 ".$wh." ";





        $data = DB::select(DB::raw( $sale_sum));



        if($data[0]->total )

            return $data[0]->total;



        return false;



    }



    // get sale sum by date Range

    public function getSaleSumByDateRange($start_date,$end_date,$branch_id,$login_user=null)

    {



        $wh = "";



        switch ($login_user->role_id)

        {

            case '1':

                if($branch_id != null){

                    $wh = "AND restaurant_branch.id = ".$branch_id ;



                }else{

                    $wh = " ";

                }



                break;



            case '2':

                if($branch_id != null){

                    $wh = " AND restaurant_branch.id = ".$branch_id ;

                }else{

                    $wh = " AND restaurant_branch.restaurant_id = ".$login_user->userRestaurant->restaurant_id ;

                }



                break;

            case '3':
            case '4':

                $wh = " AND order.restaurant_branch_id = ".$login_user->userRestaurant->branch_id ;

                break;



            default:

                $wh = " ";

        }



            $sale_sum = "SELECT SUM(IFNULL(order.total , 0)) as total FROM `order`

                        JOIN restaurant_branch on `order`.`restaurant_branch_id` = `restaurant_branch`.`id`

                        WHERE DATE(`order`.`order_date`) >= '".$start_date."'

                        AND DATE(`order`.`order_date`) <= '".$end_date."'

                        AND `order`.`status_id` <> 8 ".$wh." ";



        $data = DB::select(DB::raw( $sale_sum));



        if($data[0]->total )

        return $data[0]->total;



      return false;

    }



    // get sale count by date

    public function getOrderCountByDate($datee,$branch_id,$login_user=null)

    {

        $wh = "";



        switch ($login_user->role_id)

        {

            case '1':

                if($branch_id != null){

                    $wh = "AND restaurant_branch.id = ".$branch_id ;



                }else{

                    $wh = " ";

                }



                break;



            case '2':

                if($branch_id != null){

                    $wh = " AND restaurant_branch.id = ".$branch_id ;

                }else{

                    $wh = " AND restaurant_branch.restaurant_id = ".$login_user->userRestaurant->restaurant_id ;

                }



                break;

            case '3':
            case '4':

                $wh = " AND order.restaurant_branch_id = ".$login_user->userRestaurant->branch_id ;

                break;



            default:

                $wh = " ";

        }





        $order = "select count(*) as total

                 from `order`

                 inner join `restaurant_branch` on `order`.`restaurant_branch_id` = `restaurant_branch`.`id`

                 where date(`order`.`order_date`) = '".$datee."' and  `order`.`status_id` <> 8 ".$wh." ";



        $data = DB::select(DB::raw($order));



        $order_count= $data[0]->total;

        if(!$order_count)

            return false;





        return $order_count;





    }



    // get sale count by date Range

    public function getOrderCountByDateRange($start_date,$end_date,$branch_id,$login_user=null)

    {



        $wh = "";



        switch ($login_user->role_id)

        {

            case '1':

                if($branch_id != null){

                    $wh = "AND restaurant_branch.id = ".$branch_id ;



                }else{

                    $wh = " ";

                }



                break;



            case '2':

                if($branch_id != null){

                    $wh = " AND restaurant_branch.id = ".$branch_id ;

                }else{

                    $wh = " AND restaurant_branch.restaurant_id = ".$login_user->userRestaurant->restaurant_id ;

                }



                break;

            case '3':
            case '4':

                $wh = " AND order.restaurant_branch_id = ".$login_user->userRestaurant->branch_id ;

                break;



            default:

                $wh = " ";

        }



        $order = "select count(*) as total

                 from `order`

                 inner join `restaurant_branch` on `order`.`restaurant_branch_id` = `restaurant_branch`.`id`

                 where date(`order`.`order_date`) >= '".$start_date."' and date(`order`.`order_date`) <= '".$end_date."' and  `order`.`status_id` <> 8 ".$wh." ";



        $data = DB::select(DB::raw($order));



        $order_count= $data[0]->total;

        if(!$order_count)

            return false;





        return $order_count;



    }



     // get order count by resource and date

    public function getOrderCountByResourceAndDateRange($start_date,$end_date,$branch_id, $login_user=null)

    {

        $wh = "";



        switch ($login_user->role_id)

        {

            case '1':



                if($branch_id != null)

                {

                    $where_conditions = [

                        ['order.restaurant_branch_id','=',$branch_id]

                    ];

                }

                else

                {

                    $where_conditions = [];

                }



                break;



            case '2':



                if($branch_id != null)

                {

                    $where_conditions = [

                        ['order.restaurant_branch_id','=',$branch_id]

                    ];

                }

                else

                {

                    $where_conditions = [

                        ['restaurant_branch.restaurant_id', '=', $login_user->userRestaurant->restaurant_id]

                    ];

                }



                break;

            case '3':
            case '4':

                if($branch_id != null)

                {

                    $where_conditions = [

                        ['order.restaurant_branch_id','=',$branch_id]

                    ];

                }

                else

                {

                    $where_conditions = [

                        ['restaurant_branch.restaurant_id', '=', $login_user->userRestaurant->branch_id]

                    ];

                }

                break;



            default:

                $where_conditions = [];

        }



        $order_count = $this->model_order->getModel()::select('order.order_resource',\DB::raw('COUNT(order.id) as count'))

            ->whereDate('order.order_date','>=',$start_date)

            ->whereDate('order.order_date','<=',$end_date)

            ->Join('restaurant_branch', 'order.restaurant_branch_id', '=', 'restaurant_branch.id')

            ->where('order.status_id','<>',8)

            ->where($where_conditions)

            ->groupby('order.order_resource')

            ->get();



        if(!$order_count)

            return false;





        return $order_count;





    }





    // get order sum by order type and date

    public function getOrderSumByTypeAndDateRange($start_date,$end_date,$login_user=null)

    {



        switch ($login_user->role_id)

        {

            case '2':



                $order_sum = $this->model_order->getModel()::JOIN('order_type','order_type.id','=','order.order_type_id')

                        ->select('order_type.type',\DB::raw('Sum(order.total) as sum_total'))

                        ->whereDate('order.order_date','>=',$start_date)

                        ->whereDate('order.order_date','<=',$end_date)

                        ->Join('restaurant_branch', 'order.restaurant_branch_id', '=', 'restaurant_branch.id')

                        ->where('restaurant_branch.restaurant_id','=',$login_user->userRestaurant->restaurant_id)

                        ->where('order.status_id','<>',8)

                        ->groupby('order_type.type')

                        ->orderBy('order_type.id')

                        ->get();



                break;

            case '3':

                $order_sum = $this->model_order->getModel()::JOIN('order_type','order_type.id','=','order.order_type_id')

                            ->select('order_type.type',\DB::raw('Sum(order.total) as sum_total'))

                            ->whereDate('order.order_date','>=',$start_date)

                            ->whereDate('order.order_date','<=',$end_date)

                            ->where('order.restaurant_branch_id','=',$login_user->userRestaurant->branch_id)

                            ->where('order.status_id','<>',8)

                            ->groupby('order_type.type')

                            ->orderBy('order_type.id')

                            ->get();



                break;

            default:



                $order_sum = $this->model_order->getModel()::JOIN('order_type','order_type.id','=','order.order_type_id')

                            ->select('order_type.type',\DB::raw('Sum(order.total) as sum_total'))

                            ->whereDate('order.order_date','>=',$start_date)

                            ->whereDate('order.order_date','<=',$end_date)

                            ->where('order.status_id','<>',8)

                            ->groupby('order_type.type')

                            ->orderBy('order_type.id')

                            ->get();

        }



        if(!$order_sum)

            return false;



            return $order_sum;



    }



    //get users order with detail by restaurant id

    public function getUserOrdersWithDetailByRestID($user_id,$restaurant_id)

    {



        $orders  =   $this->model_order->getModel()::join('restaurant_branch','order.restaurant_branch_id', '=', 'restaurant_branch.id')

        ->select(['order.id as orderid','order.*', 'restaurant_branch.*'])

        ->where('order.user_id','=',$user_id)

                    ->where('restaurant_branch.restaurant_id','=',$restaurant_id)

                    ->where('order.status_id','<>',8)

                    ->orderBy('orderid', 'ASC')

                    ->limit(15)

                    ->get();



        $order_ids = $orders->pluck('orderid')->toArray();



        // get order detail

        $detail_service = new OrderDetailService();

        $order_details  = $detail_service->getOrderDetailByOrderIds($order_ids);



        // get delivery detail

        $delivery_detail_service = new DeliveryDetailService();

        $delivery_details = $delivery_detail_service->getDetailByOrderIds($order_ids);



        $response = [];



        foreach($orders as $order)

        {

            $details = [];

            $order_detail = $order_details->where('order_id','=',$order->orderid);

            $delivery_details   =  $delivery_details->where('order_id','=',$order->orderid)->first();

            foreach($order_detail as $detail)

            {

                $details[] = [

                    'id' => $detail->id,

                    'menu_name' => $detail->menu_name ,

                    'price' => $detail->price ,

                    'quantity' => $detail->quantity,

                    'delivery_details'  =>  $delivery_details

                ];

            }



            $response[] = [



                'id'                    =>  $order->orderid,

                'user_id'               =>  $order->user_id,

                'restaurant_branch_id'  =>  $order->restaurant_branch_id,

                'tracking_id'           =>  $order->tracking_id,

                'order_date'            =>  $order->order_date,

                'status_id'             =>  $order->status_id,

                'total'                 =>  $order->total,

                'sub_total'             =>  $order->sub_total,

                "OrderDetail"           =>  $details



            ];

        }



        if(!$response)

            return false;



        return $response;



    }



     //get users order with detail by branch id

    public function getUserOrdersWithDetailByBranchId($user_id,$branch_id)

    {



         $orders  =   $this->model_order->getModel()::join('restaurant_branch','order.restaurant_branch_id', '=', 'restaurant_branch.id')

         ->select(['order.id as orderid','order.dailyorder_id as dailyorderid','order.*', 'restaurant_branch.*'])

         ->where('order.user_id','=',$user_id)

                     ->where('restaurant_branch.id','=',$branch_id)

                     ->where('order.status_id','<>',8)

                     ->orderBy('orderid', 'DESC')

                     ->limit(50)

                     ->get();



         $order_ids = $orders->pluck('orderid')->toArray();



         // get order detail

         $detail_service = new OrderDetailService();

         $order_details  = $detail_service->getOrderDetailByOrderIds($order_ids);



         // get delivery detail

         $delivery_detail_service = new DeliveryDetailService();

         $delivery_details = $delivery_detail_service->getDetailByOrderIds($order_ids);



         $response = [];



         foreach($orders as $order)

         {

             $details = [];

             $order_detail = $order_details->where('order_id','=',$order->orderid);

             $delivery_details   =  $delivery_details->where('order_id','=',$order->orderid)->first();

             foreach($order_detail as $detail)

             {

                 $details[] = [

                     'id' => $detail->id,

                     'menuName' => $detail->menu_name ,

                     'price' => $detail->price ,

                     'quantity' => $detail->quantity,

                     'delivery_details'  =>  $delivery_details

                 ];

             }



             $response[] = [



                 'id'                    =>  $order->orderid,

                 'user_id'               =>  $order->user_id,

                 'restaurant_branch_id'  =>  $order->restaurant_branch_id,

                 'tracking_id'           =>  $order->tracking_id,

                 'dailyOrderId'          =>  $order->dailyorderid,

                 'orderTypeId'           =>  $order->order_type_id,

                 'orderDate'             =>  $order->order_date,

                 'status_id'             =>  $order->status_id,

                 'status'                =>  $order->status->name,

                 'total'                 =>  $order->total,

                 'sub_total'             =>  $order->sub_total,

                 'tableNo'               =>  $order->table_no,

                 'taxAmount'             =>  $order->tax_amount??0,

                 'taxPercent'            =>  $order->tax_percent??0,

                 'discountPer'           =>  $order->discount_per??0,

                 'discountAmount'        =>  $order->discount_amount??0,

                 'amountReturn'          =>  $order->amount_return??0,

                 'amountPaid'            =>  $order->amount_paid??0,

                 "orderDetail"           =>  $details



             ];

         }



         if(!$response)

             return false;



         return $response;



    }



    //get users order with detail by branch id for mobile app

    public function getMobileUserOrdersWithDetailByBranchId($user_id,$branch_id)

    {



        $orders  =   $this->model_order->getModel()::join('restaurant_branch','order.restaurant_branch_id', '=', 'restaurant_branch.id')

        ->select(['order.id as orderid','order.dailyorder_id as dailyorderno','order.*', 'restaurant_branch.*'])

        ->where('order.user_id','=',$user_id)

                    ->where('restaurant_branch.id','=',$branch_id)

                    ->orderBy('orderid', 'DESC')

                    ->limit(50)

                    ->get();



        $order_ids = $orders->pluck('orderid')->toArray();



        // get order detail

        $detail_service = new OrderDetailService();

        $order_details  = $detail_service->getOrderDetailByOrderIds($order_ids);



        // get delivery detail

        $delivery_detail_service = new DeliveryDetailService();

        $delivery_details = $delivery_detail_service->getDetailByOrderIds($order_ids);



        $response = [];



        foreach($orders as $order)

        {

            $details = [];

            $order_detail = $order_details->where('order_id','=',$order->orderid);



            $obj_del_details   =  $delivery_details?$delivery_details->where('order_id','=',$order->orderid)->first():[];



            foreach($order_detail as $detail)

            {

                $details[] = [

                    'id' => $detail->id,

                    'menuName' => $detail->menu_name ,

                    'price' => $detail->price ,

                    'quantity' => $detail->quantity

                ];

            }



            if(isset($obj_del_details)){

                $del_detail = [



                    "userId"        =>  $obj_del_details->user_id,

                    "firstName"     =>  $obj_del_details->user->first_name,

                    "lastName"      =>  $obj_del_details->user->last_name,

                    "contact"       =>  $obj_del_details->mobile_num,

                    "gender"        =>  $obj_del_details->gender,

                    "email"         =>  $obj_del_details->email,

                    "dateBirth"     =>  $obj_del_details->date_birth,

                    "townId"        =>  $obj_del_details->town_id,

                    "blockId"       =>  $obj_del_details->town_block_id,

                    "address"       =>  $obj_del_details->address,

                    "latitude"      =>  $obj_del_details->latitude,

                    "longitude"     =>  $obj_del_details->longitude,

                    "instructions"  =>  $obj_del_details->instructions



                ];

            }

            else

                $del_detail = [];



            $response[] = [



                'id'                =>  $order->orderid,

                'userId'            =>  $order->user_id,

                'branchId'          =>  $order->restaurant_branch_id,

                'tracking_id'       =>  $order->tracking_id,

                'orderDate'         =>  $order->order_date,

                'statusId'          =>  $order->status_id,

                'total'             =>  $order->total,

                'subTotal'          =>  $order->sub_total,

                'dailyOrderId'      =>  $order->dailyorderno,

                'orderTypeId'       =>  $order->order_type_id,

                "amountPaid"        => $order->amount_paid,

                "amountReturn"      => $order->amount_return,

                "discountPer"       => $order->discount_per,

                "discountAmount"    => $order->discount_amount,

                "deliveryFee"       => $order->delivery_charge,

                "serviceFee"        => $order->service_charge,

                'status'            => $order->status->name,

                'tax_include'       => $order->tax_include??true,

                'taxPercent'        => $order->tax_percent,

                'taxPercent'        => $order->tax_percent??true,

                'taxAmount'         => $order->tax_amount,

                'tableNo'           => $order->table_no,

                'orderDetail'       => $details,

                'delivery_details'  => $del_detail



            ];

        }



        if(!$response)

            return false;



        return $response;



    }



    // get orders by branch id and optional date filter

    public function getOrdersByBranchId($branch_id,$from_date=null,$end_date=null,  $from=0, $limit=300, $order_by="order.id", $sorting="DESC")
    {
        if($from_date && $end_date)
        {
            $where_conditions = [

                ['order.order_date', '>=', $from_date." 00:00:00"],

                ['order.order_date', '<=', $end_date." 23:59:59"],

                ['order.restaurant_branch_id','=',$branch_id]

            ];

        }
        else
        {
            $where_conditions = [

                ['restaurant_branch.id','=',$branch_id]

            ];

        }

        $orders  =   $this->model_order->with([
                    'orderDetails'  => function($query){
                                    $query->whereNull('order_detail.is_deleted')
                                        ->orWhere('order_detail.is_deleted', '=', '0');
                                    },
                    'orderDetails.menu',
                    'orderDetails.menuVariation',
                    'orderDetails.orderDetailChoice',
                    'deliveryDetails',
                    'restaurantBranch',
                    'user',
                    'status'
                    ])
                    ->where('order.status_id','<>',8)
                    ->where($where_conditions)
                    ->orderBy($order_by, $sorting)
                    // ->skip($from)
                    ->take($limit)
                    ->get();

        // get agent users

        $agent_user_service    = new AgentUserService();
        $agent_users    = $agent_user_service->getByBranchId($branch_id);

        $response = [];

        foreach($orders as $order)
        {
            $details = [];

            foreach($order->orderDetails as $detail)
            {
                $details[] = [

                    'id'        => $detail->id,
                    'menu_id'   => $detail->menu_id,
                    'menu_name' => $detail->menu_name ,
                    'price'     => $detail->price ,
                    'menu_original_price'  => $detail->menu->price ,
                    'quantity'  => $detail->quantity,
                    'order_detail_status_id'=> $detail->order_detail_status_id,
                    'menu_variation_id' => $detail->menu_variation_id,
                    'variation_name'    => $detail->variation_name,
                    'variation_price'   => isset($detail->menuVariation)?$detail->menuVariation->price:0,
                    'choices'           =>  $detail->orderDetailChoice,
                    'delivery_details'  =>  $order->deliveryDetails
                ];
            }

            $response[] = [

                'id'                    =>  $order->id,
                'user_id'               =>  $order->user_id,
                'user_name'             =>  isset($order->user->last_name)?$order->user->first_name." ".$order->user->last_name:$order->user->first_name,
                'restaurant_branch_id'  =>  $order->restaurant_branch_id,
                'tracking_id'           =>  $order->tracking_id,
                'order_date'            =>  $order->order_date,
                'status_id'             =>  $order->status_id,
                'status'                =>  $order->status->name,
                'total'                 =>  $order->total,
                'sub_total'             =>  $order->sub_total,
                'order_type_id'         =>  $order->order_type_id,
                "createdby_id"          =>  $order->createdby_id,
                "dailyorder_id"         =>  $order->dailyorder_id,
                "table_no"              =>  $order->table_no,
                "order_resource"        =>  $order->order_resource,
                "amount_paid"           =>  $order->amount_paid,
                "delivery_charge"       =>  $order->delivery_charge,
                "service_charge"        =>  $order->service_charge,
                "tax_percent"           =>  $order->tax_percent,
                "tax_amount"            =>  $order->tax_amount,
                "tax_include"           =>  $order->tax_include,
                "tax_on_original_amt"   =>  $order->tax_on_original_amt,
                "discount_per"          =>  $order->discount_per,
                "discount_amount"       =>  $order->discount_amount,
                "amount_return"         =>  $order->amount_return,
                "order_edit"            =>  $order->order_edit,
                "order_taker"           =>  $order->order_taker,
                "useragent_id"          =>  $order->useragent_id,
                "num_persons"           =>  $order->num_persons,
                "login_user"            =>  $agent_users->where('id','=',$order->createdby_id)->pluck('username')->first() ?? null,
                "login_user_name"       =>  $agent_users->where('id','=',$order->createdby_id)->pluck('name')->first() ?? null,
                "restaurant_branch"     =>  (object)[

                        'address'       =>  $order->restaurantBranch->address,
                        'name'          =>  $order->restaurantBranch->name,
                        'logo'          =>  $order->restaurantBranch->logo,
                        'contact_number'=>  $order->restaurantBranch->contact_number,
                        'strn_number'   =>  $order->restaurantBranch->strn_number,
                        'ntn_number'    =>  $order->restaurantBranch->ntn_number
                ],
                "OrderDetail"           =>  $details



            ];

        }

        if(!$response)
            return false;

        return $response;

    }



    // get orders by branch id and and agentid optional date filter

    public function getOrdersByBranchIdAgentId($branch_id,$agent_id,$from_date=null,$end_date=null)
    {
        if($from_date && $end_date)
        {
            $where_conditions = [

                ['order.order_date', '>=', $from_date." 00:00:00"],

                ['order.order_date', '<=', $end_date." 23:59:59"],

                ['order.restaurant_branch_id','=',$branch_id]

            ];
        }
        else
        {
            $where_conditions = [

                ['restaurant_branch.id','=',$branch_id]

            ];
        }

        $orders  =   $this->model_order->getModel()::where('order.status_id','<>',8)

                    ->where($where_conditions)

                    ->orderBy('id', 'ASC')

                    ->get();



        $order_ids = $orders->pluck('orderid')->toArray();



        // get delivery detail

        $delivery_detail_service = new DeliveryDetailService();

        $delivery_details = $delivery_detail_service->getDetailByOrderIds($order_ids);



        $response = [];



        foreach($orders as $order)

        {

            $details = [];

            // $order_detail = $order_details->where('order_id','=',$order->orderid);

            $delivery_details   =  $delivery_details->where('order_id','=',$order->id)->first();





            foreach($order->orderDetails as $detail)

            {

                if($detail->menu->kitchen->agents[0]->id != $agent_id)

                continue;

                $details[] = [

                    'id' => $detail->id,

                    'menu_id'   => $detail->menu->id,

                    'menu_name' => $detail->menu->name ,

                    'price' => $detail->price ,

                    'quantity' => $detail->quantity,

                    'order_detail_status_id'=> $detail->order_detail_status_id,

                    'variation_name' => $detail->variation_name,

                    'choices'    =>  $detail->orderDetailChoice,

                    'delivery_details'  =>  $delivery_details

                ];

            }



            $response[] = [



                'id'                    =>  $order->id,

                'user_id'               =>  $order->user_id,

                'user_name'             =>  isset($order->user->last_name)?$order->user->first_name." ".$order->user->last_name:$order->user->first_name,

                'restaurant_branch_id'  =>  $order->restaurant_branch_id,

                'tracking_id'           =>  $order->tracking_id,

                'order_date'            =>  $order->order_date,

                'status_id'             =>  $order->status_id,

                'status'                =>  $order->status->name,

                'total'                 =>  $order->total,

                'sub_total'             =>  $order->sub_total,

                'order_type_id'         =>  $order->order_type_id,

                "createdby_id"          =>  $order->createdby_id,

                "dailyorder_id"         =>  $order->dailyorder_id,

                "table_no"              =>  $order->table_no,

                "order_resource"        =>  $order->resource->resource_name,

                "amount_paid"           =>  $order->amount_paid,

                "tax_percent"           =>  $order->tax_percent,

                "tax_amount"            =>  $order->tax_amount,

                "tax_include"           =>  $order->tax_include,

                "discount_per"          =>  $order->discount_per,

                "discount_amount"       =>  $order->discount_amount,

                "amount_return"         =>  $order->amount_return,

                "order_edit"            =>  $order->order_edit,

                "OrderDetail"           =>  $details



            ];

        }



        if(!$response)

            return false;



        return $response;



    }



    // get orders by branch id and optional date filter in pos mobile

    public function getPosMobileOrdersByBranchId($branch_id,$from_date=null,$end_date=null, $from=0, $limit=300, $order_by="order.id", $sorting="DESC" )
    {
        if($from_date && $end_date)
        {

            $where_conditions = [

                ['order.order_date', '>=', $from_date." 00:00:00"],

                ['order.order_date', '<=', $end_date." 23:59:59"],

                ['order.restaurant_branch_id','=',$branch_id]

            ];
        }
        else
        {

            $where_conditions = [

                ['order.restaurant_branch_id','=',$branch_id]

            ];

        }

        $orders  =  $this->model_order->with([
                            'orderDetails' => function($query){
                                $query->whereNull('order_detail.is_deleted')
                                        ->orWhere('order_detail.is_deleted', '=', '0');

                            },
                            'orderDetails.menu',
                            'deliveryDetails',
                            'orderDetails.menuVariation',
                            'orderDetails.orderDetailChoice'
                        ])
                        ->where($where_conditions)->where('status_id','<>','8')
                        ->orderBy($order_by, $sorting)
                        // ->skip($from)
                        ->take($limit)
                        ->get();

        
        $total_Sale =   $this->model_order->getModel()::where($where_conditions)->where('status_id','<>','8')
                            ->selectRaw('SUM(IFNULL(order.total , 0)) as grand_total')
                            ->first();
        
        $grand_total = $total_Sale->grand_total;


        // // get agent users

        $agent_user_service    = new AgentUserService();
        $agent_users    = $agent_user_service->getByBranchId($branch_id);

        $response = [];

        foreach($orders as $order)
        {
            $details = [];

            foreach($order->orderDetails as $detail)
            {
                if($detail->order_detail_status_id == 2 && (!isset($detail->is_deleted) || $detail->is_deleted == 0) )
                {

                    $details[] = [

                        'id'                => $detail->id,
                        'menuId'            => $detail->menu_id,
                        'menuName'          => isset($detail->menu->name)?$detail->menu->name:'Custom Menu',
                        'price'             => $detail->price ,
                        'quantity'          => $detail->quantity,
                        'menu_variation_id' => $detail->menu_variation_id,
                        'variation_name'    => $detail->variation_name,
                        'variation_price'   => isset($detail->menuVariation)?$detail->menuVariation->price:0,
                        'choices'           =>  $detail->orderDetailChoice,
                    ];
                }
            }

            $delivery_detail = [];

            if(isset($order->deliveryDetails))
            {
                $delivery_detail = [

                    "address"   =>  $order->deliveryDetails->address,
                    "latitude"  =>  $order->deliveryDetails->latitude,
                    "longitude" =>  $order->deliveryDetails->longitude,
                    "townId"    =>  $order->deliveryDetails->town_id,
                    "blockId"   =>  $order->deliveryDetails->town_block_id,
                    "userId"    =>  $order->deliveryDetails->user_id,
                    "firstName" =>  $order->deliveryDetails->user->first_name,
                    "lastName"  =>  $order->deliveryDetails->user->last_name,
                    "contact"   =>  $order->deliveryDetails->user->cell_num,
                    "gender"    =>  $order->deliveryDetails->user->gender,
                    "dateBirth" =>  $order->deliveryDetails->user->date_birth,
                    "instructions" =>  $order->deliveryDetails->instructions
                ];
            }

            $response[] = [

                'id'                   =>  $order->id,
                'userId'               =>  $order->user_id,
                'branchId'             =>  $order->restaurant_branch_id,
                'orderDate'            =>  $order->order_date,
                'statusId'             =>  $order->status_id,
                'status'               =>  $order->status->name,
                'total'                =>  $order->total,
                'subTotal'             =>  $order->sub_total,
                'orderTypeId'          =>  $order->order_type_id,
                "createdby_id"         =>  $order->createdby_id,
                "dailyOrderId"         =>  $order->dailyorder_id,
                "tableNo"              =>  $order->table_no,
                "order_resource"       =>  $order->order_resource,
                "amountPaid"           =>  $order->amount_paid??0,
                "taxPercent"           =>  $order->tax_percent??0,
                "taxAmount"            =>  $order->tax_amount??0,
                "serviceCharge"        =>  $order->service_charge??0,
                "deliveryCharge"       =>  $order->delivery_charge??0,
                "tax_include"          =>  $order->tax_include,
                "discountPer"          =>  $order->discount_per??0,
                "discountAmount"       =>  $order->discount_amount??0,
                "amountReturn"         =>  $order->amount_return??0,
                "numofPersons"         =>  $order->num_persons??0,
                "orderEdit"            =>  $order->order_edit,
                "orderDetail"          =>  $details,
                "loginUserId"          =>  $agent_users->where('id','=',$order->createdby_id)->pluck('id')->first() ?? null,
                "loginUser"            =>  $agent_users->where('id','=',$order->createdby_id)->pluck('username')->first() ?? null,
                "loginUserName"        =>  $agent_users->where('id','=',$order->createdby_id)->pluck('name')->first() ?? null,
                "delivery_details"     =>  $delivery_detail
            ];

        }
        
        if(!$response)

            return false;

        return  [
                    'data' => $response,
                    'total_Sale' => $grand_total
                ];
    }

    public function getUnpaidOrdersByBranchId($branch_id,$from_date = null,$end_date = null,  $from = 0, $limit = 300, $order_by = "order.id", $sorting = "ASC")
    {
        if($from_date && $end_date)
        {
            $where_conditions = [

                ['order.order_date', '>=', $from_date." 00:00:00"],

                ['order.order_date', '<=', $end_date." 23:59:59"],

                ['order.restaurant_branch_id','=',$branch_id]

            ];

        }
        else
        {
            $where_conditions = [

                ['order.restaurant_branch_id','=',$branch_id]

            ];

        }

        $orders  =   $this->model_order->with([
                    'orderDetails'  => function($query){
                                    $query->whereNull('order_detail.is_deleted')
                                        ->orWhere('order_detail.is_deleted', '=', '0');
                                    },
                    'orderDetails.menu',
                    'orderDetails.menuVariation',
                    'orderDetails.orderDetailChoice',
                    'deliveryDetails',
                    'restaurantBranch',
                    'user',
                    'status',
                    'type'
                    ])
                    ->where('order.status_id','<>',8)
                    ->where('order.amount_paid','=',0)
                    ->where($where_conditions)
                    ->orderBy($order_by, $sorting)
                    ->skip($from)
                    ->take($limit)
                    ->get();

        // get agent users

        $agent_user_service    = new AgentUserService();
        $agent_users    = $agent_user_service->getByBranchId($branch_id);

        $response = [];

        foreach($orders as $order)
        {
            $details = [];

            foreach($order->orderDetails as $detail)
            {
                $details[] = [

                    'id'        => $detail->id,
                    'menu_id'   => $detail->menu_id,
                    'menu_name' => $detail->menu_name ,
                    'price'     => $detail->price ,
                    'menu_original_price'  => $detail->menu->price ,
                    'quantity'  => $detail->quantity,
                    'order_detail_status_id'=> $detail->order_detail_status_id,
                    'menu_variation_id' => $detail->menu_variation_id,
                    'variation_name'    => $detail->variation_name,
                    'variation_price'   => isset($detail->menuVariation)?$detail->menuVariation->price:0,
                    'choices'           =>  $detail->orderDetailChoice,
                    'delivery_details'  =>  $order->deliveryDetails
                ];
            }

            $response[] = [

                'id'                    =>  $order->id,
                'user_id'               =>  $order->user_id,
                'user_name'             =>  isset($order->user->last_name)?$order->user->first_name." ".$order->user->last_name:$order->user->first_name,
                'restaurant_branch_id'  =>  $order->restaurant_branch_id,
                'tracking_id'           =>  $order->tracking_id,
                'order_date'            =>  $order->order_date,
                'status_id'             =>  $order->status_id,
                'status'                =>  $order->status->name,
                'total'                 =>  $order->total,
                'sub_total'             =>  $order->sub_total,
                'order_type_id'         =>  $order->order_type_id,
                'type'                  =>  $order->type->type,
                "createdby_id"          =>  $order->createdby_id,
                "dailyorder_id"         =>  $order->dailyorder_id,
                "table_no"              =>  $order->table_no,
                "order_resource"        =>  $order->order_resource,
                "amount_paid"           =>  $order->amount_paid,
                "delivery_charge"       =>  $order->delivery_charge,
                "service_charge"        =>  $order->service_charge,
                "tax_percent"           =>  $order->tax_percent,
                "tax_amount"            =>  $order->tax_amount,
                "tax_include"           =>  $order->tax_include,
                "tax_on_original_amt"   =>  $order->tax_on_original_amt,
                "discount_per"          =>  $order->discount_per,
                "discount_amount"       =>  $order->discount_amount,
                "amount_return"         =>  $order->amount_return,
                "order_edit"            =>  $order->order_edit,
                "order_taker"           =>  $order->order_taker,
                "useragent_id"          =>  $order->useragent_id,
                "num_persons"           =>  $order->num_persons,
                "login_user"            =>  $agent_users->where('id','=',$order->oc_id)->pluck('username')->first() ?? null,
                "login_user_name"       =>  $agent_users->where('id','=',$order->oc_id)->pluck('name')->first() ?? null,
                "restaurant_branch"     =>  (object)[

                        'address'       =>  $order->restaurantBranch->address,
                        'name'          =>  $order->restaurantBranch->name,
                        'logo'          =>  $order->restaurantBranch->logo,
                        'contact_number'=>  $order->restaurantBranch->contact_number,
                        'strn_number'   =>  $order->restaurantBranch->strn_number,
                        'ntn_number'    =>  $order->restaurantBranch->ntn_number
                ],
                "OrderDetail"           =>  $details



            ];

        }

        if(!$response)
            return false;

        return $response;

    }
    public function getOrderByTrackId($id, $branch_id){

        $order = $this->model_order->getModel()::where('order.tracking_id', '=', $id)->first();
        
        // dd($order);

        $agent_user_service    = new AgentUserService();

        $agent_users    = $agent_user_service->getByBranchId($branch_id);

        $details = [];

        // dd($order->orderDetails);
        
        foreach($order->orderDetails as $detail)

        {


                $details[] = [

                    'id'        => $detail->id,

                    'menuId'    => $detail->menu_id,

                    'menuName'  => $detail->menu->name,

                    'price'     => $detail->price ,

                    'quantity'  => $detail->quantity,

                    'menu_variation_id' => $detail->menu_variation_id,

                    'variation_name'    => $detail->variation_name,

                    'variation_price'   => isset($detail->menuVariation)?$detail->menuVariation->price:0,

                    'choices'           =>  $detail->orderDetailChoice,

                ];


        }

        $delivery_detail = [];

        if(isset($order->deliveryDetails)){

            $delivery_detail = [

                "address"   =>  $order->deliveryDetails->address,

                "latitude"  =>  $order->deliveryDetails->latitude,

                "longitude" =>  $order->deliveryDetails->longitude,

                "townId"    =>  $order->deliveryDetails->town_id,

                "blockId"   =>  $order->deliveryDetails->town_block_id,

                "userId"    =>  $order->deliveryDetails->user_id,

                "firstName" =>  $order->deliveryDetails->user->first_name,

                "lastName"  =>  $order->deliveryDetails->user->last_name,

                "contact"   =>  $order->deliveryDetails->user->cell_num,

                "gender"    =>  $order->deliveryDetails->user->gender,

                "dateBirth" =>  $order->deliveryDetails->user->date_birth,

                "instructions" =>  $order->deliveryDetails->instructions

            ];

        }



        $response[] = [

            'id'                   =>  $order->id,

            'userId'               =>  $order->user_id,

            'branchId'             =>  $order->restaurant_branch_id,

            'orderDate'            =>  $order->order_date,

            'statusId'             =>  $order->status_id,

            'status'               =>  $order->status->name,

            'total'                =>  $order->total,

            'subTotal'             =>  $order->sub_total,

            'orderTypeId'          =>  $order->order_type_id,

            "createdby_id"         =>  $order->createdby_id,

            "dailyOrderId"         =>  $order->dailyorder_id,

            "tableNo"              =>  $order->table_no,

            "order_resource"       =>  $order->order_resource,

            "amountPaid"           =>  $order->amount_paid??0,

            "taxPercent"           =>  $order->tax_percent??0,

            "taxAmount"            =>  $order->tax_amount??0,

            "serviceCharge"        =>  $order->service_charge??0,

            "deliveryCharge"       =>  $order->delivery_charge??0,

            "tax_include"          =>  $order->tax_include,

            "discountPer"          =>  $order->discount_per??0,

            "discountAmount"       =>  $order->discount_amount??0,

            "amountReturn"         =>  $order->amount_return??0,

            "numofPersons"         =>  $order->num_persons??0,

            "orderEdit"            =>  $order->order_edit,

            "orderDetail"          =>  $details,

            "loginUserId"          =>  $agent_users->where('id','=',$order->createdby_id)->pluck('id')->first() ?? null,

            "loginUser"            =>  $agent_users->where('id','=',$order->createdby_id)->pluck('username')->first() ?? null,

            "delivery_details"     =>  $delivery_detail

        ];

        return $response;
    
    }


    public function getOrderDetailsByOrderId($order_id)

    {

        $obj_order = $this->getById($order_id);



        foreach($obj_order->orderDetails as $item)

        {

            $detail[] = [

                "name"      =>  $item->menu->name,

                "quantity"  =>  $item->quantity,

                "price"     =>  $item->price

            ];

        }





        $response = [

            'tax_percentage'    =>  $obj_order->tax_percent,

            'discount_amount'   =>  $obj_order->discount_amount,

            'delivery_charge'   =>  $obj_order->delivery_charge,

            'first_name'        =>  $obj_order->user->first_name,

            'last_name'         =>  isset($obj_order->user->last_name)?$obj_order->user->last_name:"",

            'daily_order'       =>  $obj_order->dailyorder_id,

            'address'           =>  $obj_order->deliveryDetails->address,

            'resource'          =>  $obj_order->resource->resource_name,

            'order_date'        =>  $obj_order->order_date,

            'delivery_date'     =>  $obj_order->deliver_date,

            'total'             =>  $obj_order->total??0.0,

            'tax_anount'        =>  $obj_order->tax_amount??0.0,

            'amount_paid'       =>  $obj_order->amount_paid??0.0,

            'amount_return'     =>  $obj_order->amount_return??0.0,

            'order_type'        =>  $obj_order->type->type,

            'order_status'      =>  $obj_order->status->name,

            'delivered_time'    =>  $obj_order->delivered_time,

            'recieved_time'     =>  $obj_order->recieved_time,

            'preparing_time'    =>  $obj_order->preparing_time,

            'ready_time'        =>  $obj_order->ready_time,

            'dispatch_time'     =>  $obj_order->dispatch_time,

            'cancel_time'       =>  $obj_order->cancel_time,

            'menu_details'      =>  $detail

        ];



        return  $response;

    }



    public function changeOrdStatustoReadyViaAllDetailReady($order_detail_id)

    {

        $order_id = $this->model_order_detail->getModel()::where(function ($query) {

                $query->whereNull('order_detail.is_deleted')

                  ->orWhere('order_detail.is_deleted', '=', '0');

                })

                ->where('id',$order_detail_id)

                ->select('order_id')

                ->first();

        $order_id = $order_id->order_id;



        // get count of all order detail items against order id

        $total_dtl_count = $this->model_order_detail->getModel()::where(function ($query) {

                    $query->whereNull('order_detail.is_deleted')

                      ->orWhere('order_detail.is_deleted', '=', '0');

                    })

                    ->where('order_id',$order_id)

                    ->count();

        // get count of ready order detail items with status = 4

        $ready_dtl_count = $this->model_order_detail->getModel()::where(function ($query) {

                        $query->whereNull('order_detail.is_deleted')

                          ->orWhere('order_detail.is_deleted', '=', '0');

                        })

                        ->where('order_id',$order_id)

                        ->where('order_detail_status_id',4)

                        ->count();



        if ($total_dtl_count == $ready_dtl_count)

           $this->changeOrderStatus($order_id,7);





    }



    public function insertErpInventoryTrans($order_id)

    {

        $order = $this->getById($order_id);



        if($order->restaurantBranch->erp_active && isset($order->restaurantBranch->erp_url))

        {

            $api_url = $order->restaurantBranch->erp_url."api/inventory/consumeMenuItems";



            foreach($order->orderDetails as $item )

            {

                if(isset($item->is_deleted) && $item->is_deleted == true)

                    continue;



                $response = Http::post($api_url, [

                    'menuId'  => $item->menu_id,

                    'qty'     => $item->quantity,

                    'orderId' => $item->order_id,

                ]);

            }

        }

    }



    // get average reparation time of day

    public function getAveragePreparationTimeByDay($datee, $branch_id, $login_user=null)

    {

        $wh = "";



        switch ($login_user->role_id)

        {

            case '1':

                if($branch_id != null)

                {

                    $wh = " AND restaurant_branch.restaurant_id = ".$branch_id;

                }else{

                    $wh = "";

                }



            break;



            case '2':

                if($branch_id != null)

                {

                    $wh = " AND restaurant_branch.restaurant_id = ".$branch_id;

                }else{

                    $wh = " AND restaurant_branch.restaurant_id = ".$login_user->userRestaurant->restaurant_id;



                }



            break;



            case '3':
            case '4':



                $wh = " AND restaurant_branch.id = ".$login_user->userRestaurant->branch_id;



            break;



            default:



                $wh = " AND 1 ";

        }



        $qry = "SELECT  FORMAT(IFNULL(AVG(TIMESTAMPDIFF(MINUTE,recieved_time,delivered_time)),0),2) as avgtime

                FROM `order`

                JOIN restaurant_branch on `order`.restaurant_branch_id = restaurant_branch.id

                WHERE 1  $wh AND date(order_date) = '$datee' AND status_id <> 8";



        $results = DB::select( DB::raw($qry));





        if(!$results[0]->avgtime)

            return 0;



        return $results[0]->avgtime;



    }



    public function getOrderDetailString($order,$branch,$status_id)

    {

        $detail_string ="";



        $order_details = $order->orderDetails;



        foreach ($order_details as $key => $value) 

        {

            $detail_string .= ($key+1) . ".".  $value->menu_name ."=" . $value->price . "*" . $value->quantity . "\n";

        }



        $message = "";

        

        

        switch($status_id)

        {

            case '3':



                $message .= "Well Come to ".$branch->name . "\n";

                $message .= "Your Order No.". $order->dailyorder_id ." is Recieved ". "\n";

                



            break;



            case '4':

                

                $message .= "Your Order No.".$order->dailyorder_id." is preparing ". "\n";

               



            break;

                

            case '6':

                

                $message .= "Your Order NO.".$order->dailyorder_id ." is Ready and dispatched " . "\n";

               



            break;

            

            default:

        }



        $message .= date('Y-m-d h:i a',strtotime($order->order_date."+5 hours")) . "\n";

        $message .= $detail_string ."Total Amount=Rs. ".$order->total;



        

        return $message ;

    }



}

