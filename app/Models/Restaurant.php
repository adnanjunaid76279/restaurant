<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model  
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'restaurant';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id','name', 'logo', 'owner', 'contact_person', 'contact_email', 'contact_number', 'date_created',  'createdby_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_created', 'date_updated'];

    public $timestamps = false;

}
