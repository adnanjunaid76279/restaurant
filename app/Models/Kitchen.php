<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kitchen extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'kitchen';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['kitchen_name', 'branch_id', 'date_created', 'date_updated', 'createdby_id', 'updatedby_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_created', 'date_updated'];

    public function agents()
    {
    	return $this->belongsToMany('\App\Models\AgentUsers','\App\Models\AgentKitchen', 'kitchen_id', 'agent_id');
    }

}
