<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $branch_id
 * @property int $order_type_id
 * @property string $day_name
 * @property string $open_time
 * @property string $close_time
 * @property int $sorting
 * @property OrderType $orderType
 * @property RestaurantBranch $restaurantBranch
 */
class BranchTime extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['branch_id', 'order_type_id', 'day_name', 'open_time', 'close_time', 'sorting'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderType()
    {
        return $this->belongsTo('App\Models\OrderType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantBranch()
    {
        return $this->belongsTo('App\Models\RestaurantBranch', 'branch_id');
    }
}
