<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $code
 * @property float $dicount
 * @property string $discount_type
 * @property boolean $is_enabled
 * @property string $valid_from
 * @property string $vali_till
 * @property float $max_value
 * @property float $min_value
 * @property int $branch_id
 * @property User[] $users
 */
class Coupon extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['code', 'dicount', 'discount_type', 'is_enabled', 'valid_from', 'valid_till', 'max_value', 'min_value', 'branch_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'user_coupons');
    }
    
}
