<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property float $dinein_sale
 * @property float $takeaway
 * @property float $delivery
 * @property string $cash_in_items
 * @property string $cash_out_items
 */
class PosCashFlow extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['dinein_sale', 'takeaway_sale', 'delivery_sale', 'cash_in_items', 'cash_out_items','cash_detail','branch_id','is_closed'];

    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['is_closed' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_created'];

    public $timestamps = false;

}
