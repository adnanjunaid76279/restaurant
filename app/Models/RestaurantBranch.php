<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantBranch extends Model  
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'restaurant_branch';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'address', 
        'town_id', 
        'is_takeaway', 
        'is_delivery', 
        'is_dinein', 
        'is_freedelivery', 
        'restaurant_id', 
        'phone_number', 
        'phone_number2', 
        'phone_number3', 
        'mobile_number', 
        'mobile_number2', 
        'mobile_number3', 
        'contact_person', 
        'contact_email', 
        'contact_number', 
        'logo', 
        'longitude', 
        'latitude', 
        'fb_post_id', 'description', 'have_category', 'order_taking', 'minimum_order_value', 'delivery_fee', 'close_time', 'open_time', 'createdby_id','updatedby_id', 'date_created','date_updated','currency', 'total_seats', 'dailyorder_id','strn_number', 'ntn_number', 'tax_percent', 'tax_include', 'order_discount_percent','facebook_app_id','facebook_app_secret','client_id','client_secret','firebase_server_key','firebase_sender_id','is_active','delivery_app_server_key','delivery_app_sender_id','tax_on_original_amt'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['order_taking' => 'boolean', 'tax_include' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_created', 'date_updated'];

    public $timestamps = false;
    
    public function restaurant()
    {
    	return $this->belongsTo('\App\Models\Restaurant');
    }

    public function menuCategories()
    {
    	return $this->hasMany('\App\Models\MenuCategory','restaurant_branch_id');
    }

}
