<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuCategory extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menu_category';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['restaurant_branch_id', 'name', 'image', 'is_active', 'date_created', 'date_updated', 'updatedby_id', 'createdby_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['is_active' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_created', 'date_updated'];

    public $timestamps = false;
    
    public function restaurantBranch()
    {
    	return $this->belongsTo('\App\Models\RestaurantBranch');
    }

    public function menus()
    {
    	return $this->hasMany('\App\Models\Menu','menu_category_id');
    }

}
