<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $choice_id
 * @property int $order_detail_id
 * @property string $choice_ name
 * @property float $price
 */
class OrderDetailChoices extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['choice_id','choice_name', 'price','order_detail_id'];
    
    public $timestamps = false;

}
