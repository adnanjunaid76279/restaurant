<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @property int $id
 * @property int $menu_id
 * @property string $name
 * @property float $price
 * @property int $sorting
 * @property Menu $menu
 * @property ChoiceGroup[] $choiceGroups
 */
class MenuVariation extends Model
{
    use LogsActivity;

    /**
     * @var array
     */
    protected $fillable = ['menu_id', 'name', 'price', 'sorting'];

    public $timestamps = false;


    /**
    *log changes to all the $fillable attributes
    */

    protected static $logFillable = true;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menu()
    {
        return $this->belongsTo('App\Models\Menu');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function choiceGroups()
    {
        return $this->belongsToMany('App\Models\ChoiceGroup', 'menu_variation_choice_groups', 'menu_variation_id');
    }
}
