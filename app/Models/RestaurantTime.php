<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantTime extends Model  
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'restaurant_time';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['day_id', 'open_time', 'close_time', 'restaurant_branch_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['open_time', 'close_time'];

}
