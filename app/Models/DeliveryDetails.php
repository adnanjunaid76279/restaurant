<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryDetails extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'delivery_details';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'town_id', 'address', 'mobile_num', 'phone_num', 'others', 'email', 'order_id', 'date_birth', 'gender', 'instructions', 'latitude', 'longitude', 'town_block_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_birth'];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function town()
    {
        return $this->belongsTo('App\Models\Town','town_id','id');
    }
    
    public function townBlock()
    {
        return $this->belongsTo('App\Models\TownBlock');
    }
}
