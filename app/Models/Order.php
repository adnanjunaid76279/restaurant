<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Notifications\Notifiable;

class Order extends Model
{
    // use Notifiable;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'restaurant_branch_id', 'tracking_id', 'order_date', 'status_id', 'deliver_date', 'total', 'sub_total', 'order_resource', 'date_created', 'date_updated', 'updatedby_id', 'createdby_id', 'dailyorder_id', 'useragent_id', 'order_type_id', 'amount_paid', 'amount_return', 'discount_amount', 'discount_per', 'order_edit', 'tax_percent', 'tax_amount', 'table_no', 'num_persons','delivery_charge','service_charge', 'tax_include','cancel_time','delivered_time','recieved_time','preparing_time','ready_time','dispatch_time','erp_active','erp_url','order_taker','payment_type','payment_gateway','online_pay_order_id','tax_on_original_amt'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['order_edit' => 'boolean', 'tax_include' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['order_date', 'deliver_date', 'date_created', 'date_updated'];

    public $timestamps = false;

    public function status()
    {
    	return $this->hasOne('\App\Models\OrderStatus','id','status_id');
    }

    public function resource()
    {
    	return $this->hasOne('\App\Models\OrderResource','id','order_resource');
    }

    public function type()
    {
    	return $this->hasOne('\App\Models\OrderType','id','order_type_id');
    }

    public function orderDetails()
    {
    	return $this->hasMany('\App\Models\OrderDetail','order_id');
    }

    public function deliveryDetails()
    {
    	return $this->hasOne('\App\Models\DeliveryDetails','order_id');
    }

    public function user()
    {
    	return $this->hasOne('\App\Models\Users','id','user_id');
    }

    public function restaurantBranch()
    {
    	return $this->hasOne('\App\Models\RestaurantBranch','id','restaurant_branch_id');
    }

}
