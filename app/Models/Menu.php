<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menu';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['menu_category_id',  'name', 'image', 'description', 'fb_post_id', 'price',  'date_created', 'date_updated', 'updatedby_id', 'createdby_id', 'add_image', 'is_deleted', 'is_deal', 'kitchen_id', 'ready_made','external_menu_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['have_addone' => 'boolean', 'add_image' => 'boolean', 'is_deleted' => 'boolean', 'is_deal' => 'boolean', 'ready_made' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_created', 'date_updated'];

    public $timestamps = false;
    
    public function menuCategory()
    {
    	return $this->belongsTo('\App\Models\MenuCategory');
    }

    
    public function kitchen()
    {
    	return $this->hasOne('\App\Models\Kitchen','id','kitchen_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function choiceGroups()
    {
        return $this->belongsToMany('App\Models\ChoiceGroup', 'menu_choice_groups');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function menuVariations()
    {
        return $this->hasMany('App\Models\MenuVariation');
    }

}
