<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @property int $id
 * @property int $choice_group_id
 * @property string $name
 * @property float $price
 * @property int $sorting
 * @property ChoiceGroup $choiceGroup
 */
class Choice extends Model
{
    use LogsActivity;

    /**
     * @var array
     */
    protected $fillable = ['choice_group_id', 'name', 'price', 'sorting'];

    public $timestamps = false;
    /**
    *log changes to all the $fillable attributes
    */

    protected static $logFillable = true;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function choiceGroup()
    {
        return $this->belongsTo('App\Models\ChoiceGroup');
    }
}
