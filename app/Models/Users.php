<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model  
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['login_type_id', 'password', 'first_name', 'last_name', 'phone_num', 'cell_num', 'social_app_id', 'date_created', 'date_updated', 'updatedby', 'createdby', 'date_birth', 'email', 'branch_id','device_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_created', 'date_updated', 'date_birth'];

    public $timestamps = false;

    public function appusers()
    {
        return $this->hasMany('app_users');
    }

    public function deliveryDetails()
    {
    	return $this->hasMany('App\Models\DeliveryDetails','id','user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function addresses()
    {
        return $this->hasOne('App\Models\Address','user_id','id');
    }

}
