<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @property int $menu_varitaion_id
 * @property int $choice_group_id
 * @property ChoiceGroup $choiceGroup
 * @property MenuVariation $menuVariation
 */
class MenuVariationChoiceGroup extends Model
{
    use LogsActivity;

    protected $primaryKey = null;
    public $incrementing = false;
    /**
     * @var array
     */
    protected $fillable = ['menu_variation_id', 'choice_group_id','sorting'];


    public $timestamps = false;

    
    /**
    *log changes to all the $fillable attributes
    */

    protected static $logFillable = true;

    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function choiceGroup()
    {
        return $this->belongsTo('App\Models\ChoiceGroup');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menuVariation()
    {
        return $this->belongsTo('App\Models\MenuVariation', 'menu_variation_id');
    }
}
