<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentUsers extends Model
{



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agent_users';
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['branch_id','name', 'username', 'password', 'allowPos', 'lastLogin', 'loggedin', 'category_flag', 'hcc_user_login', 'hcc_user_password', 'agent_role_id', 'hcc_server_ip'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['allowPos' => 'boolean', 'loggedin' => 'boolean', 'category_flag' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['lastLogin'];

    public function branch()
    {
        return $this->hasOne(RestaurantBranch::class,'id','branch_id');
    }
    
    public function kitchens()
    {
    	return $this->belongsToMany('\App\Models\Kitchen','\App\AgentKitchen', 'kitchen_id', 'agent_id');
    }
    
    public function agentRole()
    {
        return $this->hasOne('\App\Models\AgentRoles','id','agent_role_id');
    }

}
