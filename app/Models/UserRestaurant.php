<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRestaurant extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_restaurant';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'branch_id', 'restaurant_id', 'date_created', 'date_updated', 'updatedby_id', 'createdby_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_created', 'date_updated'];

    public $timestamps = false;
    
    public function adminUsers()
    {
    	return $this->belongsTo('\App\Models\AdminUsers');
    }

    public function restaurantBranch()
    {
    	return $this->hasOne('\App\Models\RestaurantBranch','id','branch_id');
    }

    public function restaurant()
    {
        return $this->hasOne('\App\Models\Restaurant','id','restaurant_id');
    }

}
