<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryUser extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'delivery_user';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['login', 'password', 'first_name', 'last_name', 'device_id', 'is_online', 'role_id', 'cell_num', 'branchId', 'latitude', 'longitude', 'date_created', 'date_updated', 'updatedby_id', 'createdby_id', 'app_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['is_online' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_created', 'date_updated'];

    public $timestamps = false;
    public function status()
    {
    	return $this->hasOne('\App\Models\OrderStatus','id','status_id');
    }

    public function branch()
    {
        return $this->hasOne('\App\Models\RestaurantBranch','id','branchId');
    }

}
