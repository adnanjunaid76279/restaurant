<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{

    public $table = 'roles';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'role_name',
        'created_at',
        'updated_at',
    ];

    public function rolesUsers()
    {
        return $this->belongsToMany(AdminUsers::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
}
