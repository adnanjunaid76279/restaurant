<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryUsersOrder extends Model  
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'delivery_users_order';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['order_id', 'delivery_user_id', 'delivery_user_order_status_id', 'date_created', 'date_updated', 'createdby_id', 'updatedby_id', 'start_latitude', 'start_longitude', 'current_latitude', 'current_longitude', 'end_latitude', 'end_longitude', 'order_in_progress','travel_distance'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['order_in_progress' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_created', 'date_updated'];
    
    public $timestamps = false;

    public function order()
    {
    	return $this->hasOne('\App\Models\Order','id','order_id');
    }

}
