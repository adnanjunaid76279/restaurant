<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class BranchTable extends Model
{
    protected $table = 'branch_tables';
    protected $fillable = [
        'table_no', 'description','availability','capacity','branch_id'
    ];

    public function nameTableBranch()
    {
        return $this->hasOne('\App\Models\RestaurantBranch','id','branch_id');
    }

}
