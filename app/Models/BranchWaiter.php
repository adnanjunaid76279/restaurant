<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BranchWaiter extends Model
{
    protected $table = 'branch_waiters';

    protected $fillable = ['name', 'is_active','branch_id'];


    public function getBranch()
    {
        return $this->hasOne('\App\Models\RestaurantBranch','id','branch_id');
    }
}
