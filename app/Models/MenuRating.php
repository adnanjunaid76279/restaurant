<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuRating extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menu_rating';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'menu_id', 'rating', 'date_created', 'date_updated', 'updatedby_id', 'createdby_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_created', 'date_updated'];

    public $timestamps = false;
}
