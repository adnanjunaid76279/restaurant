<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class OrderDetail extends Model  
{
    use LogsActivity;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_detail';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['order_id', 'menu_id', 'quantity', 'price', 'is_deleted', 'order_detail_status_id', 'menu_name', 'menu_variation_id', 'variation_name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['is_deleted' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    public $timestamps = false;

     /**
    *log changes to all the $fillable attributes
    */

    protected static $logFillable = true;

    public function menu()
    {
    	return $this->hasOne('\App\Models\Menu','id','menu_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderDetailStatus()
    {
        return $this->belongsTo('App\Models\OrderDetailStatus');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderDetailChoice()
    {
        return $this->hasMany('App\Models\OrderDetailChoices','order_detail_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function menuVariation()
    {
        return $this->hasOne('App\Models\MenuVariation','id','menu_variation_id');
    }

   

}
