<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
/**
 * @property int $id
 * @property string $name
 * @property int $min_choices
 * @property int $max_choices
 * @property int $sorting
 * @property Choice[] $choices
 * @property MenuVariation[] $menuVariations
 */
class ChoiceGroup extends Model
{
    use LogsActivity;

    /**
     * @var array
     */
    protected $fillable = ['name', 'min_choices', 'max_choices', 'sorting','branch_id'];

    
    public $timestamps = false;

    /**
    *log changes to all the $fillable attributes
    */

    protected static $logFillable = true;
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function choices()
    {
        return $this->hasMany('App\Models\Choice');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function menuVariations()
    {
        return $this->belongsToMany('App\MenuVariation', 'menu_variation_choice_groups', null, 'menu_variation_id');
    }

     /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantBranch()
    {
    	return $this->hasOne('\App\Models\RestaurantBranch','id','branch_id');
    }
}
