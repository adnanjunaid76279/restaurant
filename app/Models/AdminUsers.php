<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AdminUsers extends Authenticatable  
{

    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $guard = 'admin_users';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['login', 'password','remember_token', 'role_id', 'first_name', 'last_name', 'cell_num', 'date_created', 'date_updated', 'updatedby_id', 'createdby_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_created', 'date_updated'];

    public $timestamps = false;
    
    public function userRestaurant()
    {
    	return $this->hasOne('\App\Models\UserRestaurant','user_id');
    }

    public function userRole()
    {
    	return $this->hasOne('\App\Models\Roles','id','role_id');
    }
    public function getIsAdminAttribute()
    {
        return $this->roles()->where('id', 1)->exists();
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        self::created(function (User $user) {
            $registrationRole = config('panel.registration_default_role');

            if (!$user->roles()->get()->contains($registrationRole)) {
                $user->roles()->attach($registrationRole);
            }
        });
    }
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
}
    