<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reservation';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'restaurant_id', 'branch_id', 'name', 'email', 'phone_num', 'total_seats', 'reservation_date', 'reservation_source', 'from_time', 'to_time', 'time_slot', 'is_canceled', 'reserved_tables'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['is_canceled' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['reservation_date'];

}
