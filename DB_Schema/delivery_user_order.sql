-- add new column for erp server
ALTER TABLE `delivery_users_order` ADD `travel_distance` DECIMAL(18,2) NOT NULL DEFAULT '0' AFTER `order_in_progress`;