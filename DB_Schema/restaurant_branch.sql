-- add new column for erp connectivity status
ALTER TABLE `restaurant_branch` ADD `erp_active` TINYINT(1) NOT NULL DEFAULT '0' AFTER `delivery_fee`;
-- add new column for erp server
ALTER TABLE `restaurant_branch` ADD `erp_url` VARCHAR(200) NULL AFTER `erp_active`;
-- change filed type from datetime to time
ALTER TABLE `restaurant_branch` CHANGE `open_time` `open_time` TIME NULL DEFAULT NULL;
-- change filed type from datetime to time
ALTER TABLE `restaurant_branch` CHANGE `close_time` `close_time` TIME NULL DEFAULT NULL;
--  add columns facebook_app_id
alter table restaurant_branch add  `facebook_app_id` longtext NULL;
--  add columns facebook_app_id
alter table restaurant_branch add  `facebook_app_secret` longtext NULL;
--  add columns client_id
alter table restaurant_branch add  `client_id` longtext NULL;
--  add columns client_secret
alter table restaurant_branch add  `client_secret` longtext NULL;
--  add columns firebase_server_key
alter table restaurant_branch add  `firebase_server_key` longtext NULL;
--  add columns firebase_sender_id
alter table restaurant_branch add  `firebase_sender_id` longtext NULL;

