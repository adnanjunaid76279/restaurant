-- add new column for login_type i.e. social,or pos, username password
ALTER TABLE `users` ADD `login_type_id` BIGINT NULL;

-- device id fro mobile app
ALTER TABLE `users` ADD `device_id` LONGTEXT NULL;

-- social app id for social sites
ALTER TABLE `users` ADD `social_app_id` LONGTEXT NULL;

-- change name of branchId
ALTER TABLE `users` CHANGE `branchId` `branch_id` INT(11) NULL DEFAULT NULL;

-- change createdby id
ALTER TABLE `users` CHANGE `createdby_id` `createdby_id` VARCHAR(100) NULL DEFAULT NULL;

-- updated by id 
ALTER TABLE `users` CHANGE `updatedby_id` `updatedby_id` VARCHAR(100) NULL DEFAULT NULL;

-- delete role id column
ALTER TABLE users DROP COLUMN role_id;

-- delete login column
ALTER TABLE users DROP COLUMN login;

-- delete fb_id column
ALTER TABLE users DROP COLUMN fb_id;


-- delete access_token column
ALTER TABLE users DROP COLUMN access_token;

-- delete expiry column
ALTER TABLE users DROP COLUMN expiry;

-- delete expiry column
ALTER TABLE users DROP COLUMN pos_user;

-- change name of createdby_id 
ALTER TABLE `users` CHANGE `createdby_id` `createdby` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;


-- change name of updatedby_id 
ALTER TABLE `users` CHANGE `updatedby_id` `updatedby` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

