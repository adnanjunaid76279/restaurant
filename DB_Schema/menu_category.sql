-- change name of column 
ALTER TABLE `menu_category` CHANGE `have_menu` `is_active` TINYINT(1) NULL DEFAULT NULL;

-- drop column
ALTER TABLE menu_category DROP COLUMN likes;

