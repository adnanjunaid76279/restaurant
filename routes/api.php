<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
    });
    
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::post('PosLogin', 'PosController@posLogin');
Route::get('GetRestaurantBranchesNameAndId/{restaurant_id}', 'PosController@getRestaurantBranchesNameAndId');
Route::middleware('cors')->get('GetTownsByCityId', 'PosController@getTownsByCityId');
Route::middleware('cors')->get('GetTownBlocksByTownId', 'PosController@getTownBlocksByTownId');
Route::middleware('cors')->post('GetCategoriesByBranchId', 'PosController@getCategoriesByBranchId');
Route::post('GetUserByNumberandBranchId', 'PosController@getUserByNumberandBranchId');
Route::get('UpdateDailyCount', 'PosController@updateBranchDailyCount');
Route::post('getMenuVariationAndChoicesByMenuId', 'PosController@getMenuVariationAndChoicesByMenuId');


//pos web
Route::post('UserOrdersByRestID', 'PosController@getUserOrdersByRestID');
Route::get('TodayOrdersByBranchId', 'PosController@getOrdersByBranchId');
Route::get('TodayOrdersByBranchIdAgentId', 'PosController@getOrdersByBranchIdAgentId');
Route::get('OrdersByBranchIdDateRange', 'PosController@getOrdersByBranchId');
Route::get('orderstatus', 'PosController@getOrderStatus');
Route::post('PlaceOrderPos', 'PosController@placeOrder');  ///PosMobileAppOrderPlace
Route::post('SavePOSUser', 'PosController@savePOSUser');
Route::post('changeOrderstatus', 'PosController@changeOrderstatus')->name('changeOrderstatus');
Route::post('ChangeOrderDetailStatus', 'PosController@changeOrderDetailStatus')->name('changeOrderDetailStatus');
Route::get('GetAddressTypes', 'PosController@getAddressTypes');
Route::get('GetAddressByUserIdAddTypeId', 'PosController@getAddressByUserAddType');
Route::post('InsertPosOrderPayment', 'PosController@insertPosOrderPayment');
Route::get('UnpaidOrdersByBranchId', 'PosController@getUnpaidOrdersByBranchId');

//rider app
Route::get('GetActiveRidersByBranchId', 'PosController@getActiveRidersByBranchId');
Route::get('GetDeliveryRoute', 'PosController@getDeliveryRoute');
Route::get('PosGetOrderTracking', 'PosController@posGetOrderTracking');
Route::post('DeliveryAppLogin', 'DeliveryAppController@riderLogin');
Route::post('UpdateRiderAfterLogin', 'DeliveryAppController@updateRider');
Route::post('UpdateRiderProfile', 'DeliveryAppController@updatePassword');
Route::post('UpdateRiderAvailableStatus', 'DeliveryAppController@isRiderAvailable');
Route::post('AssignOrderToRider', 'DeliveryAppController@assignOrderToRider');
Route::get('GetActiveDeliveryOrders', 'DeliveryAppController@getActiveDeliveryOrders');
Route::post('StartDeliveryOrder', 'DeliveryAppController@startDeliveryOrder');  // start order by rider
Route::post('EndDeliveryOrder', 'DeliveryAppController@endDeliveryOrder');  // End order by rider
Route::post('UpdateOnwayDeliveryOrder', 'DeliveryAppController@updateOnwayDeliveryOrder');  // update onway order
Route::get('GetDeliveryOrderHistory', 'DeliveryAppController@getDeliveryOrderHistory');     // order history of rider


// web
Route::post('WebAppMainData', 'WebAppController@getMainData');
Route::post('WebSaveSocialUser', 'WebAppController@saveWebUser');
Route::post('WebPlaceOrder', 'WebAppController@placeOrder');
Route::post('WebOrderHistory', 'WebAppController@getOrderHistory');
Route::post('WebRateDish', 'WebAppController@rateDish');
Route::post('WebUserSignUp', 'WebAppController@userSignUp');
Route::post('WebUserLogin', 'WebAppController@userLogin');
Route::post('UpdatePaymentStatusToPaidByOnlinePayOrderId', 'WebAppController@updatePaymentStatusToPaidByOnlinePayOrderId');

// MobileAppMainData
Route::post('MobileAppMainData', 'MobileAppController@getMainData'); //id
Route::post('MobileGetCategoriesByBranchId', 'MobileAppController@getCategoriesByBranchId');
Route::post('MobileSaveFBIdDeviceId', 'MobileAppController@saveMobileUser');
Route::post('MobilePlaceOrder', 'MobileAppController@placeOrder');
Route::post('MobileOrderHistory', 'MobileAppController@getOrderHistory');
Route::post('MobileRateDish', 'MobileAppController@rateDish');
Route::post('MobileAddToFavDish', 'MobileAppController@addRemoveFavMenu');
Route::post('MobileAddToFavRest', 'MobileAppController@addRemoveFavBranch');
Route::get('MobileGetOrderTracking', 'MobileAppController@getOrderTrackParams');
Route::post('MobileUserSignUp', 'MobileAppController@userSignUp');
Route::post('MobileUserLogin', 'MobileAppController@userLogin');

// PosMobileAPP
Route::post('PosMobileAppLogin', 'PosMobileAppController@login');
Route::post('PosMobileAppOrderPlace', 'PosMobileAppController@placeOrder');
Route::get('GetPosMobileAppUserByContactAppId', 'PosMobileAppController@getPosMobileAppUserByContactAppId');
Route::get('GetPosMobileAppOrdersByBranchId', 'PosMobileAppController@getOrdersByBranchId');
Route::get('getOrderByTrackId', 'PosMobileAppController@getOrderByTrackId');