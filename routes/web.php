<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('login', 'Auth\LoginController@showadminUserLoginForm')->name('login');
Route::post('loginadmin', 'Auth\LoginController@adminUserLogin');
Route::get('logout', 'Auth\LoginController@adminUserLogout')->name('logout');


Route::group(['middleware' =>'auth:admin_user'], function(){
// Permissions
    Route::get('permissions', 'PermissionsController@index');
    Route::view('list-permissions', 'permissions.index')->name('list-permissions');
    Route::get('create-permission', 'PermissionsController@create');
    Route::post('store-permission', 'PermissionsController@store');
    Route::get('show-permission/{id}', 'PermissionsController@show');
    Route::get('edit-permission/{id}', 'PermissionsController@edit');
    Route::post('update-permission/{id}', 'PermissionsController@update');
    
    // Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    // Route::view('permissions', 'PermissionsController');

    // Roles
    // Route::get('roles', 'RolesController@index');
    // Route::view('list-roles', 'roles.index')->name('list-roles');
    // Route::get('create-role', 'RolesController@create');
    // Route::post('store-role', 'RolesController@store');
    // Route::get('show-role/{id}', 'RolesController@show');
    // Route::get('edit-role/{id}', 'RolesController@edit');
    // Route::post('update-role/{id}', 'RolesController@update');
    // Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Restaurants
    Route::get('getrestaurants', 'RestaurantController@index');
    Route::view('list-restaurants', 'restaurants.restaurant-list')->name('list-restaurants');
    Route::get('create-restaurant', 'RestaurantController@create');
    Route::post('store-restaurant', 'RestaurantController@store');
    Route::get('view-restaurant/{id}', 'RestaurantController@show');
    Route::get('edit-restaurant/{id}', 'RestaurantController@edit');
    Route::post('update-restaurant', 'RestaurantController@update');

    // Restaurant Branches
    Route::get('getrestaurantbranches', 'RestaurantBranchController@index');
    Route::view('list-restaurant-branches', 'restaurant-branches.branch-list')->name('list-restaurant-branches');
    Route::get('create-branch', 'RestaurantBranchController@create');
    Route::post('store-branch', 'RestaurantBranchController@store');
    Route::get('view-branch/{id}', 'RestaurantBranchController@show');
    Route::get('edit-branch/{id}', 'RestaurantBranchController@edit');
    Route::post('update-branch', 'RestaurantBranchController@update');


    // Agent User
    Route::get('get-agent-users','AgentUserController@index');
    Route::get('edit-agent-user/{id}','AgentUserController@edit');
    Route::post('store-agent-user', 'AgentUserController@store');
    Route::get('view-agent-user/{id}', 'AgentUserController@show');
    Route::post('update-agent-user', 'AgentUserController@update');
    Route::view('list-agent-user', 'agent-users.agent-user-list')->name('list-agent-user');    //Done
    Route::get('create-agent-user','AgentUserController@create')->name('create-agent-user');;

    // Menu Category
    Route::get('getcategories', 'MenuCategoryController@index');
    Route::view('list-categories', 'menu-categories.category-list')->name('list-categories');
    Route::get('create-category', 'MenuCategoryController@create')->name('create-category');
    Route::post('store-category', 'MenuCategoryController@store');
    Route::get('view-category/{id}', 'MenuCategoryController@show');
    Route::get('edit-category/{id}', 'MenuCategoryController@edit');
    Route::post('update-category', 'MenuCategoryController@update');
    Route::get('searchRestaurantsByName','MenuCategoryController@searchRestaurantsByName');
    Route::get('getBranchesByRestaurantId','MenuCategoryController@getBranchesByRestaurantId');
    Route::post('saveImage','MenuCategoryController@saveImage');
    Route::post('enableCategory','MenuCategoryController@enableCategory');

    // Menu
    Route::get('getmenus', 'MenuController@index');
    Route::view('list-menus', 'menus.menu-list')->name('list-menus');
    Route::get('create-menu', 'MenuController@create')->name('create-menu');
    Route::post('store-menu', 'MenuController@store');
    Route::get('view-menu/{id}', 'MenuController@show');
    Route::get('edit-menu/{id}', 'MenuController@edit');
    Route::post('update-menu', 'MenuController@update');
    Route::post('deleted-menu/{id}', 'MenuController@update');
    Route::get('getCategoriesByBranchId','MenuController@getCategoriesByBranchId');
    Route::post('enableMenu','MenuController@enableMenu');

    //user management system
    Route::get('get-admin-users','AdminUserController@index');
    Route::get('edit-admin-user/{id}','AdminUserController@edit');
    Route::post('store-admin-user', 'AdminUserController@store');
    Route::get('view-admin-user/{id}', 'AdminUserController@show');
    Route::post('update-admin-user', 'AdminUserController@update');
    Route::view('list-admin-users', 'admin-users.admin-user-list')->name('list-admin-users')->middleware(['auth:admin_user']);
    Route::get('create-user','AdminUserController@create')->name('create-user');;

    // Pos reconcile drawer
    Route::get('reconcile-drawer', 'PosController@getViewPosCashFlow')->name('reconcile-drawer');
    Route::get('reconcile-drawer-subview', 'PosController@getPosCashFlow')->name('reconcile-drawer-subview');
    Route::post('reconcile-drawer-store', 'PosController@storePosCashFlow')->name('reconcile-drawer-store');

    // Dashboard
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('home', 'HomeController@index')->name('home');
    // Route::get('/', 'HomeController@getNotifications')->name('home');
    // Route::get('mark/{id}', 'HomeController@readNotification')->name('mark');
    
    // Branch Filter Route for Dashboard
    Route::post('getDashboardStatsByBranchId','HomeController@getDashboardStatsByBranchId');

    // orders
    Route::get('get-orders','OrderController@getorders')->name('get-orders');
    Route::get('list-orders','OrderController@index')->name('list-orders');
    Route::post('order-details','OrderController@getOrderDetailsByOrderId')->name('getOrderDetailsByOrderId');

    // Reports
    Route::get('rpt-consumed-menu', 'ReportController@showMenuConsumedReport')->name('rpt-consumed-menu'); // get view of report
    Route::get('consumed-menu','ReportController@consumedReport')->name('consumed-menu');    // get data in view
    Route::get('rpt-sale-detail', 'ReportController@showSaleDetailReport')->name('rpt-sale-detail'); // get view of report
    Route::get('sale-detail','ReportController@saleDetailReport')->name('sale-detail');    // get data in view
    Route::get('rpt-rider-delivery', 'ReportController@showRiderMonthlyDeliveryReport')->name('rpt-rider-delivery'); // get view of report
    Route::get('rider-delivery','ReportController@riderMonthlyDeliveryReport')->name('rider-delivery');    // get data in view


    // Reports in pdf,excel
    Route::post('export-consumed-menu','ReportController@exportConsumedReport')->name('export-consumed-menu');
    Route::post('export-sale-detail','ReportController@exportSaleDetailReport')->name('export-sale-detail');
    Route::post('export-rider-delivery','ReportController@exportRiderMonthlyDeliveryReport')->name('export-rider-delivery');


    //Delivery Users
    Route::get('get-riders','DeliveryUserController@index')->name('get-riders');
    Route::view('list-riders','delivery-users.delivery-users-list')->name('list-riders');
    Route::get('edit-rider/{id}','DeliveryUserController@edit')->name('edit-rider');
    Route::get('view-rider/{id}','DeliveryUserController@show')->name('view-rider');
    Route::post('update-rider','DeliveryUserController@update')->name('update-rider');
    Route::get('create-rider','DeliveryUserController@create')->name('create-rider');
    Route::post('store-rider','DeliveryUserController@store')->name('store-rider');


    // Choice Groups
    Route::get('getchoicegroups', 'ChoiceGroupController@index');
    Route::view('list-choicegroups', 'choice-groups.choice-group-list')->name('list-choicegroups');
    Route::get('create-choice-group', 'ChoiceGroupController@create')->name('create-choice-group');
    Route::post('store-choice-group', 'ChoiceGroupController@store');
    Route::get('view-choice-group/{id}', 'ChoiceGroupController@show');
    Route::get('edit-choice-group/{id}', 'ChoiceGroupController@edit');
    Route::post('update-choice-group', 'ChoiceGroupController@update');


    // Branch Table

    Route::view('list-branch-tables','branch-tables.branch-table-list')->name('list-branch-tables');
    Route::get('getBranchTables','BranchTableController@index');
    Route::get('create-branch-table','BranchTableController@create')->name('create-branch-table');
    Route::post('store-branch-table','BranchTableController@store');
    Route::get('view-branch-table/{id}','BranchTableController@show');
    Route::get('edit-branch-table/{id}','BranchTableController@edit');
    Route::post('update-branch-table','BranchTableController@update');
    //    Route::delete('delete-branch-table/{id}','BranchTableController@destroy');

    // Branch Waiter

    Route::view('list-branch-waiters','branch-waiters.branch-waiter-list')->name('list-branch-waiters');
    Route::get('getBranchWaiters','RestaurantBranchController@getBranchWaiters');
    Route::get('create-branch-waiter','RestaurantBranchController@createBranchWaiters')->name('create-branch-waiter');
    Route::post('store-branch-waiter','RestaurantBranchController@storeBranchWaiters');
    Route::get('view-branch-waiter/{id}','RestaurantBranchController@showBranchWaiters');
    Route::get('edit-branch-waiter/{id}','RestaurantBranchController@editBranchWaiters');
    Route::post('update-branch-waiter','RestaurantBranchController@updateBranchWaiters');

});


