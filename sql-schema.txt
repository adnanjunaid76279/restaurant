

ALTER TABLE `kitchen` ADD CONSTRAINT `FK_kitchen_restaurant_branch` FOREIGN KEY (`branch_id`) REFERENCES `restaurant_branch`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
ALTER TABLE `restaurant_branch` ADD `strn_number` VARCHAR(50) NULL DEFAULT NULL AFTER `dailyorder_id`;

ALTER TABLE `restaurant_branch` ADD `delivery_app_server_key` LONGTEXT NULL DEFAULT NULL AFTER `firebase_sender_id`, ADD `delivery_app_sender_id` VARCHAR(50) NULL DEFAULT NULL AFTER `delivery_app_server_key`;